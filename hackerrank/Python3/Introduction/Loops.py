#!/usr/bin/env python3

"""
LOOPS:
  https://www.hackerrank.com/challenges/python-loops/problem?isFullScreen=true&h_r=next-challenge&h_v=zen

TASK:
  The provided code stub reads an integer, "n", from STDIN. For all
  non-negative integers i < n, print i**2.

EXAMPLE:
  The list of non-negative integers that are less than "n = 3" is [0,1,2]. Print
  the square of each number on a separate line.

  0
  1
  4

INPUT FORMAT:
  The first and only line contains the integer, "n".

CONSTRAINTS:
  1 ≤ n ≤ 20

OUTPUT FORMAT:
  Print "n" lines, one corresponding to each "i".

SAMPLE INPUT 0:
  5

Sample Output 0
  0
  1
  4
  9
  16

NOTE:
  "What does the if __name__ == “__main__”: do?"
  https://www.geeksforgeeks.org/what-does-the-if-__name__-__main__-do/
  
  "__main__ — Top-level code environment"
  https://docs.python.org/3/library/__main__.html
"""

# CODE (SOLVED):
if __name__ == '__main__':  # Check if True. See NOTE above for details.
    n = int(input('Enter a number: '))

l = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
ll = len(l)
if n <= ll:
    for i in l[:n]:
        print(i ** 2)
else:
    print('INFO: enter a number <=',ll)


