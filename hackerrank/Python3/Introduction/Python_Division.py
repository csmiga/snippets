#!/usr/bin/env python3

"""
PYTHON: DIVISION:
  https://www.hackerrank.com/challenges/python-division/problem?isFullScreen=true&h_r=next-challenge&h_v=zen

TASK:
  The provided code stub reads two integers, "a" and "b", from STDIN.
  Add logic to print two lines.
  The first line should contain the result of integer division, a//b.
  The second line should contain the result of float division, a/b.

  No rounding or formatting is necessary.

EXAMPLE:
  a = 3
  b = 5

  • The result of the integer division 3//5 = 0.
  • The result of the float division is 3/5 = 0.6.

PRINT:
  0
  0.6

INPUT FORMAT:
  The first line contains the first integer, "a".
  The second line contains the second integer, "b"".

OUTPUT FORMAT:
  Print the two lines as described above.

SAMPLE INPUT 0:
  4
  3

SAMPLE OUTPUT 0:
  1
  1.33333333333

NOTE:
  "What does the if __name__ == “__main__”: do?"
  https://www.geeksforgeeks.org/what-does-the-if-__name__-__main__-do/
  
  "__main__ — Top-level code environment"
  https://docs.python.org/3/library/__main__.html
"""

# CODE (SOLVED):
if __name__ == '__main__':  # Check if True. See NOTE above for details.
    a = int(input('Enter a number: '))
    b = int(input('Enter a number: '))
    print(a//b)
    print(a/b)

