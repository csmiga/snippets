#!/usr/bin/env python3

"""
SAY "HELLO, WORLD!" WITH PYTHON:
  https://www.hackerrank.com/challenges/py-hello-world/problem?isFullScreen=true

TASK:
  Here is a sample line of code that can be executed in Python:
    print("Hello, World!")

  You can just as easily store a string as a variable and then print it to
  stdout:
    my_string = "Hello, World!"
    print(my_string)

  The above code will print "Hello, World!" on your screen. Try it yourself in
  the editor below!

INPUT FORMAT:
  You do not need to read any input in this challenge.

OUTPUT FORMAT:
  Print "Hello, World!" to stdout.

SAMPLE OUTPUT 0
  Hello, World!
"""

# CODE (SOLVED):
print("Hello, World!")

