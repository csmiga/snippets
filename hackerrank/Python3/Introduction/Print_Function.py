#!/usr/bin/env python3

"""
PRINT FUNCTION:
  https://www.hackerrank.com/challenges/python-print/problem?isFullScreen=true

TASK:
  The included code stub will read an integer, "n", from STDIN. Without using
  any string methods, try to print the following:

  123...n

  Note that "..." represents the consecutive values in between.

EXAMPLE:
  n = 5

  Print the string 12345.

INPUT FORMAT:
  The first line contains an integer "n".

CONSTRAINTS:
  1 ≤ n ≤ 150

OUTPUT FORMAT:
  Print the list of integers from 1 through "n" as a string, without spaces.

SAMPLE INPUT 0:

  3

SAMPLE OUTPUT 0:

  123

NOTE:
  "What does the if __name__ == “__main__”: do?"
  https://www.geeksforgeeks.org/what-does-the-if-__name__-__main__-do/
  
  "__main__ — Top-level code environment"
  https://docs.python.org/3/library/__main__.html
"""

# CODE (UNSOLVED):
if __name__ == '__main__':  # Check if True. See NOTE above for details.
    n = int(input('Enter a number: '))

