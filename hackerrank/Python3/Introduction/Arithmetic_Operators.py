#!/usr/bin/env python3

"""
ARITHMETIC OPERATORS:
  https://www.hackerrank.com/challenges/python-arithmetic-operators/problem?isFullScreen=true

TASK:
  The provided code stub reads two integers from STDIN, "a" and "b". Add code to
  print three lines where:
    1. The first line contains the sum of the two numbers.
    2. The second line contains the difference of the two numbers
       (first - second).
    3. The third line contains the product of the two numbers.

EXAMPLE:
  a = 3
  b = 5

PRINT THE FOLLOWING:
  8
  -2
  15

INPUT FORMAT:
  The first line contains the first integer, "a".
  The second line contains the second integer, "b".

CONSTRAINTS:
  1 ≤ a ≤ 10**10
  1 ≤ b ≤ 10**10

OUTPUT FORMAT:
  Print the three lines as explained above.

SAMPLE INPUT 0:
  3
  2

SAMPLE OUTPUT 0:
  5
  1
  6

EXPLANATION 0:
  3 + 2 = 5
  3 - 2 = 1
  3 * 2 = 6

NOTE:
  "What does the if __name__ == “__main__”: do?"
  https://www.geeksforgeeks.org/what-does-the-if-__name__-__main__-do/
  
  "__main__ — Top-level code environment"
  https://docs.python.org/3/library/__main__.html
"""

# CODE (SOLVED):
if __name__ == '__main__':  # Check if True. See NOTE above for details.
    a = int(input('Enter a number: '))
    b = int(input('Enter a number: '))
    print(a + b)
    print(a - b)
    print(a * b)

