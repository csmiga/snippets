#!/usr/bin/env python3

"""
PYTHON IF-ELSE:
  https://www.hackerrank.com/challenges/py-if-else/problem?isFullScreen=true

TASK:
  Given an integer, n, perform the following conditional actions:

  • If n is odd, print "Weird"
  • If n is even and in the inclusive range of 2 to 5, print "Not Weird"
  • If n is even and in the inclusive range of 6 to 20, print "Weird"
  • If n is even and greater than 20, print "Not Weird"

INPUT FORMAT:
  A single line containing a positive integer, n.

CONSTRAINTS:
  • 1 ≤ n ≤ 100

OUTPUT FORMAT:
  Print "Weird" if the number is weird. Otherwise, print "Not Weird".

SAMPLE INPUT 0:
  3

SAMPLE OUTPUT 0:
  Weird

EXPLANATION 0:
  n = 3
  n is odd and odd numbers are weird, so print "Weird".

SAMPLE INPUT 1:
  24

SAMPLE OUTPUT 1:
  Not Weird

EXPLANATION 1:
  n = 24
  n > 20 and n is even, so it is not weird.

NOTE:
  "What does the if __name__ == “__main__”: do?"
  https://www.geeksforgeeks.org/what-does-the-if-__name__-__main__-do/
  
  "__main__ — Top-level code environment"
  https://docs.python.org/3/library/__main__.html
"""

# CODE (SOLVED):
import math
import os
import random
import re
import sys

if __name__ == '__main__':                      # Check if True. See NOTE above for details.
    n = int(input('Enter a number: ').strip())  # Strip white space characters before/after input

if n % 2 == 0 and n in range(2, 5):             # % = modulo operation is used to get the remainder of a division
    print('Not Weird')
elif n % 2 == 0 and n in range(6, 20):
    print('Weird')
elif n % 2 == 0 and n > 20:
    print('Not Weird')
else:
    print('Weird')

