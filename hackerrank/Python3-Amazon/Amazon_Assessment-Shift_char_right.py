#!/usr/bin/env python3

"""
AMAZON ASSESSMENT:
SHIFT RIGHT AND COMPARE IN LIST

TASK:
  Amazon Prime Video is a subscription video-on-demand over-the-top streaming and
  rental service. At Prime Video, the product team is creating a family plan for
  shared use. An Amazon business analyst needs to compute the pairs of family
  login strings are part of analysis of the family plan adoption.

  The analyst has an array of strings, logins, where the lengths of all strings
  are equal. A string can be transformed one time by rotating its character right
  by one step. In other words, change character 'a' to 'b', 'b' to 'c' and so on
  through changing character 'z' to 'a'. Two strings 'logins[i]' and 'logins[j]'
  are called family logins if one of them can be transformed into another. As an
  examnple, strings ("bcd","abc") are family logins because each character in
  "bcd" is one above the characters in "abc". The pairs ("abc","abc"),
  ("abc","acd") are not family logins.

  Find the number of pairs of strings (i, j) which are family logins. Note that
  that any pair of strings can be for a login pair if the above condition is
  met.

SAMPLE OUTPUT:
  3

EXPLANATION:
  The pairs of sibling strings are:
  • indices(1,3) that is "corn" and "dpso"
  • indices(3,4) that is "dpso" and "eqtp"
  • indices(3,5) that is "dpso" and "corn"
"""

import math
import os
import random
import re
import sys

logins = ["corn", "horn", "dpso", "eqtp", "corn"]
#logins = ["cbu","bat",cbu]
shift = 1

# Complete the 'countFamilyLogins' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING_ARRAY logins as parameter.

def countFamilyLogins(logins):
    # Write your code here
    ll = len(logins)
    #print(ll)
    for i in logins:
        #print(i)
        x = i
        y = x
        if y.isalpha():
            a = ord(i) + shift
            s += char(a)
        else:
            s += i

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    logins_count = int(input().strip())

    logins = []

    for _ in range(logins_count):
        logins_item = input()
        logins.append(logins_item)

    result = countFamilyLogins(logins)

    fptr.write(str(result) + '\n')

    fptr.close()

