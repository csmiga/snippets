# C# Fundamentals

[Pluralsight Course by Scott Allen](https://app.pluralsight.com/library/courses/csharp-fundamentals-dev/table-of-contents)

## Contents

### 2 - Introducing C# and .NET

* 2.6 - Creating the First C# Project
* 2.7 - Editing C# Code with Visual Studio Code
* 2.9 - Saying Hello with C#
* 2.10 - Debugging a C# Application

### 3 - Learning the C# Syntax

* 3.3 - Working with Code Blocks and Statements
* 3.4 - Adding Numbers and Creating Arrays
* 3.5 - Looping through Arrays
* 3.6 - Using a List
* 3.7 - Computing and Formatting the Result

### 4 - Working with Classes and Objects

* 4.2 - Creating a Class
* 4.3 - Adding State and Behavior
* 4.4 - Defining a Method
* 4.5 - Defining a Field
* 4.6 - Adding a Constructor
* 4.7 - Requiring Constructor Parameters
* 4.8 - Working with Static Members
* 4.9 - Computing Statistics
* 4.10 - Solving the Statistics Challenge

### 5 - Testing Your Code

* 5.3 - Creating a Unit Test Project
* 5.4 - Writing and Running a Test
* 5.5 - Referencing Projects and Packages
* 5.6 - Refactoring for Testability

### 6 - Working with Reference Types and Value Types

* 6.3 - Creating a Solution File
* 6.4 - Testing Object References
* 6.5 - Referencing Different Objects
* 6.6 - Passing Parameters by Value
* 6.7 - Returning Object References
* 6.8 - Passing Parameters by Reference
* 6.9 - Working with Value Types
* 6.10 - Value Type Parameters
* 6.11 - Looking for Reference Types and Value Types
* 6.12 - The Special Case of Strings in .NET

### 7 - Controlling the Flow of Execution

* 7.2 - Branching with if Statements
* 7.3 - Looping with for, foreach, do, and while
* 7.4 - Jumping with break and continue
* 7.5 - Switching with the switch Statement
* 7.6 - Pattern Matching with switch
* 7.7 - Challenge: Taking User Input from the Console
* 7.8 - One Solution
* 7.9 - Throwing Exceptions
* 7.10 - Catching Exceptions

### 8 - Building Types

* 8.2 - Overloading Methods
* 8.3 - Defining Properties
* 8.4 - Defining Property Getters and Setters
* 8.5 - Defining readonly Members
* 8.6 - Defining const Members
* 8.8 - Defining a Delegate
* 8.9 - Using Multi-cast Delegates
* 8.10 - Defining an Event
* 8.11 - Subscribing to an Event

### 9 - Object-oriented Programming with C#

* 9.3 - Deriving from a Base Class
* 9.4 - Chaining Constructors
* 9.6 - Setting up a Scenario
* 9.7 - Defining an Abstract Class
* 9.8 - Defining an Interface
* 9.9 - Writing Grades to a File
* 9.10 - Using IDisposable
* 9.11 - A Statistical Challenge
* 9.12 - Refactoring Statistics

### 10 - Catching up with the Latest in C#

* 10.2 - Working with Non-nullable Reference Types