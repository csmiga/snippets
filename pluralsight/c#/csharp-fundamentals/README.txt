
Pluralsight:
  C# Fundamentals by Scott Allen
    https://www.pluralsight.com/courses/csharp-fundamentals-dev

GitHub:
  ChrisWebbDeveloper / csharp-fundamentals
    https://github.com/ChrisWebbDeveloper/csharp-fundamentals
