﻿using System;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length == 2)
            {
                bool xIsNumeric = double.TryParse(args[0], out double x);
                bool yIsNumeric = double.TryParse(args[1], out double y);

                if (xIsNumeric && yIsNumeric) {
                    double total = x + y;
                    Console.WriteLine($"The total is {total}");
                }
                else
                {
                    Console.WriteLine("The values provided must be numbers");
                }

            }
            else {
                Console.WriteLine("You must provide two numbers to add");
            }
        }
    }
}
