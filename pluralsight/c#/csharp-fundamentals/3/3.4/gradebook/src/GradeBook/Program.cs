﻿using System;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new double[3];
            numbers[0] = 12.7;
            numbers[1] = 2.3;
            numbers[2] = 6.7;

            double total = 0;

            foreach (var number in numbers)
            {
                total += number;
            }

            Console.WriteLine(total);
        }
    }
}
