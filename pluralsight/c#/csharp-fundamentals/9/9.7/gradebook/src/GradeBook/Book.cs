using System;
using System.Collections.Generic;

namespace GradeBook
{
    public abstract class Book : NamedObject
    {
        public Book(string name) : base(name)
        {
        }

        public abstract void AddGrade (double grade);
    }
}
