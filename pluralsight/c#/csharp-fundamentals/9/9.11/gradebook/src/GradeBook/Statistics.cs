using System;
using System.Collections.Generic;

namespace GradeBook
{
    public class Statistics
    {
        public double Average;
        public double High;
        public double Low;
        public char Letter;

        public Statistics()
        {
            Average = 0.0;
            High = double.MinValue;
            Low = double.MaxValue;  
        }

        public Statistics GetStatistics(List<double> grades)
        {
            for (var index = 0; index < grades.Count; index++)
            {
                High = Math.Max(grades[index], High);
                Low = Math.Min(grades[index], Low);
                Average += grades[index];
            }

            if (grades.Count > 0)
            {
                Average /= grades.Count;

                switch (Average)
                {
                    case var d when d >= 90.0:
                        Letter = 'A';
                        break;

                    case var d when d >= 80.0:
                        Letter = 'B';
                        break;

                    case var d when d >= 70.0:
                        Letter = 'C';
                        break;

                    case var d when d >= 60.0:
                        Letter = 'D';
                        break;

                    default:
                        Letter = 'F';
                        break;
                }
            }

            return this;
        }
    }
}