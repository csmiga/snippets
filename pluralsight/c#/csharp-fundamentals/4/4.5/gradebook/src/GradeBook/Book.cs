using System;
using System.Collections.Generic;

namespace GradeBook
{
    class Book
    {
        public List<double> grades;

        public Book() {
            this.grades = new List<double>();
        }        

        public void AddGrade(double grade)
        {
            this.grades.Add(grade);
        }
    }
}
