﻿using System;
using System.Collections.Generic;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new Book("Chris");

            string input;

            do {
                Console.WriteLine("What is the grade you want to add? (Enter 'q' to end): ");
                input = Console.ReadLine();
                double grade;

                if (double.TryParse(input, out grade))
                {
                    book.AddGrade(grade);
                }
                else if (input.ToLower() == "q")
                {
                    Console.WriteLine("Grades Added");
                }
                else
                {
                    Console.WriteLine("Invalid input");
                }
            } while (input.ToLower() != "q");

            var stats = book.GetStatistics();
            Console.WriteLine($"The lowest grade is {stats.Low}");
            Console.WriteLine($"The highest grade is {stats.High}");
            Console.WriteLine($"The average grade is {stats.Average:N1}");
            Console.WriteLine($"The letter grade is {stats.Letter}");
        }
    }
}
