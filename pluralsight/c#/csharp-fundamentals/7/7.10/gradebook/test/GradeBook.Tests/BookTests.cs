using System;
using Xunit;

namespace GradeBook.Tests
{
    public class BookTests
    {
        [Fact]
        public void BookCalculatesAnAverageGrade()
        {
            // arrange
            var book = new Book("");
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.3);

            // act
            var result = book.GetStatistics();

            // assert
            Assert.Equal(85.6, result.Average, 1);
            Assert.Equal(90.5, result.High, 1);
            Assert.Equal(77.3, result.Low, 1);
            Assert.Equal('B', result.Letter);
        }

        [Fact]
        public void GradesAbove100ThrowAnError()
        {
            var book = new Book("");
            
            try
            {
                book.AddGrade(105);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            var result = book.GetStatistics();

            Assert.Equal(0.0, result.Average, 1);
            Assert.Equal(double.MinValue, result.High);
            Assert.Equal(double.MaxValue, result.Low);
        }
    }
}
