#! /bin/sh

# nodeX

apk update

apk add python3 \
    py2-pip \
    py3-virtualenv \
    py2-virtualenv

mkdir ~/Projects

virtualenv --python=/usr/bin/python3 ~/Projects/venv3
virtualenv --python=/usr/bin/python ~/Projects/venv2

. ~/Projects/venv3/bin/activate

