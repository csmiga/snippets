// Anonymous Classes:
// Anonymous classes are a way to extend the existing classes on the fly.
// For example, consider having a class Machine:

class Machine
{
    public void start()
    {
        System.out.println("Starting...");
    }
}

// When creating the Machine object, we can change the start method on the fly.

// Try It Yourself:
// https://code.sololearn.com/java
class Machine
{
    public void start()
    {
        System.out.println("Starting...");
    }
}
class Program
{
    public static void main(String[ ] args)
    {
        Machine m = new Machine()
        {
            @Override public void start()
            {
                System.out.println("Wooooo");
            }
        };
        m.start();
    }
}

// After the constructor call, we have opened the curly braces and have
// overridden the start method's implementation on the fly.

// The @Override annotation is used to make your code easier to understand,
// because it makes it more obvious when methods are overridden.


// Question:
//   Fill in the blanks to override the start method of the Machine class.

Machine m = ___ Machine()
{
    _Override public void _____()
    {
        System.out.println("Hi");
    }
}

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
Machine m = new Machine()
{
    @Override public void start()
    {
        System.out.println("Hi");
    }
}


// Anonymous Classes:
// The modification is applicable only to the current object, and not the class
// itself. So if we create another object of that class, the start method's
// implementation will be the one defined in the class.

// Try It Yourself:
// https://code.sololearn.com/java
class Machine
{
    public void start()
    {
        System.out.println("Starting...");
    }
}
class Program
{
    public static void main(String[ ] args)
    {
        Machine m1 = new Machine()
        {
            @Override public void start()
            {
                System.out.println("Wooooo");
            }
        };
        Machine m2 = new Machine();
        m2.start();
    }
}


// Question:
//   Drag and drop from the options below to print "Hello".

//     class A
//     {
//         public void print()
//         {
//             System.out.println("A");
//         }
//     }
//     class B {
//         public static void main(String[ ] args)
//         {
//             _ object = ___ A()
//             {
//                 @Override public void _____()
//                 {
//                     System.out.println(_______);
//                 }
//             };
//             object.print();
//         }
//     }

// A, B, String, new, print, extends, "Hello"

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class A
{
    public void print()
    {
        System.out.println("A");
    }
}
class B {
    public static void main(String[ ] args)
    {
        A object = new A()
        {
            @Override public void print()
            {
                System.out.println("Hello");
            }
        };
        object.print();
    }
}
