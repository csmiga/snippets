// Comparing Objects:
// Remember that when you create objects, the variables store references to the
// objects.
// So, when you compare objects using the equality testing operator (==), it
// actually compares the references and not the object values.

// Example:
//   Try It Yourself:
//   https://code.sololearn.com/java
class Animal
{
    String name;
    Animal(String n)
    {
        name = n;
    }
}
class MyClass
{
    public static void main(String[ ] args)
    {
        Animal a1 = new Animal("Robby");
        Animal a2 = new Animal("Robby");
        System.out.println(a1 == a2);
    }
}

// Despite having two objects with the same name, the equality testing returns
// false, because we have two different objects (two different references or
// memory locations).


// Question:
// What is the output of this code?
//   class A
//   {
//       private int x;
//       public static void main(String[ ] args)
//       {
//           A a = new A();
//           a.x = 5;
//           A b = new A();
//           b.x = 5;
//           System.out.println(a == b);
//       }
//   }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class A
{
    private int x;
    public static void main(String[ ] args)
    {
        A a = new A();
        a.x = 5;
        A b = new A();
        b.x = 5;
        System.out.println(a == b);
    }
}


// equals():
// Each object has a predefined equals() method that is used for semantical
// equality testing.
// But, to make it work for our classes, we need to override it and check the
// conditions we need.
// There is a simple and fast way of generating the equals() method, other than
// writing it manually.
// Just right click in your class, go to Source->Generate hashCode() and
// equals()...

// This will automatically create the necessary methods.

// Try It Yourself:
// https://code.sololearn.com/java
class Animal
{
    String name;
    Animal(String n)
    {
        name = n;
    }
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (getClass() != obj.getClass())
            return false;
            Animal other = (Animal) obj;
        
        if (name == null)
        {
            if (other.name != null)
            return false;
        }
        else if (!name.equals(other.name))
            return false;
            return true;
    }
}

// The automatically generated hashCode() method is used to determine where to
// store the object internally. Whenever you implement equals, you MUST also
// implement hashCode.
// We can run the test again, using the equals method:

// Try It Yourself:
// https://code.sololearn.com/java
class Animal
{
    String name;
    Animal(String n)
    {
        name = n;
    }
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Animal other = (Animal) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }
}
class Program
{
    public static void main(String[ ] args)
    {
        Animal a1 = new Animal("Robby");
        Animal a2 = new Animal("Robby");
        System.out.println(a1.equals(a2));
    }
}


// Question:
//   Drag and drop from the options below to check whether the two objects of
//   type A are semantically equal.

//     class A
//     {
//         private int x;
//         public _______ equals(Object o)
//         {
//             ______ ((A)o).x == this.x;
//         }
//         public static void main(String[ ] args)
//         {
//             A a = new A();
//             a.x = 9;
//             A b = new _();
//             b.x = 5;
//             System.out.println(a.______(b));
//        }
//     }

//   new, boolean, equals, return, A, x, b

// Answer:
// Try It Yourself:
// https://code.sololearn.com/java
class A
{
    private int x;
    public boolean equals(Object o)
    {
        return ((A)o).x == this.x;
    }
    public static void main(String[ ] args)
    {
        A a = new A();
        a.x = 9;
        A b = new A();
        b.x = 5;
        System.out.println(a.equals(b));
    }
}
