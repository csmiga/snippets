// Interfaces:
// An interface is a completely abstract class that contains only abstract
// methods.

// Some specifications for interfaces:
//   - Defined using the interface keyword.
//   - May contain only static final variables.
//   - Cannot contain a constructor because interfaces cannot be instantiated.
//   - Interfaces can extend other interfaces.
//   - A class can implement any number of interfaces.

// An example of a simple interface:

// interface Animal
// {
//     public void eat();
//     public void makeSound();
// }

// Interfaces have the following properties:
//   - An interface is implicitly abstract. You do not need to use the abstract
//     keyword while declaring an interface.
//   - Each method in an interface is also implicitly abstract, so the abstract
//     keyword is not needed.
//   - Methods in an interface are implicitly public.

// A class can inherit from just one superclass, but can implement multiple
// interfaces!


// Question:
//   In Java, how many superclasses can your inherited subclass have?

// Answer:
//   only one


// Interfaces:
// Use the implements keyword to use an interface with your class.

// Try It Yourself:
// https://code.sololearn.com/java
interface Animal
{
    public void eat();
    public void makeSound();
}
class Cat implements Animal
{
    public void makeSound()
    {
        System.out.println("Meow");
    }
    public void eat()
    {
        System.out.println("omnomnom");
    }
}
public class Program
{
    public static void main(String[] args)
    {
        Cat c = new Cat();
        c.eat();
    }
}

// When you implement an interface, you need to override all of its methods.


// Question:
//   Drag and drop from the options below to implement an interface.

//     interface Animal
//     {
//         public void eat();
//     }
//     _____ Cat implements _____
//     {
//         public _____ eat()
//         {
//             System.out.println("Cat eats");
//         }
//     }

//   return, void, Animal, Cat, class, abstract

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
interface Animal
{
    public void eat();
}
class Cat implements Animal
{
    public void eat()
    {
        System.out.println("Cat eats");
    }
}
