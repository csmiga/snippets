// Inner Classes:
// Java supports nesting classes; a class can be a member of another class.
// Creating an inner class is quite simple. Just write a class within a class.
// Unlike a class, an inner class can be private. Once you declare an inner
// class private, it cannot be accessed from an object outside the class.

// Example:
//   Try It Yourself:
//   https://code.sololearn.com/java
class Robot
{
    int id;
    Robot(int i)
    {
        id = i;
        Brain b = new Brain();
        b.think();
    }
    private class Brain
    {
        public void think()
        {
            System.out.println(id + " is thinking");
        }
    }
}
public class Program
{
    public static void main(String[] args)
    {
        Robot r = new Robot(1);
    }
}

// The class Robot has an inner class Brain. The inner class can access all of
// the member variables and methods of its outer class, but it cannot be
// accessed from any outside class.


// Question:
//   Rearrange the code to have an inner class Hand, which has a method called
//   "shake" that prints "Hi".

//   = public void shake() {
//   = class Hand
//   = public class Person {
//   = System.out.println("Hi"); }
//   = } }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class Person
{
    class Hand
    {
        public void shake()
        {
            System.out.println("Hi");
        }
    }
}
