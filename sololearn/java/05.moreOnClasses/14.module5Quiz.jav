// Question:
//   What is the output of this code?
//     class A
//     {
//         private void print()
//         {
//             System.out.println("a");
//         }
//         private void print(String str)
//         {
//             System.out.println("b");
//         }
//         private void print(int x)
//         {
//             System.out.println("c");
//         }
//         public static void main(String[ ] args)
//         {
//             A object = new A();
//             object.print(12);
//         }
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class A
{
    private void print()
    {
        System.out.println("a");
    }
    private void print(String str)
    {
        System.out.println("b");
    }
    private void print(int x)
    {
        System.out.println("c");
    }
    public static void main(String[ ] args)
    {	
        A object = new A();
        object.print(12);
    }
}


// Question:
//   Fill in the blanks to define a new class Falcon, based on the superclass
//   Bird.

//   _____ Falcon _____ Bird
//   {
//   }

// Answer:
//   class Falcon extends Bird
//   {
//   }


// Question:
// Object variables store...

// Answer:
//   References


// Question:
//   What term is used for hiding the details of an object from the other parts
//   of a program?

// Answer:
//   Encapsulation


// Question:
//   A class Car and its subclass BMW each have a method run(), which was
//   written by the developer as part of the class definition. If CarObj refers
//   to an object of type BMW, what will CarObj.run(); do?

// Answer:
//   The run() method defined in BMW will be called.


// Question:
//   Valentine, Holiday, and Birthday inherit from the class Card. In order for
//   the following code to be correct, what type must the reference variable
//   card be?

//   card = new Valentine( "A", 14 ) ;
//   card.greeting();
//   card = new Holiday( "B" ) ;
//   card.greeting();
//   card = new Birthday( "C", 12 ) ;
//   card.greeting();

// Answer:
//   Card
