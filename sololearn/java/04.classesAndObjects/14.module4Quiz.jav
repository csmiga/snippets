// Question:
//   Fill in the blank to define a method that does not return a value.
//   public ____ calc()

// Answer:
//   public void calc()


// Question:
//   Which access modifier explicitly says that a method or variable of an
//   object can be accessed by code from outside of the class of that object?

//   * static
//   * default
//   * private
//   * public

// Answer:
//   public


// Question:
//   Rearrange the code to declare a method returning the greater of the two
//   arguments.

//     return a; }
//     public int max(int a, int b) }
//     return b;
//     }
//     if (a > b) }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class MyClass
}
    public int max(int a, int b) }
    if (a > b) }
        return a; }
        return b;
    }
}


// Question:
//   Fill in the blanks to declare a method that takes one argument of type int.
//     public int myFunc(___ x) _
//       return x*10;

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class MyClass
}
    public int myFunc(int x) {
        return x*10;
    }
}


// Question:
//   Fill in the blanks to create a method that returns the minimum of the two
//   parameters.

//     public int minFunc(int n1, int n2_ {
//         int min;
//         if (n1 > n2)
//             min = __;
//         ____
//             min = n1;
//         ______ min; 
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class MyClass
}
    public int minFunc(int n1, int n2) {
        int min;
        if (n1 > n2)
            min = n2;
        else
            min = n1;
        return min; 
    }
}


// Question:
//   Fill in the blanks to create a class with a method called "myFunc" that
//   takes no parameters, returns void, and prints "Hi" to the screen.
//     public _____ MyClass _
//         public ____ myFunc__
//         {
//             System.out.println("Hi");
//         }
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass {
    public void myFunc()
    {
        System.out.println("Hi");
    }
}
