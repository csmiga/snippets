// Static:
// When you declare a variable or a method as static, it belongs to the class,
// rather than to a specific instance. This means that only one instance of a
// static member exists, even if you create multiple objects of the class, or if
// you don't create any. It will be shared by all objects.

// Example:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class Counter
{
    public static int COUNT=0;
    Counter()
    {
        COUNT++;
    }
}

// The COUNT variable will be shared by all objects of that class.
// Now, we can create objects of our Counter class in main, and access the
// static variable.

// Try It Yourself:
// https://code.sololearn.com/java
public class Counter
{
    public static int COUNT=0;
    Counter()
    {
        COUNT++;
    }
}
public class MyClass
{
    public static void main(String[ ] args)
    {
        Counter c1 = new Counter();
        Counter c2 = new Counter();
        System.out.println(Counter.COUNT);
    }
}

// The output is 2, because the COUNT variable is static and gets incremented by
// one each time a new object of the Counter class is created. In the code
// above, we created 2 objects.
// You can also access the static variable using any object of that class, such
// as c1.COUNT.

// It’s a common practice to use upper case when naming a static variable,
// although not mandatory.


// Question:
//   Fill in the blank to declare a static variable.
//   public ______ int x=0;

// Answer:
//   public static int x=0;


// Static:
// The same concept applies to static methods.

// Try It Yourself:
// https://code.sololearn.com/java
public class Vehicle
{
    public static void horn()
    {
        System.out.println("Beep");
    }
}

// Now, the horn method can be called without creating an object:

// Try It Yourself:
// https://code.sololearn.com/java
public class Vehicle
{
    public static void horn()
    {
        System.out.println("Beep");
    }
}
public class MyClass
{
    public static void main(String[ ] args)
    {
        Vehicle.horn();
    }
}

// Another example of static methods are those of the Math class, which is why
// you can call them without creating a Math object.

// Also, the main method must always be static.


// Question:
//   What output results from this code?
//     class Person
//     {
//         public static int pCount;	
//         public static void main(String[ ] args)
//         {
//             Person.pCount = 1;
//             Person.pCount++;
//             System.out.println(Person.pCount);
//         }
//     }

// Answer:
class Person
{
    public static int pCount;	
    public static void main(String[ ] args)
    {
        Person.pCount = 1;
        Person.pCount++;
        System.out.println(Person.pCount);
    }
}
