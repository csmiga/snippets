// Defining Attributes:
// A class has attributes and methods. The attributes are basically variables
// within a class.
// Let's create a class called Vehicle, with its corresponding attributes and
// methods.

//   public class Vehicle
//   {
//       int maxSpeed;
//       int wheels;
//       String color;
//       double fuelCapacity;
//       void horn()
//       {
//           System.out.println("Beep!");
//       }
//   }

// Try It Yourself:
// https://code.sololearn.com/java
public class Vehicle
{
    int maxSpeed;
    int wheels;
    String color;
    double fuelCapacity;
    void horn()
    {
        System.out.println("Beep!");
    }
}
class B
{
    public static void main(String args[ ])
    {
        Vehicle obj = new Vehicle();
        obj.horn();
    }
}

// maxSpeed, wheels, color and fuelCapacity are the attributes of our Vehicle
// class, and horn() is the only method.

// You can define as many attributes and methods as necessary.


// Question:
//   Drag and drop from the options below to define a class with these
//   attributes:
//   age of type integer, height as a double, and name as a string.

//     _____ Person
//     {
//         ___ age;
//         ______ height;
//         ______ name;
//     }

//   void, String, class, attribute, define, double, int

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class Person
{
    int age;
    double height;
    String name;
}


// Creating Objects:
// Next, we can create multiple objects of our Vehicle class, and use the dot
// syntax to access their attributes and methods.

// Try It Yourself:
// https://code.sololearn.com/java
public class Vehicle
{
    int maxSpeed;
    int wheels;
    String color;
    double fuelCapacity;  
    
    void horn()
    {
        System.out.println("Beep!");
    }
}
class MyClass
{
    public static void main(String[ ] args)
    {
        Vehicle v1 = new Vehicle();
        Vehicle v2 = new Vehicle();
        v1.color = "red";
        v2.horn();
    }
}

// Tap Try It Yourself to play around with the code!


// Question:
// Fill in the blanks to create two objects from the class "people".
// people obj1 = ___ people();
// ______ obj2 = new people__;

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class people
{
    String race;
    
    void hair()
    {
        System.out.println("Pretty!");
    }
}
class MyClass
{
    public static void main(String[ ] args)
    {
        people obj1 = new people();
        people obj2 = new people();
        obj1.race = "white";
        obj2.hair();
    }
}
