// Question:
//   Fill in the blanks to declare a function that takes two integers as
//   arguments and prints their division. Handle the possible exception.

//     void myFunc(int a, ___ b)
//     {
//         ___
//         {
//             System.out.println(a __ b);
//         }
//         _____ (Exception e)
//         {
//             System.out.println("Error");
//         }
//     }

// Answer:
//     void myFunc(int a, int b)
//     {
//         try
//         {
//             System.out.println(a / b);
//         }
//         catch (Exception e)
//         {
//             System.out.println("Error");
//         }
//     }


// Question:
//   Fill in the blanks to define a function ''myFunc'', which throws an
//   exception if its parameter is less than 0.

//     void myFunc(int arg) _____ IllegalArgumentException
//     {
//         __ (arg < _)
//         {
//             throw ___ IllegalArgumentException();
//         }
//     }

// Answer:
//     void myFunc(int arg) throws IllegalArgumentException
//     {
//         if (arg < 0)
//         {
//             throw new IllegalArgumentException();
//         }
//     }


// Question:
//   How many lines of output will this code produce?

//     class B implements Runnable
//     {
//         public void run()
//         {
//             System.out.println("B");
//         }
//     }
//     class A extends Thread
//     {
//         public void run()
//         {
//             System.out.println("A");
//             Thread t = new Thread(new B());
//             t.start();
//         }
//         public static void main(String[ ] args)
//         {
//             A object = new A();
//             object.start();
//         }
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class B implements Runnable
{
    public void run()
    {
        System.out.println("B");
    }
}
class A extends Thread
{
    public void run()
    {
        System.out.println("A");
        Thread t = new Thread(new B());
        t.start();
    }
    public static void main(String[ ] args)
    {
        A object = new A();
        object.start();
    }
}


// Question:
//   Which two options allow you to create new threads?

// Answer:
//   - extend the Thread class
//   - implement Runnable


// Question:
//   What is the output of this code?

//     ArrayList<Integer> list = new ArrayList<Integer>();
//     for (int i = 0; i < 6; i++)
//     {
//         list.add(i);
//     }
//     int x = 0;
//     Iterator<Integer> it = list.iterator();
//     while (it.hasNext())
//     {
//         x+= it.next();
//     }
//     System.out.println(x);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
import java.util.ArrayList;
import java.util.Iterator;
public class Program
{
    public static void main(String[] args)
    {
	    ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 6; i++)
        {
	        list.add(i);
        }
        int x = 0;
        Iterator<Integer> it = list.iterator();
        while (it.hasNext())
        {
            x+= it.next();
        }
        System.out.println(x);
	}
}


// Question:
//   What is the output of the following code?
//     int f=1, i=2;
//     while(++i<5)
//         f*=i;
//         System.out.println(f);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class Program
{
    public static void main(String[] args)
    {
        int f=1, i=2;
        while(++i<5)
            f*=i;
            System.out.println(f);
    }
}
