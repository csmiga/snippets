// Increment Operators:
// An increment or decrement operator provides a more convenient and compact way
// to increase or decrease the value of a variable by one.
// For example, the statement x=x+1; can be simplified to ++x;

// Example:
//   int test = 5;
//   ++test; // test is now 6

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int test = 5;
        ++test; // test is now 6
        System.out.println(test);
    }
}

// The decrement operator (--) is used to decrease the value of a variable by
// one.

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int test = 5;
        --test; // test is now 4
        System.out.println(test);
    }
}

// Note: Use this operator with caution to avoid calculation mistakes.


// Question:
//   Fill in the blanks to print 11.
//     int a = 10;
//     __ a;
//     System.out.println(a);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int a = 10;
        ++a;
        System.out.println(a);
    }
}


// Prefix & Postfix:
// Two forms, prefix and postfix, may be used with both the increment and
// decrement operators.
// With prefix form, the operator appears before the operand, while in postfix
// form, the operator appears after the operand. Below is an explanation of how
// the two forms work:

// Prefix: Increments the variable's value and uses the new value in the
//         expression.

// Example:
//   int x = 34;
//   int y = ++x; // y is 35

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 34;
        int y = ++x; // y is 35
        System.out.println(y);
    }
}

// The value of x is first incremented to 35, and is then assigned to y, so the
// values of both x and y are now 35.
// Postfix: The variable's value is first used in the expression and is then
// increased.

// Example:
//   int x = 34;
//   int y = x++; // y is 34

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 34;
        int y = x++; // y is 34
        System.out.println(y);
    }
}

// x is first assigned to y, and is then incremented by one. Therefore, x
// becomes 35, while y is assigned the value of 34.

// Note: The same applies to the decrement operator.


// Question:
//   What is the output of the following code?
//     int x = 14; 
//     System.out.println(x++);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 14; 
        System.out.println(x++);
    }
}


// Assignment Operators:
// You are already familiar with the assignment operator (=), which assigns a
// value to a variable.
//   int value = 5;

// This assigned the value 5 to a variable called value of type int.
// Java provides a number of assignment operators to make it easier to write
// code.

// Addition and assignment (+=):
//   int num1 = 4;
//   int num2 = 8;
//   num2 += num1; // num2 = num2 + num1;
// Output num2 is 12 and num1 is 4

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int num1 = 4;
        int num2 = 8;
        num2 += num1; // num2 = num2 + num1;
        System.out.println(num2);
    }
}

// Subtraction and assignment (-=):
//   int num1 = 4;
//   int num2 = 8;
//   num2 -= num1; // num2 = num2 - num1;

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int num1 = 4;
        int num2 = 8;
        num2 -= num1; // num2 = num2 - num1;
        System.out.println(num2);
    }
}

// num2 is 4 and num1 is 4

// Note: Similarly, Java supports multiplication and assignment (*=), division
// and assignment (/=), and remainder and assignment (%=).


// Question:
//   Fill in the missing parts in the following code to print 13.
//     int x = 25; int y;
//     y = _ - 12;
//     System.out.println(_);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 25; int y;
        y = x - 12;
        System.out.println(y);
    }
}
