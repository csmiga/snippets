// The Math Operators:
// Java provides a rich set of operators to use in manipulating variables. A
// value used on either side of an operator is called an operand.
// For example, in the expression below, the numbers 6 and 3 are operands of the
// plus operator: 
//   int x = 6 + 3;

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 6 + 3;
        System.out.println(x);
    }
}

// Java arithmetic operators:
//   + addition
//   - subtraction
//   * multiplication
//   / division
//   % modulo

// Note: Arithmetic operators are used in mathematical expressions in the same
// way that they are used in algebraic equations.


// Question:
//   Fill in the blank to declare an integer variable and set its value to 5.
//     ___ var = _;

// Answer:
public class MyClass
{
    public static void main(String[] args)
    {
        int var = 5;
        System.out.println(var);
    }
}


// Addition:
// The + operator adds together two values, such as two constants, a constant
// and a variable, or a variable and a variable. Here are a few examples of
// addition:
//   int sum1 = 50 + 10; 
//   int sum2 = sum1 + 66; 
//   int sum3 = sum2 + sum2;

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int sum1 = 50 + 10; 
        int sum2 = sum1 + 66; 
        int sum3 = sum2 + sum2;
        System.out.println(sum3);
    }
}

// Substraction:
// The - operator subtracts one value from another.
//   int sum1 = 1000 - 10;
//   int sum2 = sum1 - 5;
//   int sum3 = sum1 - sum2;

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int sum1 = 1000 - 10;
        int sum2 = sum1 - 5;
        int sum3 = sum1 - sum2;
        System.out.println(sum3);
    }
}

// Note: Just like in algebra, you can use both of the operations in a single
// line. For example: int val = 10 + 5 - 2;


// Question: 
//   Fill in the blanks to print the sum of the two variables.

// Answer:
//   int x = 2; int y = 4;
//   int result = x _ _;
//   System.out.println(______);
// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
       int x = 2; int y = 4;
        int result = x + y;
       System.out.println(result);
   }
}


// Multiplication:
// The * operator multiplies two values.
//   int sum1 = 1000 * 2;
//   int sum2 = sum1 * 10;
//   int sum3 = sum1 * sum2;

// Division:
// The / operator divides one value by another.
//   int sum1 = 1000 / 5;
//   int sum2 = sum1 / 2;
//   int sum3 = sum1 / sum2;

// Note: In the example above, the result of the division equation will be a
// whole number, as int is used as the data type. You can use double to retrieve
// a value with a decimal point.


// Question:
//   What is the result of the following code?
//   int x = 15; int y = 4;
//   int result = x / y;
//   System.out.println(result);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
       int x = 15; int y = 4;
       int result = x / y;
       System.out.println(result);
    }
}


// Modulo:
// The modulo (or remainder) math operation performs an integer division of one
// value by another, and returns the remainder of that division.
// The operator for the modulo operation is the percentage (%) character.

// Example:
//   int value = 23;
//   int res = value % 6; // res is 5

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int value = 23;
        int res = value % 6; // res is 5
        System.out.println(res);
   }
}

// Note: Dividing 23 by 6 returns a quotient of 3, with a remainder of 5. Thus,
// the value of 5 is assigned to the res variable.


// Question:
//   What value is stored in the result variable?
//     int x = 8, y = 5;
//     int result = x % y;

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 8, y = 5;
        int result = x % y;
        System.out.println(result);
    }
}
