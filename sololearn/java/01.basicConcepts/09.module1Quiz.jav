// Question:
//   Please type in a code to declare two variables of type int and print their
//   sum using the sum variable.
//     int x = 4;
//     ___ y = 7;
//     int sum = x _ y;
//     System.out.println(___);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 4;
        int y = 7;
        int sum = x + y;
        System.out.println(sum);
    }
}


// Question:
//   In every Java program...

// Answer:
//   ...there must be a method called "main".


// Question:
//   Drag and drop from the options below to output the name:
//     ______ name;
//     name = "David";
//     ______.out.println(____);

//   name, System, Java, int, String

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        String name;
        name = "David";
        System.out.println(name);
    }
}
