// Strings:
// A String is an object that represents a sequence of characters.
// For example, "Hello" is a string of 5 characters.

// For example:
//   String s = "SoloLearn";

// Note: You are allowed to define an empty string.
// For example, String str = "";


// Question:
//   Drag and drop from the options below to print "Hello".
//     ______ var;
//     var = "Hello";
//     System.out.println(___);

//   char, print, var, Hello, String

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        String var;
        var = "Hello";
        System.out.println(var);
    }
}


// String Concatenation:
// The + (plus) operator between strings adds them together to make a new
// string. This process is called concatenation.
// The resulted string is the first string put together with the second string.

// For example:
//   String firstName, lastName;
//   firstName = "David";
//   lastName = "Williams";
//   System.out.println("My name is " + firstName +" "+lastName);
//   // Prints: My name is David Williams

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        String firstName, lastName;
        firstName = "David";
        lastName = "Williams";
        System.out.println("My name is " + firstName +" "+lastName);
    }
}

// Note: The char data type represents a single character.


// Question:
//   Which statement in regard to the char data type is true?

// Answer:
//   'k' is a char
