// Variables:
// Variables store data for processing.
// A variable is given a name (or identifier), such as area, age, height, and
// the like. The name uniquely identifies each variable, assigning a value to
// the variable and retrieving the value stored.

// Variables have types. Some examples:
//   - int: for integers (whole numbers) such as 123 and -456
//   - double: for floating-point or real numbers with optional decimal points
//     and fractional parts in fixed or scientific notations, such as
//     3.1416, -55.66.
//   - String: for texts such as "Hello" or "Good Morning!". Text strings are
//     enclosed within double quotes.

// You can declare a variable of a type and assign it a value.

// Example:
//   String name = "David";

// This creates a variable called name of type String, and assigns it the value
// "David".

// Note: It is important to note that a variable is associated with a type, and
// is only capable of storing values of that particular type. For example, an
// int variable can store integer values, such as 123; but it cannot store real
// numbers, such as 12.34, or texts, such as "Hello".


// Question:
//   Which variable type would you use for a city name?

// Answer:
//   String


// Variables:
// Examples of variable declarations:

// Try It Yourself:
// https://code.sololearn.com/java
class MyClass
{
    public static void main(String[ ] args)
    {
        String name ="David";
        int age = 42;
        double score = 15.9;
        char group = 'Z';
    }
}

// char stands for character and holds a single character.
// Another type is the Boolean type, which has only two possible values: true
// and false.
// This data type is used for simple flags that track true/false conditions.

// For example:
//   boolean online = true;

// Note: You can use a comma-separated list to declare more than one variable of
// the specified type.

// Example:
//   int a = 42, b = 11;


// Question:
//   Drag and drop from the options below to have a valid Java program.
//     class Apples
//     {
//         public static void main(String[ ]args)
//         {
//             ______ name = "John";
//             ___ age = 24;
//             ______ height = 189.87;
//         }
//     }

//   class, int, true, void, String, double

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
class Apples
{
    public static void main(String[ ]args)
    {
        String name = "John";
        int age = 24;
        double height = 189.87;
    }
}
