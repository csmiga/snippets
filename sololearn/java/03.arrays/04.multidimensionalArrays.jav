// Multidimensional Arrays:
// Multidimensional arrays are array that contain other arrays. The
// two-dimensional array is the most basic multidimensional array.
// To create multidimensional arrays, place each array within its own set of
// square brackets.

// Example of a two-dimensional array:
//   int[ ][ ] sample = { {1, 2, 3}, {4, 5, 6} }; 

// This declares an array with two arrays as its elements.
// To access an element in the two-dimensional array, provide two indexes, one
// for the array, and another for the element inside that array.
// The following example accesses the first element in the second array of
// sample.

// int x = sample[1][0];
// System.out.println(x);
// Outputs 4

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int[ ][ ] sample = { {1, 2, 3}, {4, 5, 6} }; 
        int x = sample[1][0];
        System.out.println(x);
    }
}

// The array's two indexes are called row index and column index.


// Question:
//   What is the output of this code?
//     String array[ ][ ] =  { {"Hey", "John", "Bye"},
//     {"John", "Johnson", "Hello"} }; 
//     System.out.println(array[1][1]);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        String array[ ][ ] =  { {"Hey", "John", "Bye"}, {"John", "Johnson", "Hello"} }; 
        System.out.println(array[1][1]);
    }
}
// Output Johnson


// Multidimensional Arrays:
// You can get and set a multidimensional array's elements using the same pair
// of square brackets.

// Example:
//   int[ ][ ] myArr = { {1, 2, 3}, {4}, {5, 6, 7} };
//   myArr[0][2] = 42;
//   int x = myArr[1][0]; // 4

//  Try It Yourself:
//  https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int[ ][ ] myArr = { {1, 2, 3}, {4}, {5, 6, 7} };
        myArr[0][2] = 42;
        int x = myArr[1][0];
        System.out.println(x);
    }
}

// The above two-dimensional array contains three arrays. The first array has
// three elements, the second has a single element and the last of these has
// three elements.

// In Java, you're not limited to just two-dimensional arrays. Arrays can be
// nested within arrays to as many levels as your program needs. All you need to
// declare an array with more than two dimensions, is to add as many sets of
// empty brackets as you need. However, these are harder to maintain.
// Remember, that all array members must be of the same type.


// Question:
//   What is the output of this code?
//     int array[ ][ ] =  {{3, 5, 8}, {7, 54, 1, 12, 4}}; 
//     System.out.println(array[0][2]);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int array[ ][ ] =  {{3, 5, 8}, {7, 54, 1, 12, 4}}; 
        System.out.println(array[0][2]);
    }
}
