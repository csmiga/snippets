// Question:
//   What is the output of this code?
//     int arr[ ] = new int[3];
//     for (int i = 0; i < 3; i++)
//     {
//         arr[i] = i;
//     } 
//     int res = arr[0] + arr[2];
//     System.out.println(res);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int arr[ ] = new int[3];
        for (int i = 0; i < 3; i++)
        {
            arr[i] = i;
        } 
        int res = arr[0] + arr[2];
        System.out.println(res);
    }
}


// Question:
//   What is the output of this code?
//     int result = 0;
//     for (int i = 0; i < 5; i++)
//     {
//         if (i == 3)
//         {
//             result += 10;
//         }
//         else
//         {
//             result += i;
//         }
//     }
//     System.out.println(result);

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int result = 0;
        for (int i = 0; i < 5; i++)
        {
            if (i == 3)
            {
                result += 10;
            }
            else
            {
                result += i;
            }
        }
        System.out.println(result);
    }
}


// Question:
//   Fill in the blanks to calculate the sum of all elements in the array "arr"
//   using an enhanced for loop:
//     int res = 0;
//     ___(int el_ arr)
//     {
//         res += __;
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int[ ] arr = {2, 3, 5, 7};
        int res = 0;
        for(int el: arr)
        {
            res += el;
        }
    }
}
