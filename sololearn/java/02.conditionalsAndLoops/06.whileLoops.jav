// while Loops:
// A loop statement allows to repeatedly execute a statement or group of
// statements.
// A while loop statement repeatedly executes a target statement as long as a
// given condition is true.

// Example:
//   int x = 3;
//   while(x > 0)
//   {
//       System.out.println(x);
//       x--;
//   }
//   /* 
//       Outputs
//       3
//       2
//       1
//   */

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 3;
        while(x > 0)
        {
            System.out.println(x);
            x--;
        }
    }
}

// The while loops check for the condition x > 0. If it evaluates to true, it
// executes the statements within its body. Then it checks for the statement
// again and repeats.

// Notice the statement x--. This decrements x each time the loop runs, and
// makes the loop stop when x reaches 0.
// Without the statement, the loop would run forever.


// Question:
//   Rearrange the code to produce a valid finite loop which prints a text to the
//   screen in the loop.
//     System.out.println("Java rocks!");
//     x++;
//     while ( x < 100) {
//     int x = 12

// Answer:
// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 12;
        while ( x < 100) {
            System.out.println("Java rocks!");
            x++;
        }
    }
}


// while Loops:
// When the expression is tested and the result is false, the loop body is
// skipped and the first statement after the while loop is executed.

// Example:
//   int x = 6;
//   while ( x < 10 )
//   {
//       System.out.println(x);
//       x++;
//   }
//   System.out.println("Loop ended");
//   /*
//       6
//       7
//       8
//       9
//       Loop ended
//   */

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 6;
        while( x < 10 )
        {
            System.out.println(x);
            x++;
        }
        System.out.println("Loop ended");
    }
}

// Notice that the last print method is out of the while scope.


// Question:
//   How many times will the following loop work?
//     int x = 0;
//     int y = 5;
//     while (x < y)
//     {
//         System.out.println("Hello");
//         x++;
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 0;
        int y = 5;
        while (x < y)
        {
            System.out.println("Hello");
            x++;
        }
    }
}
