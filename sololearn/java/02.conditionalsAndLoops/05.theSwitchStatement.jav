// The switch Statement:
// A switch statement tests a variable for equality against a list of values.
// Each value is called a case, and the variable being switched on is checked
// for each case.

// Syntax:
//   switch(expression)
//   {
//       case value1 :
//           //Statements
//           break; //optional
//       case value2 :
//           //Statements
//           break; //optional
//       //You can have any number of case statements.
//       default : //Optional
//           //Statements
//   }

// - When the variable being switched on is equal to a case, the statements
//   following that case will execute until a break statement is reached.
// - When a break statement is reached, the switch terminates, and the flow of
//   control jumps to the next line after the switch statement.
// - Not every case needs to contain a break. If no break appears, the flow of
//   control will fall through to subsequent cases until a break is reached.
// The example below tests day against a set of values and prints a
// corresponding message.

//   int day = 3;
//   switch(day)
//   {
//       case 1:
//           System.out.println("Monday");
//           break;
//       case 2:
//           System.out.println("Tuesday");
//           break;
//       case 3:
//           System.out.println("Wednesday");
//           break;
//   }
// Outputs "Wednesday"

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int day = 3;
        switch(day)
        {
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
        }
    }
}

// You can have any number of case statements within a switch. Each case is
// followed by the comparison value and a colon.


// Question:
//   Fill in the blanks to test the variable's value using the switch statement.
//     int x = 10;
//     ______(_)
//     {
//         case 10:
//             System.out.println("A");
//             break;
//         ____ 20:
//             System.out.println("B");
//             break; 
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 10;
        switch(x)
        {
            case 10:
                System.out.println("A");
                break;
            case 20:
                System.out.println("B");
                break; 
        }
    }
}


// The default Statement:
// A switch statement can have an optional default case.
// The default case can be used for performing a task when none of the cases is
// matched.

// For example:
//   int day = 3;
//   switch(day)
//   {
//       case 6:
//           System.out.println("Saturday");
//           break;
//       case 7:
//           System.out.println("Sunday");
//           break;
//       default:
//           System.out.println("Weekday");
//   }
// Outputs "Weekday"

// Try It Yourself:
// https://code.sololearn.com/java
public class MyClass {
    public static void main(String[] args)
        {
        int day = 3;
        switch(day)
        {
            case 6:
                System.out.println("Saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;
            default:
                System.out.println("Weekday");
        }
    }
}

// No break is needed in the default case, as it is always the last statement in
// the switch. 


// Question:
//   What is the output of the following code?
//     int a = 11; int b = 12; int c = 40;
//         switch(a)
//         {
//             case 40:
//                 System.out.println(b);
//                 break;
//             default:
//                 System.out.println(c);
//         }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int a = 11; int b = 12; int c = 40;
        switch(a)
        {
            case 40:
                System.out.println(b);
                break;
            default:
                System.out.println(c);
        }
    }
}
