// Nested if Statements:
// You can use one if-else statement inside another if or else statement.

// For example:
//   int age = 25;
//   if (age > 0)
//   {
//       if (age > 16)
//       {
//           System.out.println("Welcome!");
//       }
//       else
//       {
//           System.out.println("Too Young");
//       }
//   }
//   else
//   {
//       System.out.println("Error");
//   }
// Outputs "Welcome!"

// You can nest as many if-else statements as you want.


// Question:
//   Please fill in the missing parts of the nested if statement to print
//   "it works!" to the screen.
//   int x = 37;
//   if (x > 22)
//   {
//       __ (x > 31)
//       {
//           System.___.println("it works!");
//       }
//   }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 37;
        if (x > 22)
        {
            if (x > 31)
            {
                System.out.println("it works!");
            }
        }
    }
}
