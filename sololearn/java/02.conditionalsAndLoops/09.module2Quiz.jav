// Question:
//   Fill in the blanks to print "in a loop" 7 times, using the while loop.
//     int x = 1;
//     while (x <= _)
//     {
//         System.out.println("in a loop");
//         _++;
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 1;
        while (x <= 7)
        {
            System.out.println("in a loop");
            x++;
        }
    }
}


// Question:
//   Please select the correct statements about && and || operators.

// Answer:
//   (a||b) && c is true if c is true and either a or b is true
//   a||b is true if either a or b is true


// Question:
//   Fill in the blanks to print "You rock!" if variable "a" is greater than 15,
//   and variable "b" is less than or equal to 72.
//     int a = 144;
//     int b = 33;
//     if (a > 15 __ b <= __)
//     {
//         System.out.println("You rock!");
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int a = 144;
        int b = 33;
        if (a > 15 && b <= 72)
        {
            System.out.println("You rock!");
        }
    }
}


// Question:
//   Fill in the blanks to print "in a loop" 5 times using the for loop.
//     ___(int x = 0; _ < 5; x++)
//     {
//         System.out.println("in a loop");
//     }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        for(int x = 0; x < 5; x++)
        {
            System.out.println("in a loop");
        }
    }
}
