// Decision Making:
// Conditional statements are used to perform different actions based on
// different conditions.
// The if statement is one of the most frequently used conditional statements.
// If the if statement's condition expression evaluates to true, the block of
// code inside the if statement is executed. If the expression is found to be
// false, the first set of code after the end of the if statement (after the
// closing curly brace) is executed.

// Syntax:
//   if (condition)
//   {
//       //Executes when the condition is true
//   }

// Any of the following comparison operators may be used to form the condition:
//   < less than
//   > greater than
//   != not equal to
//   == equal to
//   <= less than or equal to
//   >= greater than or equal to

// For example:
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 7;
        if (x < 42)
        {
            System.out.println("Hi");
        }
    }
}

// Remember that you need to use two equal signs (==) to test for equality,
// since a single equal sign is the assignment operator.


// Question:
//   Fill in the blanks to print "Yeah".
//   int x = 5;
//   __ ( x == 5 )
//   {
//       System.___.println("Yeah");
//   }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 5;
        if ( x == 5 )
        {
            System.out.println("Yeah");
        }
    }
}


// if...else Statements
// An if statement can be followed by an optional else statement, which
// executes when the condition evaluates to false.

// For example:
public class MyClass
{
    public static void main(String[] args)
    {
        int age = 30;
        if (age < 16)
        {
            System.out.println("Too Young");
        }
        else
        { 
            System.out.println("Welcome!");
        }
    }
}
// Outputs "Welcome!"

// As age equals 30, the condition in the if statement evaluates to false and
// the else statement is executed.


// Question:
//   Fill in the blanks to print the greater number.
//     int x = 10; int y = 5;
//     __ ( x > y )
//         {
//             System.out.println(_);
//         }
//     ____
//         {
//             System.out.println(y);
//         }

// Answer:
//   Try It Yourself:
//   https://code.sololearn.com/java
public class MyClass
{
    public static void main(String[] args)
    {
        int x = 10; int y = 5;
        if ( x > y )
        {
            System.out.println(x);
        }
        else
        { 
            System.out.println(y);
        }
    }
}
