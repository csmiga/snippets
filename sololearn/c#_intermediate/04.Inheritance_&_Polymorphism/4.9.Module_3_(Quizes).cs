// MODULE 4 QUIZ

// QUESTION:
// Can you instantiate objects of an abstract class?
//
//    [ No ]
//    [ Yes ]

// ANSWER:
//    No

// Want to know why?
// Your answer is correct because an abstract class cannot be directly
// instantiated. It can only be used as a base class for other classes, which
// can be instantiated. An abstract class is meant to be inherited from and it
// provides an interface for its subclasses to follow.


// QUESTION:
// Fill in the blanks to define a new class Falcon, which is derived from the
// class Bird.
//
//    _____ Falcon _ Bird
//    {
//    }

// ANSWER:
//    class Falcon : Bird
//    {
//    }

// Want to know why?
// Your answer is correct because it properly defines a new class named Falcon
// that extends (inherits from) the class Bird using the colon ":" symbol. This
// means that Falcon will have all the properties and methods of Bird in
// addition to any unique properties and methods it defines.


// QUESTION:
// Which keyword makes class members accessible to only its derived class
// members?
// 
//    [  ]

// ANSWER:
//    protected


// QUESTION:
// Fill in the blanks to declare a Person class with a Hello abstract method,
// and then declare a Student class that is derived from the Person class and
// overrides its Hello method.
//
//    ________ class Person
//    {
//        public abstract void Hello();
//    }
//
//    class Student _ Person
//    {
//        public ________ void Hello()
//        {
//            Console.Write("Hello");
//        }
//    }

// ANSWER:
//    abstract class Person
//    {
//        public abstract void Hello();
//    }
//
//    class Student : Person
//    {
//        public override void Hello()
//        {
//            Console.Write("Hello");
//        }
//    }

// Want to know why?
// Your answer is correct because it correctly declares an abstract class
// `Person` with an abstract method `Hello()`. It also correctly declares a
// derived class `Student` which overrides the `Hello()` method with a
// non-abstract implementation that writes "Hello" to the console.


// QUESTION:
// A sealed class can be marked as abstract.
//
//    [ False ]
//    [ True ]

// ANSWER:
//    False

// Want to know why?
// Your answer is correct. A sealed class cannot be marked as abstract because
// a sealed class cannot be inherited and therefore cannot have any abstract
// methods that need to be implemented in its derived classes.


// QUESTION:
// Fill in the blanks to implement the A and B interfaces.
//
//    interface A
//    {
//    }
//
//    _________ B
//    {
//    }
//
//    class Test _____ A _____ B
//    {
//    }

// ANSWER:
//    interface A
//    {
//    }
//
//    interface B
//    {
//    }
//
//    class Test : A , B
//    {
//    }

// Want to know why?
// The student correctly created two interfaces A and B, and then implemented
// both of them in the Test class using the "class Test : A, B {}" syntax. This
// means that the Test class inherits all the members of both interfaces A and
// B.


// QUESTION:
// Can a single class inherit from multiple abstract classes?
//
//    [ No ]
//    [ Yes ]

// ANSWER:
//    No

// Want to know why?
// Your answer is correct because C# doesn't allow a single class to inherit
// from multiple abstract classes. However, a class can implement multiple
// interfaces.
