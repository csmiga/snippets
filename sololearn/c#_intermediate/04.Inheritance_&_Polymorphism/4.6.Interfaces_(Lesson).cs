// Interfaces 

// An interface is a completely abstract class, which contains only abstract
// members.
// It is declared using the interface keyword:

public interface IShape
{
    void Draw();
}

// All members of the interface are by default abstract, so no need to use the
// abstract keyword.
// Interfaces can have public (by default), private and protected members.

// Note:
// It is common to use the capital letter I as the starting letter for an
// interface name.
// Interfaces can contain properties, methods, etc. but cannot contain fields
// (variables).

// QUESTION:
// Is the following interface valid?
//
//    public interface ITest
//    {
//        int test;
//        void Func();
//    }
//
//    [No]
//    [Yes]

// ANSWER:
//    No

// Want to know why?
// Your answer is correct because interfaces cannot have fields. They only
// define methods and properties. Therefore, the declaration `int test;` in the
// interface `ITest` is invalid.


// When a class implements an interface, it must also implement, or define, all
// of its methods.
// The term implementing an interface is used (opposed to the term "inheriting
// from") to describe the process of creating a class based on an interface. The
// interface simply describes what a class should do. The class implementing the
// interface must define how to accomplish the behaviors.

// The syntax to implement an interface is the same as that to derive a class:

// Note, that the override keyword is not needed when you implement an
// interface.

// Note:
// But why use interfaces rather than abstract classes?
// A class can inherit from just one base class, but it can implement multiple
// interfaces!
// Therefore, by using interfaces you can include behavior from multiple sources
// in a class.
// To implement multiple interfaces, use a comma separated list of interfaces
// when creating the class: class A: IShape, IAnimal, etc.

// PRACTICE (PROBLEM SET): Online Car Shopping
// On the car dealership website, you can pre-order a car by specifying its
// color and equipment.
// The program you are given takes the color and the equipment type as input and
// pass them to constructor of already declared Car class.
// Implement IColor and IEquipment interfaces for the Car class and reimplement
// their GetColor and GetEquipment methods inside it. Each of them should output
// corresponding message about color/equipment (see sample output).

// Sample Input
// Blue
// Standard

// Sample Output
// Color: Blue

// Equipment: Standard

// Note:
// To implement multiple interfaces, use a comma separated list of interfaces
// when creating the class.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string color = Console.ReadLine();
            string equipment = Console.ReadLine();

            Car car = new Car(color, equipment);

            car.GetColor();
            car.GetEquipment();
        }
    }

    public interface IColor
    {
        void GetColor();
    }

    public interface IEquipment
    {
        void GetEquipment();
    }
    
    // Implement IColor & IEquipment interfaces
    public class Car
    {
        public string color;
        public string equipment;

        public Car(string color, string equipment)
        {
            this.color = color;
            this.equipment = equipment;
        }
        
        // Reimplement this method
        public void GetColor()
        {
            Console.WriteLine($"Color: {color}");
        }

        // Reimplement this method
        public void GetEquipment()
        {
            Console.WriteLine($"Equipment: {equipment}");
        }
    }
}


// QUESTION:
// Fill in the blanks to create an interface and implement it.
//
//    _________ IAnimal
//    {
//        void Eat();
//    }
//
//    _____ Dog ____ IAnimal
//    {
//        public _ Eat()
//        {
//            Console.WriteLine("Omnomnom");
//        }
//    }

// ANSWER:
//    interface IAnimal
//    {
//        void Eat();
//    }
//
//    class Dog : IAnimal
//    {
//        public void Eat()
//        {
//            Console.WriteLine("Omnomnom");
//        }
//    }

// Want to know why?
// Your answer is correct because you have defined an interface `IAnimal` with a
// single method `Eat()`. Then, you have implemented this interface in the `Dog`
// class and defined the `Eat()` method for it. The implementation satisfies the
// contract defined by the `IAnimal` interface.


// Default Implementation
// Default implementation in interfaces allows to write an implementation of any
// method. This is useful when there is a need to provide a single
// implementation for common functionality.

// Let's suppose we need to add new common functionality to our already existing
// interface, which is implemented by many classes. Without default
// implementation (before C# 8), this operation would create errors, because the
// method we have added isn't implemented in the classes, and we would need to
// implement the same operation one by one in each class. Default implementation
// in interface solves this problem.

// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        public interface IShape
        {
            void Draw();
            void Finish()
            {
                Console.WriteLine("Done!");
            }
        }
        
        class Circle : IShape
        {
            public void Draw()
            {
                Console.WriteLine("Circle Draw");
            }
        }
        
        static void Main(string[] args)
        {
            IShape c = new Circle();
            c.Draw();
            c.Finish();
        }
    }
}

// We added the Finish() method with default implementation to our IShape
// interface and called it without implementing it inside the Circle class.

// Note:
// Methods with default implementation can be freely overridden inside the class
// which implements that interface.


// QUESTION:
// What is the output of this code?
//
//    public interface ISomeFunc
//    {
//        void Greet()
//        {
//            Console.Write("Hello!");
//        }
//    }
//
//    class SomeObject : ISomeFunc
//    {
//        void Greet()
//        {
//            Console.Write("Hi!");
//        }
//    }
//
//    static void Main(string[] args)
//    {
//        ISomeFunc obj = new SomeObject();
//        obj.Greet();
//    }

//    [Hi!]
//    [Hello!]

// ANSWER:
//    Hi!

// Want to know why?
// Your answer is correct because the output of the code will be "Hi!". This is
// because the `SomeObject` class overrides the `Greet()` method of the
// `ISomeFunc` interface and prints "Hi!" instead of "Hello!". When an object of
// the `SomeObject` class is created and assigned to a variable of type
// `ISomeFunc`, calling the `Greet()` method on that variable will execute.
