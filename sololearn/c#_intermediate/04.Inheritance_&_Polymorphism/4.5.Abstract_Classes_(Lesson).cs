// Abstract Classes 

// As described in the previous example, polymorphism is used when you have
// different derived classes with the same method, which has different
// implementations in each class. This behavior is achieved through virtual
// methods that are overridden in the derived classes.
// In some situations there is no meaningful need for the virtual method to have
// a separate definition in the base class.
// These methods are defined using the abstract keyword and specify that the
// derived classes must define that method on their own. 
// You cannot create objects of a class containing an abstract method, which is
// why the class itself should be abstract.
// We could use an abstract method in the Shape class:

abstract class Shape
{
   public abstract void Draw();
}

// As you can see, the Draw method is abstract and thus has no body. You do not
// even need the curly brackets; just end the statement with a semicolon.
// The Shape class itself must be declared abstract because it contains an
// abstract method. Abstract method declarations are only permitted in abstract
// classes.

// Note:
// Remember, abstract method declarations are only permitted in abstract
// classes. Members marked as abstract, or included in an abstract class, must
// be implemented by classes that derive from the abstract class. An abstract
// class can have multiple abstract members.

// QUESTION:
// Fill in the blanks to define an abstract method Print for class A:
//
//    ________ class A
//    {
//        public ________ void Print() _
//    }

// ANSWER:
//    abstract class A
//    {
//        public abstract void Print();
//    }

// Want to know why?
// Your answer is correct because it defines an abstract method called `Print`
// inside an abstract class called `A`. An abstract method has no implementation
// and must be overridden by any non-abstract derived class.


// Abstract Classes
// An abstract class is intended to be a base class of other classes. It acts
// like a template for its derived classes.
// Now, having the abstract class, we can derive the other classes and define
// their own Draw() methods:

using System;

namespace SoloLearn
{
    class Program
    {
        abstract class Shape
        {
            public abstract void Draw();
        }
        
        class Circle : Shape
        {
            public override void Draw()
            {
                Console.WriteLine("Circle Draw");
            }
        }
        
        class Rectangle : Shape
        {
            public override void Draw()
            {
                Console.WriteLine("Rectangle Draw");
            }
        }
        
        static void Main(string[] args)
        {
            Shape c = new Circle();
            c.Draw();
        }
    }
}

// Abstract classes have the following features:
// - An abstract class cannot be instantiated.
// - An abstract class may contain abstract methods and accessors.
// - A non-abstract class derived from an abstract class must include actual
//   implementations of all inherited abstract methods and accessors.

// Note:
// It is not possible to modify an abstract class with the sealed modifier
// because the two modifiers have opposite meanings. The sealed modifier
// prevents a class from being inherited and the abstract modifier requires a
// class to be inherited.

// PRACTICE (PROBLEM SET): Perimeter calculator
// The program you are given defines abstract class Figure and derives
// Rectangle and Triangle classes from it.
// Add an abstract method Perimeter to class Figure, than override it in
// derived classes to calculate perimeters of already created Rectangle and
// Triangle objects.

// Hint
// Perimeter of rectangle with width w and height h => 2*w+2*h.
// Perimeter of triangle with sides s1, s2, s3 => s1+s2+s3.

// Note:
// The abstract Perimeter method inside abstract Figure class should have no
// body.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Figure rectangle = new Rectangle(5, 6);
            Figure triangle = new Triangle(4, 8, 3);

            Console.WriteLine(rectangle.Perimeter());
            Console.WriteLine(triangle.Perimeter());
        }
    }
    abstract class Figure
    {
        // Define abstract method Perimeter with no body
        public abstract int Perimeter();
    }
    class Rectangle : Figure
    {
        public int width;
        public int height;
        public Rectangle(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
        // Override Perimeter method for rectangle
        public override int Perimeter()
        {
            return (2 * width) + (2 * height);
        }
    }
    class Triangle : Figure
    {
        public int side1;
        public int side2;
        public int side3;
        public Triangle(int s1, int s2, int s3)
        {
            this.side1 = s1;
            this.side2 = s2;
            this.side3 = s3;
        }
        
        // Override Perimeter method for triangle
        public override int Perimeter()
        {
            return side1 + side2 + side3;
        }
    }
}


// QUESTION:
// Fill in the blanks to create an abstract class with an abstract method and
// then use it as a base class.
//
//    abstract _____ Animal
//    {
//        public ________ void Eat();
//    }
//    
//    class Dog _ Animal
//    {
//        public ________ void Eat()
//        {
//            Console.WriteLine("Omnomnom");
//        }
//    }
//

// ANSWER:
//    abstract class Animal
//    {
//        public abstract void Eat();
//    }
//    
//    class Dog : Animal
//    {
//        public override void Eat()
//        {
//            Console.WriteLine("Omnomnom");
//        }
//    }

// Want to know why?
// The answer is correct because it demonstrates how to create an abstract class
// with an abstract method and then use it as a base class by inheriting from it
// and overriding the abstract method in the derived class.
