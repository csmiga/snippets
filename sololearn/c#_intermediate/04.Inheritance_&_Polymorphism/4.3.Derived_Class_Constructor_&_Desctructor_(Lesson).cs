// Derived Class Constructor & Destructor

// Constructors are called when objects of a class are created. With
// inheritance, the base class constructor and destructor are not inherited, so
// you should define constructors for the derived classes.

// However, the base class constructor and destructor are being invoked
// automatically when an object of the derived class is created or deleted. 

// Consider the following example:
class Animal
{
    public Animal()
    {
        Console.WriteLine("Animal created");
    }

    ~Animal()
    {
        Console.WriteLine("Animal deleted");
    }
}

class Dog: Animal
{
    public Dog()
    {
        Console.WriteLine("Dog created");
    }
    ~Dog()
    {
        Console.WriteLine("Dog deleted");
    }
}

// We have defined the Animal class with a constructor and destructor and a
// derived Dog class with its own constructor and destructor.

// Note:
// So what will happen when we create an object of the derived class? Tap next
// to find out!


// QUESTION:
// What is the maximum number of base classes a sealed class can have?
//
//    [none]
//    [one]

// ANSWER:
//    one

// Want to know why?
// Your answer is correct. A sealed class cannot be inherited, so it doesn't
// make sense to have multiple base classes. Therefore, a sealed class can only
// have one base class.


// Inheritance
// Let's create a Dog object:
using System;

namespace SoloLearn
{
    class Program
    {
        class Animal
        {
            public Animal()
            {
                Console.WriteLine("Animal created");
            }
            
            ~Animal()
            {
                Console.WriteLine("Animal deleted");
            }
        }
        
        class Dog : Animal
        {
            public Dog()
            {
                Console.WriteLine("Dog created");
            }
            
            ~Dog()
            {
                Console.WriteLine("Dog deleted");
            }
        }
        
        static void Main(string[] args)
        {
            Dog d = new Dog();
        }
    }
}

// Note that the base class constructor is called first and the derived class
// constructor is called next.
// When the object is destroyed, the derived class destructor is invoked and
// then the base class destructor is invoked.

// Note:
// You can think of it as the following: The derived class needs its base class
// in order to work, which is why the base class constructor is called first.

// PRACTICE (PROBLEM SET): Where Are The Planes?
// You are the airport administrator responsible for setting flight statuses.
// At first the flight program was showing only "Registration" and "Closed"
// statuses, but we need to expand it to give more detailed information.
// Derive WayStatus class from Flight class and complete its
// 1. constructor, to output "On the way"
// 2. destructor, to output "Landed" so that the program correctly outputs all
//    the statuses of the flight.

// Note:
// Remember, that while creating derived class object the constructor and the
// destructor of base class are also called - the constructor is called at
// beginning and the destructor at very end.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            WayStatus status = new WayStatus();
        }
    }
    
    class Flight
    {
        public Flight()
        {
            Console.WriteLine("Registration");
        }
        
        ~Flight()
        {
            Console.WriteLine("Closed");
        }
    }

    /*
    Derive this class from Flight class, define constructor and destructor
    for it.
    */
    class WayStatus : Flight
    {
        public WayStatus()
        {
            Console.WriteLine("On the way");
        }

        ~WayStatus()
        {
            Console.WriteLine("Landed");
        }
    }
}


// QUESTION:
// What is the value of x after a B object is created?
//
//    class A
//    {
//        public int x = 7;
//        public A()
//        {
//            x++;
//        }
//    }
//
//    class B : A
//    {
//        public B()
//        {
//            x++;
//        }
//    }

// ANSWER:
//    9