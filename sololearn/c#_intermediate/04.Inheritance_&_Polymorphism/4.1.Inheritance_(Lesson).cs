// Inheritance

// Inheritance allows us to define a class based on another class. This makes
// creating and maintaining an application easy.
// The class whose properties are inherited by another class is called the Base
// class. The class which inherits the properties is called the Derived class.
// For example, base class Animal can be used to derive Cat and Dog classes.
// The derived class inherits all the features from the base class, and can have
// its own additional features.

//    Base Class            Derive Class
//                    (inherited from base class)
//   ------------          ---------------
//  |            |        |               |
//  |            |        |   base class  |
//  |            |        |    features   |
//  | base class |        |               |
//  |  features  |        |---------------|
//  |            |        |               |
//  |            |        | derived class |
//  |            |        |   features    |
//  |            |        |               |
//   ------------          ---------------

// Note:
// Inheritance allows us to define a class based on another class.


// QUESTION:
// Assume the Employee class inherits the members of the Person class. What is
// the Person class called?
//
//    [ The Main class    ]
//    [ The Base class    ]
//    [ The Derived class ]

// ANSWER:
//    The Base class


// Let's define our base class Animal:
//    class Animal
//    {
//        public int Legs {get; set;}
//        public int Age {get; set;}
//    }

// Now we can derive class Dog from it:
//    class Dog : Animal
//    {
//        public Dog()
//        {
//            Legs = 4;
//        }
//
//        public void Bark()
//        {
//            Console.Write("Woof");
//        }
//    }

// Note the syntax for a derived class. A colon and the name of the base class
// follow the name of the derived class.
// All public members of Animal become public members of Dog. That is why we can
// access the Legs member in the Dog constructor.
// Now we can instantiate an object of type Dog and access the inherited members
// as well as call its own Bark method.

using System;

namespace SoloLearn
{
    class Program
    {
        class Animal
        {
            public int Legs {get; set;}
            public int Age {get; set;}
        }
        class Dog : Animal
        {
            public Dog()
            {
                Legs = 4;
            }
            public void Bark()
            {
                Console.Write("Woof");
            }
        }
        static void Main(string[] args)
        {
            Dog d = new Dog();
            Console.WriteLine(d.Legs);
            
            d.Bark();
        }
    }
}

//  A base class can have multiple derived classes. For example, a Cat class can
//  inherit from Animal.

// Note:
// Inheritance allows the derived class to reuse the code in the base class
// without having to rewrite it. And the derived class can be customized by
// adding more members. In this manner, the derived class extends the
// functionality of the base class.


// QUESTION:
// Fill in the blanks to derive Cat from Animal:
//
//    _____ Cat __ Animal
//    {
//        // Some code
//    }

// ANSWER:
//    class Cat : Animal
//    {
//        // Some code
//    }


// A derived class inherits all the members of the base class, including its
// methods. 

// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class Person
        {
            public void Speak()
            {
                Console.WriteLine("Hi there");
            }
        }
        class Student : Person
        {
            int number;
        }
        static void Main(string[] args)
        {
            Student s = new Student();
            s.Speak();
        }
    }
}

// We created a Student object and called the Speak method, which was declared
// in the base class Person.

// Note:
// C# does not support multiple inheritance, so you cannot inherit from multiple
// classes.
// However, you can use interfaces to implement multiple inheritance. You will
// learn more about interfaces in the coming lessons.


// PRACTICE (PROBLEM SET): Make And Model
// The program you are given takes the brand and model of the car as input, and
// defines a Vehicle class with model property and ShowModel() method.

// Complete the Car class to inherit the Vehicle class, and add the Model
// property and ShowModel() method so that the given method call of the car
// object works correctly(see sample output).

// Sample Input
// BMW
// 5 Series

// Sample Output
// Brand: BMW
// Model: 5 Series

// Note:
// Use semicolon (:) to derive one class from another.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string brandName = Console.ReadLine();
            string modelName = Console.ReadLine();

            Car car = new Car();
            car.Brand = brandName;
            car.Model = modelName;

            car.ShowBrand();
            car.ShowModel();
        }
    }

    class Vehicle
    {
        public string Brand { get; set; }

        public void ShowBrand()
        {
            Console.WriteLine("Brand: " + Brand);
        }
    }
    
    // Complete the Car class
    class Car : Vehicle
    {
        public string Model { get; set; }

        public void ShowModel()
        {
            Console.WriteLine("Model: " + Model);
        }

    }
}


// QUESTION:
// How many base classes can one class have?
// 
//    [multiple]
//    [two]
//    [one]

// ANSWER:
//    one

// Want to know why?
// Your answer is correct because in C#, a class can have only one direct base
// class. This is known as single inheritance. However, a class can implement
// multiple interfaces, which can also provide additional functionality.
