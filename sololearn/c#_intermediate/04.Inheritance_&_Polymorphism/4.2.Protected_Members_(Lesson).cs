// Protected Members

// Up to this point, we have worked exclusively with public and private access
// modifiers.
// Public members may be accessed from anywhere outside of the class, while
// access to private members is limited to their class. 
// The protected access modifier is very similar to private with one difference;
// it can be accessed in the derived classes. So, a protected member is
// accessible only from derived classes.

// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class Person
        {
            protected int Age {get; set;}
            protected string Name {get; set;}
        }

        class Student : Person
        {
            public Student(string nm)
            {
                Name = nm;
            }

            public void Speak()
            {
                Console.Write("Name: " + Name);
            }
        }

        static void Main(string[] args)
        {
            Student s = new Student("David");
            s.Speak();
        }
    }
}

// As you can see, we can access and modify the Name property of the base class
// from the derived class. 
// But, if we try to access it from outside code, we will get an error:

using System;

namespace SoloLearn
{
    class Program
    {
        class Person
        {
            protected int Age {get; set;}
            protected string Name {get; set;}
        }

        class Student : Person
        {
            public Student(string nm)
            {
                Name = nm;
            }

            public void Speak()
            {
                Console.Write("Name: " + Name);
            }
        }

        static void Main(string[] args)
        {
            Student s = new Student("David");
            s.Name = "Bob"; 
        }
    }
}

// Note:
// Run the code and see how it works!


// QUESTION:
// Fill in the blanks to make the Area method of the Shape class accessible only
// in the derived class:
//
//    _____ Shape
//    {
//        public int H {get; set;}
//        public int W {get; set;}
//        _________ int Area()
//        {
//            return H * W;
//        }
//    }

// ANSWER:
//    class Shape
//    {
//        public int H {get; set;}
//        public int W {get; set;}
//        protected int Area()
//        {
//            return H * W;
//        }
//    }

// Want to know why?
// Your answer is correct because you have changed the access modifier of the
// `Area` method from `public` to `protected`. This makes the method accessible
// only in the derived classes of the `Shape` class.


// Sealed
// A class can prevent other classes from inheriting it, or any of its members,
// by using the sealed modifier.

// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        sealed class Animal
        {
            // Some code
        }

        class Dog : Animal { }  // Error
        
        static void Main(string[] args)
        {
            
        }
    }
}

// In this case, we cannot derive the Dog class from the Animal class because
// Animal is sealed.

// Note:
// The sealed keyword provides a level of protection to your class so that other
// classes cannot inherit from it.


// PRACTICE (PROBLEM SET): What's My Account Balance?
// The program you are given takes an account number and its balance as input.

// It defines Account class with 1 member balance and derives User class from it
// with 1 additional member - the account number, then creates a user object and
// tries to store the balance and account number in it, and shows the details.
// But something is wrong.

// Fix the program so that it completes the User() constructor, which should
// assign the parameters to the corresponding members of the User class.

// Also, check the access modifier of balance member of Account class.

// Sample Input
// 005615216
// 1488.36

// Sample Output
// Account N: 005615216
// Balance: 1488.36

// Note:
// Remember, private members can't be accessed in derived classes, while
// protected ones can.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string accNumber = Console.ReadLine();
            double balance = Convert.ToDouble(Console.ReadLine());

            User user = new User(accNumber, balance);

            user.ShowDetails();
        }
    }

    class Account
    {
        protected double Balance { get; set; }
    }

    class User : Account
    {
        public string AccNumber { get; set; }
        
        // Complete the constructor
        public User(string accNumber, double balance)
        {
            this.AccNumber = accNumber;
            Balance = balance;
        }

        public void ShowDetails()
        {
            Console.WriteLine("Account N: " + AccNumber);
            Console.WriteLine("Balance: " + Balance);
        }
    }
}


// QUESTION:
// Fill in the blanks to derive a class B from class A and prevent class B from
// being inherited.
//
//    _____ A
//    {
//    }
//
//    ______ class B _ A
//    {
//    }

// ANSWER:
//    class A
//    {
//    }
//
//    sealed class B : A
//    {
//    }

// Want to know why?
// Great job! Your answer is correct. Using the "sealed" keyword in the class
// definition prevents any further inheritance from that class, which means that
// no other class can be derived from it.
