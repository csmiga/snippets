 // Polymorphism

// The word polymorphism means "having many forms". 
// Typically, polymorphism occurs when there is a hierarchy of classes and they
// are related through inheritance from a common base class.
// Polymorphism means that a call to a member method will cause a different
// implementation to be executed depending on the type of object that invokes
// the method. 

// Note:
// Simply, polymorphism means that a single method can have a number of
// different implementations.

// QUESTION:
// Briefly, polymorphism is:
//
//    [one implementation with different methods]
//    [each implementation in a different method]
//    [one method with different implementations]

// ANSWER:
//    one method with different implementations

// Want to know why?
// Good job! Your answer about polymorphism being one method with different
// implementations is correct. Polymorphism allows us to use the same method or
// object in different contexts, and this is achieved through inheritance,
// interfaces, and overloading.


// Consider having a program that allows users to draw different shapes. Each
// shape is drawn differently, and you do not know which shape the user will
// choose. 
// Here, polymorphism can be leveraged to invoke the appropriate Draw method of
// any derived class by overriding the same method in the base class. Such
// methods must be declared using the virtual keyword in the base class.

// For example:
class Shape
{
    public virtual void Draw()
    {
        Console.Write("Base Draw");
    }
}

// The virtual keyword allows methods to be overridden in derived classes.

// Note:
// Virtual methods enable you to work with groups of related objects in a
// uniform way.


// QUESTION:
// Fill in the blanks to define a public and virtual method Bark in the Animal
// class:
//
//    _____ Animal
//    {
//        ______ _______ void Bark()
//        {
//            Console.Write("Barking.");
//        }
//    }

// ANSWER:
//    class Animal
//    {
//        public virtual void Bark()
//        {
//            Console.Write("Barking.");
//        }
//    }

// Want to know why?
// The answer is correct because it defines a `public` and `virtual` method
// named `Bark` in the `Animal` class that prints "Barking" to the console. The
// `public` keyword allows the method to be accessed from outside the class, and
// the `virtual` keyword allows it to be overridden in a derived class.


// Now, we can derive different shape classes that define their own Draw methods
// using the override keyword: 

class Circle : Shape
{
    public override void Draw()
    {
        // draw a circle...
        Console.WriteLine("Circle Draw");
    }
}

class Rectangle : Shape
{
    public override void Draw()
    {
        // draw a rectangle...
        Console.WriteLine("Rect Draw");
    }
}

// The virtual Draw method in the Shape base class can be overridden in the
// derived classes. In this case, Circle and Rectangle have their own Draw
// methods.
// Now, we can create separate Shape objects for each derived type and then
// call their Draw methods:

using System;

namespace SoloLearn
{
    class Program
    {
        class Shape
        {
            public virtual void Draw()
            {
                Console.Write("Base Draw");
            }
        }
       
        class Circle : Shape
        {
            public override void Draw()
            {
                // Draw a circle...
                Console.WriteLine("Circle Draw");
            }
        }
        
        class Rectangle : Shape
        {
            public override void Draw()
            {
                // Draw a rectangle...
                Console.WriteLine("Rectangle Draw");
            }
        }
        
        static void Main(string[] args)
        {
            Shape c = new Circle();
            c.Draw();

            Shape r = new Rectangle();
            r.Draw();
        }
    }
}

// Note:
// As you can see, each object invoked its own Draw method, thanks to
// polymorphism.


// QUESTION:
// Fill in the blanks to implement the Bark method in the Dog class:
//
//    class Dog _ Animal
//    {
//        public ________ void Bark __
//        {
//            Console.WriteLine("Woof!");
//        }
//    }

// ANSWER:
//    class Dog : Animal
//    {
//        public override void Bark ()
//        {
//            Console.WriteLine("Woof!");
//        }
//    }

// Want to know why?
// Your answer is correct because it properly implements the Bark method in the
// Dog class by overriding the Bark method in the Animal class and printing
// "Woof!" to the console.


// To summarize, polymorphism is a way to call the same method for different
// objects and generate different results based on the object type. This
// behavior is achieved through virtual methods in the base class.
// To implement this, we create objects of the base type, but instantiate them
// as the derived type: 

Shape c = new Circle();

// Shape is the base class. Circle is the derived class.

// So why use polymorphism? We could just instantiate each object of its type
// and call its method, as in:

Circle c = new Circle();
c.Draw();

// The polymorphic approach allows us to treat each object the same way. As all
// objects are of type Shape, it is easier to maintain and work with them. You
// could, for example, have a list (or array) of objects of that type and work
// with them dynamically, without knowing the actual derived type of each
// object.

// Note:
// Polymorphism can be useful in many cases. For example, we could create a game
// where we would have different Player types with each Player having a separate
// behavior for the Attack method.
// In this case, Attack would be a virtual method of the base class Player and
// each derived class would override it.

// PRACTICE (PROBLEM SET): Attack!
// In a turn-based strategy game, each unit can attack.
// The standard unit attacks with a sword. But there are two more types of
// units - musketeers and magicians, who attack in their own way.

// The program you are given declares Unit class which has a method Attack().
// It outputs "Using sword!".

// Derive Musketeer and Magician classes from the Unit class and override its
// Attack() method to output the corresponding messages while attacking:

// Musketeer => "Using musket!"
// Magician =>"Using magic!"

// Note:
// Don't forget about virtual keyword, which allows for override the method in
// derived classes.

using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Unit unit1 = new Unit();
            Unit musketeer = new Musketeer();
            Unit magician = new Magician();

            unit1.Attack();
            musketeer.Attack();
            magician.Attack();
        }
    }

    class Unit
    {
        public virtual void Attack()
        {
            Console.WriteLine("Using sword!");
        }
    }
    
    /*
    Derive the class from Unit class and override Attack() method
    */
    class Musketeer : Unit
    {
            public override void Attack()
            {
                Console.WriteLine("Using musket!");
            }
    }
    /*
    Derive the class from Unit class and override Attack() method
    */
    class Magician : Unit
    {
            public override void Attack()
            {
                Console.WriteLine("Using magic!");
            }
    }
}

// Note:
// Don't forget about virtual keyword, which allows for override the method in
// derived classes.


// QUESTION:
// Class B inherits from class A. The constructor of class A displays "a", while
// the class B constructor displays "b".
// What is output to the screen when the code A a = new B(); is run?
//
//    [ab]
//    [ba]
//    [b]
//    [a]

// ANSWER:
//    ab

// Want to know why?
// Great job! Your answer is correct. When the code `A a = new B();` is run, it
// creates an instance of class B using its constructor. Since class B inherits
// from class A, the constructor of class A is also executed first which
// displays "a". Then, the constructor of class B is executed which displays
// "b". Therefore, the output will be "ab".
