// ENCAPSULATION

// Part of the meaning of the word encapsulation is the idea of "surrounding" an
// entity, not just to keep what's inside together, but also to protect it.

// In programming, encapsulation means more than simply combining members
// together within a class; it also means restricting access to the inner
// workings of that class.

// Encapsulation is implemented by using access modifiers. An access modifier
// defines the scope and visibility of a class member.

// NOTE:
// Encapsulation is also called information hiding.


// QUESTION:
// Encapsulation allows you to:
// 
//    [ Assign values to variables          ]
//    [ Declare a method                    ]
//    [ Hide details of a class realization ]

// ANSWER:
//    • Hide details of a class realization


// C# supports the following access modifiers: public, private, protected,
// internal, protected internal.

// As seen in the previous examples, the public access modifier makes the member
// accessible from the outside of the class. 

// The private access modifier makes members accessible only from within the
// class and hides them from the outside.


// QUESTION:
// Which one is NOT an access modifier in C#?
// 
//    [ private   ]
//    [ closed    ]
//    [ internal  ]
//    [ protected ]

// ANSWER:
//    closed


// To show encapsulation in action, let’s consider the following example:
// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class BankAccount
    {
        private double balance = 0;
        public void Deposit(double n)
        {
            balance += n;
        }
        public void Withdraw(double n)
        {
            balance -= n;
        }
        public double GetBalance()
        {
            return balance;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount b = new BankAccount();
            b.Deposit(199);
            b.Withdraw(42);
            Console.WriteLine(b.GetBalance());
        }
    }
}

// We used encapsulation to hide the balance member from the outside code. Then
// we provided restricted access to it using public methods. The class data can
// be read through the GetBalance method and modified only through the Deposit
// and Withdraw methods.

// You cannot directly change the balance variable. You can only view its value
// using the public method. This helps maintain data integrity.

// We could add different verification and checking mechanisms to the methods to
// provide additional security and prevent errors.

// NOTE:
// In summary, the benefits of encapsulation are:
// - Control the way data is accessed or modified.
// - Code is more flexible and easy to change with new requirements.
// - Change one part of code without affecting other parts of code.


// PRACTICE (PROBLEM SET): Calculating Wins
// 
// Encapsulation
// We are developing a profile system for player of our online game.
// The program already takes the number of games and wins as input and creates a
// player object.
// Complete the GetWinRate() method inside the given Player class to calculate
// and output the win rate.
//
// Sample Input
// 130
// 70
//
// Sample Output
// 53
//
// Explanation
// Win rate is calculated by this formula: wins*100/games. So, in this case win
// rate is 70*100/130 = 53 (the final result should be an integer).
//
// NOTE:
// Notice that you should execute the output of the win rate inside the method.

// PRACTICE (SOLUTION): Calculating Wins
// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int games = Convert.ToInt32(Console.ReadLine()); 
            int wins = Convert.ToInt32(Console.ReadLine()); 

            // Creating the player object
            Player player1 = new Player();
            player1.games = games;
            player1.wins = wins;

            // Output
            player1.GetWinRate();
        }
    }
    
    class Player
    {
        public int games;
        public int wins;
        // winrate is private
        private int winrate;

        // Complete the method
        public void GetWinRate()
        {
            winrate = (wins * 100) / games;
            Console.WriteLine(winrate);
        }
    }
}


// QUESTION:
// Fill in the blanks to declare a Person class, hide the age member, and make
// it accessible through the GetAge method.
// 
//    _____ Person
//    {
//        private int age;
//        ______ int GetAge()
//        {
//            ______ age;
//        }
//        
//        public void SetAge(int n)
//        {
//            age = n;
//        }
//    }

// ANSWER:
//    class Person
//    {
//        private int age;
//        public int GetAge()
//        {
//            return age;
//        }
//        
//        public void SetAge(int n)
//        {
//            age = n;
//        }
//    }
