// CONSTRUCTORS

// A class constructor is a special member method of a class that is executed
// whenever a new object of that class is created.

// A constructor has exactly the same name as its class, is public, and does not
// have any return type.

// For example:
class Person
{
    private int age;
    public Person()
    {
        Console.WriteLine("Hi there");
    }
}

// Now, upon the creation of an object of type Person, the constructor is
// automatically called.

// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        class Person
        {
            private int age;
            public Person()
            {
                Console.WriteLine("Hi there");
            }
        }
        static void Main(string[] args)
        {
            Person p = new Person();
        }
    }
}

// This can be useful in a number of situations. For example, when creating an
// object of type BankAccount, you could send an email notification to the
// owner.

// The same functionality could be achieved using a separate public method. The
// advantage of the constructor is that it is called automatically.


// PRACTICE (PROBLEM SET): Creating A Project
// Complete the given class by adding a constructor to show the success message.
// 
// Your graphic application needs to report that a new project has been created
// successfully once the "Create" button has been pressed.
// 
// Complete the given class by adding a constructor that will show message
// "Project created" once the operation is done.
// 
// You need to execute the output inside the constructor.

// PRACTICE (SOLUTION): Creating A Project
// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Project project = new Project();
        }
    }
    class Project
    {
        // Create a constructor to show "Project created"
        public Project()
        {
            Console.WriteLine("Project created");
        }
    }
}


// QUESTION:
// When is the constructor called?
// 
//    [ When the class is being declared ]
//    [ Never                            ]
//    [ When a class object is created   ]

// ANSWER:
//    When a class object is created


// Constructors can be very useful for setting initial values for certain member
// variables.

// A default constructor has no parameters. However, when needed, parameters can
// be added to a constructor. This makes it possible to assign an initial value
// to an object when it's created, as shown in the following example:

// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        class Person
        {
            private int age;
            private string name;
            public Person(string nm)
            {
                name = nm;
            }
            public string getName()
            {
                return name;
            }
        }
        static void Main(string[] args)
        {
            Person p = new Person("David");
            Console.WriteLine(p.getName());
        }
    }
}

// Now, when the object is created, we can pass a parameter that will be
// assigned to the name variable.

// NOTE:
// Constructors can be overloaded like any method by using different numbers of
// parameters.


// QUESTION:
//    class Dog
//    {
//        public Dog()
//        {
//            Console.WriteLine(1);
//        }
//        
//        public Dog(string name)
//        {
//            Console.WriteLine(name);
//        }
//
//        static void Main(string[] args)
//        {
//            Dog d = new Dog("2");
//        }

// ANSWER:
//    2
