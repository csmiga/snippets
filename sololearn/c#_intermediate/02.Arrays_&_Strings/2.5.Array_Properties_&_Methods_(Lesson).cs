// Arrays Properties

// The Array class in C# provides various properties and methods to work with
// arrays. 
// For example, the Length and Rank properties return the number of elements and
// the number of dimensions of the array, respectively. You can access them
// using the dot syntax, just like any class members:

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[ ] arr = {2, 4, 7};
            Console.WriteLine(arr.Length); 
            Console.WriteLine(arr.Rank); 
        }
    }
}

// The Length property can be useful in "for" loops where you need to specify
// the number of times the loop should run.

// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[ ] arr = {2, 4, 7};
            for(int k = 0; k < arr.Length; k++)
            {
                Console.WriteLine(arr[k]);
            }
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// What is the output of this code?
// 
//    int[ , , ] a = new int[2, 3, 4];
//    Console.Write(a.Rank);

// ANSWER:
//    3


// ARRAY METHODS

// There are a number of methods available for arrays.
//
//    Max - returns the largest value.
//    Min - returns the smallest value.
//    Sum - returns the sum of all elements.

// For example:
using System;
using System.Linq;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[ ] arr = { 2, 4, 7, 1};
            Console.WriteLine(arr.Max());
            Console.WriteLine(arr.Min());
            Console.WriteLine(arr.Sum());
        }
    }
}

// NOTE:
// C# also provides a static Array class with additional methods. You will learn
// about those in the next module.

// PRACTICE (PROBLEM SET): Array Properties & Methods - Maximum And Minimum
// Write a program to take 5 numbers as input, then calculate and output the sum
// of the maximum and the minimum inputted values.

// Sample Input
// 5
// 6
// 14
// 2
// 1

// Sample Output
// 15

// Explanation
// The minimum value is 1, the maximum is 14. So 14 + 1 = 15 should be output.
// Hint
// Create an array, use while loop to store the inputted numbers in it, and then
// do the calculations.

using System;
using System.Linq;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            // Your code goes here
            int[ ] arr = new int[5];
            int i = 0;
            while (i < 5)
            {
                arr[i] = Convert.ToInt32(Console.ReadLine());
                i++;
            }
            Console.WriteLine(arr.Min() + arr.Max());
        }
    }
}


// QUESTION:
// What is the output of this code?
// 
//    int[ ] a = {4, 6, 5, 2};
//    int x = a[0] + a.Min();
//    Console.Write(x);

// ANSWER:
//    6
