// ARRAYS & LOOPS

// It's occasionally necessary to iterate through the elements of an array,
// making element assignments based on certain calculations. This can be easily
// done using loops.

// For example, you can declare an array of 10 integers and assign each element
// an even value with the following loop: 

int[ ] a = new int[10];
for (int k = 0; k < 10; k++)
{
    a[k] = k * 2;
}

// We can also use a loop to read the values of an array.
// For example, we can display the contents of the array we just created:

// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[ ] a = new int[10];
            for (int k = 0; k < 10; k++)
            {
                a[k] = k * 2;
            }

            for (int k = 0; k < 10; k++)
            {
                Console.WriteLine(a[k]);
            }
        }
    }
}

// This will display the values of the elements of the array.

// NOTE:
// The variable k is used to access each array element.
// The last index in the array is 9, so the for loop condition is k < 10.


// QUESTION:
// Fill in the blanks to print all elements of the array using a for loop.
// 
//    int[ ] arr = new int[7];
//    ___ (int k = 0; k < _ ; k++)
//    {
//        Console.WriteLine(___[k]);
//    }

// ANSWER:
//    int[ ] arr = new int[7];
//    for (int k = 0; k < 7 ; k++)
//    {
//        Console.WriteLine(arr[k]);
//    }


// THE FOREACH LOOP

// The foreach loop provides a shorter and easier way of accessing array
// elements.
// The previous example of accessing the elements could be written using a
// foreach loop:

// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[ ] a = new int[10];
            for (int k = 0; k < 10; k++)
            {
                a[k] = k * 2;
            }
            foreach (int k in a)
            {
                Console.WriteLine(k);
            }
        }
    }
}

// The foreach loop iterates through the array a and assigns the value of the
// current element to the variable k at each iteration of the loop. So, at the
// first iteration, k=a[0], at the second, k=a[1], etc.

// NOTE:
// The data type of the variable in the foreach loop should match the type of
// the array elements.
// Often the keyword var is used as the type of the variable, as in: foreach
// (var k in a). The compiler determines the appropriate type for var.


// QUESTION:
// Fill in the blanks to create a valid foreach loop that displays all even
// elements of the array.
// 
//    int[ ] nums = {5, 2, 3, 4, 7};
//    _____ (var n _____ nums)
//    {
//        if (n%2 == 0)
//            Console.WriteLine(_____);
//    }

// ANSWER:
//    int[ ] nums = {5, 2, 3, 4, 7};
//    _______ (var n __ nums)
//    {
//        if (n%2 == 0)
//            Console.WriteLine(_);
//    }


// ARRAYS

// The following code uses a foreach loop to calculate the sum of all the
// elements of an array:

// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[ ] arr = {11, 35, 62, 555, 989};
            int sum = 0; 
            
            foreach (int x in arr)
            {
                sum += x;
            }
            Console.WriteLine(sum);
        }
    }
}

// To review, we declared an array and a variable sum that will hold the sum of
// the elements.

// Next, we utilized a foreach loop to iterate through each element of the
// array, adding the corresponding element's value to the sum variable.

// NOTE:
// The Array class provides some useful methods that will be discussed in the
// coming lessons.


// PRACTICE (PROBLEM SET): Using Arrays in Loops
// The program you are given takes 5 numbers as input and stores them in an
// array. Complete the program to go through the array and output the the sum of
// even numbers.
// 
// Sample Input
// 10
// 890
// 15
// 3699
// 14
// 
// Sample Output
// 914
//
// Hint
// An integer is even if it is divisible by two, so it means that n number is
// even if n%2 equals 0.

// NOTE:
// You need to declare a separate variable to store the sum in it.

// PRACTICE (SOLUTION): Using Arrays in Loops
// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[5];
            int count = 0;

            while (count < 5)
            {
                numbers[count] = Convert.ToInt32(Console.ReadLine());
                count++;
            }

            // Your code goes here
            int sum = 0;
            foreach (int x in numbers)
            {
                if (x % 2 == 0)
                {
                    sum += x;
                }
            }
            Console.WriteLine(sum);
        }
    }
}


// QUESTION:
// What is the output of this code?
// 
//    int[ ] arr = {8, 2, 6};
//    int y = 0;
//    foreach (int x in arr)
//    {
//        y += x / 2;
//    }
//    Console.Write(y);

// ANSWER:
//     8
