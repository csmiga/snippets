// JAGGED ARRAYS

// A jagged array is an array whose elements are arrays. So it is basically an
// array of arrays.
// The following is a declaration of a single-dimensional array that has three
// elements, each of which is a single-dimensional array of integers:

int[ ][ ] jaggedArr = new int[3][ ];

// Each dimension is an array, so you can also initialize the array upon
// declaration like this:

int[ ][ ] jaggedArr = new int[ ][ ] 
{
  new int[ ] {1,8,2,7,9},
  new int[ ] {2,4,6},
  new int[ ] {33,42}
};

// You can access individual array elements as shown in the example below:

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[ ][ ] jaggedArr = new int[ ][ ] 
            {
                new int[ ] {1,8,2,7,9},
                new int[ ] {2,4,6},
                new int[ ] {33,42}
            };
            int x = jaggedArr[2][1];
            Console.WriteLine(x);
        }
    }
}

// This accesses the second element of the third array.

// NOTE:
// A jagged array is an array-of-arrays, so an int[ ][ ] is an array of int[ ],
// each of which can be of different lengths and occupy their own block in
// memory.
// A multidimensional array (int[,]) is a single block of memory (essentially a
// matrix). It always has the same amount of columns for every row.


// PRACTICE (PROBLEM SET): Qualifying For The Olympics
// The qualifiers for the Olympiad lasts 3 days, and one winner is selected each
// qualifying day.
// The jagged array you are given represents the list of all participants,
// divided by the number of days (there are 3 arrays inside the main one, each
// representing the participants who took part on that day).

string[][] olympiad = new string[][]
{
    // Day 1 => 5 participants
    new string[] { "Jill Yan", "Bridgette Ramona", "Sree Sanda", "Jareth Charlene", "Carl Soner" },
    // Day 2 => 7 participants
    new string[] { "Anna Hel", "Mariette Vedrana", "Fran Mayur", "Drake Hilmar", "Nikolay Brooks", "Eliana Vlatko", "Villem Mario" },
    // Day 3 => 4 participants
    new string[] { "Hieremias Zavia", "Ziya Ollie", "Christoffel Casper", "Kristian Dana",}
}; 

// Write a program to take the numbers of each day's winners as input and output
// them.
//
// Sample Input
// 2
// 3
// 4
//
// Sample Output
// Bridgette Ramona
// Fran Mayur
// Kristian Dana
//
// Explanation
// Day 1 winner is Bridgette Ramona (the 2nd participant of day 1)
// Day 2 winner is Fran Mayur (the 3rd participant of day 2)
// Day 3 winner is Kristian Dana (the 4th participant of day 3)
//
// Note:
// Remember that the indexing starts from 0, so, if you want  to output Nth
// participant, you should use the N-1 index.

// PRACTICE (SOLUTION): Qualifying For The Olympics
// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            //int day1Winner = Convert.ToInt32(Console.ReadLine());
            int day1Winner = 2;
            //int day2Winner = Convert.ToInt32(Console.ReadLine());
            int day2Winner = 3;
            //int day3Winner = Convert.ToInt32(Console.ReadLine());
            int day3Winner = 4;
            
            string[][] olympiad = new string[][]
            {
                // day 1 - 5 participants
                new string[] { "Jill Yan", "Bridgette Ramona", "Sree Sanda", "Jareth Charlene", "Carl Soner" },
                // day 2 - 7 participants
                new string[] { "Anna Hel", "Mariette Vedrana", "Fran Mayur", "Drake Hilmar", "Nikolay Brooks", "Eliana Vlatko", "Villem Mario" },
                // day 3 - 4 participants
                new string[] { "Hieremias Zavia", "Ziya Ollie", "Christoffel Casper", "Kristian Dana", }
            };
            
            // your code goes here
            Console.WriteLine(olympiad[0][day1Winner - 1]);
            Console.WriteLine(olympiad[1][day2Winner - 1]);
            Console.WriteLine(olympiad[2][day3Winner - 1]);
        }
    }
}


// QUESTION:
// Fill in the blanks to declare a jagged array that contains 8 two-dimensional arrays.
// 
//    int[ ][,] a = new int[_][_];

// ANSWER:
//    int[ ][,] a = new int[8][,];
