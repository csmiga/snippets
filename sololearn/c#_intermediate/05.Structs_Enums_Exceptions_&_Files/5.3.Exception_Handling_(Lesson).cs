// Exception Handling

// An exception is a problem that occurs during program execution. Exceptions
// cause abnormal termination of the program. 

// An exception can occur for many different reasons. Some examples:
// - A user has entered invalid data.
// - A file that needs to be opened cannot be found.
// - A network connection has been lost in the middle of communications.
// - Insufficient memory and other issues related to physical resources.

// For example, the following code will produce an exception when run because we
// request an index which does not exist: 

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 4, 5, 8 };
            Console.Write(arr[8]);
        }
    }
}

// Note:
// As you can see, exceptions are caused by user error, programmer error, or
// physical resource issues. However, a well-written program should handle all
// possible exceptions.


// QUESTION:
// What is the maximum index of the following array?
//
//    int[] arr = new int[] { 4, 5, 8 };

// ANSWER:
//    2


// C# provides a flexible mechanism called the try-catch statement to handle
// exceptions so that a program won't crash when an error occurs.

// The try and catch blocks are used similar to:

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] arr = new int[] { 4, 5, 8 };
                Console.Write(arr[8]);
            }
            
            catch(Exception e)
            {
                Console.WriteLine("An error occurred");
            }
        }
    }
}

// The code that might generate an exception is placed in the try block. If an
// exception occurs, the catch blocks is executed without stopping the program.

// The type of exception you want to catch appears in parentheses following the
// keyword catch.

// We use the general Exception type to handle all kinds of exceptions. We can
// also use the exception object e to access the exception details, such as the
// original error message (e.Message):

// Note:
// You can also catch and handle different exceptions separately. Tap next to
// learn more!

// QUESTION:
// Fill in the blanks to handle all possible exceptions.
//
//    int x = 12;
//    int y = 0;
//    ___
//    {
//        int z = x / y;
//        Console.WriteLine(z);
//    }
//    _____ (Exception e)
//    {
//        Console.WriteLine("Error");
//    }

// ANSWER:
//    int x = 12;
//    int y = 0;
//    try
//    {
//        int z = x / y;
//        Console.WriteLine(z);
//    }
//    catch (Exception e)
//    {
//        Console.WriteLine("Error");
//    }

// Want to know why?
// Your answer is correct as it uses a try-catch block to handle any exception
// that might occur during the execution of the code. It catches the most
// general exception type, which is the base class of all exceptions in C#, and
// prints an error message to the console.


// Handling Multiple Exceptions
// A single try block can contain multiple catch blocks that handle different
// exceptions separately. 
// Exception handling is particularly useful when dealing with user input.
// For example, for a program that requests user input of two numbers and then
// outputs their quotient, be sure that you handle division by zero, in case
// your user enters 0 as the second number.

int x, y;
try
{
    x = Convert.ToInt32(Console.Read());
    y = Convert.ToInt32(Console.Read());
    Console.WriteLine(x / y);
}

catch (DivideByZeroException e)
{
    Console.WriteLine("Cannot divide by 0");
}

catch(Exception e)
{
    Console.WriteLine("An error occurred");
}

// The above code handles the DivideByZeroException separately. The last catch
// handles all the other exceptions that might occur. If multiple exceptions are 
// andled, the Exception type must be defined last.

// Now, if the user enters 0 for the second number, "Cannot divide by 0" will be
// displayed.

// If, for example, the user enters non-integer values, "An error occurred" will
// be displayed.

// Note:
// The following exception types are some of the most commonly used:
// FileNotFoundException, FormatException, IndexOutOfRangeException,
// InvalidOperationException, OutOfMemoryException.


// QUESTION:
// What is the value of x after this code?
//
//    int[] arr = {2, 5, 3};
//    int x = 0;
//    try
//    {
//        x = arr[5];
//    }
//
//    catch (Exception e)
//    {
//        x = arr[x];
//    }

// ANSWER:
//    2

// finally
// An optional finally block can be used after the catch blocks. The finally
// block is used to execute a given set of statements, whether an exception is
// thrown or not. 

// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int result=0;
            int num1 = 8;
            int num2 = 4;
            
            try
            {
                result = num1 / num2;
            }
            
            catch (DivideByZeroException e)
            {
                Console.WriteLine("Error");
            }
            
            finally
            {
                Console.WriteLine(result);
            }
        }
    }
}

// Note:
// The finally block can be used, for example, when you work with files or other
// resources. These should be closed or released in the finally block, whether
// an exception is raised or not.

// PRACTICE (PROBLEM SET): Going On Vacation
// A tour operator offers package holidays in England, Spain, Italy, Portugal
// and France.

// The program you are given defines an array with those options and takes N
// number as input.

// Write a program to output the package option with N index. If the number is
// out of range, program should output "Wrong number". Regardless of the option
// results, the program should output "Goodbye" at the end.

// Sample Input
// 2

// Sample Output
// Italy
// Goodbye

// It's not mandatory to mention the exception type.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tours = { "England", "Spain", "Italy", "Portugal", "France" };
            int choice = Convert.ToInt32(Console.ReadLine());
            
            // Your code goes here
            try
            {
                Console.WriteLine(tours[choice]);
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong number");
            }
            finally
            {
                Console.WriteLine("Goodbye");
            }
        }
    }
}

// QUESTION:
// Which class should you use to handle all possible exceptions?
//
//    [ System ]
//    [ Main ]
//    [ Exception ]

// ANSWER:
//    Exception

// Want to know why?
// Your answer is correct because the Exception class is the base class for all
// exceptions in C#. This means that you can catch and handle any possible
// exception by using this class.
