// MODULE 5 QUIZ

// QUESTION:
// Can you inherit from a struct?
// 
//    [ No ]
//    [ Yes ]

// ANSWER:
//    No

// Want to know why?
// Your answer is correct because structs are value types and cannot be
// inherited. Inheritance is a mechanism used to create new classes based on
// existing classes, but structs cannot be used as a base class for inheritance.


// QUESTION:
// What is the output of this code?
//
//    enum Test { A, B, C = 5, D }; 
//    static void Main(string[] args)
//    {
//        int x = (int)Test.D;
//        Console.WriteLine(x);
//    }

// ANSWER:
//    6


// QUESTION:
// int is a Struct.
//
//    [ False ]
//    [ True ]

// ANSWER:
//    True

// Want to know why?
// Correct! "int" is a value type in C# and is represented by the struct
// System.Int32. It has a default value of 0 and is used to store integer
// values.


// QUESTION:
// Which of the following is the correct way to define a variable of the struct
// Person declared below?
//
//    struct Person
//    {
//        private string name; 
//        private int age; 
//    }
//
//    [ Person p = new Person("Bob"); ]
//    [ Person p; ]
//    [ Person e = new Person; ]

// ANSWER:
//    Person p;

// Want to know why?
// Your answer is correct because it declares a variable "p" of type Person.
// Since Person is a struct, it will be initialized with default values for its
// fields (name and age).


// QUESTION:
// What is the value of x after this code?
//
//    int x = 0;
//    try
//    {
//        x /= x;
//        x += 1;
//    }
//
//    catch (Exception e)
//    {
//        x += 3;
//    }
//
//    finally
//    {
//        x += 4;
//    }

// ANSWER:
//    7


// QUESTION:
// Fill in the blanks to handle exceptions.
//
//    int x = 0;
//    ___
//    {
//        x = Convert.ToInt32("AAA");
//    }
//
//    _____ (Exception e)
//    {
//        Console.WriteLine("Error");
//    }

// ANSWER:
//    int x = 0;
//    try
//    {
//        x = Convert.ToInt32("AAA");
//    }
//
//    catch (Exception e)
//    {
//        Console.WriteLine("Error");
//    }

// Want to know why?
// Your answer is correct because it uses a try-catch block to handle
// exceptions. It tries to convert a string to an integer, which will throw an
// exception because the string is not a valid integer. The catch block catches
// the exception and prints an error message to the console.
