// Structs

// A struct type is a value type that is typically used to encapsulate small
// groups of related variables, such as the coordinates of a rectangle or the
// characteristics of an item in an inventory. The following example shows a
// simple struct declaration: 

struct Book
{
    public string title;  
    public double price;
    public string author;
}

// Structs share most of the same syntax as classes, but are more limited than
// classes.
// Unlike classes, structs can be instantiated without using a new operator.

using System;

namespace SoloLearn
{
    class Program
    {
        struct Book
        {
            public string title;  
            public double price;
            public string author;
        }

        static void Main(string[] args)
        {
            Book b;
            b.title = "Test";
            b.price = 5.99;
            b.author = "David";
            
            Console.WriteLine(b.title);
        }
    }
}

// Note:
// Structs do not support inheritance and cannot contain virtual methods.


// QUESTION:
// Which of the following statements are true?
//
//    [ A struct can be abstract ]
//    [ You cannot derive from a struct ]
//    [ A struct can contain virtual methods ]

// ANSWER:
//    You cannot derive from a struct

// Want to know why?
// Your answer is correct because in C#, structs are value types and do not
// support inheritance. Therefore, it is not possible to derive from a struct.
// However, you can implement interfaces in a struct.


// Structs can contain methods, properties, indexers, and so on. Structs cannot
// contain default constructors (a constructor without parameters), but they can
// have constructors that take parameters. In that case the new keyword is used
// to instantiate a struct object, similar to class objects.

// For example: 
using System;

namespace SoloLearn
{
    class Program
    {
        struct Point
        {
            public int x;
            public int y;
            public Point(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }
        
        static void Main(string[] args)
        {
            Point p = new Point(10, 15);
            Console.WriteLine(p.x);
        }
    }
}


// Structs vs Classes
// In general, classes are used to model more complex behavior, or data, that is
// intended to be modified after a class object is created. Structs are best
// suited for small data structures that contain primarily data that is not
// intended to be modified after the struct is created. Consider defining a
// struct instead of a class if you are trying to represent a simple set of
// data.

// Note:
// All standard C# types (int, double, bool, char, etc.) are actually structs.

// PRACTICE (PROBLEM SET): Fun With Dimensions
// A cuboid is a three-dimensional shape with a length, width, and a height.

// The program you are given takes takes those dimensions as inputs, defines
// Cuboid struct and creates its object. 

// Complete the program by creating a constructor, which will take the length,
// the width, and the height as parameters and assign them to its struct
// members. 

// Also complete Volume() and Perimeter() methods inside the struct, to
// calculate and return them, so that the given methods calls work correctly.

// Sample Input
// 5
// 4
// 5

// Sample Output
// Volume: 100
// Perimeter: 56

// Cuboid volume: length * width * height.
// Cuboid perimeter: 4 * (length + width + height).

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = Convert.ToInt32(Console.ReadLine());
            int width = Convert.ToInt32(Console.ReadLine());
            int height = Convert.ToInt32(Console.ReadLine());

            Cuboid cuboid = new Cuboid(length, width, height);

            Console.WriteLine("Volume: " + cuboid.Volume());
            Console.WriteLine("Perimeter: " + cuboid.Perimeter());
        }
    }
    struct Cuboid
    {
        public int length;
        public int width;
        public int height;

        // Create a constructor
        public Cuboid(int x, int y, int z)
        {
            this.length = x;
            this.width = y;
            this.height = z;
        }
        
        // Complete this method
        public int Volume()
        {
            return length*width*height;
        }

        // Complete this method
        public int Perimeter()
        {
            return 4*(length+width+height);
        }
    }
}


// QUESTION:
// Fill in the blanks to declare a struct.
//
//    public ______ Car _
//        public string brand;  
//        public double price;
//    }

// ANSWER:
//    public struct Car {
//        public string brand;  
//        public double price;
//    }

// Want to know why?
// The struct declaration provided is correct as it follows the correct syntax
// for declaring a struct in C#. It defines a struct named Car with two public
// fields of type string and double respectively.
