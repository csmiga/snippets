// Enums

// The enum keyword is used to declare an enumeration: a type that consists of a
// set of named constants called the enumerator list.
// By default, the first enumerator has the value 0, and the value of each
// successive enumerator is increased by 1.
// For example, in the following enumeration, Sun is 0, Mon is 1, Tue is 2, and
// so on: 

// enum Days {Sun, Mon, Tue, Wed, Thu, Fri, Sat};

// You can also assign your own enumerator values:

// enum Days {Sun, Mon, Tue=4, Wed, Thu, Fri, Sat};

// In the example above, the enumeration will start from 0, then Mon is 1, Tue
// is 4, Wed is 5, and so on. The value of the next item in an Enum is one
// increment of the previous value.
// Note that the values are comma separated.
// You can refer to the values in the Enum with the dot syntax.
// In order to assign Enum values to int variables, you have to specify the type
// in parentheses:

using System;

namespace SoloLearn
{
    class Program
    {
        enum Days { Sun, Mon, Tue, Wed, Thu, Fri, Sat }; 
        static void Main(string[] args)
        {
            int x = (int)Days.Tue;
            Console.WriteLine(x);
        }
    }
}

// Note:
// Basically, Enums define variables that represent members of a fixed set.
// Some sample Enum uses include month names, days of the week, cards in a deck,
// etc.


// QUESTION:
// What is the output of this code?
//
//    enum Test { a=2, b, c, d, e }; 
//    static void Main(string[] args)
//    {
//        int x = (int)Test.c;
//        Console.WriteLine(x);
//    }

// ANSWER:
//    4


// Enums are often used with switch statements.

// For example: 
using System;

namespace SoloLearn
{
    class Program
    {
        enum TrafficLights { Green, Red, Yellow };
        static void Main(string[] args)
        {
            TrafficLights x = TrafficLights.Red;
            switch (x)
            {
                case TrafficLights.Green:
                    Console.WriteLine("Go!");
                    break;
                case TrafficLights.Red:
                    Console.WriteLine("Stop!");
                    break;
                case TrafficLights.Yellow:
                    Console.WriteLine("Caution!");
                    break;
            }
        }
    }
}

// Note:
// Run the code and see how it works!

// PRACTICE (PROBLEM SET): Accelerate!
// A racing video game has 3 difficulty levels: Easy, Medium, and Hard.
// Each difficulty level is assigned maximum time to complete the track: the
// higher the difficulty, the lower the time.
// The program you are given defines Player class and Difficulty enum, and
// creates 3 Player objects with different difficulties as parameter for the
// constructor.
// Complete the Player constructor, which takes the enum as a parameter to check
// the time for each difficulty option and outputs the corresponding message:

// Easy => "You have 3 minutes 45 seconds"
// Medium = > "You have 3 minutes 20 seconds"
// Hard => "You have 3 minutes"

// Note
// Use a switch statement to check for each option of the enum and execute the
// output.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {

            Player player1 = new Player(Difficulty.Easy);
            Player player2 = new Player(Difficulty.Medium);
            Player player3 = new Player(Difficulty.Hard);
        }
    }
    
    /*
    Easy => "You have 3 minutes 45 seconds"
    Medium = > "You have 3 minutes 20 seconds"
    Hard => "You have 3 minutes"
    */

    class Player
    {
        public Player(Difficulty x)
        {
            // Your code goes here
            switch (x)
            {
                case Difficulty.Easy:
                Console.WriteLine("You have 3 minutes 45 seconds");
                break;

                case Difficulty.Medium:
                Console.WriteLine("You have 3 minutes 20 seconds");
                break;

                case Difficulty.Hard:
                Console.WriteLine("You have 3 minutes");
                break;
            }
        }
    }
    enum Difficulty
    {
        Easy,
        Medium,
        Hard
    };
}

// QUESTION:
// Drag and drop from the options below to create an Enum called Color, with the
// values RED, BLUE, GREEN.
//
//    _____ Color
//    {
//        RED, _____, GREEN
//    }
//
//    [ RED ] [ class ] [ enum ] [ static ]

// ANSWER:
//    enum Color
//    {
//        RED, BLUE, GREEN
//    }

// Want to know why?
// The answer is correct because it defines an enum named Color and its three
// possible values: RED, BLUE, and GREEN. The syntax is also correct: enum
// keyword followed by the name of the enum and the list of values in a
// comma-separated list.
