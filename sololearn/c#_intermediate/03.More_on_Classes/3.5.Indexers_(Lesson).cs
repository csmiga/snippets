// Indexers

// An indexer allows objects to be indexed like an array. 
// As discussed earlier, a string variable is actually an object of the String
// class. Further, the String class is actually an array of Char objects. In
// this way, the string class implements an indexer so we can access any
// character (Char object) by its index: 

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "Hello World";
            char x = str[4];
            Console.WriteLine(x);
        }
    }
}

// Note:
// Arrays use integer indexes, but indexers can use any type of index, such as
// strings, characters, etc.


// QUESTION:
// What is the output of this code?
//
//    string str = "I know C#";
//    char x = str[3];
//    Console.WriteLine(x);

// ANSWER:
//    n


// Declaration of an indexer is to some extent similar to a property. The
// difference is that indexer accessors require an index. 
// Like a property, you use get and set accessors for defining an indexer.
// However, where properties return or set a specific data member, indexers
// return or set a particular value from the object instance. 

// Indexers are defined with the "this" keyword.

// For example:
class Clients
{
    private string[] names = new string[10];

    public string this[int index]
    {
        get
        {
            return names[index];
        }

        set
        {
            names[index] = value;
        }
    }
}

// As you can see, the indexer definition includes the "this" keyword and an
// index, which is used to get and set the appropriate value.

// Now, when we declare an object of class Clients, we use an index to refer to
// specific objects like the elements of an array:

using System;

namespace SoloLearn
{
    class Program
    {
        class Clients
        {
            private string[] names = new string[10];
            public string this[int index]
            {
                get
                {
                    return names[index];
                }

                set
                {
                    names[index] = value;
                }
            }
        }
        static void Main(string[] args)
        {
            Clients c = new Clients();
            c[0] = "Dave";
            c[1] = "Bob";
            
            Console.WriteLine(c[1]);
        }
    }
}

// Note:
// You typically use an indexer if the class represents a list, collection, or
// array of objects.


// PRACTICE (PROBLEM SET): Music Selector
// Create a music application where one can choose 5 music genres.

// The music app you enjoy allows you to choose 5 music genres to follow.
// The program you are given takes 5 music genres as input and stores them in
// he MusicGenres object as an array.

// Fix the program by declaring an indexer inside the MusicGenres class so that
// the given outputs work correctly.

// Sample Input
// Blues
// Rock
// Hip Hop
// Country
// Soul

// Sample Output
// Following: Blues
// Following: Rock
// Following: Hip Hop
// Following: Country
// Following: Soul

// Hint:
// Remember to use this keyword for declaration of indexer.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            MusicGenres genres = new MusicGenres();

            int count = 0;
            while (count < 5)
            {
                genres[count] = Console.ReadLine();
                count++;
            }

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Following: " + genres[i]);
            }
        }
    }
    class MusicGenres
    {
        private string[] genres = new string[5];
        
        // Declare an indexer
        public string this[int index]
        {
            get
            {
                return genres[index];
            }

            set
            {
                genres[index] = value;
            }
        }
    }
}


// QUESTION:
// Fill in the blanks to define a valid indexer:
//
//    class Person
//    {
//        private string name;
//        public char ____ [int index]
//        {
//            get
//            {
//                return ____ [index];
//            }
//        }
//    }

// ANSWER:
//    class Person
//    {
//        private string name;
//        public char this [int index]
//        {
//            get
//            {
//                return name [index];
//            }
//        }
//    }
