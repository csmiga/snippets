// Static

// Now it's time to discuss the static keyword.
// You first noticed it in the Main method's declaration: 

static void Main(string[] args)

// Class members (variables, properties, methods) can also be declared as
// static. This makes those members belong to the class itself, instead of
// belonging to individual objects. No matter how many objects of the class are
// created, there is only one copy of the static member.

// For example:
class Cat
{
  public static int count = 0;
  public Cat()
  {
    count++;
  }
}

// In this case, we declared a public member variable count, which is static.
// The constructor of the class increments the count variable by one.

// Note:
// No matter how many Cat objects are instantiated, there is always only one
// count variable that belongs to the Cat class because it was declared static.


// QUESTION:
// Drag and drop from the options below to have a valid Main method:
//
//    ______ ____ Main(string[] args)
//
//    function, ~Main, static, new, void

// ANSWER:
//    static void Main(string[] args)


// Because of their global nature, static members can be accessed directly using
// the class name without an object.

// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class Cat
        {
            public static int coun = 0;
            public Cat()
            {
                count++;
            }
        }
        static void Main(string[] args)
        {
            Cat c1 = new Cat();
            Cat c2 = new Cat();
            Console.WriteLine(Cat.count);
        }
    }
}

// As you can see, we can access the static variable using the
// class name: Cat.count.

// The count variable is shared between all Cat objects. For this class, each
// time an object is created, the static value is incremented. The program above
// demonstrates this when 2 is displayed after creating two objects of that
// class.

// Note:
// You must access static members using the class name. If you try to access
// them via an object of that class, you will generate an error.


// QUESTION:
// Drag and drop from the options below to assign the value of the Math class
// static PI variable to x.
//
//    double x = ____.__;
//
//    static, Math, void, return, PI

// ANSWER:
//    double x = Math.PI;


// Static Methods 

// The same concept applies to static methods.

// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class Dog
        {
            public static void Bark()
            {
                Console.WriteLine("Woof");
            }
        }
        static void Main(string[] args)
        {
            Dog.Bark();
        }
    }
}

// Static methods can access only static members.

// Note:
// The Main method is static, as it is the starting point of any program.
// Therefore any method called directly from Main had to be static.


// QUESTION:
// To be able to directly call a method in Main, it should be:
//
//    [ void      ]
//    [ recursive ]
//    [ static    ]

// ANSWER:
//    static


// Static

// Constant members are static by definition.
// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class Dog
        {
            public static void Bark()
            {
                Console.WriteLine("Woof");
            }
        }
        static void Main(string[] args)
        {
            Dog.Bark();
        }
    }
}

// Note:
// The Main method is static, as it is the starting point of any program.
// Therefore any method called directly from Main had to be static.


// QUESTION:
// To be able to directly call a method in Main, it should be:
//
//    [ void      ]
//    [ recursive ]
//    [ static    ]

// ANSWER:
//    static


// Constant members are static by definition.
// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class MathClass 
        {
            public const int ONE = 1;
        }
        static void Main(string[] args)
        {
            Console.Write(MathClass.ONE);
        }
    }
}

// As you can see, we access the property ONE using the name of the class, just
// like a static member. This is because all const members are static by
// default.


// Static Constructors
// Constructors can be declared static to initialize static members of the
// class.
// The static constructor is automatically called once when we access a static
// member of the class.
// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class SomeClass
        {
            public static int X { get; set; }
            public static int Y { get; set; }
            
            static SomeClass()
            {
                X = 10;
                Y = 20;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine(SomeClass.X);
        }
    }
}

// Note:
// The constructor will get called once when we try to access SomeClass.X or
// SomeClass.Y.

// The constructor will get called once when we try to access SomeClass.X or
// SomeClass.Y.


// PRACTICE (PROBLEM SET): Grow Your Business
// Complete the Department class for the the constructor that shows appropriate
// message based on the departments count.

// A company has 2 departments and it is growing, so more departments are
// needed.

// The program you are given takes the number of departments to be opened as
// input, then takes their names and creates Department objects, passing their
// names as the constructor.

// Complete the Department class to have 1 static member depCount with an
// initial value of 2 for the number of departments and the constructor that
// will count it and output corresponding message (see sample output).

// Sample Input
// 2
// Finance
// Marketing

// Sample Output
// Department opened: Finance
// Department opened: Marketing
// Number of departments: 4

// Explanation
// The first input represents the number of departments to be opened, followed
// by their names.
// As a result, the program outputs the corresponding messages (the 1st and the
// 2nd outputs) and the total number of departments:
// 2 (initial) + 2 (opened) = 4.

// Hint:
// You need to increment the static number of departments inside the
// constructor.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfDeps = Convert.ToInt32(Console.ReadLine());

            int count = 0;
            while (count < numOfDeps)
            {
                string depName = Console.ReadLine();
                Department dep = new Department(depName);
                count++;
            }

            Console.WriteLine("Number of departments: " + Department.depCount);
        }
    }
    
    class Department
    {
        public string depName;
        // Declare static depCount member with value of 2
        public static int depCount = 2;

        // Complete the constructor
        public Department(string name)
        {
            Console.WriteLine("Department opened: " + name);
            this.depName = name;
            depCount++;

        }
    }
}


// Constant members are static by definition.
// For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class MathClass
        {
            public const int ONE = 1;
        }
        static void Main(string[] args)
        {
            Console.Write(MathClass.ONE);
        }
    }
}

// As you can see, we access the property ONE using the name of the class, just
// like a static member. This is because all const members are static by default.

// Static Constructors 
// Constructors can be declared static to initialize static members of the
// class.

The static constructor is automatically called once when we access a static member of the class.

For example:
using System;

namespace SoloLearn
{
    class Program
    {
        class SomeClass
        {
            public static int X { get; set; }
            public static int Y { get; set; }
            
            static SomeClass()
            {
                X = 10;
                Y = 20;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine(SomeClass.X);
        }
    }
}


// QUESTION:
// Drag and drop from the options below to initialize the static Age property of
// the Person class using a static constructor:
//
//    _____ Person
//    {
//        Public ______ int Age { get; set; }
//        static ________
//        {
//            Age = 0;
//        }
//    }
//
//    Person(), void, class, static, int

// ANSWER:
//    class Person
//    {
//        Public static int Age { get; set; }
//        static Person()
//        {
//            Age = 0;
//        }
//    }
