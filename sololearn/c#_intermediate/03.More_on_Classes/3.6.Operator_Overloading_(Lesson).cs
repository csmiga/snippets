// Operator Overloading

// Most operators in C# can be overloaded, meaning they can be redefined for
// custom actions. For example, you can redefine the action of the plus (+)
// operator in a custom class. Consider the Box class that has Height and Width
// properties:

class Box
{
    public int Height {get; set;}
    public int Width {get; set;}
    public Box(int h, int w)
    {
        Height = h;
        Width = w;
    }
}

static void Main(string[] args)
{
    Box b1 = new Box(14, 3);
    Box b2 = new Box(5, 7);
}

// We would like to add these two Box objects, which would result in a new,
// bigger Box. So, basically, we would like the following code to work:

// Box b3 = b1 + b2;

// The Height and Width properties of object b3 should be equal to the sum of
// the corresponding properties of the b1 and b2 objects.

// Note:
// This is achieved through operator overloading. Tap next to learn more!


// QUESTION:
// Operator overloading means:
//
//     [ defining constructors                 ]
//     [ defining constant members of a class  ]
//     [ defining custom actions for operators ]

// ANSWER:
//     defining custom actions for operators


// Overloaded operators are methods with special names, where the keyword
// operator is followed by the symbol for the operator being defined.
// Similar to any other method, an overloaded operator has a return type and a
// parameter list.

public static Box operator + (Box a, Box b)
{
  int h = a.Height + b.Height;
  int w = a.Width + b.Width;
  Box res = new Box(h, w);
  return res;
}

// The method above defines an overloaded operator + with two Box object
// parameters and returning a new Box object whose Height and Width properties
// equal the sum of its parameter's corresponding properties.

// Additionally, the overloaded operator must be static.

// Putting it all together:
using System;

namespace SoloLearn
{
    class Program
    {
        class Box
        {
            public int Height { get; set; }
            public int Width { get; set; }
            public Box(int h, int w)
            {
                Height = h;
                Width = w;
            }
            public static Box operator + (Box a, Box b)
            {
                int h = a.Height + b.Height;
                int w = a.Width + b.Width;
                Box res = new Box(h, w);
                return res;
            }
        }
        static void Main(string[] args)
        {
            Box b1 = new Box(14, 3);
            Box b2 = new Box(5, 7);
            Box b3 = b1 + b2;
            
            Console.WriteLine(b3.Height);
            Console.WriteLine(b3.Width);
        }
    }
}

// Note:
// All arithmetic and comparison operators can be overloaded. For instance, you
// could define greater than and less than operators for the boxes that would
// compare the Boxes and return a boolean result. Just keep in mind that when
// overloading the greater than operator, the less than operator should also be
// defined.


// PRACTICE (PROBLEM SET): Teamwork Makes the Dream Work
// You and your friend are playing a game as one team. Each player must pass 2
// rounds and gets points for each round passed.
// The program you are given creates two Score objects where each round scores
// are stored (they are passed to a constructor).
// Overload the + operator for the Score class to calculate the team score for
// every round.

// Note:
// Remember to use operator keyword while operator overloading.

using System;

namespace SoloLearn
{
    class Program
    {

        static void Main(string[] args)
        {
            Score tm1 = new Score(2, 3);
            Score tm2 = new Score(4, 2);

            Score finalScores = tm1 + tm2;


            Console.WriteLine("Round 1: " + finalScores.round1Score);
            Console.WriteLine("Round 2: " + finalScores.round2Score);
        }
    }
    class Score
    {
        public int round1Score { get; set; }
        public int round2Score { get; set; }
        public Score(int r1, int r2)
        {
            round1Score = r1;
            round2Score = r2;
        }
        
        // Your code goes here
        public static Score operator + (Score a, Score b)
        {
            int r1 = a.round1Score + b.round1Score;
            int r2 = a.round2Score + b.round2Score;

            Score res = new Score (r1, r2);
            return res;

        }
    }
}


// QUESTION:
// Fill in the blanks to overload the greater than operator for the Box class.
//
//    public static _____ operator _ (Box a,Box b)
//    {
//        if (a.Height * a.Width > b.Height * b.Width) ______ true;
//        else
//            return false;
//    }

// ANSWEER:
//    public static bool operator > (Box a,Box b)
//    {
//        if (a.Height * a.Width > b.Height * b.Width) return true;
//        else
//            return false;
//    }
