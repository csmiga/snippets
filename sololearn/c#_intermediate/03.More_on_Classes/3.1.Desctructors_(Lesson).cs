// Destructors

// As constructors are used when a class is instantiated, destructors are
// automatically invoked when an object is destroyed or deleted.

// Destructors have the following attributes:
// - A class can only have one destructor.
// - Destructors cannot be called. They are invoked automatically.
// - A destructor does not take modifiers or have parameters. 
// - The name of a destructor is exactly the same as the class prefixed with a
//   tilde (~).

// For Example:
class Dog
{
    ~Dog() 
    {
        // code statements
    }
}

// Note:
// Destructors can be very useful for releasing resources before coming out of
// the program. This can include closing files, releasing memory, and so on.


// QUESTION:
// Drag and drop from the options below to declare a destructor for class
// Person.
//
//    class Person
//    {
//        _________
//    }
//
//    Person~  ~Person()  Person()

// ANSWER:
//    class Person
//    {
//        ~Person()
//    }


// Let’s include WriteLine statements in the destructor and constructor of our
// class and see how the program behaves when an object of that class is created
// and when the program ends:

using System;

namespace SoloLearn
{
    class Program
    {
        class Dog
        {
            public Dog()
            {
                Console.WriteLine("Constructor");
            }
            ~Dog()
            {
                Console.WriteLine("Destructor");
            }
        }
        static void Main(string[] args)
        {
            Dog d = new Dog();
        }
    }
}

// When the program runs, it first creates the object, which calls the
// constructor. The object is deleted at the end of the program and the
// destructor is invoked when the program's execution is complete.

// Note:
// This can be useful, for example, if your class is working with storage or
// files. The constructor would initialize and open the files. Then, when the
// program ends, the destructor would close the files.

// PRACTICE (PROBLEM SET): Who Won?
// Complete the FinalRound class to show the corresponding message to the
// winner.
// Five participants have advanced to the final round of a TV trivia show, and
// it is time to announce the winner and end the game.
// The program you are given defines the array of finalists, takes the index of
// the winner as input, and creates the FinalRound object.
// Complete the FinalRound class by writing the needed code in constructor to
// take the array and the winner index as parameters and output the
// corresponding message to show the winner. Then create a destructor to finish
// the game and output "Game Over".

// Sample Input
// 2

// Sample Output
// Winner is Leyla Brown
// Game Over

// Hint
// Use ~ and the class name to create the destructor.

// PRACTICE (SOLUTION): Destructors
// https://www.sololearn.com/compiler-playground/csharp
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] finalists = { "James Van", "John Smith", "Leyla Brown", "Tom Homerton", "Bob Douglas" };

            int winner = Convert.ToInt32(Console.ReadLine());

            // This should show the winner and "Game Over"
            FinalRound finalRound = new FinalRound(finalists, winner);
        }
    }

    class FinalRound
    {
        public FinalRound(string[] finalists, int winner)
        {
            // Complete the constructor
            Console.WriteLine("Winner is " + finalists[winner]);
        }
        
        // Create destructor => "Game Over"
        ~FinalRound()
        {
            Console.WriteLine("Game Over");
        }
    }
}



// QUESTION:
// Destructors are:
//
//    [ invoked only if a constructor is not called ]
//    [ invoked when an object is deleted           ]
//    [ class member variables                      ]

// ANSWER:
//    invoked when an object is deleted
