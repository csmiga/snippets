// The this Keyword

// The "this" keyword is used inside the class and refers to the current
// instance of the class, meaning it refers to the current object.

// One of the common uses of "this" is to distinguish class members from other
// data, such as local or formal parameters of a method, as shown in the
// following example:

// class Person
// {
//     private string name;
//     public Person(string name)
//     {
//         this.name = name;
//     }
// }

// Here, "this.name" represents the member of the class, whereas name represents
// the parameter of the constructor.

// Note:
// Another common use of "this" is for passing the current instance to a method
// as parameter: ShowPersonInfo(this);


// QUESTION:
// Fill in the blanks to print the age variable of the Person class using the
// "this" keyword.
//
//    _____ Person
//    {
//        private int age;
//        public void Print()
//        {
//            Console.Write(_____age);
//        }
//    }

// ANSWER:
//    class Person
//    {
//        private int age;
//        public void Print()
//        {
//            Console.Write(this.age);
//        }
//    }


// The readonly Modifier
// The readonly modifier prevents a member of a class from being modified after
// construction. It means that the field declared as readonly can be modified
// only when you declare it or from within a constructor.

// For example:
class Person
{
    private readonly string name = "John"; 
    public Person(string name)
    {
        this.name = name; 
    }
}

// If we try to modify the name field anywhere else, we will get an error.
// There are three major differences between readonly and const fields.
// First, a constant field must be initialized when it is declared, whereas a
// readonly field can be declared without initialization, as in:

readonly string name;  // OK
const double PI;  // Error

// Second, a readonly field value can be changed in a constructor, but a
// constant value cannot.

// Third, the readonly field can be assigned a value that is a result of a
// calculation, but constants cannot, as in:

readonly double a = Math.Sin(60);  // OK
const double b = Math.Sin(60);  // Error!

// Note:
// The readonly modifier prevents a member of a class from being modified after
// construction.


// PRACTICE (PROBLEM SET): All About This
// Complete the Avg class by creating the constructor to perform necessary
// calculations.

// The "this" Keyword
// The program you are given takes 2 numbers as input and should calculate and
// output their average. But something is wrong.
// Complete the Avg class by creating the constructor, where the 2 parameters
// will be assigned to members of the class.

// Sample Input
// 5.0
// 4.0

// Sample Output
// 4.5

// Hint:
// Inside the constructor, use this keyword with class members for assignation.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1 = Convert.ToDouble(Console.ReadLine());
            double num2 = Convert.ToDouble(Console.ReadLine());

            Avg avg = new Avg(num1, num2);

            Console.WriteLine(avg.GetAvg());
        }
    }

    class Avg
    {
        double num1;
        double num2;
        
        // Create the constructor
        public Avg(double num1, double num2)
        {
            this.num1 = num1;
            this.num2 = num2;
        }

        public double GetAvg()
        {
            return (num1 + num2) / 2;
        }
    }
}


// QUESTION
// Which statements are true?
//
//    ☐ constants should be assigned a value when declared
//    ☐ constants can be initilialized by the constructor
//    ☐ readonly fields can be initialized by the constructor
//    ☐ readonly fields can be initialized only when declared

// ANSWER:
//    ✅ constants should be assigned a value when declared
//    ✅ readonly fields can be initialized by the constructor
