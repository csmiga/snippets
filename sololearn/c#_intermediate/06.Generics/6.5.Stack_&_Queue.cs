// Stack<T>

// A stack is a Last In, First Out (LIFO) collection of elements where the last
// element that goes into the stack will be the first element that comes out.

// Inserting an element onto a stack is called pushing. Deleting an element from
// a stack is called popping. Pushing and popping can be performed only at the
// top of the stack.

// Note:
// Stacks can be used to create undo-redo functionalities, parsing expressions
// (infix to postfix/prefix conversion), and much more.

// The C# generic collection Stack<T> class requires all elements to be of the same type T.

// Stack<T> properties include:
// Count:     Returns the number of elements in the stack.

// Stack<T> methods include:
// Peek():    Returns the element at the top of the stack without removing it.
// Pop():     Returns the element at the top of the stack and removes it from the stack.
// Push(T t): Inserts an element t at the top of the stack.

// Now let's try Stack<T>:
using System;
using System.Collections.Generic;

namespace SoloLearn
{
	class Program
	{
		static void Main(string[] args)
		{
		    Stack<int> s = new Stack<int>();
            
		    s.Push(59);
		    s.Push(72);
		    s.Push(65);

		    Console.Write("Stack: ");
		    foreach (int i in s)
		    Console.Write(i + " ");  // 65  72  59
		    Console.Write("\nCount: " + s.Count);  // 3
            
		    Console.Write("\nTop: " + s.Peek());  // 65
		    Console.Write("\nPop: " + s.Pop());  // 65
            
		    Console.Write("\nStack: ");
		    foreach (int i in s)
		        Console.Write(i + " ");  // 72  59
		    Console.Write("\nCount: " + s.Count);  // 2
		}
	}
}

// Here are additional Stack<T> methods:
// 
// Clear():       Removes all the elements from the stack.
// Contains(T t): Returns true when the element t is present in the stack.
// ToArray():     Copies the stack into a new array.

// Note:
// Run the code and see how it works!


// QUESTION:
// Fill in the blanks to create a stack of integers and push 11, 42, and 15 to
// it. Print "yes" if the stack contains 42.
//
//    Stack<int> st = new Stack<___>();
//    __.Push(11);
//    st.Push(42);
//    st.Push(15);
//    __(st.Contains(42))
//    {
//        Console.WriteLine("yes");
//    }

// ANSWER:
//    Stack<int> st = new Stack<int>();
//    st.Push(11);
//    st.Push(42);
//    st.Push(15);
//    if(st.Contains(42))
//    {
//        Console.WriteLine("yes");
//    }

// Want to know why?
// Your answer is correct because it creates a stack of integers, pushes three
// values to it, and then checks if it contains the value 42. The Contains
// method returns true if the stack contains the specified value, and in this
// case, it will print "yes" since 42 is in the stack.


// Queue<T>
// A queue is a First In, First Out (FIFO) collection of elements where the
// first element that goes into a queue is also the first element that comes
// out.

// Inserting an element into a queue is referred to as Enqueue. Deleting an
// element from a queue is referred to as Dequeue.

// Note:
// Queues are used whenever we need to manage objects in order starting with the
// first one in.
// Scenarios include printing documents on a printer, call center systems
// answering people on hold people, and so on.

// The C# generic collection Queue<T> class requires that all elements be of the
// same type T.

// Queue<T> properties include:
// Count:        Gets the number of elements in the queue.

// And methods include:
// Dequeue():    Returns the object at the beginning of the queue and also removes
// it.

// Enqueue(T t): Adds the object t to the end of the queue.

// Now let's try Queue<T>:
using System;
using System.Collections.Generic;

namespace SoloLearn
{
	class Program
	{
		static void Main(string[] args)
		{
            Queue<int> q = new Queue<int>();
            
            q.Enqueue(5);
            q.Enqueue(10);
            q.Enqueue(15);
            Console.Write("Queue: ");
            foreach (int i in q)
                Console.Write(i + " ");  // 5  10  15
            Console.Write("\nCount: " + q.Count);  // 3
            
            Console.Write("\nDequeue: " + q.Dequeue()); // 5
            
            Console.Write("\nQueue: ");
            foreach (int i in q)
                Console.Write(i + " ");  // 10  15
            Console.Write("\nCount: " + q.Count);  // 2
		}
	}
}

// Here are additional Queue<T> methods:
// Clear():       Removes all objects from the queue.
// Contains(T t): Returns true when the element t is present in the queue.
// Peek():        Returns the object at the beginning of the queue without removing it.
// ToArray():     Copies the queue into a new array.

// Note:
// Run the code and see how it works!

// PRACTICE (PROBLEM SET): Queue It Up!
// Write a program that will take 3 numbers as input and store them in a defined
// queue.
// Also, add code to output the sorted sequence of elements in the queue,
// separated by a space.

// Sample Input
// 6
// 3
// 14

// Sample Output
// Queue: 6 3 14 
// Sorted: 3 6 14 

// Note:
// To copy the queue into a new array, use the ToArray() method of the queue:
// int[] arr = queue.ToArray();.

/ Then, recall the Array.Sort() method.

using System;
using System.Collections.Generic;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> q = new Queue<int>();

            while (q.Count<3)
            {
                int num = Convert.ToInt32(Console.ReadLine());
                // Your code goes here
                q.Enqueue(num);
            }

            Console.Write("Queue: ");
            
            foreach (int i in q)
                Console.Write(i + " ");
            Console.WriteLine();
            
            Console.Write("Sorted: ");
            
            // Your code goes here
            List<int> sorted = new List<int>();
            
            foreach (int i in q)
            {
                sorted.Add(i);
            }
            sorted.Sort();
            
            foreach (int i in sorted)
                Console.Write(i + " ");
        }
    }
}


// QUESTION:
// What is the output of this code?'
//
//    Queue<string> q = new Queue<string>();
//    q.Enqueue("A");
//    q.Enqueue("B");
//    q.Enqueue("C");
//    foreach (string s in q)
//        Console.Write(s + " ");
//
//    [ C B A ]
//    [ A B C ]
//    [ B C A ]
//    [ s ]

// ANSWER:
//    A B C

// Want to know why?
// Your answer is correct because the code creates a queue of strings and
// enqueues three strings "A", "B", and "C". Then, it iterates over the queue
// using a foreach loop and prints the contents of each item in the queue
// separated by a space, resulting in the output "A B C".
