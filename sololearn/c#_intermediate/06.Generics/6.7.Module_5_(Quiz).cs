// MODULE 6 QUIZ

// QUESTION:
// What is the output of this code?
//
//    List<int> li = new List<int>();
//    li.Add(59);
//    li.Add(72);
//    li.Add(95);
//    li.RemoveAt(1);
//    for (int x = 0; x < li.Count; x++)
//        Console.Write(li[x] + " ");
//
//    [ 59 72 ]
//    [ 72 95 ]
//    [ 59 72 95 ]
//    [ 59 95 ]

// ANSWER:
//    59 95

// Want to know why?
// Your answer is correct because the code creates a `List<int>` and adds 3
// elements to it, then removes the element at index 1 with `RemoveAt(1)`. This
// removes the element with value 72, leaving the list with elements 59 and 95.
// Finally, the `for` loop prints out the remaining elements, separated by
// spaces, resulting in the output "59 95".


// QUESTION:
// Drag and drop from the options below to define a generic method that swaps
// the values of its arguments and returns the value of the first argument.
//
//    static _____ Swap _____ (ref T a, ref T b)
//    {
//        T temp = a;
//        a = b;
//        b = _____;
//        _____ a;
//    }
//
//    [ temp ]  [ b ]  [ retuen ]  [ a ]  [ T ]  [ <T> ]

// ANSWER:
//    static T Swap<T> (ref T a, ref T b)
//    {
//        T temp = a;
//        a = b;
//        b = temp;
//        return a;
//    }

// Want to know why?
// The answer is correct because it defines a generic method 'Swap' that takes
// two arguments of the same type 'T' by reference, swaps their values, and
// returns the value of the first argument.


// QUESTION:
// Reorder the code to declare a Queue of integers, add values 1 through 5 and
// print the contents of the Queue.
//
//    [ foreach (int val in q) ]
//    [ Queue<int> q = new Queue<int>(); ]
//    [ for (int x = 1; x <= 5; x++) ]
//    [ Console.Write(val + " "); ]
//    [ q.Enqueue(x); ]

// ANSWER:
//    Queue<int> q = new Queue<int>();
//    for (int x = 1; x <= 5; x++)
//    q.Enqueue(x);
//    foreach (int val in q)
//    Console.Write(val + " ");

// Want to know why?
// Your answer is correct because it creates a new Queue of integers, adds
// values 1 through 5 to the Queue using the Enqueue method, and then prints the
// contents of the Queue using a foreach loop. This meets the requirements of
// the question.


// QUESTION:
// Drag and drop from the options below to define a generic class, which has a
// member method that returns the value of x.
//
//    _____ Temp___
//    {
//        T x;
//        public _ Func()
//        {
//            ______ x;
//        }
//    }
//
//    [ T ]  [ x ]  [ static ]  [ <x> ]  [ class ]  [ return ]  [ <T> ]

// ANSWER:
//    class Temp<T>
//    {
//        T x;
//        public T Func()
//        {
//            return x;
//        }
//    }

// Want to know why?
// Your answer is correct because you defined a generic class "Temp" with a type
// parameter "T" and a member variable "x" of type "T". You also defined a
// method "Func" that returns the value of "x" of type "T". This allows the
// class to work with any type of data, making it flexible and reusable.


// QUESTION:
// What is the output of this code?
//
//    List<int> a = new List<int>();
//    a.Add(5);
//    a.Add(2);
//    a.Add(8);
//    a.Reverse();
//    Console.Write(a[1]);

// ANSWER:
//    2


// QUESTION:
// Drag and drop from the options below to create a List that will contain
// objects of type Student.
//
//    List<_______> L = ___ ____ <Student>();
//
//    [ <List> ]  [ Student ]  [ public ]  [ List ]  [ Main ]  [ new ]

// ANSWER:
//    List<Student> L = new List <Student>(); 

// Want to know why?
// The answer "List<Student> L = new List<Student>();" is correct because it
// creates a new List object that will contain objects of type Student. The
// generic parameter <Student> specifies the type of objects that will be stored
// in the list.


// QUESTION:
// What is the output of this code?
//
//    BitArray ba1 = new BitArray(4);
//    BitArray ba2 = new BitArray(4);            
//    ba1.SetAll(true);
//    ba2.SetAll(false);            
//    ba1.Set(2, false);
//    ba2.Set(3, true);
//    Console.Write(ba1.And(ba2).Get(2));
//
//    [ False ]  [ True ]

// ANSWER:
//    Fale

// Want to know why?
// Your answer is correct because the `And` method of `BitArray` performs a
// bitwise AND operation between two `BitArray` objects and returns a new
// `BitArray` object. In this case, `ba1.And(ba2)` will result in a new
// `BitArray` object where only the third bit (index 2) is set to false, since
// it is false in `ba1` and true.


// QUESTION:
// What is the output of this code?
//
//    Stack<int> s = new Stack<int>();
//    s.Push(4);
//    s.Push(5);
//    s.Push(4);
//    s.Pop();
//    Console.Write(s.Peek());

// ANSWER:
//    5


// QUESTION:
// Fill in the blanks to declare a dictionary to map names and ages and John's,
// Ann's and Peter's ages. Print John's age.
//
//    Dictionary<______, int> d = ___ Dictionary<string, ___>();
//    _.Add("John", 24);
//    d.Add("Ann", 18);
//    d.Add("Peter", 27);
//    string name = "John";
//    Console.Write(d_ name _);

// ANSWER:
//    Dictionary<string, int> d = new Dictionary< string, int >();
//    d .Add("John", 24);
//    d.Add("Ann", 18);
//    d.Add("Peter", 27);
//    string name = "John";
//    Console.Write(d[name]);

// Want to know why?
// The answer is correct because it correctly declares a dictionary with string
// keys and integer values, adds the ages of John, Ann, and Peter to it, and
// then retrieves and prints John's age by accessing the value associated with
// the "John" key.
