// Generic Classes

// Generic types can also be used with classes.
// The most common use for generic classes is with collections of items, where
// operations such as adding and removing items from the collection are
// performed in basically the same way regardless of the type of data being
// stored. One type of collection is called a stack. Items are "pushed", or
// added to the collection, and "popped", or removed from the collection. A
// stack is sometimes called a Last In First Out (LIFO) data structure.

// For example: 
class Stack<T>
{
    int index=0;
    T[] innerArray = new T[100];
    
    public void Push(T item)
    {
        innerArray[index++] = item; 
    }
    
    public T Pop()
    {
        return innerArray[--index]; 
    }
    
    public T Get(int k) { return innerArray[k]; }
}

// The generic class stores elements in an array. As you can see, the generic
// type T is used as the type of the array, the parameter type for the Push
// method, and the return type for the Pop and Get methods.

// Now we can create objects of our generic class:
Stack<int> intStack = new Stack<int>();
Stack<string> strStack = new Stack<string>();
Stack<Person> PersonStack = new Stack<Person>();

// We can also use the generic class with custom types, such as the custom
// defined Person type.

// Note:
// In a generic class we do not need to define the generic type for its methods,
// because the generic type is already defined on the class level.

// QUESTION:
// Drag and drop from the options below to define a generic Store class with a
// generic data member x, which is initialized in the constructor.
//
//    class Store ___
//    {
//        _ x;
//        ______ Store(T val)
//        {
//            x = val;
//        }
//    }

//    [ template ]  [ <T> ]  [ ~Store ]  [public ]  [ T ]

// ANSWER:
//    class Store <T>
//    {
//        T x;
//        public Store(T val)
//        {
//            x = val;
//        }
//    }

// Want to know why?
// Your answer is correct because it defines a generic class, `Store`, with a
// type parameter `T`, and a generic data member `x` of type `T`. The
// constructor initializes `x` with a value of type `T` passed as an argument.
// This allows instances of `Store` to hold any type of data.


// Generic class methods are called the same as for any other object:
using System;

namespace SoloLearn
{
    class Program
    {
        class Stack<T>
        {
            int index=0;
            T[] innerArray = new T[100];
            public void Push(T item)
            {
                innerArray[index++] = item; 
            }
            
            public T Pop()
            {
                return innerArray[--index]; 
            }
            public T Get(int k) { return innerArray[k]; }
        }
        
        static void Main(string[] args)
        {
            Stack<int> intStack = new Stack<int>();
            intStack.Push(3);
            intStack.Push(6);
            intStack.Push(7);
            
            Console.WriteLine(intStack.Get(1));
        }
    }
}

// Note:
// Run the code and see how it works!

// PRACTICE (PROBLEM SET): Generic All Around
// The class Elems creates a 3-sized array of integers, defines Add() and Show()
// methods to store the elements into the array, and shows them separated by a
// space.
// Modify the class to make it generic to execute the same actions with string
// type, given in the Main function.

// Note:
// You need to replace the int type by the generic <T> type.
// Each output should end with a space (including the last one).

 namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {

            Elems<string> elems1 = new Elems<string>();
            elems1.Add("John", "Tamara", "David");
            elems1.Show();

            Console.WriteLine();

            Elems<int> elems2 = new Elems<int>();
            elems2.Add(5, 14, 13);
            elems2.Show();

        }
    }
    
    // Make this class generic
    class Elems<T>
    {
        public T[] elements = new T[3];

        public void Add(T elem1, T elem2, T elem3)
        {
            elements[0] = elem1;
            elements[1] = elem2;
            elements[2] = elem3;
        }

        public void Show()
        {
            foreach (T item in elements)
            {
                Console.Write(item + " ");
            }
        }
    }
}


// QUESTION:
// Can a custom class be used as the type for the generic class?
//
//    [ Only if it is abstract ]
//    [ Yes ]
//    [ No ]

// ANSWER:
//    Yes

// Want to know why?
// Your answer is correct because in C#, we can use custom classes as the type
// for a generic class. This allows us to create classes that can work with
// different types of data, making our code more flexible and reusable.
