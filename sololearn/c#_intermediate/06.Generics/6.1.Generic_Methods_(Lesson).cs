// Generic Methods

// Generics allow the reuse of code across different types.
// For example, let's declare a method that swaps the values of its two
// parameters: 

static void Swap(ref int a, ref int b)
{
    int temp = a;
    a = b;
    b = temp;
}

// Our Swap method will work only for integer parameters. If we want to use it
// for other types, for example, doubles or strings, we have to overload it for
// all the types we want to use it with. Besides a lot of code repetition, it
// becomes harder to manage the code because changes in one method mean changes
// to all of the overloaded methods.
// Generics provide a flexible mechanism to define a generic type.

static void Swap<T>(ref T a, ref T b)
{
    T temp = a;
    a = b;
    b = temp;
}

// In the code above, T is the name of our generic type. We can name it
// anything we want, but T is a commonly used name. Our Swap method now takes
// two parameters of type T. We also use the T type for our temp variable that
// is used to swap the values.

// Note:
// Note the brackets in the syntax <T>, which are used to define a generic type.


// QUESTION:
// Fill in the blanks to declare a generic method that displays its argument
// value.
//
//    static void Print _T_ (T x)
//    {
//        Console.WriteLine(_);
//    }

// ANSWER:
//    static void Print <T> (T x)
//    {
//        Console.WriteLine(x);
//    }

// Want to know why?
// Your answer is correct because it declares a generic method named "Print"
// that takes a type parameter T and a parameter x of type T. The method then
// prints out the value of x to the console using the Console.WriteLine method.
// This allows the method to work with any type that can be converted to a
// string, making it very flexible and reusable.


// Generic Methods
// Now, we can use our Swap method with different types, as in: 
using System;

namespace SoloLearn
{
    class Program
    {
        static void Swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }
        
        static void Main(string[] args)
        {
            int a = 4, b = 9;
            Swap<int>(ref a, ref b);
            Console.WriteLine(a+" "+b);
            
            string x = "Hello";
            string y = "World";
            Swap<string>(ref x, ref y);
            Console.WriteLine(x+" "+y);
        }
    }
}

// When calling a generic method, we need to specify the type it will work with
// by using brackets. So, when Swap<int> is called, the T type is replaced by
// int. For Swap<string>, T is replaced by string.
// If you omit specifying the type when calling a generic method, the compiler
// will use the type based on the arguments passed to the method.

// Note:
// Multiple generic parameters can be used with a single method.
// For example: Func<T, U> takes two different generic types.

// PRACTICE (PROBLEM SET): Print To Printer
// You are writing a program that can output the value of a variable of any
// type. It takes a string, an integer, and a double value as input and then it
// should output them.
// Create a generic method Print for a Printer class to execute the given calls
// correctly.

// Sample Input
// Hello
// 14
// 7.6

// Sample Output
// Showing Hello
// Showing 14
// Showing 7.6

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            int intNum = Convert.ToInt32(Console.ReadLine());
            double doubNum = Convert.ToDouble(Console.ReadLine());

            Printer.Print<string>(ref text);
            Printer.Print<int>(ref intNum);
            Printer.Print<double>(ref doubNum);
        }
    }
    
    class Printer
    {
        // Your code goes here
        public static void Print<T>(ref T a)
        {
            Console.WriteLine($"Showing {a}");
        }
    }
}


// QUESTION:
// Fill in the blanks to use the generic method Func for the x and y variables:
//
//    static void Func<T_ U_ (T x_ U y)
//    {
//        Console.WriteLine(x+" "+y);
//    }
//
//    static void Main(string[] args)
//    {
//        double x = 7.42;
//        string y = "test";
//        Func(x, _ );
//    }

// ANSWER:
//    static void Func<T, U> (T x, U y)
//    {
//        Console.WriteLine(x+" "+y);
//    }
//
//    static void Main(string[] args)
//    {
//        double x = 7.42;
//        string y = "test";
//        Func(x, y );
//    }

// Want to know why?
// Your answer is correct because you defined a generic method `Func` that takes
// two arguments of different types `T` and `U`, and used it in the `Main`
// method where `x` is of type `double` and `y` is of type `string`. The `Func`
// method then prints the values of `x` and `y` using `Console.WriteLine`.
