-- Inserting Data:
-- SQL tables store data in rows, one row after another. The INSERT INTO
-- statement is used to add new rows of data to a table in the database.
-- The SQL INSERT INTO syntax is as follows:

INSERT INTO
  table_name
VALUES
(
  value1,
  value2,
  value3,
  ...
);

-- Make sure the order of the values is in the same order as the columns in the
-- table.

-- Consider the following Employees table:

--   ID    FirstName    LastName    Age
--   1     Emily        Adams       34
--   2     Chloe        Anderson    27
--   3     Daniel       Harris      30
--   4     James        Roberts     31
--   5     John         Smith       35
--   6     Andrew       Thomas      45
--   7     David        Williams    23

-- Use the following SQL statement to insert a new row:

-- Try It Yourself:
INSERT INTO
  Employees 
VALUES
  (
    8,
    'Anthony',
    'Young',
    35
);
SELECT
  *
FROM
  Employees;

-- Result:
--   ID    FirstName    LastName    Age
--   1     Emily        Adams       34
--   2     Chloe        Anderson    27
--   3     Daniel       Harris      30
--   4     James        Roberts     31
--   5     John         Smith       35
--   6     Andrew       Thomas      45
--   7     David        Williams    23
--   8     Anthony      Young       35

-- Note:
-- When inserting records into a table using the SQL INSERT statement, you
-- must provide a value for every column that does not have a default value, or
-- does not support NULL.


-- QUESTION:
-- Drag and drop from the options below to insert the data into the "students"
-- table.
--
--   ______ ____
--     students
--   ______
--   (
--     'John Smith',
--     'MIT'
--   );
--
-- IN, BY, INSERT, TEXT, INTO, VALUES

-- ANSWER:
--   INSERT INTO
--     students
--   VALUES
--   (
--     'John Smith',
--     'MIT'
--   );


-- Inserting Data:
-- Alternatively, you can specify the table's column names in the INSERT INTO
-- statement:

INSERT INTO
  table_name
  (
    column1,
    column2,
    column3,
    ...,
    columnN
  )  
VALUES
  (
    value1,
    value2,
    value3,
    ...
    valueN
  );

-- column1, column2, ..., columnN are the names of the columns that you want to
-- insert data into.

-- Try It Yourself:
INSERT INTO
  Employees
  (
    ID,
    FirstName,
    LastName,
    Age
  )
VALUES
  (
    8,
    'Anthony',
    'Young',
    35
  );
SELECT
  *
FROM
  Employees;

-- This will insert the data into the corresponding columns:

--   ID    FirstName    LastName    Age
--   1     Emily        Adams       34
--   2     Chloe        Anderson    27
--   3     Daniel       Harris      30
--   4     James        Roberts     31
--   5     John         Smith       35
--   6     Andrew       Thomas      45
--   7     David        Williams    23
--   8     Anthony      Young       35

-- QUESTION:
-- Rearrange the code to insert the data into the ''student'' table, using
-- actual column names.

--   = (name, university)
--   = INSERT INTO students
--   = ('Peter Parker', 'Stanford');
--   = VALUES

-- ANSWER:
--   INSERT INTO
--     students
--    (
--      name,
--      university
--    )
--   VALUES
--   (
--     'Peter Parker',
--     'Stanford'
--   );


-- Inserting Data:
-- It is also possible to insert data into specific columns only.

-- Try It Yourself:
INSERT INTO Employees (ID, FirstName, LastName) 
VALUES (9, 'Samuel', 'Clark');
SELECT * from Employees;

--   ID    FirstName    LastName    Age
--   1     Emily        Adams       34
--   2     Chloe        Anderson    27
--   3     Daniel       Harris      30
--   4     James        Roberts     31
--   5     John         Smith       35
--   6     Andrew       Thomas      45
--   7     David        Williams    23
--   8     Anthony      Young       35
--   9     Samuel       Clark        0

-- Note:
-- The Age column for that row automatically became 0, as that is its
-- default value.


-- QUESTION:
-- When inserting data into a table:

-- ANSWER:
--   We don't have to insert values for all columns in the table
