-- Custom Names:
-- Custom names can be used for tables as well. You can shorten the join
-- statements by giving the tables "nicknames":

SELECT
  ct.ID,
  ct.Name,
  ord.Name,
  ord.Amount
FROM
  customers AS ct,
  orders AS ord
WHERE
  ct.ID = ord.Customer_ID
ORDER BY
  ct.ID;

-- Note:
-- As you can see, we shortened the table names as we used them in our
-- query.


-- QUESTION:
-- Fill in the blanks to select item names and names of customers who bought
-- the items. Use custom names to shorten the statement.

--   SELECT
--     ct.name,
--     __.name
--   FROM
--     customers __ ct,
--     items AS it
--   WHERE
--     it.seller_id = __.id;

-- ANSWER:
--   SELECT
--     ct.name,
--     it.name
--   FROM
--     customers AS ct,
--     items AS it
--   WHERE
--     it.seller_id = ct.id;


-- Types of Join:
-- The following are the types of JOIN that can be used in MySQL:
--   - INNER JOIN
--   - LEFT JOIN
--   - RIGHT JOIN

-- INNER JOIN is equivalent to JOIN. It returns rows when there is a match
-- between the tables.

-- Syntax:
SELECT
  column_name(s)
FROM
  table1 INNER
JOIN table2 
  ON table1.column_name=table2.column_name;

-- Note: Note the ON keyword for specifying the inner join condition.

-- The image below demonstrates how INNER JOIN works:

--   ( table1 ( ) table2 )

-- Note:
-- Only the records matching the join condition are returned.


-- QUESTION:
-- Rearrange the query to select the names of students and the names of the
-- universities where those students study.

--   = FROM students, university
--   = SELECT students.name, universities.name
--   = students.universities_id.universities.id
--   = WHERE

-- ANSWER:
--   SELECT students.name, universities.name
--   FROM students, university
--   WHERE students.universities_id.universities.id


-- LEFT JOIN:
-- The LEFT JOIN returns all rows from the left table, even if there are no
-- matches in the right table.
-- This means that if there are no matches for the ON clause in the table on the
-- right, the join will still return the rows from the first table in the
-- result.

-- The basic syntax of LEFT JOIN is as follows:
SELECT
  table1.column1,
  table2.column2...
FROM
  table1
LEFT OUTER JOIN table2
  ON table1.column_name = table2.column_name;

-- Note:
-- The OUTER keyword is optional, and can be omitted.


-- QUESTION:
-- Drag and drop from the options below to outer join the table "items" with
-- "customers".

--   SELECT customers.name, items.name
--   FROM customers
--   LEFT _____ JOIN items
--   _____ customers.id = seller_id

--   FROM, ON, OUTER, RIGHT, AS

-- ANSWER:
--   SELECT
--     customers.name,
--     items.name
--   FROM
--     customers
--   LEFT OUTER JOIN items
--   ON customers.id = seller_id


-- RIGHT JOIN:
-- The RIGHT JOIN returns all rows from the right table, even if there are no
-- matches in the left table.

--   ( table1 ( table2 )

-- The basic syntax of RIGHT JOIN is as follows:

SELECT
  table1.column1,
  table2.column2...
FROM
  table1
RIGHT OUTER JOIN table2
  ON table1.column_name = table2.column_name;

-- Note:
-- Again, the OUTER keyword is optional, and can be omitted.

-- Try It Yourself:
SELECT
  customers.Name,
  items.Name
FROM
  customers
RIGHT JOIN items
  ON customers.ID=items.Seller_id;

-- Result:
--  name      name
--  John      Book
--  John      Box
--  David     Toy
--  David     Flowers
--  Chloe     T-Shirt
--  Emily     Notebook
--  Andrew    Perfume

-- The RIGHT JOIN returns all the rows from the right table (items), even if
-- there are no matches in the left table (customers).

-- Note:
-- There are other types of joins in the SQL language, but they are not
-- supported by MySQL.


-- QUESTION:
-- Rearrange the code to select student names and all university names (use
-- right join to show all university names).

--   = SELECT students.names, universities.names
--   = RIGHT OUTER JOIN universities
--   = ON students.university_id = universities.id;
--   = FROM students

-- ANSWER:
--   SELECT
--     students.names, universities.names
--   FROM
--     students
--   RIGHT OUTER JOIN universities
--     ON students.university_id = universities.id;
