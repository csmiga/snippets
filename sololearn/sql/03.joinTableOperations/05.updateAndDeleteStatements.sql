-- Updating Data:
-- The UPDATE statement allows us to alter data in the table.
-- The basic syntax of an UPDATE query with a WHERE clause is as follows:

UPDATE
  table_name
SET
  column1=value1,
  column2=value2,
  ...
WHERE
  condition;

-- You specify the column and its new value in a comma-separated list after the
-- SET keyword.

-- Note:
-- If you omit the WHERE clause, all records in the table will be updated!


-- QUESTION:
-- Type in the command used to update a table.

-- ANSWER:
--   update


-- Updating Data:
-- Consider the following table called "Employees":

--   ID    FirstName    LastName    Salary
--   1     John         Smith       2000
--   2     David        Williams    1500
--   3     Chloe        Anderson    3000
--   4     Emily        Adams       4500

-- To update John's salary, we can use the following query: 

-- Try It Yourself:
UPDATE
  Employees 
SET
  Salary=5000
WHERE
  ID=1;
SELECT
  *
FROM
  Employees;

-- Result:
--   ID    FirstName    LastName    Salary
--   1     John         Smith       5000
--   2     David        Williams    1500
--   3     Chloe        Anderson    3000
--   4     Emily        Adams       4500


-- QUESTION:
-- Drag and drop from the options below to update the "students" table by
-- changing the university's value to "Stanford" if the student's name is
-- "John".
--
--   JOIN, ON, SELECT, SET, UPDATE, WHERE
--
--   ______ students
--   ___ university = 'Stanford'
--   _____ name = 'John';

-- ANSWER:
--   UPDATE
--     students
--   SET
--     university = 'Stanford'
--   WHERE
--     name = 'John';


-- Updating Multiple Columns:
-- It is also possible to UPDATE multiple columns at the same time by
-- comma-separating them:

-- Try It Yourself:
UPDATE
  Employees 
SET
  Salary=5000,
  FirstName='Robert'
WHERE
  ID=1;
SELECT
  *
FROM
  Employees;

-- Result:
--   ID    FirstName    LastName    Salary
--   1     Robert       Smith       5000
--   2     David        Williams    1500
--   3     Chloe        Anderson    3000
--   4     Emily        Adams       4500

-- Note:
-- You can specify the column order any way you like in the SET clause.


-- QUESTION:
-- Rearrange the code to update the "name" and "age" columns of the "students"
-- table.

--   = name = 'Peter',
--   = UPDATE students SET
--   = WHERE id = 147;
--   = age = 32

-- ANSWER:
--   UPDATE students SET
--   name = 'Peter',
--   age = 32
--   WHERE id = 147;


-- Deleting Data:
-- The DELETE statement is used to remove data from your table. DELETE queries
-- work much like UPDATE queries.

DELETE FROM
  table_name
WHERE
  condition;

-- For example, you can delete a specific employee from the table:

-- Try It Yourself:
DELETE FROM
  Employees
WHERE
  ID=1;
SELECT
  *
FROM
  Employees;

-- Result:
--   id    firstname    lastname    salary
--   2     David        Williams    1500
--   3     Chloe        Anderson    3000
--   4     Emily        Adams       4500

-- Note:
-- If you omit the WHERE clause, all records in the table will be deleted!
-- The DELETE statement removes the data from the table permanently.


-- QUESTION:
-- Drag and drop from the options below to delete a row from "people" in which
-- the ids falls in the range of 5 to 10.
--
--   ______ FROM
--     people
--   _____
--     id>5 ___ id<10;
--
--   FROM, WHERE, SELECT, AND, SELECT, OR

-- ANSWER:
--   DELETE FROM
--     people
--   WHERE
--     id > 5 AND id<10;
