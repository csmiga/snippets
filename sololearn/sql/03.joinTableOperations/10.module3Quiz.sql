-- QUESTION:
-- Rearrange to select all student names and university names (use left join
-- to show all student names).

--   = FROM students
--   = ON students.university_id = universities.id;
--   = LEFT OUTER JOIN universities
--   = SELECT students.names, universities.name

-- ANSWER:
--   SELECT
--     students.names,
--     universities.name
--   FROM
--     students
--   LEFT OUTER JOIN universities
--     ON students.university_id = universities.id;


-- QUESTION:
-- Drag and drop from the options below to insert a data item into the
-- "people" table.

--   ______ ____ people
--   ______ ('John Smith', '1', 22);

--   INSERT, ON, INTO, TABLE, BY, VALUES

-- ANSWER:
--   INSERT INTO people
--   VALUES ('John Smith', '1', 22);


-- QUESTION:
-- Drag and drop from the options below to update the "people" table.
--
--   ______
--     people
--   ___
--     name = 'Jordan'
--   WHERE
--     id = 147;
--
--   INSERT, INTO, SET, UPDATE, CREATE

-- ANSWER:
--   UPDATE
--     people
--   SET
--     name = 'Jordan'
--   WHERE
--     id = 147;


-- QUESTION:
-- Fill in the blanks to create a table with two columns: "id" as a primary
-- key integer, and "name" of type varchar.
--
--   CREATE TABLE
--   (
--     id ___,
--     name _______(30),
--     PRIMARY KEY(__)
--   );

-- ANSWER:
--   CREATE TABLE
--   (
--     id int,
--     name varchar(30),
--     PRIMARY KEY(id)
--   );


-- QUESTION:
-- Rearrange to remove the column "age" from the "people" table.
--
--   = people
--   = DROP COLUMN
--   = ALTER TABLE
--   = age

-- ANSWER:
--   = ALTER TABLE
--   = people
--   = DROP COLUMN
--   = age


-- QUESTION:
-- Which choice is the correct command for changing the name of a table?

-- ANSWER:
--   RENAME


-- QUESTION:
-- Drag and drop from the options below to create a view named "most_abs"
-- for the students with the greatest number of absences.
--
--   ______ ____ most_abs __
--   SELECT
--     id,
--     name,
--     absences
--   FROM
--     students
--   ORDER BY
--     absences DESC
--   LIMIT
--     10;
--
--   CREATE, TABLE, SELECT, INTO, AS, VIEW

-- ANSWER:
--   CREATE VIEW most_abs AS 
--   SELECT 
--     id, 
--     name, 
--     absences 
--   FROM 
--     students 
--   ORDER BY 
--     absences DESC 
--   LIMIT
--     10;



-- QUESTION:
-- Drag and drop from the options below to delete the table "students" from
-- the database.
--
--   ____ _____ ________;
--
--   students, DROP, TABLE, ALTER, DATABASE, DELETE

-- ANSWER:
--   DROP TABLE students;


-- QUESTION:
-- Drag and drop from the options below to remove the column "temp" from the
-- table "students".
--
--   _____ TABLE
--     students
--   _____
--     _____ temp;
--
--   DELETE, ALTER, REPLACE, DROP, COLUMN, REMOVE

-- ANSWER:
--   ALTER TABLE 
--     students 
--   DROP 
--     COLUMN temp;


-- QUESTION:
-- Zoo:
-- You manage a zoo. Each animal in the zoo comes from a different country. Here
-- are the tables you have:
--
-- Animals:
--   name      type        country_id
--   Candy     Elephant    3
--   Pop       Horse       1
--   Vova      Bear        2
--   Merlin    Lion        1
--   Bert      Tiger       3

-- Countries:
--   id    country
--   1     USA
--   2     Russia
--   3     India

-- 1) A new animal has come in, with the following details:
--    name - "Slim", type - "Giraffe", country_id - 1
--    Add him to the Animals table.

-- 2) You want to make a complete list of the animals for the zoo’s visitors.
--    Write a query to output a new table with each animal's name, type and
--    country fields, sorted by countries.

-- Note: Recall INSERT and INNER JOIN keywords.

-- ANSWER:
--   INSERT INTO Animals (name, type, country_id) 
--   VALUES 
--   (
--     'Slim',
--     'Giraffe',
--     1
--   );
--   SELECT 
--     animals.name, 
--     animals.type, 
--     countries.country 
--   FROM 
--     animals, 
--     countries 
--   WHERE 
--     animals.country_id = countries.id 
--   ORDER BY 
--     animals.country_id DESC
