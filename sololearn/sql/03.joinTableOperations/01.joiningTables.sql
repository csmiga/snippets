-- Joining Tables:
-- All of the queries shown up until now have selected from just one table at a
-- time.

-- One of the most beneficial features of SQL is the ability to combine data
-- from two or more tables.

-- In the two tables that follow, the table named customers stores information
-- about customers:

--   ID  FirstName    LastName    Age
--   1   John         Smith       35
--   2   David        Williams    23
--   3   Chloe        Anderson    27
--   4   Emily        Adams       34
--   5   James        Roberts     31

-- The orders table stores information about individual orders with their
-- corresponding amount:

--   ID    Name       Customer_ID    Amount
--   1    Book       3              5000
--   2    Box        5              3000
--   3    Toy        2              4500
--   4    Flowers    4              1800
--   5    Cake       1              6700

-- Note:
-- In SQL, "joining tables" means combining data from two or more tables.
-- A table join creates a temporary table showing the data from the joined
-- tables.


-- QUESTION:
-- What does the Table Join do?

-- ANSWER:
--   Creates a temporary table with the joined tables' data.


-- Joining Tables:
-- Rather than storing the customer name in both tables, the orders table
-- contains a reference to the customer ID that appears in the customers table.
-- This approach is more efficient, as opposed to storing the same text values
-- in both tables.
-- In order to be able to select the corresponding data from both tables, we
-- will need to join them on that condition.


-- QUESTION:
-- Drag and drop from the options below to select ''id'' from ''students''.
-- Order the results by id, in descending order.

--   ______ id FROM students
--   ________ id ____;

--   ASC, WHERE DESC, ORDER BY, SELECT

-- ANSWER:
--   SELECT
--     id
--   FROM
--     students
--   ORDER BY
--     id DESC;


-- Joining Tables:
-- To join the two tables, specify them as a comma-separated list in the FROM
-- clause:

-- Try It Yourself:
SELECT
  customers.ID,
  customers.Name,
  orders.Name,
  orders.Amount
FROM
  customers,
  orders
WHERE
  customers.ID=orders.Customer_ID
ORDER BY
  customers.ID;

-- Note:
-- Each table contains "ID" and "Name" columns, so in order to select the
-- correct ID and Name, fully qualified names are used.

-- Note that the WHERE clause "joins" the tables on the condition that the ID
-- from the customers table should be equal to the customer_ID of the orders
-- table.

-- Result:
--   id    name     name       amount
--   1     John     Cake       6700
--   2     David    Toy        4500
--   3     Chloe    Book       5000
--   4     Emily    Flowers    1800
--   5     James    Box        3000

-- The returned data shows customer orders and their corresponding amount.

-- Note:
-- Specify multiple table names in the FROM by comma-separating them.


-- QUESTION:
-- Drag and drop from the options below to complete the following statement,
-- which shows item names and the names of customers who bought the items.

--   SELECT
--     customers.name,
--     items.names
--   FROM
--     _________,
--     items
--   _____
--     items.seller_id = customers.id;

--   customers, items, BY, ON, WHERE

-- ANSWER:
--   SELECT
--     customers.name,
--     items.names
--   FROM
--     customers,
--     items
--   WHERE
--     items.seller_id = customers.id;
