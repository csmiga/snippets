-- ALTER TABLE:
-- The ALTER TABLE command is used to add, delete, or modify columns in an
-- existing table.
-- You would also use the ALTER TABLE command to add and drop various
-- constraints on an existing table.

-- Consider the following table called People:

--   ID    FirstName    LastName    City
--   1    John         Smith       New York
--   2    David        Williams    Los Angeles
--   3    Chloe        Anderson    Chicago

-- The following SQL code adds a new column named DateOfBirth

-- Try It Yourself:
ALTER TABLE
  People ADD DateOfBirth date;
SELECT
  *
FROM
  People;

-- Result:
--   ID    FirstName    LastName    City        DateOfBirth
--   1     John         Smith       New York
--   2     David        Williams    Los Angeles
--   3     Chloe        Anderson    Chicago

-- Note:
-- All rows will have the default value in the newly added column, which,
-- in this case, is NULL.


-- QUESTION:
-- Drag and drop from the options below to add a new column entitled
-- "specialty" to the table "students".

--   _____ TABLE students 
--   ___ specialty varchar(50);

--   ALTER, INSERT, VALUES, ADD, REPLACE

-- ANSWER:
--   ALTER TABLE
--     students 
--   ADD specialty varchar(50);


-- Dropping:
-- The following SQL code demonstrates how to delete the column named
-- DateOfBirth in the People table.

-- Try It Yourself:
ALTER TABLE
  People 
DROP
  COLUMN DateOfBirth;
SELECT
  *
FROM
  People;

-- Result:
--   ID    FirstName    LastName    City
--   1     John         Smith       New York
--   2     David        Williams    Los Angeles
--   3     Chloe        Anderson    Chicago

-- Note:
-- The column, along with all of its data, will be completely removed from
-- the table.

-- To delete the entire table, use the DROP TABLE command:

DROP TABLE
  People;

-- Note:
-- Be careful when dropping a table. Deleting a table will result in the
-- complete loss of the information stored in the table!


-- QUESTION:
-- Which choice is the correct command for deleting a table?

-- ANSWER:
--   DROP TABLE


-- Renaming:
-- The ALTER TABLE command is also used to rename columns:

-- Try It Yourself:
ALTER TABLE 
  People RENAME FirstName TO name;
SELECT 
  * 
FROM 
  People;

-- This query will rename the column called FirstName to name.

-- Result:
--   ID    Name     LastName    City
--   1     John     Smith       New York
--   2     David    Williams    Los Angeles
--   3     Chloe    Anderson    Chicago

-- Renaming Tables:
-- You can rename the entire table using the RENAME command: 

RENAME TABLE People TO Users;

-- Note:
-- This will rename the table People to Users.


-- QUESTION:
-- Drag and drop from the options below to rename the table "people" as
-- "humans".
--
--   _______ TABLE people
--   __ ______;
--
--   REPLACE RENAME TABLE people humans TO

-- ANSWER:
--   RENAME TABLE people
--   TO humans;
