-- QUESTION:
-- In the "users" table of website logins and passwords, select the first 10
-- records in the table.
--
--   SELECT
--     *
--   ____
--     users
--   _____
--     10;
--
--   RECORDS, FROM, AT, LIMIT

-- ANSWER:
--   SELECT
--     *
--   FROM
--     users
--   LIMIT
--     10;


-- QUESTION:
-- Drag and drop from the options below to create the table "users" to store
-- website user logins and passwords.
--
--   ______ TABLE users
--   (
--     id INT NOT NULL
--     AUTO_INCREMENT,
--     login _______(100),
--     password VARCHAR(100)
--   );

-- ANSWER:
--   CREATE TABLE users
--   (
--     id INT NOT NULL
--     AUTO_INCREMENT,
--     login VARCHAR(100),
--     password VARCHAR(100)
--   );


-- QUESTION:
--   Rearrange the query to select all students under age 21. The result should
--   be sorted according to the students' names.
--
--   = SELECT *
--   = ORDER BY name;
--   = FROM students
--   = WHERE age < 21

-- ANSWER:
--   SELECT
--     *
--   FROM
--     students
--   WHERE
--     age < 21
--   ORDER BY
--     name;


-- QUESTION:
-- Your boss asks you to print the list of the first one hundred customers who
-- have balances greater than $1000 or who are from NY.
--
--   SELECT
--     *
--   FROM
--     customers
--   _____ balance > 1000
--   __ city = 'NY'
--   _____ 100;
--
--   OR, WHERE, LIMIT, ORDER

-- ANSWER:
--   SELECT
--     *
--   FROM
--     customers
--   WHERE
--     balance > 1000
--     OR city = 'NY'
--   LIMIT 100;


-- QUESTION:
-- You need the ages of all bears and lions. The first query shows the ages of
-- bears and birds from zoo1, the other shows the ages of lions and crocodiles
-- from zoo2.
--
--   = UNION
--   = SELECT age FROM zoo2
--   = WHERE animal IN ('bear', 'bird')
--   = WHERE animal IN ('lion', 'crocodile');
--   = SELECT age FROM zoo1

-- ANSWER:
--   SELECT
--     age
--   FROM
--     zoo1
--   WHERE
--     animal IN ('bear', 'bird')
--   UNION
--   SELECT
--     age
--   FROM
--     zoo2
--   WHERE
--     animal IN ('lion', 'crocodile');


-- QUESTION:
-- Drag and drop from the options below to create a list of customers in the
-- form "name is from city".
--
--   SELECT
--     _____ (name, ' is from ', ____)
--   FROM
--     customers;
--
--   CONCAT, form, city, merge, AVG

-- ANSWER:
--  SELECT
--    CONCAT (name, ' is from ', city)
--  FROM
--    customers;


-- QUESTION:
-- The zoo administration wants a list of animals whose age is greater than
-- the average age of all of the animals.
--
--   = SELECT * FROM zoo
--   = (SELECT AVG(age)
--   = FROM zoo)
--   = WHERE age >

-- ANSWER:
--   SELECT
--     *
--   FROM
--     zoo
--   WHERE
--     age >
--   (
--     SELECT
--       AVG(age)
--     FROM
--       zoo
--   );


-- QUESTION:
-- There are many wolves in the zoo: black wolf, white wolf, lucky wolf,
-- little wolf. They all have 'wolf' at the end of their names. Print the ages
-- of all of the wolves.
--
--   SELECT
--     age
--   FROM
--     zoo
--   WHERE
--     animal LIKE '_____';

-- ANSWER:
--   SELECT
--     age
--   FROM
--     zoo
--   WHERE
--     animal LIKE '%wolf';
