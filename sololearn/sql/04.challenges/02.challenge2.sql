-- QUESTION:
--   Drag and drop from the options below to retrieve all students between the
--   ages of 18 and 22.
--
--   SELECT
--     name ____ students 
--   WHERE
--     age 
--   _______ 18 ___ 22;
--
--   OR, FROM, AND, ALL, TO, BETWEEN

-- ANSWER:
--   SELECT
--     name
--   FROM
--     students 
--   WHERE
--     age
--   BETWEEN 18 AND 22;


-- QUESTION:
-- Drag and drop from the options below to update the "students" table to set
-- Jake's university to MIT. His id is 682.
--
--   _____
--     students
--   SET
--     university = '_____'
--   WHERE
--     _____ = 682;
--
--   UPDATE, name, Jake, NIT, id

-- ANSWER:
--   UPDATE
--     students
--   SET
--     university = 'MIT'
--   WHERE
--     id = 682;


-- QUESTION:
-- When you inserted "elephant" as a new animal, you forgot to include the
-- elephant's age. Correct this mistake by updating the "zoo" table.
--
--   ______
--     zoo
--   ___
--     age = 14
--   WHERE
--     animal = 'elephant';
--
--   ASSIGN, SET, CHANGE, UPDATE, MODIFY

-- ANSWER:
--   UPDATE
--     zoo
--   SET
--     age = 14
--   WHERE
--     animal = 'elephant';


-- QUESTION:
-- Drag and drop from the options below to update the food_balance to 23 for
-- animals whose age is greater than the average age of the animals.
--
--   UPDATE
--     zoo
--   SET
--     food_balance = 23
--   ______ age >
--   (
--     SELECT ___(age)
--     FROM ___
---  );
--
--   INSERT, SUM, WHERE, zoo, age, AVG

-- ANSWER:
--   UPDATE
--     zoo
--   SET
--     food_balance = 23
--   WHERE age >
--   (
--       SELECT AVG(age)
--       FROM zoo
--   );


-- QUESTION:
-- You need your customer's names, along with the names of the cities in which
-- they live. The names of the cities are stored in a separate table called
-- "cities".
--
--   = SELECT customers.name, cities.name
--   = OUTER JOIN cities
--   = ON cities.id=customers.city_id;
--   = FROM customers
--   = RIGHT

-- ANSWER:
--   SELECT
--     customers.name,
--     cities.name
--   FROM
--     customers
--   RIGHT OUTER JOIN cities
--     ON cities.id=customers.city_id;

-- QUESTION:
--   In the university's table containing student data, the students' last names
--   have been omitted. Correct this by adding a new column to the table.
--
--   _____ TABLE students
--   _____ last_name VARCHAR(100);
--
--   CHANGE, ALTER, DELETE, ADD, CREATE

-- ANSWER:
--   ALTER TABLE students
--   ADD last_name VARCHAR(100);


-- QUESTION:
-- Drag and drop from the options below to retrieve from MIT, Stanford, and
-- Harvard the names of all students whose first name is Jake.
--
--   SELECT
--     name
--   FROM
--     students
--   WHERE
--     university __
--     (
--       'MIT',
--       'Stanford',
--       'Harvard'
--     )
--     ___ name = 'Jake';
--
--   AND, IN, FROM, OR, BETWEEN

-- ANSWER:
--   SELECT
--     name
--   FROM
--     students
--   WHERE
--     university IN
--     (
--       'MIT',
--       'Stanford',
--       'Harvard'
--     )
--     AND nam = 'Jake';
