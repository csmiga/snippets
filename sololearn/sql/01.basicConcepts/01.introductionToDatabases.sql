-- A Database:
-- A database is a collection of data that is organized in a manner that
-- facilitates ease of access, as well as efficient management and updating.
-- A database is made up of tables that store relevant information.

-- For example, you would use a database, if you were to create a website like
-- YouTube, which contains a lot of information like videos, usernames,
-- passwords, comments.

--   USERS      VIDEOS     COMMENTS
--   table 1    table 2    table 3

-- Note:
-- In this course we will learn how to create and query databases using SQL!


-- QUESTION:
-- Which of the following is true about a database?

-- ANSWER:
--   A database is a collection of data


-- Database Tables:
-- A table stores and displays data in a structured format consisting of columns
-- and rows that are similar to those seen in Excel spreadsheets.

-- Databases often contain multiple tables, each designed for a specific
-- purpose. For example, imagine creating a database table of names and
-- telephone numbers.

-- First, we would set up columns with the titles FirstName, LastName and
-- TelephoneNumber.

-- Each table includes its own set of fields, based on the data it will store.

--   FirstName    LastName    PhoneNumber
--   John         Smith       715-555-1230
--   David        Williams    569-999-1719
--   Chloe        Anderson    715-777-2010
--   Emily        Adams       566-333-1223
--   James        Roberts     763-777-2956

-- Note:
-- A table has a specified number of columns but can have any number of rows.


-- QUESTION:
-- Tables are made up of:

-- ANSWER:
-- Columns and rows


-- Primary Keys:
-- A primary key is a field in the table that uniquely identifies the table
-- records.

-- The primary key's main features:
--   - It must contain a unique value for each row.
--   - It cannot contain NULL values.

-- For example, our table contains a record for each name in a phone book. The
-- unique ID number would be a good choice for a primary key in the table, as
-- there is always the chance for more than one person to have the same name.

--   FirstName    LastName    PhoneNumber
--   John         Smith       715-555-1230
--   David        Williams    569-999-1719
--   Chloe        Anderson    715-777-2010
--   Emily        Adams       566-333-1223
--   James        Roberts     763-777-2956

-- Note:
--   - Tables are limited to ONE primary key each.
--   - The primary key's value must be different for each row.


-- QUESTION:
-- The unique column used to identify the records is called:

-- ANSWER:
-- primary key


-- What is SQL?:
-- Once you understand what a database is, understanding SQL is easy. SQL stands
-- for Structured Query Language.

-- SQL is used to access and manipulate a database.
-- MySQL is a program that understands SQL.

-- SQL can:
--   - insert, update, or delete records in a database.
--   - create new databases, table, stored procedures, views.
--   - retrieve data from a database, etc.

-- Note:
-- SQL is an ANSI (American National Standards Institute) standard, but
-- there are different versions of the SQL language. Most SQL database programs
-- have their own proprietary extensions in addition to the SQL standard, but
-- all of them support the major commands.


-- QUESTION:
-- What is the name of the language used in creating & accessing databases?

-- ANSWER:
--   sql
