-- Selecting Multiple Columns:
-- As previously mentioned, the SQL SELECT statement retrieves records from
-- tables in your SQL database.

-- You can select multiple table columns at once.
-- Just list the column names, separated by commas

-- Try It Yourself:
SELECT
  FirstName,
  LastName,
  City
FROM
  customers;

--     FirstName    LastName    City
--     John         Smith       New York
--     David        Williams    Los Angeles
--     Chloe        Anderson    Chicago
--     Emily        Adams       Houston
--     James        Roberts     Philadelphia

-- Note:
-- Do not put a comma after the last column name.


-- QUESTION:
--   Fill in the blanks to select name and city from the ''people'' table.
--
--   SELECT ____, city FROM ______;

-- ANSWER:
--   SELECT name, city FROM people;


-- Selecting All Columns:
-- To retrieve all of the information contained in your table, place an
-- asterisk (*) sign after the SELECT command, rather than typing in each column
-- names separately.

-- The following SQL statement selects all of the columns in the customers
-- table:

-- Try It Yourself:
SELECT
  *
FROM
  customers;

-- Result:
--     FirstName    LastName    City          ZipCode
--     John         Smith       New York      10199
--     David        Williams    Los Angeles   90052
--     Chloe        Anderson    Chicago       60607
--     Emily        Adams       Houston       77201
--     James        Roberts     Philadelphia  19104

-- Note:
-- In SQL, the asterisk means all.


-- QUESTION:
--   Type in the missing symbol to select all of the columns from the
--   ''students'' table.
--
--   SELECT _ FROM students;

-- ANSWER:
--   SELECT
--     *
--   FROM
--     students;
