-- Multiple Queries:
-- SQL allows to run multiple queries or commands at the same time.
-- The following SQL statement selects the FirstName and City columns from the
-- customers table:

-- Try It Yourself:
SELECT
  FirstName
FROM
  customers;

SELECT
  City
FROM
  customers;

-- Result:
--   FirstName
--   John
--   David
--   Chloe
--   Emily
--   James

--   City
--   New York
--   Los Angeles
--   Chicago
--   Houston
--   Philadelphia

-- Note:
-- Remember to end each SQL statement with a semicolon to indicate that the
-- statement is complete and ready to be interpreted. In this tutorial, we
-- will use semicolon at the end of each SQL statement.


-- QUESTION:
-- Whenever you run multiple queries:

-- ANSWER:
--   Each query must end with a semicolon.


-- Case Sensitivity:
-- SQL is case insensitive.
-- The following statements are equivalent and will produce the same result:

SELECT
  City
FROM
  customers;

SELECT
  City
FROM
  customers;

SELECT
  City
FROM
  customers;

-- Note:
-- It is common practice to write all SQL commands in upper-case.


-- QUESTION:
-- SQL is:

-- ANSWER:
--   Case insensitive


-- Syntax Rules:
-- A single SQL statement can be placed on one or more text lines. In addition,
-- multiple SQL statements can be combined on a single text line.

-- White spaces and multiple lines are ignored in SQL.
-- For example, the following query is absolutely correct.

SELECT
  City
FROM
  customers;

-- However, it is recommended to avoid unnecessary white spaces and lines.

-- Note:
-- Combined with proper spacing and indenting, breaking up the commands
-- into logical lines will make your SQL statements much easier to read and
-- maintain.


-- QUESTION:
-- Which is true about whitespaces?

-- ANSWER:
--   Whitespaces and line breaks are ignored
