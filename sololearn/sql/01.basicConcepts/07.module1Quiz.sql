-- QUESTION:
-- A database consists of:

-- ANSWER:
--   Tables


-- QUESTION:
-- Drag and drop from the options below to list the table names.
--
--   ____ TABLES;
--
--   SELECT, SHOW, VIEW, LIMIT

-- ANSWER:
--   SHOW TABLES;


-- QUESTION:
--   Why use primary keys?

-- ANSWER:
--   To guarantee the uniqueness of a row


-- QUESTION:
-- Drag and drop from the options below to select distinct names from the
-- ''students'' table, ordered by name.
--
--   SELECT ________
--     name
--   ____
--     students
--   ________
--       name;
--
--   FROM, DISTINCT, LIMIT, ORDER BY, BY, IN

-- ANSWER:
--   SELECT DISTINCT
--     name
--   FROM
--     students
--   ORDER BY
--     name;
