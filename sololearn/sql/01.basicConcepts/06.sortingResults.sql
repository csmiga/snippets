-- Fully Qualified Names:
-- In SQL, you can provide the table name prior to the column name, by
-- separating them with a dot.
-- The following statements are equivalent:

SELECT
  City
FROM
  customers;

SELECT
  customers.City
FROM
  customers;

-- The term for the above-mentioned syntax is called the "fully qualified name"
-- of that column.

-- Note:
-- This form of writing is especially useful when working with multiple
-- tables that may share the same column names.


-- QUESTION:
-- Fill in the blanks to select the ''address'' from ''customers'', using the
-- fully qualified name for the ''address'' column.
--
--   SELECT
--     _________._______
--   FROM
--     customers;

-- ANSWER:
--   SELECT
--     customers.address
--   FROM
--     customers;


-- Order By:
-- ORDER BY is used with SELECT to sort the returned data.

-- The following example sorts our customers table by the FirstName column.

-- Try It Yourself:
SELECT
  *
FROM
  customers
ORDER BY
  FirstName;

-- Result:
--   ID  FirstName    LastName    City
--   6   Andrew       Thomas      New York
--   10  Anthony      Young       Los Angeles
--   8   Charlotte    Walker      Chicago
--   3   Chloe        Anderson    Chicago
--   7   Daniel       Harris      New York
--   2   David        Williams    Los Angeles
--   4   Emily        Adams       Houston
--   5   James        Roberts     Philadelphia
--   1   John         Smith       New York
--   9   Samuel       Clark       San Diego

-- As you can see, the rows are ordered alphabetically by the FirstName column.

-- Note:
-- By default, the ORDER BY keyword sorts the results in ascending order.


-- QUESTION:
-- Build a query to select ''name'' and ''city'' from the ''people'' table,
-- and order by the ''id''.
--
--   SELECT
--     name,
--     ____
--   FROM
--     ______
--   ________
--     id;
--
--   LIMIT, city, people, BY, SELECT, ORDER BY

-- ANSWER:
--   SELECT
--     name,
--     city
--   FROM
--     people
--   ORDER BY
--     id;


-- Sorting Multiple Columns:
-- ORDER BY can sort retrieved data by multiple columns. When using ORDER BY
-- with more than one column, separate the list of columns to follow ORDER BY
-- with commas.
-- Here is the customers table, showing the following records:

--   ID  FirstName    LastName    Age
--   1   John         Smith       35
--   2   David        Williams    23
--   3   Chloe        Anderson    27
--   4   Emily        Adams       34
--   5   James        Roberts     31
--   6   Andrew       Thomas      45
--   7   Daniel       Harris      30

-- To order by LastName and Age:

-- Try It Yourself:
SELECT
  *
FROM
  customers
ORDER BY
  LastName,
  Age;

-- This ORDER BY statement returns the following result: 
--   ID  FirstName    LastName    Age
--   4   Emily        Adams       34
--   3   Chloe        Anderson    27
--   7   Daniel       Harris      30
--   5   James        Roberts     31
--   2   David        Williams    23
--   1   John         Smith       35
--   6   Andrew       Thomas      45

-- As we have two Smiths, they will be ordered by the Age column in ascending
-- order.

-- Note:
-- The ORDER BY command starts ordering in the same sequence as the
-- columns. It will order by the first column listed, then by the second, and so
-- on.


-- QUESTION:
-- Fill in the blanks to order the query results by ''name'', and then by
-- ''state''.
--
--   SELECT name, state, address FROM customers ORDER BY ____, _____;

-- ANSWER:
--   SELECT
--     name,
--     state,
--     address
--   FROM
--     customers
--   ORDER BY
--     name,
--     state;
