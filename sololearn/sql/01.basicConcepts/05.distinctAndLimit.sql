-- The DISTINCT Keyword:
-- In situations in which you have multiple duplicate records in a table, it
-- might make more sense to return only unique records, instead of fetching the
-- duplicates.
-- The SQL DISTINCT keyword is used in conjunction with SELECT to eliminate all
-- duplicate records and return only unique ones.
-- The basic syntax of DISTINCT is as follows:

SELECT DISTINCT
  column_name1,
  column_name2
FROM
  table_name;

-- See the customers table below:

--   ID  FirstName    LastName    City
--   1   John         Smith       New York
--   2   David        Williams    Los Angeles
--   3   Chloe        Anderson    Chicago
--   4   Emily        Adams       Houston
--   5   James        Roberts     Philadelphia
--   6   Andrew       Thomas      New York
--   7   Daniel       Harris      New York
--   8   Charlotte    Walker      Chicago
--   9   Samuel       Clark       San Diego
--   10  Anthony      Young       Los Angeles

-- Note that there are duplicate City names. The following SQL statement selects
-- only distinct values from the City column:

SELECT DISTINCT
  City
FROM
  customers;

--   City
--   New York
--   Los Angeles
--   Chicago
--   Houston
--   Philadelphia
--   San Diego

-- Note:
-- The DISTINCT keyword only fetches the unique values.


-- QUESTION:
-- Drag and drop from the options below to build a query to get distinct
-- results from the ''customers'' table.
--
--   ______ ________ state ____ customers;
--
--   COLUMNS, SELECT, TABLE, DISTINCT, FROM

-- ANSWER:
--   SELECT DISTINCT
--     state
--   FROM
--     customers;


-- The LIMIT Keyword:
-- By default, all results that satisfy the conditions specified in the SQL
-- statement are returned. However, sometimes we need to retrieve just a subset
-- of records. In MySQL, this is accomplished by using the LIMIT keyword.

-- The syntax for LIMIT is as follows:

SELECT
  column list
FROM
  table_name
LIMIT
  [number of records];

-- Try It Yourself:
SELECT
  ID,
  FirstName,
  LastName,
  City
FROM
  customers
LIMIT
  5;

-- This would produce the following result:

--   ID  FirstName    LastName    City
--   1   John         Smith       New York
--   2   David        Williams    Los Angeles
--   3   Chloe        Anderson    Chicago
--   4   Emily        Adams       Houston
--   5   James        Roberts     Philadelphia

-- Note:
-- By default, all results that satisfy the conditions specified in the
-- SQL statement are returned.


-- QUESTION:
-- Drag and drop from the options below to complete the following statement,
-- which selects five names from ''students''.
--
--   SELECT
--     name
--   _____
--     students
--   _____
--     5;
--
--   FROM, VIEW, LIMIT, SELECT, NAMES

-- ANSWER:
--   SELECT
--     name
--   FROM
--     students
--   LIMIT
--     5;


-- The LIMIT Keyword:
-- You can also pick up a set of records from a particular offset.
-- In the following example, we pick up four records, starting from the third
-- position:

-- Try It Yourself:
SELECT
  ID,
  FirstName,
  LastName,
  City
FROM
  customers OFFSET 3
LIMIT
  4;

-- This would produce the following result:
--   ID  FirstName    LastName    City
--   4   Emily        Adams       Houston
--   5   James        Roberts     Philadelphia
--   6   Andrew       Thomas      New York
--   7   Daniel       Harris      New York

-- Note:
-- The reason that it produces results starting from ID number four, and
-- not three, is that MySQL starts counting from zero, meaning that the offset
-- of the first row is 0, not 1.


-- QUESTION:
-- Use LIMIT to select the ''id'' and ''name'' columns from ''customers''.
-- Show 12 results, starting from the fifth.
--
--   ______ id, name FROM customers LIMIT _, __;
--
--   RESULT, SELECT, BY, 4, SHOW, 12

-- ANSWER:
--   SELECT
--     id,
--     name
--   FROM
--     customers
--   LIMIT
--     4, 12;
