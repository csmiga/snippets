-- Cakes:

-- A local bakery creates unique cake sets. Each cake set contains three
-- different cakes.
-- Here is the cakes table:

--   name            calories
--   Apple Cake      100
--   Banana Cake     200
--   Pound Cake      180
--   Sponge Cake     100
--   Genoise Cake    360
--   Chiffon Cake    250
--   Opera Cake      90
--   Cheese Cake     370

-- Тoday a customer want a cake set that has minimal calories.
-- Write a query to sort the cakes by calorie count and select the first 3 cakes
-- from the list to offer the customer.

-- Note:
-- Try to combine ORDER BY and LIMIT keywords.

-- Solution:
SELECT
  *
FROM
  cakes
ORDER BY
  calories
LIMIT
  3;
