-- The CONCAT Function:
-- The CONCAT function is used to concatenate two or more text values and
-- returns the concatenating string.

-- Let's concatenate the FirstName with the City, separating them with a comma:

-- Try It Yourself:
SELECT CONCAT(FirstName, ', ', City) FROM customers;

-- The output result is:
--   CONCAT(FirstName,' , ', City)
--   John, New York
--   David, Los Angeles
--   Chloe, Chicago
--   Emily, Houston
--   James, Philadelphia
--   Andrew, New York
--   Daniel, New York
--   Charlotte, Chicago
--   Samuel, San Diego

-- Note: The CONCAT() function takes two or more parameters.


-- QUESTION:
-- What do we call the function used for concatenation?
--
--   ______

-- ANSWER:
--   concat


-- The AS Keyword:
-- A concatenation results in a new column. The default column name will be the
-- CONCAT function.
-- You can assign a custom name to the resulting column using the AS keyword: 

-- Try It Yourself:
SELECT CONCAT(FirstName, ', ', City)
AS new_column 
FROM customers;

-- And when you run the query, the column name appears to be changed.

--   new_column
--   John, New York
--   David, Los Angeles
--   Chloe, Chicago
--   Emily, Houston
--   James, Philadelphia
--   Andrew, New York
--   Daniel, New York
--   Charlotte, Chicago
--   Samuel, San Diego
--   Anthony, Los Angeles

-- Note: A concatenation results in a new column.


-- QUESTION:
-- Type in the keyword used to create custom columns.
--
--   __

-- ANSWER:
--   as


-- Arithmetic Operators:
-- Arithmetic operators perform arithmetical operations on numeric operands. The
-- Arithmetic operators include addition (+), subtraction (-),
-- multiplication (*) and division (/).

-- The following employees table shows employee names and salaries:
--     ID    FirstName    LastName    Salary
--      1    John         Smith       2000
--      2    David        Williams    1500
--      3    Chloe        Anderson    3000
--      4    Emily        Adams       4500
--      5    James        Roberts     2000
--      6    Andrew       Thomas      2500
--      7    Daniel       Harris      3000
--      8    Charlotte    Walker      3500
--      9    Samuel       Clark       4000
--     10    Anthony      Young       5000

-- The example below adds 500 to each employee's salary and selects the result:

-- Try It Yourself:
SELECT 
  ID, 
  FirstName, 
  LastName, 
  Salary + 500 AS Salary 
FROM 
  employees;

-- Result:
--   ID    FirstName    LastName    Salary
--   1     John         Smith       2500
--   2     David        Williams    2000
--   3     Chloe        Anderson    3500
--   4     Emily        Adams       5000
--   5     James        Roberts     2500
--   6     Andrew       Thomas      3000
--   7     Daniel       Harris      3500
--   8     Charlotte    Walker      4000
--   9     Samuel       Clark       4500
--   10    Anthony      Young       5500

-- Note: Parentheses can be used to force an operation to take priority over any
-- other operators. They are also used to improve code readability.


-- QUESTION:
-- Drag and drop from the options below to select concatenated ''city'' and
-- ''state'' columns, represented with a new custom column named
-- ''new_address''.
--
--   SELECT
--     ______ ___________________
--   __ new_address
--   FROM
--     customers;
--
--   CONCAT, BY, NEW, CUSTOM, AS, (city, ', ', state)

-- ANSWER:
--   SELECT 
--     CONCAT(city, ', ', state) AS new_address 
--   FROM 
--     customers;
