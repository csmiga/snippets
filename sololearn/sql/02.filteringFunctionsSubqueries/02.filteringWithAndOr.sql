-- Logical Operators:
-- Logical operators can be used to combine two Boolean values and return a
-- result of true, false, or null.
-- The following operators can be used:

--   Operator    Description
--   AND         TRUE if both expressions are TRUE
--   OR          TRUE if either expression is TRUE
--   IN          TRUE if the operand is equal to one of a list of expressions
--   NOT         Returns TRUE if expression is not TRUE

-- When retrieving data using a SELECT statement, use logical operators in the
-- WHERE clause to combine multiple conditions.

-- If you want to select rows that satisfy all of the given conditions, use the
-- logical operator, AND.

--   ID  FirstName    LastName    Age
--   1  John         Smith       35
--   2  David        Williams    23
--   3  Chloe        Anderson    27
--   4  Emily        Adams       34
--   5  James        Roberts     31
--   6  Andrew       Thomas      45
--   7  Daniel       Harris      30

-- To find the names of the customers between 30 to 40 years of age, set up the
-- query as seen here:

-- Try It Yourself:
SELECT
  ID,
  FirstName,
  LastName,
  Age
FROM
  customers
WHERE
  Age >= 30
  AND Age <= 40;

-- This results in the following output:
--   ID  FirstName    LastName    Age
--   1  John         Smith       35
--   4  Emily        Adams       34
--   5  James        Roberts     31
--   7  Daniel       Harris      30

-- Note:
-- You can combine as many conditions as needed to return the desired results.


-- QUESTION:
-- Drag and drop from the options below to select customers who live in
-- Hollywood, CA.

--   SELECT
--     *
--   FROM
--     customers 
--   WHERE
--     state = 'CA'
--     ___ city = 'Hollywood';

--   ,, OR, AND, +

-- ANSWER:
--   SELECT
--     *
--   FROM
--     customers 
--   WHERE
--     state = 'CA'
--   AND
--     city = 'Hollywood';


-- OR:
-- If you want to select rows that satisfy at least one of the given conditions,
-- you can use the logical OR operator.

-- The following table describes how the logical OR operator functions:

--   Condition1    Condition2    Result
--   True          True          True
--   True          False         True
--   False         True          True
--   False         False         False

-- For example, if you want to find the customers who live either in New York or
-- Chicago, the query would look like this:

-- Try It Yourself:
SELECT
  *
FROM
  customers 
WHERE
  City = 'New York'
OR
  City = 'Chicago';

-- Result:
--   ID  FirstName    LastName    City
--   1   John         Smith       New York
--   3   Chloe        Anderson    Chicago
--   6   Andrew       Thomas      New York
--   7   Daniel       Harris      New York
--   8   Charlotte    Walker      Chicago

-- Note:
-- You can OR two or more conditions.


-- QUESTION:
-- Drag and drop from the options below to select customers who live either in
-- CA or in Boston.
--
--   SELECT
--     name,
--     state,
--     city 
--   FROM
--     customers 
--   _____
--     state = 'CA'
--     __ city = 'Boston';
--
--   BETWEEN, WHERE, DISTINCT, AND, OR

-- ANSWER:
--   SELECT
--     name,
--     state,
--     city 
--   FROM
--     customers 
--   WHERE
--     state = 'CA'
--     OR city = 'Boston';


-- Combining AND & OR;
-- The SQL AND and OR conditions may be combined to test multiple conditions in
-- a query.
-- These two operators are called conjunctive operators.

-- When combining these conditions, it is important to use parentheses, so that
-- the order to evaluate each condition is known.

-- Consider the following table:

--   ID  FirstName    LastName    City            Age
--   1   John         Smith       New York        35
--   2   David        Williams    Los Angeles     23
--   3   Chloe        Anderson    Chicago         27
--   4   Emily        Adams       Houston         34
--   5   James        Roberts     Philadelphia    31
--   6   Andrew       Thomas      New York        45
--   7   Daniel       Harris      New York        30
--   8   Charlotte    Walker      Chicago         35
--   9   Samuel       Clark       San Diego       20
--   10  Anthony      Young       Los Angeles     33

-- The statement below selects all customers from the city "New York" AND with
-- the age equal to "30" OR “35":

-- Try It Yourself:
SELECT
  *
FROM
  customers
WHERE
  City = 'New York'
  AND (Age=30 OR Age=35);

-- Result:
--   ID  FirstName    LastName    City            Age
--   1   John         Smith       New York        35
--   7   Daniel       Harris      New York        30

-- Note:
-- You can nest as many conditions as you need.


-- QUESTION:
--   Drag and drop from the options below to select customers whose ids are either
--   1 or 2, and whose city is ''Boston''.

--   SELECT
--     *
--   FROM
--     customers
--   WHERE
--     (id = 1 __ id = 2)
--     ___ city = 'Boston';

--   OR, DISTINCT, WHERE, AND, BETWEEN

-- ANSWER:
--   SELECT
--     *
--   FROM
--     customers
--   WHERE
--     (id = 1 OR id = 2)
--     AND city = 'Boston';
