-- The IN Operator:
-- The IN operator is used when you want to compare a column with more than one
-- value.

-- For example, you might need to select all customers from New York,
-- Los Angeles, and Chicago.
-- With the OR condition, your SQL would look like this: 

-- Try It Yourself:
SELECT
  *
FROM
  customers 
WHERE
  City = 'New York'
  OR City = 'Los Angeles'
  OR City = 'Chicago';

-- Result:
--   ID  FirstName    LastName    City
--   1   John         Smith       New York
--   2   David        Williams    Los Angeles
--   3   Chloe        Anderson    Chicago
--   6   Andrew       Thomas      New York
--   7   Daniel       Harris      New York
--   8   Charlotte    Walker      Chicago
--   10  Anthony      Young       Los Angeles

-- Note: The IN operator is used when you want to compare a column with more
-- than one value.


-- QUESTION:
--   Drag and drop from the options below to select users from NY and CA:
--
--   SELECT
--     *
--   ____
--     users
--   _____
--     state = 'NY'
--     __ state = 'CA';
--
--   FROM, OR, WHERE

-- ANSWER:
--   SELECT
--     *
--   FROM
--     users
--   WHERE
--     state = 'NY'
--     OR state = 'CA';


-- The IN Operator:
-- You can achieve the same result with a single IN condition, instead of the
-- multiple OR conditions:

-- Try It Yourself:
SELECT 
  * 
FROM 
  customers 
WHERE 
  City IN
  (
    'New York',
    'Los Angeles',
    'Chicago'
  );


-- This would also produce the same result:
--     ID  FirstName    LastName    City
--      1  John         Smith       New York
--      2  David        Williams    Los Angeles
--      3  Chloe        Anderson    Chicago
--      6  Andrew       Thomas      New York
--      7  Daniel       Harris      New York
--      8  Charlotte    Walker      Chicago
--     10  Anthony      Young       Los Angeles

-- Note: Note the use of parentheses in the syntax.


-- QUESTION:
--   Select customers from NY, CA, or NC, using the IN statement.
--
--   SELECT
--     name,
--     state 
--   ____
--     customers 
--   WHERE
--     state IN
--     __________________;
--
--   OR, ('CA'. 'NY', 'NC'), FROM, SELECT, AND

-- ANSWER:
--   SELECT
--     name,
--     state 
--   FROM
--     customers 
--   WHERE
--     state IN
--     (
--       'CA',
--       'NY',
--       'NC'
--     );


-- The NOT IN Operator:
-- The NOT IN operator allows you to exclude a list of specific values from the
-- result set.

-- If we add the NOT keyword before IN in our previous query, customers living
-- in those cities will be excluded:

SELECT
  *
FROM
  customers 
WHERE
  City NOT IN
  (
    'New York',
    'Los Angeles',
    'Chicago'
  );

-- Result:
--     ID  FirstName    LastName    City
--      4  Emily        Adams       Houston
--      5  James        Roberts     Philadelphia
--      9  Samuel       Clark       San Diego

-- Note: The NOT IN operator allows you to exclude a list of specific values
-- from the result set.


-- QUESTION:
-- Drag and drop from the options below to exclude customers from the states
-- CA, NY.
--
--   SELECT
--     name,
--     state 
--   FROM
--     customers
--   WHERE
--     state ______
--     (
--       'CA',
--       'NY'
--     );
--
--   NOT IN, EXCLUDE, IN, BETWEEN

-- ANSWER:
--   SELECT name, state 
--   FROM customers
--   WHERE state NOT IN ('CA', 'NY');
