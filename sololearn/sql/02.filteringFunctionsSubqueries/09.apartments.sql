-- Apartments:
-- You want to rent an apartment and have the following table named Apartments:

--     id    city          address                   price    status
--      1    Las Vegas     732 Hall Street           1000     Not rented
--      2    Marlboro      985 Huntz Lane            800      Not rented
--      3    Moretown      3757 Wines Lane           700      Not rented
--      4    Owatonna      314 Pritchard Court       500      Rented
--      5    Graystake     3234 Cunningham Court     600      Rented
--      6    Great Neck    1927 Romines Mill Road    900      Not rented

-- Write a query to output the apartments whose prices are greater than the
-- average and are also not rented, sorted by the 'Price' column.

-- Note:
-- Recall the AVG keyword.

-- Solution:
SELECT 
  * 
FROM 
  apartments 
WHERE 
  price >
  (
    SELECT 
      AVG(price) 
    FROM 
      apartments
  ) 
ORDER BY 
  price ASC;
