-- The Like Operator:
-- The LIKE keyword is useful when specifying a search condition within your
-- WHERE clause.

SELECT
  column_name(s)
FROM
  table_name
WHERE
  column_name
LIKE
  pattern;

-- SQL pattern matching enables you to use "_" to match any single character and
-- "%" to match an arbitrary number of characters (including zero characters).

-- For example, to select employees whose FirstNames begin with the letter A,
-- you would use the following query:

-- Try It Youself:
SELECT
  *
FROM
  employees 
WHERE
  FirstName LIKE 'A%';

-- Result:
--   id    firstname    lastname    salary
--   6     Andrew       Thomas      2500
--   10    Anthony      Young       5000

-- As another example, the following SQL query selects all employees with a
-- LastName ending with the letter "s":

-- Try It Yourself:
SELECT 
  * 
FROM 
  employees 
WHERE 
  LastName LIKE '%s';

-- Result:
--   id    firstname    lastname    salary
--   2      David        Williams    1500
--   4      Emily        Adams       4500
--   5      James        Roberts     2000
--   6      Andrew       Thomas      2500
--   7      Daniel       Harris      3000

-- Note: The % wildcard can be used multiple times within the same pattern.


-- QUESTION:
--   Drag and drop from the options below to search ''boxes'' in the ''name''
--   column of the ''items'' table.

--   SELECT seller_id
--   FROM items
--   WHERE name
--   ____ '%boxes';

--   LIKE, COMPARE, SEARCH, IN

-- ANSWER:
--   SELECT seller_id
--   FROM items
--   WHERE name
--   LIKE '%boxes';


-- The MIN Function:
-- The MIN function is used to return the minimum value of an expression in a
-- SELECT statement.

-- For example, you might wish to know the minimum salary among the employees. 

-- Try It Yourself:
SELECT
  MIN(Salary) AS Salary
FROM
  employees;

-- Result
--   salary
--   1500

-- Note:
-- All of the SQL functions can be combined together to create a single
-- expression.


-- QUESTION:
-- Drag and drop from the options below to complete the statement, which
-- selects ''name'' and minimum of the "cost'' from ''items'', filtering by
-- name and seller id.
--
--   SELECT
--     name,
--     ___ (cost)
--   FROM
--     items
--   WHERE
--     name
--     ____ '%boxes of frogs'
--     AND seller_id __
--     (
--       68,
--       6,
--       18
--     );
--
--   BETWEEN, AVG, IN, LIKE, NOT, MIN

-- ANSWER:
--   SELECT
--     name,
--     MIN (cost)
--   FROM
--     items
--   WHERE
--     name LIKE '%boxes of frogs' 
--     AND seller_id IN
--     (
--       68, 6, 18
--     );
