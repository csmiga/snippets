-- The UPPER Function:
-- The UPPER function converts all letters in the specified string to uppercase.
-- The LOWER function converts the string to lowercase.

-- The following SQL query selects all LastNames as uppercase:

-- Try It Yourself:
SELECT 
  FirstName, 
  UPPER(LastName) AS LastName 
FROM 
  employees;

-- Result:
--     ID  FirstName    LastName
--      1  John         SMITH
--      2  David        WILLIAMS
--      3  Chloe        ANDERSON
--      4  Emily        ADAMS
--      5  James        ROBERTS
--      6  Andrew       THOMAS
--      7  Daniel       HARRIS
--      8  Charlotte    Walker
--      9  Samuel       CLARK
--     10  Anthony      YOUNG

-- Note:
-- If there are characters in the string that are not letters, this
-- function will have no effect on them.


-- QUESTION:
-- What is the name of the function that converts the text to lowercase?
--
--   _____

-- ANSWER:
--   LOWER


-- SQRT and AVG:
-- The SQRT function returns the square root of given value in the argument.
-- Let's calculate the square root of each Salary:

-- Try It Yourself:
SELECT
  Salary,
  SQRT(Salary) 
FROM
  employees;

-- Result:
--     salary    sqrt
--     2000      44.721359549995796
--     1500      38.72983346207417
--     3000      54.772255750516614
--     4500      67.08203932499369
--     2000      44.721359549995796
--     2500      50
--     3000      54.772255750516614
--     3500      59.16079783099616
--     4000      63.245553203367585
--     5000      70.71067811865476

-- Similarly, the AVG function returns the average value of a numeric column: 

-- Try It Yourself:
SELECT
  AVG(Salary)
FROM
  employees;

-- Result:
--   avg
--   3100.0000000000000000

-- Note:
-- Another way to do the SQRT is to use POWER with the 1/2 exponent.
-- However, SQRT seems to work faster than POWER in this case.


-- QUESTION:
-- Drag and drop from the options below to select the average cost from the
-- ''items'' table.
--
--   SELECT
--     ___(cost)
--   ____ 
--     items;
--
--   AVG, TO, FROM, SQRT, UPPER

-- ANSWER:
--   SELECT AVG(cost)
--   FROM items;


-- The SUM function:
-- The SUM function is used to calculate the sum for a column's values.
-- For example, to get the sum of all of the salaries in the employees table,
-- our SQL query would look like this:

-- Try It Yourself:
SELECT
  SUM(Salary)
FROM
  employees;

-- Results:
--   sum
--   31000

-- Note:
-- The sum of all of the employees' salaries is 31000.


-- QUESTION:
-- Type in the aggregate function name used to calculate the sum of a column's
-- values.
--
--   ___

-- ANSWER:
--   sum
