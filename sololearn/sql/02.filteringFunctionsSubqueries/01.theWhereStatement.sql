-- The WHERE Statement:
-- The WHERE clause is used to extract only those records that fulfill a
-- specified criterion.
-- The syntax for the WHERE clause:

SELECT
  column_list 
FROM
  table_name
WHERE
  condition;

-- Consider the following table:

--   ID  FirstName    LastName    City
--   1   John         Smith       New York
--   2   David        Williams    Los Angeles
--   3   Chloe        Anderson    Chicago
--   4   Emily        Adams       Houston
--   5   James        Roberts     Philadelphia
--   6   Andrew       Thomas      New York
--   7   Daniel       Harris      New York
--   8   Charlotte    Walker      Chicago
--   9   Samuel       Clark       San Diego
--   10  Anthony      Young       Los Angeles

-- In the above table, to SELECT a specific record:

-- Try It Yourself:
SELECT
  *
FROM
  customers
WHERE
  ID = 7;

--   ID  FirstName    LastName    City
--   7   Daniel       Harris      New York

-- The WHERE clause is used to extract only those records that fulfill a
-- specified criterion.


-- QUESTION:
-- Drag and drop from the options below to select the name of the student
-- whose id is equal to 23.
--
--   SELECT
--     id,
--     name
--   FROM
--     students
--   _____
--     id = ___
--
--   =, 0, 22;, LIMIT, WHERE, DISTINCT, 23;

-- ANSWER:
--   SELECT
--     id,
--     name
--   FROM
--     students
--   WHERE
--     id = 23;


-- SQL Operators:
-- Comparison Operators and Logical Operators are used in the WHERE clause to
-- filter the data to be selected.

-- The following comparison operators can be used in the WHERE clause:

--     Operator    Description
--     =           Equal
--     !=          Not equal
--     >           Greater than
--     <           Less than
--     >=          Greater than or equal
--     <=          Less than or equal
--     BETWEEN     Between an inclusive


-- QUESTION:
-- Fill in the blank to select student names whose id is greater than or equal
-- to 12.
--
--   SELECT id, name FROM students WHERE id __ 12;

-- ANSWER:
--   SELECT
--     id,
--     name
--   FROM
--     students
--   WHERE
--     id >= 12;


-- The BETWEEN Operator:
-- The BETWEEN operator selects values within a range. The first value must be
-- lower bound and the second value, the upper bound.

-- The syntax for the BETWEEN clause is as follows:

SELECT
  column_name(s)
FROM
  table_name
WHERE
  column_name BETWEEN value1
  AND value2;

-- The following SQL statement selects all records with IDs that fall between 3
-- and 7:

-- Try It Yourself:
SELECT
  *
FROM
  customers
WHERE
  ID BETWEEN 3
  AND 7;

-- Result:
--   ID  FirstName    LastName    City
--   3   Chloe        Anderson    Chicago
--   4   Emily        Adams       Houston
--   5   James        Roberts     Philadelphia
--   6   Andrew       Thomas      New York
--   7   Daniel       Harris      New York

-- Note: As you can see, the lower bound and upper bound are both included in
-- the range.


-- QUESTION:
--  Drag and drop from the options below to build a query to select the names
--  of students whose ids are between 13 and 45.
--
--   SELECT
--     id,
--     name
--   ____
--     students
--   _____
--     id _______ __
--     AND 45;

--   WHERE, 45, FROM, BETWEEN, SELECT, 13

-- ANSWER:
--   SELECT
--     id,
--     name
--   FROM
--     students
--   WHERE
--     id BETWEEN 13
--     AND 45;


-- Text Values:
-- When working with text columns, surround any text that appears in the
-- statement with single quotation marks (').

-- The following SQL statement selects all records in which the City is equal to
-- 'New York'.

-- Try It Yourself:
SELECT
  ID,
  FirstName,
  LastName,
  City 
FROM
  customers
WHERE
  City = 'New York';

--   ID  FirstName    LastName    City
--   1   John         Smith       New York
--   6   Andrew       Thomas      New York
--   7   Daniel       Harris      New York

-- Note: If your text contains an apostrophe (single quote), you should use two
-- single quote characters to escape the apostrophe. For example: 'Can''t'.


-- QUESTION:
--  Rearrange the code to select all values from the ''people'' table where the
--  city equals to ''Boston''.

--   WHERE
--   city = 'Boston'
--   FROM people
--   SELECT *

-- ANSWER:
--   SELECT
--     *
--   FROM
--     people
--   WHERE
--     city = 'Boston'
