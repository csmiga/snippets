-- QUESTION:
-- Fill in the blanks to select all values from the "students" table in
-- which the field "university" equals "MIT".
--
--   SELECT
--     *
--   FROM
--     ________
--   WHERE
--     _____ = 'MIT';

-- ANSWER:
--   SELECT
--     *
--   FROM
--     students
--   WHERE
--     university = 'MIT';


-- QUESTION:
-- Rearrange the code to select students from MIT and Stanford, and order the
-- results by the ''university'' column.
--
--   = FROM students WHERE university
--   = SELECT name, university
--   = ORDER BY university
--   = IN ('Stanford', 'MIT')

-- ANSWER:
--   SELECT
--     name,
--     university
--   FROM
--     students
--   WHERE
--     university IN
--     (
--       'Stanford',
--       'MIT'
--     )
--   ORDER BY
--     university;


-- QUESTION:
-- Which keyword is the correct one for custom columns?
--
--   AS, LIKE, SIMILAR

-- ANSWER:
--   AS


-- QUESTION:
-- What is the name of the aggregate function for calculating the sum?
--
--   AGGR, SUM, AVG, SQRT

-- ANSWER:
--   SUM


-- QUESTION:
-- Drag and drop from the options below to select name and age from
-- "students", where age is greater than the average of all ages. Use a
-- subquery to calculate the average value of age.
--
--   ______
--     name,
--     age
--   FROM
--     students
--   _____ age >
--   (
--     SELECT
--       ___(age)
--     FROM
--       students);
--
--   SELECT, ORDER BY, WHERE, AVG, VALUE

-- ANSWER:
--   SELECT
--     name,
--     age
--   FROM
--     students
--   WHERE age >
--   (
--     SELECT
--       AVG(age)
--     FROM
--       students
--   );
