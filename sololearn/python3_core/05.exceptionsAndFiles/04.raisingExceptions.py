# Raising Exceptions:
# You can raise exceptions by using the raise statement.
print(1)
raise ValueError
print(2)
# Note: You need to specify the type of the exception raised.

# Raising Exceptions:
# Exceptions can be raised with arguments that give detail about them.
name = "123"
raise NameError("Invalid name!")

# Raising Exceptions:
# In except blocks, the raise statement can be used without arguments to
# re-raise whatever exception occurred.
try:
    num = 5 / 0
except:
    print("An error occurred")
    raise
