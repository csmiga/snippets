# Concatenation:
# As with integers and floats, strings in Python can be added, using a process
# called concatenation, which can be done on any two strings. When
# concatenating strings, it doesn't matter whether they've been created with
# single or double quotes.
print("Spam" + 'eggs')
print("First string" + ", " + "second string")

# Concatenation:
# Even if your strings contain numbers, they are still added as strings rather
# than integers. Adding a string to a number produces an error, as even though
# they might look similar, they are two different entities.
print("2" + "2")
print(1 + '2' + 3 + '4')
# Note: In future lessons, only the final line of error messages will be
# displayed, as it is the only one that gives details about the type of error
# that has occurred.

# String Operations:
# Strings can also be multiplied by integers. This produces a repeated version
# of the original string. The order of the string and the integer doesn't
# matter, but the string usually comes first.
#
# Strings can't be multiplied by other strings. Strings also can't be
# multiplied by floats, even if the floats are whole numbers.
print("spam" * 3)
print(4 * '2')
print('17' * '87')
print('pythonisfun' * 7.0)
