# In-Place Operators:
# In-place operators allow you to write code like 'x = x + 3' more concisely,
# as 'x += 3'. The same thing is possible with other operators such as -, *, /
# and % as well.
x = 2
print(x)
x += 3
print(x)

# In-Place Operators:
# These operators can be used on types other than numbers, as well, such as
# strings.
x = "spam"
print(x)
x += "eggs"
print(x)
# Note: Many other languages have special operators such as '++' as a shortcut
# for 'x += 1'. Python does not have these.
