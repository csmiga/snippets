# Boolean Logic
# Boolean logic is used to make more complicated conditions for if statements
# that rely on more than one condition. Python's Boolean operators are and,
# or, and not. The and operator takes two arguments, and evaluates as True if,
# and only if, both of its arguments are True. Otherwise, it evaluates to False.
print(1 == 1 and 2 == 2)
print(1 == 1 and 2 == 3)
print(1 != 1 and 2 == 2)
print(2 < 1 and 3 >  6)
# Python uses words for its Boolean operators, whereas most other languages use
# symbols such as &&, || and !.

# Boolean Or
# The or operator also takes two arguments. It evaluates to True if either
# (or both) of its arguments are True, and False if both arguments are False.
print(1 == 1 or 2 == 2)
print(1 == 1 or 2 == 3)
print(1 != 1 or 2 == 2)
print(2 < 1 or 3 >  6)

# Boolean Not
# Unlike other operators we've seen so far, not only takes one argument, and
# inverts it. The result of not True is False, and not False goes to True.
print(not 1 == 1)
print(not 1 > 7)
# Note: You can chain multiple conditional statements in an if statement using
# the Boolean operators.
