# Range:
# The range function creates a sequential list of numbers. The code below
# generates a list containing all of the integers, up to 10.
numbers = list(range(10))
print(numbers)
# Note: The call to list is necessary because range by itself creates a range
# object, and this must be converted to a list if you want to use it as one.

# Range:
# If range is called with one argument, it produces an object with values from
# 0 to that argument. If it is called with two arguments, it produces values
# from the first to the second.
numbers = list(range(3, 8))
print(numbers)
print(range(20) == range(0, 20))

# Range:
# range can have a third argument, which determines the interval of the
# sequence produced. This third argument must be an integer.
numbers = list(range(5, 20, 2))
print(numbers)
