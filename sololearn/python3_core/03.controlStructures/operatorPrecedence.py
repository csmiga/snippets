# Operator Precedence:
# Operator precedence is a very important concept in programming. It is an
# extension of the mathematical idea of order of operations (multiplication
# being performed before addition, etc.) to include other operators, such as
# those in Boolean logic.
# The below code shows that == has a higher precedence than or:
print(False == False or True)
print(False == (False or True))
print((False == False) or True)
# Note: Python's order of operations is the same as that of normal mathematics:
# parentheses first, then exponentiation, then multiplication/division, and
# then addition/subtraction.

# Operator Precedence:
# The following table lists all of Python's operators, from highest precedence
# to lowest.
"""
Operator                    Description
--------                    -----------
**                          Exponentiation (raise to the power)
~ + -                       Complement, unary plus and minus (method names for
                            the last two are +@ and -@)
* / % / /                   Multiply, divide, modulo and floor division
+ -                         Addition and subtraction
>> <<                       Right and left bitwise shift
&                           Bitwise 'AND'
^|                          Bitwise exclusive 'OR' and regular 'OR'
<= < > >=                   Comparison operators
<> == !=                    Equality operators
= %= /= //= -= += *= **=    Assignment operators
is is not                   Identity operators
in not in                   Membership operators
not or and                  Logical operators
"""
