# Printing Text:
# Let's start off by creating a short program that displays "Hello world!".
# In Python, we use the print statement to output text.

# Try It Yourself:
# https://code.sololearn.com/python
print('Hello world!')

# The print statement needs to be followed by parentheses, which enclose the
# output we want to generate.


# Question:
#   Drag & drop the correct option to output "Hi".
#     _____("Hi")

# Answer:
#   print


# Printing Text:
# The print statement can also be used to output multiple lines of text.

# For Example:
# Try It Yourself:
# https://code.sololearn.com/python
print('Hello world!')
print('Hello world!')
print('Spam and eggs...')

# Each print statement outputs text from a new line.

# Python code often contains references to the comedy group Monty Python. This
# is why the words, "spam" and "eggs" are often used as placeholder variables in
# Python where "foo" and "bar" would be used in other programming languages.


# Question:
#   Rearrange the code to generate the following output:
#   A
#   B
#   C

# Answer:
# Try It Yourself:
# https://code.sololearn.com/python
print(A)
print(B)
print(C)
