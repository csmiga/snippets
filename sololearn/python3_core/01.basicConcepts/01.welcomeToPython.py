# Welcome to Python!

# Python is a high-level programming language, with applications in numerous
# areas, including web programming, scripting, scientific computing, and
# artificial intelligence.

# It is very popular and used by organizations such as Google, NASA, the CIA,
# and Disney.

# In this course we will be learning Python version 3, which is the most recent
# major version of Python.


# Question:
#   Python is a:

# Answer:
#   Programming language


# Welcome to Python!:
# There are almost no limitations on what can be built using Python. These
# include standalone apps, web apps, games, data science and machine learning
# models, and much more.

# To use Python, you first need to install it on your computer. In our course,
# you will be able to run, save, and share your Python code on our Code
# Playground without installing any additional software.

# Fun fact: According to the creator Guido Van Rossum, the name of Python was
# derived from the British comedy series "Monty Python's Flying Circus".


# Question:
#   Can Python be used in Machine Learning?

# Answer:
#   Yes
