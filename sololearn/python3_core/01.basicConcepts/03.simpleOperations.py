# Simple Operations:
# Python has the capability of carrying out calculations.
# Enter a calculation directly into the Python console, and it will output the
# answer.

# Try It Yourself:
# https://code.sololearn.com/python
print(2 + 2)
print(5 + 4 - 3)

# Note: The spaces around the plus and minus signs here are optional (the
# code would work without them), but they make it easier to read.


# Question:
#   What does this code output?
#     print(1 + 2 + 3)

# Answer:
# Try It Yourself:
# https://code.sololearn.com/python
#   print(1 + 2 + 3)


# Simple Operations:
# Python also carries out multiplication and division, using an asterisk to
# indicate multiplication and a forward slash to indicate division. Use
# parentheses to determine which operations are performed first.

# Try It Yourself:
# https://code.sololearn.com/python
print(2 * (3 + 4))
print(10 / 2)

# Note: Using a single slash to divide numbers produces a decimal (or float,
# as it is called in programming). We'll have more about floats in a later
# lesson.


# Question:
#   Which option is output by this code?
#     print((4 + 8) / 2)

# Answer:
#   Try It Yourself:
#   https://code.sololearn.com/python
print((4 + 8) / 2)


# Simple Operations:
# The minus sign indicates a negative number.
# Operations are performed on negative numbers, just as they are on positive
# ones.

# Try It Yourself:
# https://code.sololearn.com/python
print(-7)
print((-7 + 2) * (-4))

# Note: The plus signs can also be put in front of numbers, but this has no
# effect, and is mostly used to emphasize that a number is positive to increase
# readability of code.

# Simple Operations:
# Dividing by zero in Python produces an error, as no answer can be calculated.

# Try It Yourself:
# https://code.sololearn.com/python
print(11 / 0)

# Note: In Python, the last line of an error message indicates the error's type.
# Read error messages carefully, as they often tell you how to fix a program!
