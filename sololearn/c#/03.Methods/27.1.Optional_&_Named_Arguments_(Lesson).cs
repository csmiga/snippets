// Optional Arguments

// When defining a method, you can specify a default value for optional
// parameters. Note that optional parameters must be defined after required
// parameters. If corresponding arguments are missing when the method is called,
// the method uses the default values.
// To do this, assign values to the parameters in the method definition, as
// shown in this example.

static int Pow(int x, int y = 2)
{
    int result = 1;
    
    for (int i = 0; i < y; i++)
    {
        result *= x;
    }
    return result;
}

// The Pow method assigns a default value of 2 to the y parameter. If we call
// the method without passing the value for the y parameter, the default value
// will be used.

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static int Pow(int x, int y = 2)
        {
            int result = 1;
            
            for (int i = 0; i < y; i++)
            {
                result *= x;
            }
            return result;
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine(Pow(6));
            Console.WriteLine(Pow(3, 4));
        }
    }
}

// NOTE:
// As you can see, default parameter values can be used for calling the same
// method in different situations without requiring arguments for every
// parameter.
// Just remember, that you must have the parameters with default values at the
// end of the parameter list when defining the method.


// QUESTION:
// What is the output of this code?
//
//   static int Vol(int x, int y = 3, int z = 1)
//   {
//       return x * y * z;
//   }
//   
//   static void Main(string[] args)
//   {
//       Console.WriteLine(Vol(2, 4));
//   }

// ANSWER:
//   8


// Named Arguments

// Named arguments free you from the need to remember the order of the
// parameters in a method call. Each argument can be specified by the matching
// parameter name.
// For example, the following method calculates the area of a rectangle by its
// height and width:

static int Area(int h, int w)
{
    return h * w;
}

// When calling the method, you can use the parameter names to provide the
// arguments in any order you like:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static int Area(int h, int w)
        {
            return h * w;
        }
        
        static void Main(string[] args)
        {
            int res = Area(w: 5, h: 8);
            Console.WriteLine(res);
        }
    }
}

// NOTE:
// Named arguments use the name of the parameter followed by a colon and the
// value.

// QUESTION:
// Call the method using named arguments with the values 5 for "from", 99 for
// "to" and 2 for "step":
//
//   static int calc(int from, int to, int step = 1)
//   {
//       int res = 0;
//       
//       for(int i = from; i < to; i += step)
//       {
//           res += i;
//       }
//       
//       return res;
//   }
//   
//   static void Main(string[] args)
//   {
//       int res = ____(_____ 2, ___ 99, _____ 5);
//       Console.WriteLine(res);
//   }

// ANSWER:
//   static int calc(int from, int to, int step = 1)
//   {
//       int res = 0;
//       
//       for(int i = from; i < to; i += step)
//       {
//           res += i;
//       }
//       
//       return res;
//   }
//   
//   static void Main(string[] args)
//   {
//       int res = calc(step: 2, to: 99, from: 5);
//       Console.WriteLine(res);
//   }

