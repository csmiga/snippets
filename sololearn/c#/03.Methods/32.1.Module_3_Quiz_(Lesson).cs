// QUESTION:
// Every C# program has the method:
//
//   Main
//   Console
//   Start
//   Program

// ANSWER:
//   Main


// QUESTION:
// Fill in the blanks to create a method that calculates and returns the sum of
// its parameters.
//
//   ___ Calc(int a, int b)
//   {
//       ______ a + b;
//   }

// ANSWER:
//   int Calc(int a, int b)
//   {
//       return a + b;
//   }


// QUESTION:
// Fill in the blanks to declare a method that has two int parameters with
// default values 6 and 8, respectively, and displays their product to the
// screen. Call the method in Main using named arguments.
//
//   static void Test(int x _ 6, int y = _)
//   {
//       Console.WriteLine(x * y);
//   }
//   
//   static void Main(string[] args)
//   {
//       Test(x: 7, __ 11);
//   }

// ANSWER:
//   static void Test(int x = 6, int y = 8)
//   {
//       Console.WriteLine(x * y);
//   }
//   
//   static void Main(string[] args)
//   {
//       Test(x: 7, y: 11);
//   }


// QUESTION:
// If a method does not return any value, you should use the return type:

// ANSWER:
//   void


// QUESTION:
// What is the output of this code?
//
//   static int Test(out int x, int y = 4)
//   {
//       x = 6;
//       return x * y;
//   }
//   
//   static void Main(string[] args)
//   {
//       int a;
//       int z = Test(out a);
//       Console.WriteLine(a + z);
//   }

// ANSWER:
//   30

