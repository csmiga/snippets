// Multiple Parameters

// You can have as many parameters as needed for a method by separating them
// with commas in the definition.
// Let's create a simple method that returns the sum of two parameters:

int Sum(int x, int y)
{
    return x + y;
}

// The Sum method takes two integers and returns their sum. This is why the
// return type of the method is int. Data type and name should be defined for
// each parameter.

// NOTE:
// Methods return values using the return statement.


// QUESTION:
// Fill in the missing parts of the following code to define a method that
// returns an int value and has two parameters.
//
//     ___ Test(int a_ int b)
//     {
//         // some code
//     }

// ANSWER:
//     int Test(int a, int b)
//     {
//         // some code
//     }


// A method call with multiple parameters must separate arguments with commas.
// For example, a call to Sum requires two arguments:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static int Sum(int x, int y)
        {
            return x + y;
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine(Sum(8, 6));
        }
    }
}

// In the call above, the return value was displayed to the console window.
// Alternatively, we can assign the return value to a variable, as in the code
// below:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static int Sum(int x, int y)
        {
            return x + y;
        }
        
        static void Main(string[] args)
        {
            int res = Sum(11, 42);
            Console.WriteLine(res);
        }
    }
}

// NOTE:
// You can add as many parameters to a single method as you want. If you have
// multiple parameters, remember to separate them with commas, both when
// declaring them and when calling the method.


// QUESTION:
// Fill in the blanks to declare a method, which returns the largest value of
// its parameters:
//
//   int Max(int a_ int b)
//   {
//       if (a > b);
//       {
//           ______ a;
//       }
//       else
//       }
//           return _;
//       }
//   }

// ANSWER:
//   int Max(int a, int b)
//   {
//       if (a > b);
//       {
//           return a;
//       }
//       else
//       }
//           return b;
//       }
//   }

