// Recursion

// A recursive method is a method that calls itself.
// One of the classic tasks that can be solved easily by recursion is
// calculating the factorial of a number.

// In mathematics, the term factorial refers to the product of all positive
// integers that are less than or equal to a specific non-negative integer (n).
// The factorial of n is denoted as n!

// For example:
4! = 4 * 3 * 2 * 1 = 24

// NOTE:
// A recursive method is a method that calls itself.


// QUESTION:
// What is the factorial of 5?
//
//   600
//   120
//   0
//   24

// ANSWER:
//   120 = 5 * 4 * 3 * 2 * 1


// As you can see, a factorial can be thought of as repeatedly calculating
// num * num - 1 until you reach 1.
// Based on this solution, let's define our method:

static int Fact(int num)
{
    if (num == 1)
    {
        return 1;
    }
    return num * Fact(num - 1);
}

// In the Fact recursive method, the if statement defines the exit condition, a
// base case that requires no recursion. In this case, when num equals one, the
// solution is simply to return 1 (the factorial of one is one).
// The recursive call is placed after the exit condition and returns num
// multiplied by the factorial of n-1.
// For example, if you call the Fact method with the argument 4, it will execute
// as follows:
//
// return 4 * Fact(3), which is 4 * 3 * Fact(2), which is 4 * 3 * 2 * Fact(1),
// which is 4 * 3 * 2 * 1

// Now we can call our Fact method from Main:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static int Fact(int num)
        {
            if (num == 1)
            {
                return 1;
            }
            return num * Fact(num - 1);
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine(Fact(6));
        }
    }
}

// NOTE:
// The factorial method calls itself, and then continues to do so, until the
// argument equals 1. The exit condition prevents the method from calling itself
// indefinitely.


// QUESTION:
// What prevents the recursive method to call itself forever?
//
//   the exit condition
//   the Main method
//   the static keyword

// ANSWER:
//   the exit condition

