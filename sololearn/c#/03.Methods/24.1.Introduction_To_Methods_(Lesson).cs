// What is a Method?

// A method is a group of statements that perform a particular task.
// In addition to the C# built-in methods, you may also define your own.

// Methods have many advantages, including:
// - Reusable code.
// - Easy to test.
// - Modifications to a method do not affect the calling program.
// - One method can accept many different inputs.

// NOTE:
// Every valid C# program has at least one method, the Main method.


// QUESTION:
// Every C# program starts with the method:
//
//   using
//   Console
//   Main
//   Start

// ANSWER:
//   Main


// Declaring Methods

// To use a method, you need to declare the method and then call it.
// Each method declaration includes:
//   - the return type
//   - the method name
//   - an optional list of parameters.

<return type> name(type1 par1, type2 par2, … , typeN parN)
{
    List of statements
}

// For example, the following method has an int parameter and returns the number
// squared:

int Sqr(int x)
{
    int result = x * x;
    return result;
}

// The return type of a method is declared before its name. In the example
// above, the return type is int, which indicates that the method returns an
// integer value. When a method returns a value, it must include a return
// statement. Methods that return a value are often used in assignment
// statements.

// Occasionally, a method performs the desired operations without returning a
// value. Such methods have a return type void. In this case, the method cannot
// be called as part of an assignment statement.

// NOTE:
// void is a basic data type that defines a valueless state.


// QUESTION:
// If you do not want your method to return a value, you should use the return
// type:

// ANSWER:
//   void


// Calling Methods

// Parameters are optional; that is, you can have a method with no parameters.
// As an example, let's define a method that does not return a value, and just
// prints a line of text to the screen.

static void SayHi()
{
    Console.WriteLine("Hello");
}

// Our method, entitled SayHi, returns void, and has no parameters.
// To execute a method, you simply call the method by using the name and any
// required arguments in a statement.

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void sayHi()
        {
            Console.WriteLine("Hello");
        }
        
        static void Main(string[] args)
        {
            sayHi();
        }
    }
}

// NOTE:
// The static keyword will be discussed later; it is used to make methods
// accessible in Main.


// QUESTION:
// Fill in the blanks to declare a method that does not return a value and
// displays "Welcome" to the screen:
//
//   static ____ Greet()
//   {
//       Console.WriteLine("Welcome");
//   _

// ANSWER:
//   static void Greet()
//   {
//       Console.WriteLine("Welcome");
//   }


// You can call the same method multiple times:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void SayHi()
        {
            Console.WriteLine("Hello");
        }
        
        static void Main(string[] args)
        {
            SayHi();
            SayHi();
            SayHi();
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// Drag and drop from the options below to declare a valid method and call it
// in Main:
//
//   ______ ____ Func()
//   {
//       Console.Write("test");
//   }
//   
//   static void Main(string[] args)
//   {
//       ____();
//   }
//
//   return  using  void  Func  static

// ANSWER:
//   static void Func()
//   {
//       Console.Write("test");
//   }
//   
//   static void Main(string[] args)
//   {
//       Func();
//   }

