// PROBLEM:
// Introduction to Methods

// Complete the method provided and call it in order to output "This is my first
// method".

// NOTE:
// Don't forget about parenthesis after the method name while calling it.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            // call the method
            myFunc();
        }
        
        static void myFunc()
        {
            // complete the method to output "This is my first method"
            Console.WriteLine("This is my first method");
        }
    }
}

