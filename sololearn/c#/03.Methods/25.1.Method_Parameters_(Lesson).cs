// Parameters

// Method declarations can define a list of parameters to work with.
// Parameters are variables that accept the values passed into the method when
// called.

// For example:
void Print(int x)
{
    Console.WriteLine(x);
}

// This defines a method that takes one integer parameter and displays its
// value.

// Parameters behave within the method similarly to other local variables. They
// are created upon entering the method and are destroyed upon exiting the
// method.


// QUESTION:
// Fill in the blanks to declare a method that takes one integer parameter and
// then displays the value divided by 2.
//
//   void MyFunc(___ x)
//   {
//       int result = x _ 2;
//       Console.WriteLine(______);
//   }

// ANSWER:
//   void MyFunc(int x)
//   {
//       int result = x / 2;
//       Console.WriteLine(result);
//   }


// Now you can call the method in Main and pass in the value for its parameters
// (also called arguments):

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Print(int x) 
        {
            Console.WriteLine(x);
        }
        
        static void Main(string[] args)
        {
            Print(42);
        }
    }
}

// NOTE:
// The value 42 is passed to the method as an argument and is assigned to the
// formal parameter x.


// QUESTION:
// Fill in the blanks to declare a method and call it from Main with the
// argument 88:
//
//   ______ void func(int x)
//   {
//       Console.WriteLine(x/2);
//   }
//   
//   static void Main(string[] args)
//   {
//       ____(__);
//   }

// ANSWER:
//   static void func(int x)
//   {
//       Console.WriteLine(x/2);
//   }
//   
//   static void Main(string[] args)
//   {
//       func(88);
//   }


// You can pass different arguments to the same method as long as they are of
// the expected type.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Func(int x)
        {
            Console.WriteLine(x * 2);
        }
        
        static void Main(string[] args)
        {
            Func(5);
            Func(12);
            Func(42);
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// How many times can you call a method with different arguments?
//
//   None
//   As many as you want
//   One

// ANSWER:
//   As many as you want

