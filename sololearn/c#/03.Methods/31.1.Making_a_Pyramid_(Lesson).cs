// Making a Pyramid

// Now, let's create a method that will display a pyramid of any height to the
// console window using star (*) symbols.
// Based on this description, a parameter will be defined to reflect the number
// of rows for the pyramid.

// So, let's start by declaring the method:

static void DrawPyramid(int n)
{
    // some code will go here
}

// DrawPyramid does not need to return a value and takes an integer parameter n.
// In programming, the step by step logic required for the solution to a problem
// is called an algorithm. The algorithm for MakePyramid is:
//   1. The first row should contain one star at the top center of the pyramid.
//      The center is calculated based on the number of rows in the pyramid.
//   2. Each row after the first should contain an odd number of stars
//      (1, 3, 5, etc.), until the number of rows is reached.

// Based on the algorithm, the code will use for loops to display spaces and
// stars for each row:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void DrawPyramid(int n)
        {
            for (int i = 1; i <= n; i++)
            {
                for (int j = i; j <= n; j++)
                {
                    Console.Write("  ");
                }
                
                for (int k = 1; k <= 2 * i - 1; k++)
                {
                    Console.Write("*"+" ");
                }
                    Console.WriteLine();
            }
        }
        
        static void Main(string[] args)
        {
            DrawPyramid(5);
        }
    }
}

// The first for loop that iterates through each row of the pyramid contains two
// for loops.
// The first inner loop displays the spaces needed before the first star symbol.
// The second inner loop displays the required number of stars for each row,
// which is calculated based on the formula (2 * i - 1) where i is the current
// row. The final Console.WriteLine(); statement moves the cursor to the next
// row.

// NOTE:
// Now, if we call the DrawPyramid method, it will display a pyramid having the
// number of rows we pass to the method.


// QUESTION:
// How many loops can you nest within each other?
//
//   Any
//   None
//   One

// ANSWER:
//   ANY

