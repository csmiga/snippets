// PROBLEM:
// Recursion

// Write a program to take N number as input and recursively calculate the sum
// of all numbers from 1 to N.

// Sample Input
// 5

// Sample Output
// 15

// Explanation
// 5 + 4 + 3 + 2 + 1 = 15

// NOTE:
// Don't forget about base case.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Sum(number));
        }
        
        static int Sum(int num)
        {
            // complete the recursive method
            if (num == 1)
            {
                return 1;
            }
            return num + Sum(num - 1);
        }
    }
}

