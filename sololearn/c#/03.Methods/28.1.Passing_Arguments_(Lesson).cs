// Passing Arguments

// There are three ways to pass arguments to a method when the method is called:
// By value, By reference, and as Output.

// By value copies the argument's value into the method's formal parameter.
// Here, we can make changes to the parameter within the method without having
// any effect on the argument.

// NOTE:
// By default, C# uses call by value to pass arguments.

// The following example demonstrates by value:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Sqr(int x)
        {
            x = x * x;
        }
        
        static void Main(string[] args)
        {
            int a = 3;
            Sqr(a);
            Console.WriteLine(a);
        }
    }
}

// In this case, x is the parameter of the Sqr method and a is the actual
// argument passed into the method.

// NOTE:
// As you can see, the Sqr method does not change the original value of the
// variable, as it is passed by value, meaning that it operates on the value,
// not the actual variable.

// QUESTION:
// What is the output of this code?
//
//   static void Test(int x)
//   {
//       x = 8;
//   }
//   
//   static void Main()
//   {
//       int a = 5;
//       Test(a);
//       Console.WriteLine(a);
//   }

// ANSWER:
//   5


// Passing by Reference

// Pass by reference copies an argument's memory address into the formal
// parameter. Inside the method, the address is used to access the actual
// argument used in the call. This means that changes made to the parameter
// affect the argument.
// To pass the value by reference, the ref keyword is used in both the call and
// the method definition:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Sqr(ref int x)
        {
            x = x * x;
        }
        
        static void Main(string[] args)
        {
            int a = 3;
            Sqr(ref a);
            
            Console.WriteLine(a);
        }
    }
}

// The ref keyword passes the memory address to the method parameter, which
// allows the method to operate on the actual variable.

// NOTE:
// The ref keyword is used both when defining the method and when calling it.


// QUESTION:
// Fill in the blanks to create a method that swaps the values of its two
// arguments.
//
//   void Swap(___ int x, ___ int y)
//   {
//       int temp;
//       temp = x;
//       x = y;
//       y = ____;
//   }

// ANSWER:
//   void Swap(ref int x, ref int y)
//   {
//       int temp;
//       temp = x;
//       x = y;
//       y = temp;
//   }


// Passing by Output

// Output parameters are similar to reference parameters, except that they
// transfer data out of the method rather than accept data in. They are defined
// using the out keyword.
// The variable supplied for the output parameter need not be initialized since
// that value will not be used. Output parameters are particularly useful when
// you need to return multiple values from a method.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void GetValues(out int x, out int y)
        {
            x = 5;
            y = 42;
        }
        
        static void Main(string[] args)
        {
            int a, b;
            GetValues(out a, out b);
            Console.WriteLine(a +" "+ b);
        }
    }
}

// Unlike the previous reference type example, where the value 3 was referred to
// the method, which changed its value to 9, output parameters get their value
// from the method (5 and 42 in the above example).

// NOTE:
// Similar to the ref keyword, the out keyword is used both when defining the
// method and when calling it.


// QUESTION:
// Fill in the blanks to ask for user input in the method and return the value
// entered using output parameters.
//
//   static void Ask(___ string name)
//   {
//       _____ = Console.ReadLine();
//   }
//   
//   static void Main(string[] args)
//   {
//       string nm;
//       Ask(___ nm);
//   }

// ANSWER:
//   static void Ask(out string name)
//   {
//       name = Console.ReadLine();
//   }
//   
//   static void Main(string[] args)
//   {
//       string nm;
//       Ask(out nm);
//   }

