// PROBLEM:
// Method overloading

// Complete the Add() method so that it will calculate the sum of two numbers
// given as arguments.
// Overload it in order to do the same operation with double type values.
// The calls of the methods are already written.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Add(10, 12));
            Console.WriteLine(Add(1.5, 2.9));
        }
        
        // complete the method to sum
        static int Add(int x, int y)
        {
            int sum = x + y;
            return sum;
        }
        
        // overload it for double type
        static double Add(double x, double y)
        {
            double sum = x + y;
            return sum;
        }
    }
}

