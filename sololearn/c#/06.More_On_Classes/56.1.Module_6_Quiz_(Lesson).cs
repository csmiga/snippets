// QUESTION:
// Fill in the blanks to declare a destructor for the class Book.
//
//     _____ Book
//     {
//         _Book()
//         {
//             // some code
//         }
//     }

//ANSWER:
//     class Book
//     {
//         ~Book()
//         {
//             // some code
//         }
//     }


// QUESTION:
// What is the output of this code?
//
//     class Temp
//     {
//         public int num = 2;
//         public Temp() {num++;}
//         ~Temp() { num++; }
//     }
//     
//     static void Main(string[] args)
//     {
//         Temp t = new Temp();
//         Console.WriteLine(t.num);
//     }

//ANSWER:
//     3


// QUESTION:
// Fill in the blanks to make a valid working program.
//
//     ______ void Func__
//     {
//         Console.Write("Hi there");
//     }
//     
//     static void Main(string[] args)
//     {
//         Func();
//     }

//ANSWER:
//     static void Func()
//     {
//         Console.Write("Hi there");
//     }
//     
//     static void Main(string[] args)
//     {
//         Func();
//     }


// QUESTION:
// What is the value of x after this code?
//
//      int[] arr = {8, 5, 4};
//      Array.Reverse(arr);
//      double x = Math.Pow(arr[0], 2);

//ANSWER:
//     16


// QUESTION:
// Fill in the blanks to define a valid indexer:
//
//     public int ____ [int index]
//     {
//         ___
//         {
//             return n[index];
//         }
//         ___
//         {
//             n[index] = value;
//         }

// ANSWER:
//     public int this [int index]
//     {
//         get
//         {
//             return n[index];
//         }
//         set
//         {
//             n[index] = value;
//         }


// QUESTION:
// Fill in the blanks to overload the minus operator for the T class:
//
//     public ______ T ________ - (T a, T b)
//     {
//         T res = new T(a.num - b.num);
//         ______ res;
//     }

//ANSWER:
//     public static T operator - (T a, T b)
//     {
//         T res = new T(a.num - b.num);
//         return res;
//     }

