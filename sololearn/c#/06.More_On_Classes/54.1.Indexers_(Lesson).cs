// Indexers

// An indexer allows objects to be indexed like an array.
// As discussed earlier, a string variable is actually an object of the String
// class. Further, the String class is actually an array of Char objects. In
// this way, the string class implements an indexer so we can access any
// character (Char object) by its index:

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "Hello World";
            char x = str[4];
            Console.WriteLine(x);
        }
    }
}

// NOTE:
// Arrays use integer indexes, but indexers can use any type of index, such as
// strings, characters, etc.


// QUESTION:
// What is the output of this code?
//
//     string str = "I know C#";
//     char x = str[3];
//     Console.WriteLine(x);

// ANSWER:
//     n


// Declaration of an indexer is to some extent similar to a property. The
// difference is that indexer accessors require an index.

// Like a property, you use get and set accessors for defining an indexer.
// However, where properties return or set a specific data member, indexers
// return or set a particular value from the object instance.
// Indexers are defined with the this keyword.

// For example:
class Clients
{
    private string[] names = new string[10];
    
    public string this[int index]
    {
        get
        {
            return names[index];
        }
        set
        {
            names[index] = value;
        }
    }
}

// As you can see, the indexer definition includes the this keyword and an
// index, which is used to get and set the appropriate value.
// Now, when we declare an object of class Clients, we use an index to refer to
// specific objects like the elements of an array:

// try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        class Clients
        {
            private string[] names = new string[10];
            
            public string this[int index]
            {
                get
                {
                    return names[index];
                }
                
                set
                {
                    names[index] = value;
                }
            }
        }
        
        static void Main(string[] args)
        {
            Clients c = new Clients();
            c[0] = "Dave";
            c[1] = "Bob";
            
            Console.WriteLine(c[1]);
        }
    }
}

// NOTE:
// You typically use an indexer if the class represents a list, collection, or
// array of objects.


// QUESTION:
// Fill in the blanks to define a valid indexer:
//
//     class Person
//     {
//         private string name;
//         
//         public char ____[int index]
//         {
//             get
//             {
//                 return ____[index];
//             }
//         }
//     }

// ANSWER:
//     class Person
//     {
//         private string name;
//         
//         public char this[int index]
//         {
//             get
//             {
//                 return name[index];
//             }
//         }
//     }

