// PROBLEM:
// Operator Overloading

// You and your friend are playing a game as one team. Each player must pass 2
// rounds and gets points for each round passed.

// The program you are given creates two Score objects where each round scores
// are stored (they are passed to a constructor).

// Overload the + operator for the Score class to calculate the team score for
// every round.

// NOTE:
// Remember to use operator keyword while operator overloading.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Score tm1 = new Score(2, 3);
            Score tm2 = new Score(4, 2);
            Score finalScores = tm1 + tm2;
            
            Console.WriteLine("Round 1: " + finalScores.round1Score);
            Console.WriteLine("Round 2: " + finalScores.round2Score);
        }
    }
    class Score
    {
        public int round1Score {get; set;}
        public int round2Score {get; set;}
        
        public Score(int r1, int r2)
        {
            round1Score = r1;
            round2Score = r2;
        }
        
        // your code goes here
        public static Score operator + (Score player1, Score player2)
        {
            int round1 = player1.round1Score + player2.round1Score;
            int round2 = player1.round2Score + player2.round2Score;
            Score results = new Score(round1, round2);
            return results;
        }
    }
}

