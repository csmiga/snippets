// PROBLEM:
// Structs

// A cuboid is a three-dimensional shape with a length, width, and a height.
// The program you are given takes takes those dimensions as inputs, defines
// Cuboid struct and creates its object.
// Complete the program by creating a constructor, which will take the length,
// the width, and the height as parameters and assign them to its struct
// members.
// Also complete Volume() and Perimeter() methods inside the struct, to
// calculate and return them, so that the given methods calls work correctly.

// Sample Input
// 5
// 4
// 5

// Sample Output
// Volume: 100
// Perimeter: 56

// NOTE:
// Cuboid volume: length * width * height.
// Cuboid perimeter: 4 * (length + width + height).

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;
            int y;
            
            int length = Convert.ToInt32(Console.ReadLine());
            int width = Convert.ToInt32(Console.ReadLine());
            int height = Convert.ToInt32(Console.ReadLine());
            
            Cuboid cuboid = new Cuboid();
            
            x = cuboid.Volume(length, width, height);
            y = cuboid.Perimeter(length, width, height);
            
            Console.WriteLine("Volume: " + x);
            Console.WriteLine("Perimeter: " + y);
        }
    }
    struct Cuboid
    {
        public int length;
        public int width;
        public int height;
        
        //create a constructor
        public Cuboid (int length, int width, int height)
        {
            this.length = length;
            this.width = width;
            this.height = height;
        }
        
        //complete this method
        public int Volume(int length, int width, int height)
        {
            return length * width * height;
        }
        //complete this method
        public int Perimeter(int length, int width, int height)
        {
            return 4 * (length + width + height);
        }
    }
}

