// QUESTION:
// Can you inherit from a struct?
//
//     No
//     Yes

//ANSWER:
//     No


// QUESTION:
// What is the output of this code?
//
//     enum Test { A, B, C=5, D };
//     static void Main(string[] args)
//     {
//         int x = (int)Test.D;
//         Console.WriteLine(x);
//     }

//ANSWER:
//     6


// QUESTION:
// int is a Struct.
//
//     False
//     True

//ANSWER:
//     True


// QUESTION:
// Which of the following is the correct way to define a variable of the struct
// Person declared below?
//
//     struct Person
//     {
//         private string name;
//         private int age;
//     }
//
//     Person e = new Person;
//     Person p;
//     Person p = new Person("Bob");

//ANSWER:
//     Person p;


// QUESTION:
// What is the value of x after this code?
//
//     int x = 0;
//     try
//     {
//         x /= x;
//         x += 1;
//     }
//     catch (Exception e)
//     {
//         x += 3;
//     }
//     finally
//     {
//         x += 4;
//     }

//ANSWER:
//     7


// QUESTION:
// Fill in the blanks to handle exceptions.
//
//     int x = 0;
//     ___
//     {
//         x = Convert.ToInt32("AAA");
//     }
//     _____ (Exception e)
//     {
//           Console.WriteLine("Error");
//     }

//ANSWER:
//     int x = 0;
//     try
//     {
//         x = Convert.ToInt32("AAA");
//     }
//     catch (Exception e)
//     {
//           Console.WriteLine("Error");
//     }

