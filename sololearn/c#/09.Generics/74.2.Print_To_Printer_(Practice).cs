// PROBLEM:
// Generic Methods

// You are writing a program that can output the value of a variable of any
// type. It takes a string, an integer, and a double value as input and then it
// should output them.
// Create a generic method Print for a Printer class to execute the given calls
// correctly.

// Sample Input
// Hello
// 14
// 7.6

// Sample Output
// Showing Hello
// Showing 14
// Showing 7.6

// NOTE:
// Note that there is no object of Printer class, so the generic method should
// be static.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            int intNum = Convert.ToInt32(Console.ReadLine());
            double doubNum = Convert.ToDouble(Console.ReadLine());
            
            Printer.Print<string>( ref text);
            Printer.Print<int>( ref intNum);
            Printer.Print<double>( ref doubNum);
        }
    }
    class Printer
    {
        //your code goes here
        public static void Print <T> (ref T a)
        {
            Console.WriteLine("Showing " + a);
        }
    }
}

