// QUESTION:
// What is the output of this code?
//
//     List<int> li = new List<int>();
//     li.Add(59);
//     li.Add(72);
//     li.Add(95);
//     li.RemoveAt(1);
//     for (int x = 0; x < li.Count; x++)
//     {
//         Console.Write(li[x] + " ");
//     }
//
//     59 72 95
//     72 95
//     59 72
//     59 95

//ANSWER:
//     59 95


// QUESTION:
// Drag and drop from the options below to define a generic method that swaps
// the values of its arguments and returns the value of the first argument.
//
//     static _ Swap ___ (ref T a, ref T b)
//     {
//         T temp = a;
//         a = b;
//         b = ____;
//         ______ a;
//     }
//
//     T  a  return  b  temp  <T>

//ANSWER:
//     static T Swap <T> (ref T a, ref T b)
//     {
//         T temp = a;
//         a = b;
//         b = temp;
//         return a;
//     }


// QUESTION:
// Reorder the code to declare a Queue of integers, add values 1 through 5 and
// print the contents of the Queue.
//
//     Queue<int> q = new QUEUE<int>();
//     for (int x = 1; x <= 5; x++)
//     foreach (int val in q)
//     Console.Write(val + " ");
//     q.Enqueu(x));

//ANSWER:
//     Queue<int> q = new QUEUE<int>();
//     for (int x = 1; x <= 5; x++)
//     q.Enqueu(x));
//     foreach (int val in q)
//     Console.Write(val + " ");


// QUESTION:
// Drag and drop from the options below to define a generic class, which has a
// member method that returns the value of x.
//
//     _____ Temp ___
//     {
//         T x;
//         public _ Func()
//         {
//             ______ x;
//         }
//     }
//
//     <x>  static  x  T <T>  class  return

//ANSWER:
//     class Temp <T>
//     {
//         T x;
//         public T Func()
//         {
//             return x;
//         }
//     }


// QUESTION:
// What is the output of this code?
//
//     List<int> a = new List<int>();
//     a.Add(5);
//     a.Add(2);
//     a.Add(8);
//     a.Reverse();
//     Console.Write(a[1]);

//ANSWER:
//     2


// QUESTION:
// Drag and drop from the options below to create a List that will contain
// objects of type Student.
//
//     List<_______> L = ___ ____ <Student>();
//
//     List  new  <List>  Main  Student  public

//ANSWER:
//     List<Student> L = new List <Student>();


// QUESTION:
// What is the output of this code?
//
//     BitArray ba1 = new BitArray(4);
//     BitArray ba2 = new BitArray(4);
//     ba1.SetAll(true);
//     ba2.SetAll(false);
//     ba1.Set(2, false);
//     ba2.Set(3, true);
//     Console.Write(ba1.And(ba2).Get(2));
//
//     False
//     True

//ANSWER:
//     False


// QUESTION:
// What is the output of this code?
//
//     Stack<int> s = new Stack<int>();
//     s.Push(4);
//     s.Push(5);
//     s.Push(4);
//     s.Pop();
//     Console.Write(s.Peek());

//ANSWER:
//     5


// QUESTION:
// Fill in the blanks to declare a dictionary to map names and ages and John's,
// Ann's and Peter's ages. Print John's age.
//
//     Dictionary<______, int> d = ___ Dictionary<string, ___ >();
//     _.Add("John", 24);
//     d.Add("Ann", 18);
//     d.Add("Peter", 27);
//     string name = "John";
//     Console.Write(d_name_);

//ANSWER:
//     Dictionary<string, int> d = new Dictionary<string, int >();
//     d.Add("John", 24);
//     d.Add("Ann", 18);
//     d.Add("Peter", 27);
//     string name = "John";
//     Console.Write(d[name]);

