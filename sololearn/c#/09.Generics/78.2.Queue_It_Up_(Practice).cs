// PROBLEM:
// Queue

// Write a program that will take 3 numbers as input and store them in a defined
// queue.
// Also, add code to output the sorted sequence of elements in the queue,
// separated by a space.

// Sample Input
// 6
// 3
// 14

// Sample Output
// Queue: 6 3 14
// Sorted: 3 6 14

using System;
using System.Collections.Generic;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> q = new Queue<int>();
            
            while (q.Count < 3)
            {
                int num = Convert.ToInt32(Console.ReadLine());
                //your code goes here
                q.Enqueue(num);
            }
            
            Console.Write("Queue: ");
            foreach (int i in q)
            {
                Console.Write(i + " ");
            }
            
            Console.WriteLine();
            //your code goes here
            int[] arr = q.ToArray();
            Array.Sort(arr);
            
            Console.Write("Sorted: ");
            //your code goes here
            foreach (int j in arr)
            {
                Console.Write(j + " ");
            }
        }
    }
}

