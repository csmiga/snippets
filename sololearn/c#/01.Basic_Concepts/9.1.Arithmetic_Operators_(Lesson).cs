// OPERATORS

// An operator is a symbol that performs mathematical or logical manipulations.

// Arithmetic Operators

// C# supports the following arithmetic operators:

// Operator             |  Symbol  |  Form
// Addition             |  +       |  x + y
// Subtraction          |  -       |  x - y
// Multiplication       |  *       |  x * y
// Division             |  /       |  x / y
// Modulus (Remainder)  |  %       |  x % y

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            int y = 4;
            Console.WriteLine(x - y);
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// Fill in the blanks to display the result of the multiplication of x and y.
//
//   int x = 42;
//   int y = 7;
//   int z = x _ y;
//   Console.WriteLine(_);

// ANSWER:
//   int x = 42;
//   int y = 7;
//   int z = x * y;
//   Console.WriteLine(z);


// The division operator (/) divides the first operand by the second. If the
// operands are both integers, any remainder is dropped in order to return an
// integer value.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10 / 4;
            Console.WriteLine(x);
        }
    }
}

// NOTE:
// Division by 0 is undefined and will crash your program.


// QUESTION:
// What is the output of this code?
//
//   int x = 16;
//   int y = 5;
//   Console.WriteLine(x / y);

// ANSWER:
//    3


// The modulus operator (%) is informally known as the remainder operator
// because it returns the remainder of an integer division.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 25 % 7;
            Console.WriteLine(x);
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// Which operator is used to determine the remainder?
//
//   %  +  *

// ANSWER:
//   %


// OPERATOR PRECEDENCE

// Operator precedence (PEMDAS) determines the grouping of terms in an
// expression, which affects how an expression is evaluated. Certain operators
// take higher precedence over others; for example, the multiplication operator
// has higher precedence than the addition operator.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 4 + 3 * 2;
            Console.WriteLine(x);
        }
    }
}

// The program evaluates 3 * 2 first, and then adds the result to 4.
// As in mathematics, using parentheses alters operator precedence.

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = (4 + 3) * 2;
            Console.WriteLine(x);
        }
    }
}

// The operations within parentheses are performed first. If there are
// parenthetical expressions nested within one another, the expression within
// the innermost parentheses is evaluated first.

// NOTE:
// If none of the expressions are in parentheses (EMDmAS), multiplicative
// (multiplication, division, modulus) operators will be evaluated before
// additive (addition, subtraction) operators. Operators of equal precedence are
// evaluated from left to right.


// QUESTION:
// Fill in the missing parentheses to have x equal 15.
//
//   int x = _2 + 3_ * 3;
//   Console.WriteLine(x);

// ANSWER:
//   int x = (2 + 3) * 3;
//   Console.WriteLine(x);

