// PROBLEM:
// Getting User Input

// You are organizing an important event and you should greet each participant
// with a welcome message․ The given code outputs the message.

// Task
// Complete the program to take the participant's name as input and assign it
// to the "name" variable.
//
// Sample Input
// Tom
//
// Sample Output
// Hello Tom. Welcome to our event

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            
            // take the name as input
            name = Console.ReadLine();
            Console.WriteLine("Hello {0}. Welcome to our event", name);
        }
    }
}

