// PROBLEM:
// Modulus

// We need to pack 142 toys in boxes. Each box holds 15 toys.

// Task
// Write a program to calculate and output how many toys will be left after
// packing.

// Hint
// Find the remainder from dividing 142 by 15.

// NOTE:
// Use the modulo operator (%) to get the remainder.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            // your code goes here
            int remainingToys = 142 % 15;
            Console.Write(remainingToys);
        }
    }
}

