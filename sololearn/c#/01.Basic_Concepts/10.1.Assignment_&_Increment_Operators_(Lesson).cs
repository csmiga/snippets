// ASSIGNMENT OPERATORS

// The = assignment operator assigns the value on the right side of the operator
// to the variable on the left side.

// C# also provides compound assignment operators that perform an operation and
// an assignment in one statement.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 42;
            x += 2;  // equivalent to x = x + 2
            Console.WriteLine(x);
            
            x -= 6;  // equivalent to x = x - 6
            Console.WriteLine(x);
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// What is the alternative for x = x + 5?
//
//   x = y + 5;
//   x -= 4;
//   x += 5;

// ANSWER:
//   x += 5;


// The same shorthand syntax applies to the multiplication, division, and
// modulus operators.

x *= 8;  // equivalent to x = x * 8
x /= 5;  // equivalent to x = x / 5
x %= 2;  // equivalent to x = x % 2

// NOTE:
// The same shorthand syntax applies to the multiplication, division, and
// modulus operators.


// QUESTION:
// Fill in the missing part of the following code to divide x by 3 using the
// shorthand division operator.
//
//   int x = 42;
//   x _= _;

// ANSWER:
//   int x = 42;
//   x /= 3;


// INCREMENT OPERATOR

// The increment operator is used to increase an integer's value by one, and is
// a commonly used C# operator.

x++;  // equivalent to x = x + 1

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            x++;
            Console.WriteLine(x);
        }
    }
}

// NOTE:
// The increment operator is used to increase an integer's value by one.


// QUESTION:
// What is the output of this code?
//   int x = 6;
//   x++;
//   Console.WriteLine(x);

// ANSWER:
//   7


// PREFIX & POSTFIX FORMS

// The increment operator has two forms, prefix and postfix

++x;  // prefix
x++;  // postfix

// Prefix increments the value, and then proceeds with the expression.
// Postfix evaluates the expression and then performs the incrementing.

// Prefix example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 3;
            int y = ++x;
            Console.WriteLine(x + " " + y);
        }
    }
}

// Postfix example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 3;
            int y = x++;
            Console.WriteLine(x + " " + y);
        }
    }
}

// NOTE:
// The prefix example increments the value of x, and then assigns it to y.
// The postfix example assigns the value of x to y, and then increments x.


// QUESTION:
// What's the difference between ++x and x++?
// Select All That Apply
//   x++ uses x's value then increments it
//   ++x uses x's value before incrementing it
//   ++x increments x's value before using it
//   x++ increments x's value before using it

// ANSWER:
//   x++ uses x's value then increments it
//   ++x increments x's value before using it


// DECREMENT OPERATOR

// The decrement operator (--) works in much the same way as the increment
// operator, but instead of increasing the value, it decreases it by one

--x;  // prefix
x--;  // postfix

// NOTE:
// The decrement operator (--) works in much the same way as the increment
// operator.


// QUESTION:
// Fill in the missing operator to decrease the value of x by one.
//   int x = 42;
//   x__;
//   Console.WriteLine(x);

// ANSWER:
//   int x = 42;
//   x--;
//   Console.WriteLine(x);

// REMINDER:
// The prefix example increments the value of x, and then assigns it to y.
// The postfix example assigns the value of x to y, and then increments x.

