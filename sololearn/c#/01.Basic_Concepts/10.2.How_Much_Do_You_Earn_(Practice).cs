// PROBLEM:
// Assignment Operators

// Write a program to take monthly salary as input, and calculate and output the
// annual income.

// Sample Input
// 800

// Sample Output
// 9600

// NOTE:
// Use the *= shorthand for easier calculus.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
          int salary = Convert.ToInt32(Console.ReadLine());
          
          // your code goes here
          int annualIncome = 12;
          salary *= annualIncome;
          Console.Write(salary);
        }
    }
}

