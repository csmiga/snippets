// PROBLEM:
// Your First Program

// Write a program to print "C# is cool".
// Note that the sentence starts with a capital letter.

// HINT:
// Both Console.Write() or Console.WriteLine() methods can be used.

// NOTE:
// Remember to enclose the text into double quotes and end the statement
// with a semicolon (;).

using System;

namespace MyApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // output "C# is cool"
            Console.Write("C# is cool");
            Console.WriteLine("C# is cool");
        }
    }
}

