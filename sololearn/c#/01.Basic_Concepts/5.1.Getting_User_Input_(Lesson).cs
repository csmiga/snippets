// USER INPUT

// You can also prompt the user to enter data and then use the
// Console.ReadLine method to assign the input to a string variable.

// The following example asks the user for a name and then displays a message
// that includes the input:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string yourName;
            Console.WriteLine("What is your name?");
            yourName = Console.ReadLine();
            Console.WriteLine("Hello {0}", yourName);
        }
    }
}

// The Console.ReadLine method waits for user input and then assigns it to the
// variable. The next statement displays a formatted string containing Hello
// with the user input. For example, if you enter "David", the output will be
// Hello David.

// NOTE:
// Note the empty parentheses in the ReadLine method. This means that it does
// not take any arguments.


// QUESTION:
// Drag and drop from the options below to take user input and store it in the
// temp variable:
//
//   string temp;
//   temp =_______._______();
//
//   temp  WriteLine  Console  ReadLine  Main

// ANSWER:
//   string temp;
//   temp = Console.ReadLine();


// The Console.ReadLine() method returns a string value. If you are expecting
// another type of value (such as int or double), the entered data must be
// converted to that type. This can be done using the Convert.ToXXX methods,
// where XXX is the .NET name of the type that we want to convert to. For
// example, methods include "Convert.ToDouble" and "Convert.ToBoolean".
// For integer conversion, there are three alternatives available based on the
// bit size of the integer: Convert.ToInt16, Convert.ToInt32 and
// Convert.ToInt64. The default int type in C# is 32-bit.
// Let’s create a program that takes an integer as input and displays it in a
// message:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("You are {0} years old", age);
        }
    }
}

// If, in the program above, a non-integer value is entered (for example,
// letters), the Convert will fail and cause an error.


// QUESTION:
// Drag and drop from the options below to make the following program work:
//
//   double n;
//   string x = "77";
//   n = _______.________(x);
//
//   Write  Console  Convert  ToDouble

// ANSWER:
//   double n;
//   string x = "77";
//   n = Convert.ToDouble(x);

