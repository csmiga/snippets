// YOUR FIRST C# PROGRAM

// You can run, save, and share your C# code on our Code Playground, without
// installing any additional software.

// NOTE:
// Reference this lesson if you need to install the software on your computer.

// To create a C# program, you need to install an Integrated Development
// Environment (IDE) with coding and debugging tools. We will be using Visual
// Studio Community Edition, which is available to download for free. After
// installing it, choose the default configuration.

// Next, click
// File > New > Project

// and then choose Console Application as shown below:
// Enter a name for your Project and click OK.

// NOTE:
// Console application uses a text-only interface. We chose this type of
// application to focus on learning the fundamentals of C#.


// QUESTION:
// What is the name of the IDE used to create C# programs?
//
//   Visual Studio
//   Visual Maya
//   CStudio
//   3D Studio

// ANSWER:
//   Visual Studio


// Visual Studio will automatically generate some code for your project:

using System;
 
namespace SoloLearn
{
   class Program
   {
      static void Main(string[] args)
      {
      }
   }
}

// You will learn what each of the statements does in the upcoming lessons.
// For now, remember that every C# console application must contain a method
// (a function) named Main. Main is the starting point of every application,
// i.e. the point where our program starts execution from.

// We will learn about classes, methods, arguments, and namespaces in the
// upcoming lessons.


// QUESTION:
// Every console application in C# should contain:
//
//   Main method
//   variables
//   input-output

// ANSWER:
//   Main method


// To run your program, press Ctrl + F5. You will see the following screen:

// C:\Windows\System32\cmd.exe
// Press any key to continue ...

// This is a console window. As we did not have any statements in our Main
// method, the program just produces a general message. Pressing any key will
// close the console.

// NOTE:
// Congratulations, you just created your first C# program.


// QUESTION:
// Which type of application uses a text-only interface?
//
//   Console Application
//   Windows Application
//   Mobile Application

// ANSWER:
//   Console Application

