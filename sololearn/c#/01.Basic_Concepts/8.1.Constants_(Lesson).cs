// CONSTANTS

// Constants store a value that cannot be changed from their initial assignment.
// To declare a constant, use the const modifier.

// For example:

const double PI = 3.14;

// The value of const PI cannot be changed during program execution. For
// example, an assignment statement later in the program will cause an error:

const double PI = 3.14;
PI = 8;  // error

// NOTE:
// Constants must be initialized with a value when declared.


// QUESTION:
// Fill in the blank to make the variable num a constant.
//
//    _____ int num = 2;

// ANSWER:
//    const int num = 2;

