// QUESTION:
// Rearrange the code to create a valid program.
//
//   staic void Main(string[] args)
//   {
//       string msg = "Hello";
//   }
//   Console.WriteLine(msg);

// ANSWER:
//   staic void Main(string[] args)
//   {
//       string msg = "Hello";
//       Console.WriteLine(msg);
//   }


// QUESTION:
// Drag and drop from the options below to display the value of x to the screen.
//
//   static void ____ (string[] args)
//   {
//       int x = (4 + 3) * 2;
//       _______._________(x);
//   }
//
//   Main  WriteLine  Class  Console  double

// ANSWER:
//   static void Main (string[] args)
//   {
//       int x = (4 + 3) * 2;
//       Console.WriteLine(x);
//   }


// QUESTION:
// What is the output of this code?
//   int a = 4;
//   int b = 6;
//   b = a++;
//   Console.WriteLine(++b);

// ANSWER:
//   5


// QUESTION:
// Fill in the blanks to declare two variables of type int and display their
// sum to the screen.
//
//   int x = 8;
//   ___ y = 15;
//   Console.WriteLine(x _ y);

// ANSWER:
//   int x = 8;
//   int y = 15;
//   Console.WriteLine(x + y);


// QUESTION:
// What is the output of this code?
//
//   int x = 15;
//   int y = 6;
//   x %= y;
//   Console.WriteLine(x);

// ANSWER:
//   3

