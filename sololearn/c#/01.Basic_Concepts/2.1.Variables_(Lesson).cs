// VARIABLES

// Programs typically use data to perform tasks.

// Creating a variable reserves a memory location, or a space in memory, for
// storing values. It is called variable because the information stored in that
// location can be changed when the program is running.

// To use a variable, it must first be declared by specifying the name and data
// type.

// A variable name, also called an identifier, can contain letters, numbers and
// the underscore character (_) and must start with a letter or underscore.
// Although the name of a variable can be any set of letters and numbers, the
// best identifier is descriptive of the data it will contain. This is very
// important in order to create clear, understandable and readable code!

// NOTE:
// For example, firstName and lastName are good descriptive variable names,
// while abc and xyz are not.


// QUESTION:
// Which is a valid C# variable name?
//
//   #PersonName#
//   1Star
//   Bad_Var

// ANSWER:
//   Bad_Var


// Variable Types

// A data type defines the information that can be stored in a variable, the
// size of needed memory and the operations that can be performed with the
// variable.

// For example, to store an integer value (a whole number) in a variable, use
// the int keyword:

int myAge;

// The code above declares a variable named myAge of type integer.

// NOTE:
// A line of code that completes an action is called a statement. Each
// statement in C# must end with a semicolon.

int myAge = 18;

// or later in your code:

int myAge;
myAge = 18;

// Remember that you need to declare the variable before using it.


// QUESTION:
// Fill in the blanks to declare a variable named num of type integer and assign
// 42 to it.
//
//   ___ num;
//   num _ 42;

// ANSWER:
//   int num;
//   num = 42;


// Built-in Data Types

// There are a number of built-in data types in C#. The most common are:
//   int -    integer.
//   float -  floating point number.
//   double - double-precision version of float.
//   char -   a single character.
//   bool -   Boolean that can have only one of two values: True or False.
//   string - a sequence of characters.

// The statements below use C# data types:

int x = 42;
double pi = 3.14;
char y = 'Z';
bool isOnline = true;
string firstName = "David";

// NOTE:
// Note that char values are assigned using single quotes and string
// values require double quotes. You will learn how to perform different
// operations with variables in the upcoming lessons!


// QUESTION:
// Drag and drop the correct data types from the options below.
//
//   ____ a = false;
//   double b = 4.22;
//   ______ c = "Hi";
//   int d = 11;
//
//   string  bool  int  double

// ANSWER:
//   bool a = false;
//   double b = 4.22;
//   string c = "Hi";
//   int d = 11;

