// WELCOME TO C#

// C# is an elegant object-oriented language that enables developers to build a
// variety of secure and robust applications that run on the .NET Framework. You
// can use C# to create Windows applications, Web services, mobile applications,
// client-server applications, database applications, and much, much more.

// NOTE:
// You will learn more about these concepts in the upcoming lessons!


// QUESTION:
// C# applications run:
//
//    only on Linux
//    on the .NET Framework
//    using Java

// ANSWER:
//    on the .NET Framework


// The .NET Framework consists of the Common Language Runtime (CLR) and the .NET
// Framework Class Library (FCL).

// The CLR is the foundation of the .NET Framework. It manages code at execution
// time, providing core services such as memory management, code accuracy, and
// many other aspects of your code. The class library is a collection of
// classes, interfaces, and value types that enable you to accomplish a range of
// common programming tasks, such as data collection, file access, and working
// with text.

// C# programs use the .NET Framework Class Library extensively to do common
// tasks and provide various functionalities.

// NOTE:
// These concepts might seem complex, but for now just remember that
// applications written in C# use the .NET Framework and its components.


// QUESTION:
// Which one is NOT part of the .NET Framework?
//
//    Operation System
//    .Net Framewwork Class Library (FCL)
//    Common Language Runtime (CLR)

// ANSWER:
//    Operation System

