// COMMENTS

// Comments are explanatory statements that you can include in a program to
// benefit the reader of your code. The compiler ignores everything that appears
// in the comment, so none of that information affects the result.

// A comment beginning with two slashes (//) is called a single-line comment.
// The slashes tell the compiler to ignore everything that follows, until the
// end of the line.

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            // Prints Hello
            Console.WriteLine("Hello");
        }
    }
}

// NOTE:
// When you run this code, Hello will be displayed to the screen. The
// "// Prints Hello" line is a comment and will not appear as output.


// QUESTION:
// What is the output of this code?
//
//   int x = 8;
//   // x = 3;
//   Console.WriteLine(x);
//
//   error  3  8

// ANSWER:
//   8


// MULTI-LINE COMMENTS

// Comments that require multiple lines begin with /* and end with */ at the
// end of the comment block.
// You can place them on the same line or insert one or more lines between them.

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Some long 
                comment text
            */
            int x = 42;
            Console.WriteLine(x);
        }
    }
}

// NOTE:
// Adding comments to your code is good programming practice. It facilitates a
// clear understanding of the code for you and for others who read it.


// QUESTION:
// Fill in the blanks to make the text a comment.
//
//   __ Declaring an integer
//   and printing it __
//   int x = 42;
//   Console.WriteLine(x);

// ANSWER:
//   /* Declaring an integer
//   and printing it */
//   int x = 42;
//   Console.WriteLine(x);

