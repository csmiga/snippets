// DISPLAYING OUTPUT

// Most applications require some input from the user and give output as a
// result.

// To display text to the console window you use the Console.Write or
// Console.WriteLine methods. The difference between these two is that
// Console.WriteLine is followed by a line terminator, which moves the cursor
// to the next line after the text output.

// The program below will display "Hello World!" to the console window:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}

// NOTE:
// Note the parentheses after the WriteLine method. This is the way to pass
// data, or arguments, to methods. In our case WriteLine is the method and
// we pass "Hello World!" to it as an argument. String arguments must be
// enclosed in quotation marks.


// QUESTION:
// Drag and drop from the options below to display "Learning C#".
//
//   Console._________(_____________);
//
//   WriteLine  print  Read  "Learning C#"

// ANSWER:
//   Console.WriteLine("Learning C#");


// We can display variable values to the console window:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 89;
            Console.WriteLine(x);
        }
    }
}


// To display a formatted string, use the following syntax:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            double y = 20;
            Console.WriteLine("x = {0}; y = {1}", x, y);
        }
    }
}

// As you can see, the value of x replaced {0} and the value of y replaced {1}.

// NOTE:
// You can have as many variable placeholders as you need.
// (i.e.: {3}, {4}, etc.).


// QUESTION:
// What is the output of this code?
//
//   int a = 4;
//   int b = 2;
//   Console.Write(a);
//   Console.Write(b);

// ANSWER:
//   42

