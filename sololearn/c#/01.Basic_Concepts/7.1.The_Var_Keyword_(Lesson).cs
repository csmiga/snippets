// THE VAR KEYWORD

// A variable can be explicitly declared with its type before it is used.
// Alternatively, C# provides a handy function to enable the compiler to
// determine the type of the variable automatically based on the expression it
// is assigned to.

// The var keyword is used for those scenarios:

var num = 15;

// NOTE:
// The code above makes the compiler determine the type of the variable. Since
// the value assigned to the variable is an integer, the variable will be
// declared as an integer automatically.


// QUESTION:
// What is the type of the temp variable?
//
//   var temp = 14.55;
//
//   Double  Integer  Boolean  String

// Answer:
//   Double


// Variables declared using the var keyword are called implicitly typed
// variables. Implicitly typed variables must be initialized with a value.

// For example, the following program will cause an error:

var num;
num = 42;

// NOTE:
// Although it is easy and convenient to declare variables using the var
// keyword, overuse can harm the readability of your code. Best practice is to
// explicitly declare variables.


// QUESTION:
// What is the output of this code?
//
//   var n1;
//   n1 = true;
//   Console.WriteLine(n1);
//
//   true  error  1

// ANSWER:
//   error

