// PROBLEM:
// Array Properties & Methods

// Write a program to take 5 numbers as input, then calculate and output the sum
// of the maximum and the minimum inputted values.

// Sample Input
// 5
// 6
// 14
// 2
// 1

// Sample Output
// 15

// Explanation
// The minimum value is 1, the maximum is 14. So 14 + 1 = 15 should be output.

// Hint
// Create an array, use while loop to store the inputted numbers in it, and then
// do the calculations.

// NOTE:
// Use the Min and Max methods to get the minimum and maximum values.

using System;
using System.Linq;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            // your code goes here
            int[] arr = new int[5];
            int i = 0;
            
            while (i < 5)
            {
                arr[i] = Convert.ToInt32(Console.ReadLine());
                i++;
            }
            
            Console.Write(arr.Min() + arr.Max());
        }
    }
}

