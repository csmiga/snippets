// PROBLEM:
// Words

// The program you are given defines an array with 10 words and takes a letter
// as input.
// Write a program to iterate through the array and output words containing the
// taken letter.
// If there is no such word, the program should output "No match".

// Sample Input
// u

// Sample Output
// fun

using System;

namespace Code_Coach_Challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words =
            {
                "home",
                "programming",
                "victory",
                "C#",
                "football",
                "sport",
                "book",
                "learn",
                "dream",
                "fun"
            };
            
            //string letter = Console.ReadLine();
            string letter =  "u";
            int count = 0;
            
            // your code goes here
            bool IsMatch = false;
            
            foreach (string w in words)
            {
                if (w.Contains(letter))
                {
                    Console.WriteLine(w);
                    IsMatch = true;
                }
            }
            
            if (!IsMatch) Console.WriteLine("No match");
        }
    }
}

