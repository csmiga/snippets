// QUESTION:
// An array is a:
//
//   none of these
//   reference type
//   value type

// ANSWER:
//   reference type


// QUESTION:
// Fill in the blanks to print all elements of the array.
//
//   int[] arr = {0, 5, 3, 2, 1 };
//   
//   foreach (int item __ arr)
//   {
//       Console.WriteLine(____);
//   }

// ANSWER:
//   int[] arr = {0, 5, 3, 2, 1 };
//   
//   foreach (int item in arr)
//   {
//       Console.WriteLine(item);
//   }


// QUESTION:
// What is the output of this code?
//
//   string s = "SoloLearn";
//   Console.Write(s[6]);

// ANSWER:
//   a


// QUESTION:
// How many elements can the following array store?
//
//   int[ , , ] array = new int[4, 5, 3];
//
//   12
//   59
//   60
//   4

// ANSWER:
//   60


// QUESTION:
// What is the output of this code?
//
//   string s = "SoloLearn";
//   int x = s.Length;
//   int y = s.IndexOf("e");
//   Console.Write(x%y);

// ANSWER:
//   4

