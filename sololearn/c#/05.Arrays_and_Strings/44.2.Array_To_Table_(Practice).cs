// PROBLEM:
// Multidimensional Arrays

// Follow multidimensional array that is given:

int[,] num = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

// Complete the program to output this array in the form of a table
// (without separation):
// 123
// 456
// 789

// NOTE:
// Use nested for loops to iterate through rows and columns.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] num = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            // your code goes here
            for (int f1 = 0; f1 < 3; f1++)
            {
                for (int f2 = 0; f2 < 3; f2++)
                {
                    Console.Write(num[f1, f2]);
                }
                
                Console.WriteLine();
            }
        }
    }
}

