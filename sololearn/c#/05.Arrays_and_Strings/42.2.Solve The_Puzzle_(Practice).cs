// PROBLEM:
// Arrays

// A game machine has 5 games installed on it:

string[] games = { "Alien Shooter", "Tic Tac Toe", "Snake", "Puzzle", "Football" };

// Write a program to take N number as input and output the corresponding game
// with N index from the array.
// If user enters an invalid number that is out of array range, the program
// should output "Invalid number".

// Sample Input
// 3

// Sample Output
// Puzzle

// NOTE:
// Remember the first element of the array has 0 index.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] games = { "Alien Shooter", "Tic Tac Toe", "Snake", "Puzzle", "Football" };
            
            // your code goes here
            //int game = Convert.ToInt32(Console.ReadLine());
            int game = 3;
            
            if (game > 4)
            {
                Console.WriteLine("Invalid number");
            }
            else
            {
                Console.WriteLine(games[game]);
            }
        }
    }
}

