// PROBLEM:
// Properties

// The program you are given should output the account number on the bank card.
// But something is wrong.
// Create a get property to access the corresponding class member and also fix
// the output.
// In order to execute the output you should pass the property to
// Console.WriteLine() method.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Card card1 = new Card();

            // fix the output
            //Console.WriteLine(card1.accountNum);
            Console.WriteLine(card1.AccountNum);
        }
    }
    class Card
    {
        private string accountNum = "0011592048120";
        
        // create a property to get the account
        public string AccountNum
        {
            get { return accountNum; }
            set { accountNum = value; }
        }
    }
}

