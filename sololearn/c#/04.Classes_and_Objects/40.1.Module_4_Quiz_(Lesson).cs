// QUESTION:
// Objects of a class are stored on the:
//
//   heap
//   property
//   stack

// ANSWER:
//   heap


// QUESTION:
// Fill in the blanks to instantiate an object of the class Cat, passing to the
// constructor the value 12. Then call the Meow method for that object:
//
//   Cat c = ___ Cat( __ );
//   __ Meow();

// ANSWER:
//   Cat c = new Cat( 12 );
//   c.Meow();


// QUESTION:
// To make a member of a class accessible from outside the class declaration,
// you should declare it as:
//
//   void
//   private
//   public

// ANSWER:
//   public


// QUESTION:
// Which statement is true?
//
//   An object is a member of a class.
//   An object is an instance of a class.
//   An object is a method of a class.

// ANSWER:
//   An object is an instance of a class.


// QUESTION:
// Fill in the blanks to declare a class Student, with one public method called
// Hello. The Hello method displays "hi" to the screen.
//
//   _____ Student
//   {
//       ______ void Hello()
//       {
//           Console.WriteLine("hi");
//       }
//   }

// ANSWER:
//   class Student
//   {
//       public void Hello()
//       {
//           Console.WriteLine("hi");
//       }
//   }


// QUESTION:
// Fill in the blanks to declare a constructor that has one parameter and
// assigns it to the age member:
//
//   class Dog
//   {
//       private int age;
//       
//       ______ Dog(___ val)
//       {
//           age = ___;
//       }
//   }

// ANSWER:
//   class Dog
//   {
//       private int age;
//       
//       public Dog(int val)
//       {
//           age = val;
//       }
//   }


// QUESTION:
// Which accessor is used to read the value of a member?
//
//   get
//   return
//   void
//   set

// ANSWER:
//   get

