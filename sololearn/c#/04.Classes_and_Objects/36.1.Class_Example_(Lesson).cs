// Example of a Class

// Let’s create a Person class:
class Person
{
    int age;
    string name;
    
    public void SayHi()
    {
        Console.WriteLine("Hi");
    }
}

// The code above declares a class named Person, which has age and name fields
// as well as a SayHi method that displays a greeting to the screen.
// You can include an access modifier for fields and methods (also called
// members) of a class.

// Access modifiers are keywords used to specify the accessibility of a member.
// A member that has been defined public can be accessed from outside the class,
// as long as it's anywhere within the scope of the class object. That is why
// our SayHi method is declared public, as we are going to call it from outside
// of the class.

// NOTE:
// You can also designate class members as private or protected. This will be
// discussed in greater detail later in the course. If no access modifier is
// defined, the member is private by default.


// QUESTION:
// Fill in the blanks to create a class called Car.
//
//   _____ Car
//   _
//       string color;
//       int year;
//   _

// ANSWER:
//   class Car
//   {
//       string color;
//       int year;
//   }


// Example of a Class

// Now that we have our Person class defined, we can instantiate an object of
// that type in Main.
// The new operator instantiates an object and returns a reference to its
// location:

// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        class Person
        {
            int age;
            string name;
            
            public void SayHi()
            {
                Console.WriteLine("Hi");
            }
        }
        
        static void Main(string[] args)
        {
            Person p1 = new Person();
            p1.SayHi();
        }
    }
}

// The code above declares a Person object named p1 and then calls its public
// SayHi() method.

// NOTE:
// Notice the dot operator (.) that is used to access and call the method of the
// object.


// QUESTION:
// Fill in the blanks to create an object of type Car and call its horn()
// method.
//
//   Car c = ___ Car();
//   ______();

// ANSWER:
//
//   Car c = new Car();
//   c.horn();


// You can access all public members of a class using the dot operator.
// Besides calling a method, you can use the dot operator to make an assignment
// when valid.

// For example:
// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        class Dog
        {
            public string name;
            public int age;
        }
        
        static void Main(string[] args)
        {
            Dog bob = new Dog();
            bob.name = "Bobby";
            bob.age = 3;
            Console.WriteLine(bob.age);
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// Assign 7 to the age property of the object.
//
//   Dog d = new Dog();
//   __age = _;

// ANSWER:
//   Dog d = new Dog();
//   d.age = 7;

