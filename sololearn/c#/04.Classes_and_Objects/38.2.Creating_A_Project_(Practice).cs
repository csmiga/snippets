// PROBLEM:
// Constructors

// Your graphic application needs to report that a new project has been created
// successfully once the "Create" button has been pressed.
// Complete the given class by adding a constructor that will show message
// "Project created" once the operation is done.

// NOTE:
// You need to execute the output inside the constructor.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Project project = new Project();
        }
    }
    
    class Project
    {
        // create a constructor to show "Project created"
        public Project()
        {
            Console.Write("Project created");
        }
    }
}

