// The for Loop

// A for loop executes a set of statements a specific number of times, and has
// the syntax:

for (init; condition; increment)
{
    statement(s);
}

// A counter is declared once in init.
// Next, the condition evaluates the value of the counter and the body of the
// loop is executed if the condition is true.

// After loop execution, the increment statement updates the counter, also
// called the loop control variable.

// The condition is again evaluated, and the loop body repeats, only stopping
// when the condition becomes false.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int x = 10; x < 15; x++)
            {
                Console.WriteLine("Value of x: {0}", x);
            }
        }
    }
}

//NOTE:
// Note the semicolons in the syntax.


// QUESTION:
// Drag and drop from the options below to create a general for loop:
//
//   for (_________; ______; ___)
//   {
//       Console.WriteLine(x);
//   }
//
//   int x = 5  while  x < 10  for  x++

// ANSWER:
//   for (int x = 5; x < 10; x++)
//   {
//       Console.WriteLine(x);
//   }


// Compound arithmetic operators can be used to further control loop iterations.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int x = 0; x < 10; x += 3)
            {
                Console.WriteLine(x);
            }
        }
    }
}

// You can also decrement the counter:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int x = 10; x > 0; x -= 2)
            {
                Console.WriteLine(x);
            }
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// Fill in the blanks to print the EVEN values from 0 to 100 using a for loop:
//
//   ___ (int x = 0; x < 100; x += _)
//   {
//       Console.WriteLine(____);
//   }

// ANSWER:
//   for (int x = 0; x < 100; x += 2)
//   {
//       Console.WriteLine(x);
//   }


// The init and increment statements may be left out, if not needed, but
// remember that the semicolons are mandatory.

// For example, the init can be left out:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            
            for ( ; x > 0; x -= 3)
            {
                Console.WriteLine(x);
            }
        }
    }
}

// You can have the increment statement in the for loop body:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            
            for ( ; x > 0 ; )
            {
                Console.WriteLine(x);
                x -= 3;
            }
        }
    }
}

// NOTE:
// for (; ;) {} is an infinite loop.


// QUESTION:
// How many times will this loop run?
//
//   int x = 1;
//   
//   for ( ; x < 7; )
//   {
//       x += 2;
//   }

// ANSWER:
//   3

