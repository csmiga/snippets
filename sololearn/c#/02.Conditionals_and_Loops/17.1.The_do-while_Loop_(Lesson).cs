// do-while

// A do-while loop is similar to a while loop, except that a do-while loop is
// guaranteed to execute at least one time.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            
            do
            {
                Console.WriteLine(a);
                a++;
            }
            while (a < 5);
        }
    }
}

// Note the semicolon after the while statement.


// QUESTION:
// Fill in the blanks to create a valid loop.
//
//   int x = 0;
//   
//   __
//   {
//       Console.WriteLine(x);
//       x += 2;
//   } _____ (x < 10)_

// ANSWER:
//   int x = 0;
//   
//   do
//   {
//       Console.WriteLine(x);
//       x += 2;
//   } while (x < 10);


// do-while vs. while

// If the condition of the do-while loop evaluates to false, the statements in
// the do will still run once:

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 42;
            
            do
            {
                Console.WriteLine(x);
                x++;
            }
            while(x < 10);
        }
    }
}

// NOTE:
// The do-while loop executes the statements at least once, and then tests the
// condition.
// The while loop executes the statement only after testing condition.


// QUESTION:
// What is the output of this code?
//
//   int a = 2;
//   
//   do
//   {
//       a += 3;
//   }
//   while (a < 4);
//
//   Console.Write(a);

// ANSWER:
//   5

