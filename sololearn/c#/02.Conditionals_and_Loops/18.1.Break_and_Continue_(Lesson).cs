// break

// We saw the use of break in the switch statement.
// Another use of break is in loops: When the break statement is encountered
// inside a loop, the loop is immediately terminated and the program execution
// moves on to the next statement following the loop body.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            
            while (num < 20)
            {
                if (num == 5)
                {
                    break;
                }
                
                Console.WriteLine(num);
                num++;
            }
        }
    }
}

// NOTE:
// If you are using nested loops (i.e., one loop inside another loop), the break
// statement will stop the execution of the innermost loop and start executing
// the next line of code after the block.


// QUESTION:
// What is the largest number that will be printed by this code?
//
//   for (int x = 1; x < 8; x++)
//   {
//       if (x > 5)
//       {
//           break;
//       }
//       
//       Console.WriteLine(x);
//   }

// ANSWER:
//   5


// continue

// The continue statement is similar to the break statement, but instead of
// terminating the loop entirely, it skips the current iteration of the loop
// and continues with the next iteration.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    continue;
                }
                
                Console.WriteLine(i);
            }
        }
    }
}

// NOTE:
// As you can see, number 5 is not printed, as the continue statement skips the
// remaining statements of that iteration of the loop.


// QUESTION:
// Fill in the blanks to print only even numbers.
//
//   for (int x=0; x < 99; x++)
//   _
//       __ (x%2 != 0)
//       {
//           _________
//       }
//       
//       Console.WriteLine(x);
//     }

// ANSWER:
//   for (int x=0; x < 99; x++)
//   {
//       if (x%2 != 0)
//       {
//           continue;
//       }
//       
//       Console.WriteLine(x);
//   }

