// The if Statement

// The if statement is a conditional statement that executes a block of code
// when a condition is true.

// The general form of the if statement is:

if (condition)
{
    // Execute this code when condition is true
}

// The condition can be any expression that returns true or false.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 8;
            int y = 3;
            
            if (x > y)
            { 
                Console.WriteLine("x is greater than y");
            }
        }
    }
}

// The code above will evaluate the condition x > y. If it is true, the code
// inside the if block will execute.

// When only one line of code is in the if block, the curly braces can be
// omitted.

// For example:
if (x > y)
{
    Console.WriteLine("x is greater than y");
}

// QUESTION:
// Fill in the blanks to display Welcome to the screen when age is greater than
// 16.
//
//   int age = 24;
//
//   __ (age _ 16)
//   {
//       Console.WriteLine("Welcome");
//   }

// ANSWER:
//   int age = 24;
//
//   if (age > 16)
//   {
//       Console.WriteLine("Welcome");
//   }


// Relational Operators

// Use relational operators to evaluate conditions. In addition to the less
// than (<) and greater than (>) operators, the following operators are
// available:

// Operator | Description              | Example
// >=       | Greater than or equal to | 7 => 4 True
// <=       | Less than or equal to    | 7 <= 4 False
// ==       | Equal to                 | 7 == 4 False
// !=       | Not equal to             | 7 != 4 True

// Example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 7, b = 7;
            
            if (a == b)
            {
                Console.WriteLine("Equal");
            }
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// Which is the correct operator for equality testing?
//
//   <=
//   >=
//   ==
//   !=

// ANSWER:
//   ==


// The else Clause

// An optional else clause can be specified to execute a block of code when the
// condition in the if statement evaluates to false.

// Syntax:
if (condition) 
{
   // statements
}
else
{
   // statements
}

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int mark = 85;
            
            if (mark < 50) 
            {
                Console.WriteLine("You failed.");
            }
            else
            {
                Console.WriteLine("You passed.");
            }
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// The else Clause
// Fill in the blanks to find the larger of two variables.
//
//   int a = 42;
//   int b = 88;
//   
//   __ (a > b)
//   {
//       Console.WriteLine(a);
//   }
//   ____
//   {
//       Console.WriteLine(_);
//   }

// ANSWER:
//   int a = 42;
//   int b = 88;
//   
//   if (a > b)
//   {
//       Console.WriteLine(a);
//   }
//   else
//   {
//       Console.WriteLine(b);
//   }


// Nested if Statements

// You can also include, or nest, if statements within another if statement.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int mark = 100;
            
            if (mark >= 50)
            {
                Console.WriteLine("You passed.");
                
                if (mark == 100)
                {
                    Console.WriteLine("Perfect!");
                }
            }
            else
            {
                Console.WriteLine("You failed.");
            }
        }
    }
}

// You can nest an unlimited number of if-else statements.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 17;
            
            if (age > 14)
            {
                if(age > 18)
                {
                    Console.WriteLine("Adult");
                }
                else
                {
                    Console.WriteLine("Teenager");
                }
            }
            else
            {
                if (age > 0)
                {
                    Console.WriteLine("Child");
                }
                else
                {
                    Console.WriteLine("Something's wrong");
                }
            }
        }
    }
}

// NOTE: Remember that all else clauses must have corresponding if statements.


// QUESTION:
// Nested if Statements
// What is the output of this code?
//
//   int a = 8;
//   int b = ++a;
//   
//   if (a > 5)
//   {
//       b -= 3;
//   {
//   else
//   {
//       b = 9;
//   }
//   Console.WriteLine(b);

// ANSWER:
//   6


// The if-else if Statement

// The if-else if statement can be used to decide among three or more actions.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 33;
            
            if (x == 8)
            {
                Console.WriteLine("Value of x is 8");
            }
            else if (x == 18)
            {
                Console.WriteLine("Value of x is 18");
            }
            else if (x == 33)
            {
                Console.WriteLine("Value of x is 33");
            }
            else
            {
                Console.WriteLine("No match");
            }
        }
    }
}

// NOTE:
// Remember, that an if can have zero or more else if's and they must come
// before the last else, which is optional.
// Once an else if succeeds, none of the remaining else if's or else clause will
// be tested.


// QUESTION:
// The if-else if Statement
// How many nested if statements can an if statement contain?
//
//   None
//   As many as you want
//   Only two

// ANSWER:
//   As many as you want

