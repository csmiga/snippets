// Basic Calculator

// Now let's create a simple project that repeatedly asks the user to enter two
// values and then displays their sum, until the user enters exit.
// We start with a do-while loop that asks the user for input and calculates the
// sum:

do
{
    Console.Write("x = ");
    int x = Convert.ToInt32(Console.ReadLine());

    Console.Write("y = ");
    int y = Convert.ToInt32(Console.ReadLine());

    int sum = x + y;
    Console.WriteLine("Result: {0}", sum);
}
while(true);

// This code will ask for user input infinitely. Now we need to handle the
// "exit".

// NOTE:
// If the user enters a non-integer value, the program will crash from a
// conversion error. We will learn how to handle errors like that in the coming
// modules.


// QUESTION:
// How many times would this loop run?
//
//   do { }
//   while(false);
//
//   infinite
//   none
//   one

// ANSWER:
//   none


// If the user enters "exit" as the value of x, the program should quit the
// loop. To do this, we can use a break statement:

Console.Write("x = ");
string str = Console.ReadLine();

if (str == "exit")
{
    break;
}

int x = Convert.ToInt32(str);

// Here we compare the input with the value "exit" and break the loop.
// So the whole program looks like:

do
{
    Console.Write("x = ");
    string str = Console.ReadLine();
    
    if (str == "exit")
    {
        break;
    }
    
    int x = Convert.ToInt32(str);
    Console.Write("y = ");
    int y = Convert.ToInt32(Console.ReadLine());

    int sum = x + y;
    Console.WriteLine("Result: {0}", sum);
}
while (true);

// NOTE:
// If the user enters "exit" as the value of x, the program should quit the
// loop.


// QUESTION:
// Which of the following is used to take user input?
//
//   Console.Write
//   Console.ReadLine
//   Convert.ToInt32

// ANSWER:
//   Console.ReadLine

