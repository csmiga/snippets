// PROBLEM:
// Multiple of 3

// You are an elementary school teacher explaining multiplication to students.
// You are going to use multiplication by 3 as your example.

// The program you are given takes N number as input. Write a program to output
// all numbers from 1 to N, replacing all numbers that are multiples of 3 by
// "*".

// Sample Input
// 7

// Sample Output
// 12 * 45 * 7

// NOTE:
// The N number is a multiple of 3 if N%3==0.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = Convert.ToInt32(Console.ReadLine());
            
            // your code goes here
            // MY SOLUTION
            int num = 1;
            
            while (num <= number)
            {
                if (num%3 == 0)
                {
                    Console.Write("*");
                }
                else
                {
                    Console.Write(num);
                }
                
            num++;
            }
            
            // CLEANER SOLUTION
            /*
            for (int num = 1; num <= number; num++)
            {
                if (num%3 == 0)
                {
                    Console.Write("*");
                }
                else
                {
                    Console.Write(num);
                }
            }
            */
        }
    }
}

