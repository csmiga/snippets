// PROBLEM:
// The while Loop

// You work on a particular carousel at Disney. You have to count from 3 to 0
// on the screen before running the carousel.

// Task
// Write a program to output all the numbers from 3 to 0 in this format:
// 3
// 2
// 1
// 0

// NOTE:
// Notice that each number should be outputted on a new line.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 3;
            
            // your code goes here
            while(number >= 0)
            {
                Console.WriteLine(number);
                --number;
            }
        }
    }
}

