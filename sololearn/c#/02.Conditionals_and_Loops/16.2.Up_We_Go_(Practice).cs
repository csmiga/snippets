// PROBLEM:
// The for Loop

// Anna's hummingbird, now believed to be the fastest bird in the world relative
// to its size, can reach speeds of 80 km per hour.

// Task
// Write a program to output how many kilometers it will travel each hour over 5
// hours of flight.

// Output
// 80
// 160
// 240
// 320
// 400

// Hint
// Simply multiply 80 by a counter for each iteration. Use the for loop to
// perform the multiplications iteratively.

// Note that in this case, it's better to start the loop counter from 1.
// Remember the semicolon (;) after initialization and the condition in the
// syntax.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            // your code goes here
            for (int i = 1; i < 6; i++)
            {
                int distance = 80 * i;
                Console.WriteLine(distance);
            }
        }
    }
}

