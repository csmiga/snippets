// while

// A while loop repeatedly executes a block of code as long as a given condition
// is true.

// For example, the following code displays the numbers 1 through 5:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 1;
            
            while(num < 6)
            {
                Console.WriteLine(num);
                num++;
            }
        }
    }
}

// The example above declares a variable equal to 1 (int num = 1). The while
// loop checks the condition (num < 6) and, if true, executes the statements in
// its body, which increment the value of num by one, before checking the loop
// condition again.

// After the 5th iteration, num equals 6, the condition evaluates to false, and
// the loop stops running.

// NOTE:
// The loop body is the block of statements within curly braces.


// QUESTION:
// Fill in the blanks to display the value of x to the screen three times.
//
//   int x = 42;
//   int num = 0;
//   
//   _____ (num < 3)
//   {
//       Console.WriteLine(_);
//       num++;
//   }

// ANSWER:
//   int x = 42;
//   int num = 0;
//   
//   while (num < 3)
//   {
//       Console.WriteLine(x);
//       num++;
//   }


// The compound arithmetic operators can be used to further control the number
// of times a loop runs.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 1;
            
            while(num < 6)
            {
                Console.WriteLine(num);
                num += 2;
            }
        }
    }
}

// NOTE:
// Without a statement that eventually evaluates the loop condition to false,
// the loop will continue indefinitely.


// QUESTION:
// Fill in the blanks to increment the value of num by 2 to display only even
// values.
//
//   int num = 0;
//   
//   while (___ < 100)
//   {
//       Console.WriteLine(num);
//       num _= _;
//   }

// ANSWER:
//   int num = 0;
//   
//   while (num < 100)
//   {
//       Console.WriteLine(num);
//       num += 2;
//   }


// We can shorten the previous example, by incrementing the value of num right
// in the condition:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            
            while(++num < 6)
                Console.WriteLine(num);
        }
    }
}

// NOTE:
// What do you think, is there a difference between while(num++ < 6) and
// while(++num < 6)?

// Yes! The loop while(++num < 6) will execute 5 times, because pre-increment
// increases the value of x before checking the num < 6 condition, while post
// increment will check the condition before increasing the value of num, making
// while(num++ < 6) execute 6 times.


// QUESTION:
// How many times will the following loop execute?
//
//   int x = 1;
//   
//   while (x++ < 5)
//   {
//       if (x % 2 == 0)
//       {
//           x += 2;
//       }
//   }

// Answer:
//   2

