// Logical Operators

// Logical operators are used to join multiple expressions and return true or
// false.

// Operator | Name of Operator | Form
// &&       | AND Operator     | x && y
// ||       | OR Operator      | x || y
// !        | NOT Operator     | !x

// The AND operator (&&) works the following way:

// Left Operand | Right Operand | Result
// false        | false         | false
// false        | true          | false
// true         | false         | false
// true         | true          | true

// For example, if you wish to display text to the screen only if age is greater
// than 18 AND money is greater than 100:

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 42;
            double money = 540;
            
            if (age > 18 && money > 100)
            {
                Console.WriteLine("Welcome");
            }
        }
    }
}

// The AND operator was used to combine the two expressions.

// NOTE:
// With the AND operator, both operands must be true for the entire expression
// to be true.


// QUESTION:
// The result of a && b is true if:
//
//   Both a and b are false
//   Both a and b are true
//   Never
//   Either a or b is true

// ANSWER:
//   Both a and b are true


// AND

// You can join more than two conditions:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 42;
            int grade = 75;
            
            if (age > 16 && age < 80 && grade > 50)
            {
                Console.WriteLine("Hey there");
            }
        }
    }
}

// NOTE:
// The entire expression evaluates to true only if all of the conditions are
// true.


// QUESTION:
// How many && operators can be used in one if statement?
//
//   Two
//   Only one
//   As many as you want

// ANSWER:
//   As many as you want


// The OR Operator

// The OR operator (||) returns true if any one of its operands is true.

// Left Operand | Right Operand | Result
// false        | false         | false
// false        | true          | true
// true         | false         | true
// true         | true          | true

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 18;
            int score = 85;
            
            if (age > 20 || score > 50)
            {
                Console.WriteLine("Welcome");
            }
        }
    }
}

// NOTE:
// You can join any number of logical OR statements you want.
// In addition, multiple OR and AND statements may be joined together.


// QUESTION:
// What is the output of this code?
//
//   int x = 5; int y = 12;
//   
//   if (x > 10 || y / x > 1)
//   {
//       Console.Write(y - x);
//   }
//   else
//   {
//       Console.Write(y);
//   }
//
//   12  5  7

// ANSWER:
//   7


// Logical NOT
// The logical NOT (!) operator works with just a single operand, reversing its
// logical state. Thus, if a condition is true, the NOT operator makes it false,
// and vice versa.

// Right Operand | Result
// true          | false
// false         | true

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 8;
            
            if ( !(age > 16) )
            {
                Console.Write("Your age is less than 16");
            }
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// If a is true and b is false, what is the result of !(a && b)?
//
//   false
//   true
//   undefined

// ANSWER:
//   true

