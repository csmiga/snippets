// The ? : Operator

// Consider the following example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 42;
            string msg;
            
            if (age >= 18)
            {
                msg = "Welcome";
            }
            else
            {
                msg = "Sorry";
            }
            Console.WriteLine(msg);
        }
    }
}

// The code above checks the value of the age variable and displays the
// corresponding message to the screen.

// This can be done in a more elegant and shorter way by using the ?: operator,
// which has the following form:

Exp1 ? Exp2 : Exp3;

// The ?: operator works the following way: Exp1 is evaluated. If it is true,
// then Exp2 is evaluated and becomes the value of the entire expression. If
// Exp1 is false, then Exp3 is evaluated and its value becomes the value of the
// expression.

// So, the example above can be replaced by the following:

// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 42;
            string msg;
            msg = (age >= 18) ? "Welcome" : "Sorry";
            Console.WriteLine(msg);
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// What is the value of x after this code?
//   int x = 5;
//   int y = 3;
//   x = (x > y) ? y : x;

// ANSWER:
//   3

