// switch

// The switch statement provides a more elegant way to test a variable for
// equality against a list of values.

// Each value is called a case, and the variable being switched on is checked
// for each switch case.

// For example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 3;
            
            switch (num)
            {
                case 1:
                    Console.WriteLine("one");
                    break;
                    
                case 2:
                    Console.WriteLine("two");
                    break;
                    
                case 3:
                    Console.WriteLine("three");
                    break;
            }
        }
    }
}

// Each case represents a value to be checked, followed by a colon, and the
// statements to get executed if that case is matched.

// NOTE:
// A switch statement can include any number of cases. However, no two case
// labels may contain the same constant value.

// The break; statement that ends each case will be covered shortly.


// QUESTION:
// Fill in the blanks to create a valid switch statement.
//
//   int x = 33;
//   
//   ______ (x)
//   {
//   case 8:
//       Console.WriteLine("Value is 8");
//       break;
//       
//   case 18_
//       Console.WriteLine("Value is 18");
//       break;
//       
//   ____ 33:
//       Console.WriteLine("Value is 33");
//       break;
//   }

// ANSWER:
//   int x = 33;
//   
//   switch (x)
//   {
//   case 8:
//       Console.WriteLine("Value is 8");
//       break;
//       
//   case 18:
//       Console.WriteLine("Value is 18");
//       break;
//       
//   case 33:
//       Console.WriteLine("Value is 33");
//       break;
//   }


// The default Case

// In a switch statement, the optional default case is executed when none of the
// previous cases match.

// Example:
// Try it Yourself
using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 88;
            
            switch (age)
            {
                case 16:
                    Console.WriteLine("Too young");
                    break;
                    
                case 42:
                    Console.WriteLine("Adult");
                    break;
                    
                case 70:
                    Console.WriteLine("Senior");
                    break;
                    
                default:
                    Console.WriteLine("The default case");
                    break;
            }
        }
    }
}

// NOTE:
// The default code executes when none of the cases matches the switch
// expression.


// QUESTION:
// Drag and drop from the options below to test the x variable with a switch
// statement.
//
//   ______ (x)
//   {
//       case 10:
//           Console.WriteLine("Ten");
//       _____;
//       
//       case 20:
//           Console.WriteLine("Twenty");
//       break;
//       _______:
//       
//           Console.WriteLine("No match");.
//       break;
//   }
//
//   break  switch  case  default  x

// ANSWER:
//   switch (x)
//   {
//       case 10:
//           Console.WriteLine("Ten");
//       break;
//       
//       case 20:
//           Console.WriteLine("Twenty");
//       break;
//       
//       default:
//           Console.WriteLine("No match");.
//       break;
//   }


// The break Statement

// The role of the break statement is to terminate the switch statement.
// Without it, execution continues past the matching case statements and falls
// through to the next case statements, even when the case labels don’t match
// the switch variable.
// This behavior is called fallthrough and modern C# compilers will not compile
// such code. All case and default code must end with a break statement.

// NOTE:
// The break statement can also be used to break out of a loop. You will learn
// about loops in the coming lessons.


// QUESTION:
// What would occur if we forget to include a break statement at the end of case
// code?
//
//   "break" will be printed
//   compile error
//   nothing

// ANSWER:
//   compile error

