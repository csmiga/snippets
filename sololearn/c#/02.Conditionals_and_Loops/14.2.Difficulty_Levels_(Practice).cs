// PROBLEM:
// The default Case

// You are making a game, where the player selects the difficulty level:
// 1 - Easy
// 2 - Medium
// 3 - Hard

// You are given a program that takes the number as input.
// Complete the program so that it will output the corresponding difficulty
// level.

// If the user entered an invalid number, the program should output
// "Invalid option".

// Sample Input
// 2

// Sample Output
// Medium

// NOTE:
// Use the default case to execute the invalid option message.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = Convert.ToInt32(Console.ReadLine());
            
            /*
            1 - Easy
            2 - Medium
            3 - Hard
            other - "Invalid option"
            */
            
            // your code goes here
            switch (num)
            {
                case 1:
                    Console.WriteLine("Easy");
                    break;
                    
                case 2:
                    Console.WriteLine("Medium");
                    break;
                    
                case 3:
                    Console.WriteLine("Hard");
                    break;
                    
                default:
                    Console.WriteLine("Invalid option");
                    break;
            }
        }
    }
}

