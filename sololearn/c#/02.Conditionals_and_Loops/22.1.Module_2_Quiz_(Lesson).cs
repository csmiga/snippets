// QUESTION:
// Fill in the blanks to print the value of x five times.
//
//   int x = 42;
//   int num = 0;
//   
//   while(num < _ )
//   {
//       Console.WriteLine(_);
//       ___++;
//   }

// ANSWER:
//   int x = 42;
//   int num = 0;
//   
//   while (num < 5)
//   {
//       Console.WriteLine(x);
//       num++;
//   }


// QUESTION:
// Drag and drop from the options below to create a valid finite for loop.
//
//   ___ (_________; ______; ___)
//   {
//       Console.WriteLine(x);
//   }
//
//   x < 10  for  x++  int x = 0

// ANSWER:
//   for (int x = 0; x < 10; x++)
//   {
//       Console.WriteLine(x);
//   }


// QUESTION:
// Select the correct statements about && and || operators.
//
//   a || b is true if either a or b is true
//   (a && b) || c is true if c is true
//   a %% b is false if both a and b are true
//   a && b is true if either a or b is true

// ANSWER:
//   a || b is true if either a or b is true
//   (a && b) || c is true if c is true


// QUESTION:
// Fill in the blanks to calculate the sum of all whole numbers from 1 to 100.
//
//   int sum = 0;
//   
//   ___ (int x = 1; x <= 100; _++)
//   {
//       ___ += x;
//   }
//
//   Console.WriteLine(sum);

// ANSWER:
// Fill in the blanks to calculate the sum of all whole numbers from 1 to 100.
//
//   int sum = 0;
//   
//   for (int x = 1; x <= 100; x++)
//   {
//       sum += x;
//   }
//
//   Console.WriteLine(sum);


// QUESTION:
// What is the value of x after this code?
//
//   int x = 4; int y = 9;
//   x = (y%x != 0) ? y / x : y;

// ANSWER:
//   2

