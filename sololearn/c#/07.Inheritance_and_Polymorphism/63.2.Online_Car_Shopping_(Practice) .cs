// PROBLEM:
// Interfaces

// On the car dealership website, you can pre-order a car by specifying its
// color and equipment.
// The program you are given takes the color and the equipment type as input
// and pass them to constructor of already declared Car class.
// Implement IColor and IEquipment interfaces for the Car class and reimplement
// their GetColor and GetEquipment methods inside it. Each of them should output
// corresponding message about color/equipment (see sample output).

// Sample Input
// Blue
// Standard

// Sample Output
// Color: Blue
// Equipment: Standard

// NOTE:
// To implement multiple interfaces, use a comma separated list of interfaces
// when creating the class.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            string color = Console.ReadLine();
            string equipment = Console.ReadLine();
            
            Car car = new Car(color, equipment);
            
            car.GetColor();
            car.GetEquipment();
        }
    }
    
    public interface IColor
    {
        void GetColor();
    }
    
    public interface IEquipment
    {
        void GetEquipment();
    }
    
    //implement IColor & IEquipment interfaces
    public class Car : IColor, IEquipment
    {
        public string color;
        public string equipment;
        
        public Car(string color, string equipment)
        {
            this.color = color;
            this.equipment = equipment;
        }
        
        //reimplement this method
        public void GetColor()
        {
            Console.WriteLine("Color: " + color);
        }
        //reimplement this method
        public void GetEquipment()
        {
            Console.WriteLine("Equipment: " + equipment);
        }
    }
}

