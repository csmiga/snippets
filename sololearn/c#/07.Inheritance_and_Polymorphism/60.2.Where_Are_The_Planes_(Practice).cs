// PROBLEM:
// Derived Class Constructor & Destructor

// You are the airport administrator responsible for setting flight statuses.
// At first the flight program was showing only "Registration" and "Closed"
// statuses, but we need to expand it to give more detailed information.
// Derive WayStatus class from Flight class and complete its
// 1. constructor, to output "On the way"
// 2. destructor, to output "Landed"

// so that the program correctly outputs all the statuses of the flight.

// NOTE:
// Remember, that while creating derived class object the constructor and the
// destructor of base class also called - the constructor is called at beginning
// and the destructor at very end.

using System;

namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            WayStatus status = new WayStatus();
        }
    }
    
    class Flight
    {
        public Flight()
        {
            Console.WriteLine("Registration");
        }
        ~Flight()
        {
            Console.WriteLine("Closed");
        }
    }
    
    class WayStatus : Flight
    {
        public WayStatus()
        {
            Console.WriteLine("On the way");
        }
        ~WayStatus()
        {
            Console.WriteLine("Landed");
        }
    }
}

