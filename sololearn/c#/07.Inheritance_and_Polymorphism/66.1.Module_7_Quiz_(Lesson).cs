// QUESTION:
// Can you instantiate objects of an abstract class?
//
//     No
//     Yes

//ANSWER:
//     No


// QUESTION:
// Fill in the blanks to define a new class Falcon, which is derived from the
// class Bird.
//
//     _____ Falcon _ Bird
//     {
//     }


//ANSWER:
//     class Falcon : Bird
//     {
//     }


// QUESTION:
// Which keyword makes class members accessible to only its derived class
// members?

//ANSWER:
//     protected


// QUESTION:
// Fill in the blanks to declare a Person class with a Hello abstract method,
// and then declare a Student class that is derived from the Person class and
// overrides its Hello method.
//
//     ________ class Person
//     {
//           public abstract void Hello();
//     }
//     class Student _ Person
//     {
//         public ________ void Hello()
//         {
//                 Console.Write("Hello");
//         }
//     }

//ANSWER:
//     abstract class Person
//     {
//           public abstract void Hello();
//     }
//     class Student : Person
//     {
//         public override void Hello()
//         {
//                 Console.Write("Hello");
//         }
//     }


// QUESTION:
// A sealed class can be marked as abstract.
//
//     False
//     True

//ANSWER:
//     False


// QUESTION:
// Fill in the blanks to implement the A and B interfaces.
//
//     interface A
//     {
//     }
//     _________ B
//     {
//     }
//     class Test _ A _ B
//     {
//     }

//ANSWER:
//     interface A
//     {
//     }
//     interface B
//     {
//     }
//     class Test : A, B
//     {
//     }


// QUESTION:
// Can a single class inherit from multiple abstract classes?
//
//     No
//     Yes

//ANSWER:
//     No

