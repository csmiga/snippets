// protected

// Up to this point, we have worked exclusively with public and private access
// modifiers.

// Public members may be accessed from anywhere outside of the class, while
// access to private members is limited to their class.

// The protected access modifier is very similar to private with one difference;
// it can be accessed in the derived classes. So, a protected member is
// accessible only from derived classes.

// For example:
// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        class Person
        {
            protected int Age {get; set;}
            protected string Name {get; set;}
        }
        
        class Student : Person
        {
            public Student(string nm)
            {
                Name = nm;
            }
            
            public void Speak()
            {
                Console.Write("Name: " + Name);
            }
        }
        
        static void Main(string[] args)
        {
            Student s = new Student("David");
            s.Speak();
        }
    }
}

// As you can see, we can access and modify the Name property of the base class
// from the derived class. But, if we try to access it from outside code, we
// will get an error:

// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        class Person
        {
            protected int Age {get; set;}
            protected string Name {get; set;}
        }
        
        class Student : Person
        {
            public Student(string nm)
            {
                Name = nm;
            }
            
            public void Speak()
            {
                Console.Write("Name: " + Name);
            }
        }
        
        static void Main(string[] args)
        {
            Student s = new Student("David");
            s.Name = "Bob"; 
        }
    }
}

// NOTE:
// Run the code and see how it works!


// QUESTION:
// in the derived class:
//
//     _____ Shape
//     {
//         public int H {get; set;}
//         public int W {get; set;}
//         _________ int Area()
//         {
//             return H * W;
//         }
//     }

// ANSWER:
//     class Shape
//     {
//         public int H {get; set;}
//         public int W {get; set;}
//         protected int Area()
//         {
//             return H * W;
//         }
//     }


// sealed

// A class can prevent other classes from inheriting it, or any of its members,
// by using the sealed modifier.

// For example:
// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        sealed class Animal
        {
            // some code
        }
        
        class Dog : Animal { }  // Error
        
        static void Main(string[] args)
        {
            
        }
    }
}

// In this case, we cannot derive the Dog class from the Animal class because
// Animal is sealed.

// NOTE:
// The sealed keyword provides a level of protection to your class so that other
// classes cannot inherit from it.


// QUESTION:
// Fill in the blanks to derive a class B from class A and prevent class B from
// being inherited.
//
//     _____ A
//     {
//         
//     }
//     ______ class B _ A
//     {
//         
//     }

// ANSWER:
//     class A
//     {
//         
//     }
//     sealed class B : A
//     {
//         
//     }




