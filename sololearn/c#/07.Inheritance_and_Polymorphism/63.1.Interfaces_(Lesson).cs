// Interfaces

// An interface is a completely abstract class, which contains only abstract
// members.
// It is declared using the interface keyword:

public interface IShape
{
    void Draw();
}

// All members of the interface are by default abstract, so no need to use the
// abstract keyword.

// Interfaces can have public (by default), private and protected members.

// NOTE:
// It is common to use the capital letter I as the starting letter for an
// interface name.
// Interfaces can contain properties, methods, etc. but cannot contain fields
// (variables).


// QUESTION:
// Is the following interface valid?
//
//     public interface ITest
//     {
//         int test;
//         void Func();
//     }
//
//     No
//     Yes

// ANSWER:
//     No


// When a class implements an interface, it must also implement, or define, all
// of its methods.
// The term implementing an interface is used (opposed to the term "inheriting
// from") to describe the process of creating a class based on an interface. The
// interface simply describes what a class should do. The class implementing the
// interface must define how to accomplish the behaviors.
// The syntax to implement an interface is the same as that to derive a class:

// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        public interface IShape
        {
            void Draw();
        }
        class Circle : IShape
        {
            public void Draw()
            {
                Console.WriteLine("Circle Draw");
            }
        }
        static void Main(string[] args)
        {
            IShape c = new Circle();
            c.Draw();
        }
    }
}

// Note, that the override keyword is not needed when you implement an
// interface.

// NOTE:
// But why use interfaces rather than abstract classes?
// A class can inherit from just one base class, but it can implement multiple
// interfaces!
// Therefore, by using interfaces you can include behavior from multiple sources
// in a class.
// To implement multiple interfaces, use a comma separated list of interfaces
// when creating the class: class A: IShape, IAnimal, etc.


// QUESTION:
// Fill in the blanks to create an interface and implement it.
//
//     _________ IAnimal
//     {
//           void Eat();
//     }
//     _____ Dog _ IAnimal
//     {
//         public ____ Eat()
//         {
//             Console.WriteLine("Omnomnom");
//         }
//     }

// ANSWER:
//     interface IAnimal
//     {
//           void Eat();
//     }
//     class Dog : IAnimal
//     {
//         public void Eat()
//         {
//             Console.WriteLine("Omnomnom");
//         }
//     }


// Default implementation in interfaces allows to write an implementation of any
// method. This is useful when there is a need to provide a single
// implementation for common functionality.

// Let's suppose we need to add new common functionality to our already existing
// interface, which is implemented by many classes. Without default
// implementation (before C# 8), this operation would create errors, because the
// method we have added isn't implemented in the classes, and we would need to
// implement the same operation one by one in each class. Default implementation
// in interface solves this problem.

// For example:
// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        public interface IShape
        {
            void Draw();
            void Finish()
            {
                Console.WriteLine("Done!");
            }
        }
        class Circle : IShape
        {
            public void Draw()
            {
                Console.WriteLine("Circle Draw");
            }
        }
        static void Main(string[] args)
        {
            IShape c = new Circle();
            c.Draw();
            c.Finish();
        }
    }
}

// We added the Finish() method with default implementation to our IShape
// interface and called it without implementing it inside the Circle class.

// NOTE:
// Methods with default implementation can be freely overridden inside the class
// which implements that interface.


// QUESTION:
// What is the output of this code?
//
//     public interface ISomeFunc
//     {
//         void Greet()
//         {
//             Console.Write("Hello!");
//         }
//     }
//     class SomeObject: ISomeFunc
//     {
//         void Greet()
//         {
//             Console.Write("Hi!");
//         }
//     }
//     static void Main(string[] args)
//     {
//         ISomeFunc obj = new SomeObject();
//         obj.Greet();
//     }

// ANSWER:
//     Hi

