// Inheritance

// Constructors are called when objects of a class are created. With
// inheritance, the base class constructor and destructor are not inherited, so
// you should define constructors for the derived classes.
// However, the base class constructor and destructor are being invoked
// automatically when an object of the derived class is created or deleted.
// Consider the following example:

class Animal
{
    public Animal()
    {
        Console.WriteLine("Animal created");
    }
    ~Animal()
    {
        Console.WriteLine("Animal deleted");
    }
}
class Dog : Animal
{
    public Dog()
    {
        Console.WriteLine("Dog created");
    }
    ~Dog()
    {
        Console.WriteLine("Dog deleted");
    }
}

// We have defined the Animal class with a constructor and destructor and a
// derived Dog class with its own constructor and destructor.

// NOTE:
// So what will happen when we create an object of the derived class? Tap next
// to find out!


// QUESTION:
// What is the maximum number of base classes a sealed class can have?
//
//     multiple
//     none
//     one

// ANSWER:
//     one


// Let's create a Dog object:

// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        class Animal
        {
            public Animal()
            {
                Console.WriteLine("Animal created");
            }
            ~Animal()
            {
                Console.WriteLine("Animal deleted");
            }
        }
        
        class Dog: Animal
        {
            public Dog()
            {
                Console.WriteLine("Dog created");
            }
            ~Dog()
            {
                Console.WriteLine("Dog deleted");
            }
        }
        
        static void Main(string[] args)
        {
            Dog d = new Dog();
        }
    }
}

// Note that the base class constructor is called first and the derived class
// constructor is called next.

// When the object is destroyed, the derived class destructor is invoked and
// then the base class destructor is invoked.

// NOTE:
// You can think of it as the following: The derived class needs its base class
// in order to work, which is why the base class constructor is called first.

// QUESTION:
// What is the value of x after a B object is created?
//
//     class A
//     {
//         public int x = 7;
//         public A()
//         {
//             x++;
//         }
//     }
//     class B : A
//     {
//         public B()
//         {
//             x++;
//         }
//     }

// ANSWER:
//     9

