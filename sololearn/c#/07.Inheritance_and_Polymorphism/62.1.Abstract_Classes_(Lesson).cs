// Abstract Classes

// As described in the previous example, polymorphism is used when you have
// different derived classes with the same method, which has different
// implementations in each class. This behavior is achieved through virtual
// methods that are overridden in the derived classes.
// In some situations there is no meaningful need for the virtual method to have
// a separate definition in the base class.
// These methods are defined using the abstract keyword and specify that the
// derived classes must define that method on their own.
// You cannot create objects of a class containing an abstract method, which is
// why the class itself should be abstract.
// We could use an abstract method in the Shape class:

abstract class Shape
{
    public abstract void Draw();
}

// As you can see, the Draw method is abstract and thus has no body. You do not
// even need the curly brackets; just end the statement with a semicolon.
// The Shape class itself must be declared abstract because it contains an
// abstract method. Abstract method declarations are only permitted in abstract
// classes.

// NOTE:
// Remember, abstract method declarations are only permitted in abstract
// classes. Members marked as abstract, or included in an abstract class, must
// be implemented by classes that derive from the abstract class. An abstract
// class can have multiple abstract members.


// QUESTION:
// Fill in the blanks to define an abstract method Print for class A:
//
//     ________ class A
//     {
//          public ________ void Print()_
//     }

// ANSWER:
//     abstract class A
//     {
//          public abstract void Print();
//     }


// An abstract class is intended to be a base class of other classes. It acts
// like a template for its derived classes.
// Now, having the abstract class, we can derive the other classes and define
// their own Draw() methods:

// Try it Yourself:
using System;

namespace SoloLearn
{
    class Program
    {
        abstract class Shape
        {
            public abstract void Draw();
        }
        class Circle : Shape
        {
            public override void Draw()
            {
                Console.WriteLine("Circle Draw");
            }
        }
        class Rectangle : Shape
        {
            public override void Draw()
            {
                Console.WriteLine("Rect Draw");
            }
        }
        static void Main(string[] args)
        {
            Shape c = new Circle();
            c.Draw();
        }
    }
}

// Abstract classes have the following features:
//   - An abstract class cannot be instantiated.
//   - An abstract class may contain abstract methods and accessors.
//   - A non-abstract class derived from an abstract class must include actual
//     implementations of all inherited abstract methods and accessors.

// NOTE:
// It is not possible to modify an abstract class with the sealed modifier
// because the two modifiers have opposite meanings. The sealed modifier
// prevents a class from being inherited and the abstract modifier requires a
// class to be inherited.


// QUESTION:
// Fill in the blanks to create an abstract class with an abstract method and
// then use it as a base class.
//
//     abstract _____ Animal
//     {
//         public ________ void Eat();
//     }
//     class Dog _ Animal
//     {
//         public ________ void Eat()
//         {
//             Console.WriteLine("Omnomnom");
//         }
//     }

// ANSWER:
//     abstract class Animal
//     {
//         public abstract void Eat();
//     }
//     class Dog : Animal
//     {
//         public override void Eat()
//         {
//             Console.WriteLine("Omnomnom");
//         }
//     }

