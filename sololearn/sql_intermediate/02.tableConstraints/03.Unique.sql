 -- Unique

 -- The UNIQUE constraint ensures that all values in a column are different.

-- A PRIMARY KEY constraint automatically has a UNIQUE constraint.

-- However, you can have many UNIQUE constraints per table, but only one PRIMARY
-- KEY constraint per table.


-- QUESTION:
-- True or False: A primary key constraint automatically ensures uniqueness.
--
--   [ False ]
--   [ True ]

-- ANSWER:
--   True


-- Let's make the lastname column of our Customers unique:

ALTER TABLE Customers
ADD UNIQUE (lastname)

-- Note:
-- You can make multiple columns UNIQUE.


-- QUESTION:
-- Drag & drop to make the 'address' column unique.
--
--   _____ TABLE Houses
--   ADD ______ (_______)
--
--   [ NOT NULL ]  [ address ]  [ UNIQUE ]  [ id ]  [ CREATE ]  [ ALTER ]
--   [ COLUMN ]  [ KEY ]

-- ANSWER:
--   ALTER TABLE Houses
--   ADD UNIQUE (address)


-- Now when we try to insert a Customer with a lastname that is already present
-- in the table, we will get an error:

INSERT INTO Customers (firstname, lastname, city, age) 
VALUES 
  ('demo', 'Anderson', 'London', 24);
SELECT 
  * 
FROM 
  Customers;

-- Note:
-- NULL values are ignored by UNIQUE, meaning you can have multiple NULL values
-- in a UNIQUE column.


-- QUESTION:
-- True or False: You can have multiple NULL values in a unique column.
--
--   [ False ]
--   [ True ]

-- ANSWER:
--   True


-- Summary 
-- Let's summarze what we have learned about keys:

-- Primary key
-- The Primary key uniquely identifies each record of a table. It is usually set
-- as an auto increment integer.

-- Foreign keys
-- Foreign keys are used to create relationships between tables. They refer to
-- the primary key in other tables.
-- A table can have multiple foreign keys, but only one single primary key.

-- UNIQUE constraint
-- The UNIQUE constraint is used to make values in a column unique.

-- Note:
-- In the next module we will learn how to select data from multiple tables and
-- perform calculations on linked data. 