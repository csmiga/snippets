-- Identity

-- As we have seen, most tables we work with have a column called id, which
-- contains a number.

-- That column is often called the identity column and is similar to a row
-- number in Excel.

-- Here is the id column for our customers:

SELECT 
  id, 
  firstname 
FROM 
  Customers

-- Often, the id is an integer (a whole number) which is incremented with each
-- new row.

-- SQL allows you to create a column that gets automatically incremented with
-- each new row.
-- That is done using the AUTO_INCREMENT keyword.

-- Note:
-- When a column is set as AUTO_INCREMENT, the value is automatically set when
-- new rows are inserted, without the need for us to specify it.

-- QUESTION:
-- If we want our id column to be similar to row numbers, its data type should
-- be set to:
--
--   [ text ]
--   [ varchar ]
--   [ float ]
--   [ int ]

-- ANSWER:
--   int


-- Here is an example for our Customers table:

CREATE TABLE Customers
(
  id int NOT NULL AUTO_INCREMENT, 
  firstname varchar(255), 
  lastname varchar(255)
);

-- Note:
-- As you can see, we simply specify the id column to be AUTO_INCREMENT when
-- creating the table.


-- QUESTION:
-- Drag & drop to create a table called 'Products' with an auto increment column
-- called 'number'.
--
--   CREATE _____ Products
--   (
--     _____ int _____
--   );
--
--   [ number ]  [ COLUMN ]  [ SET ]  [ id ]  [ INT ]  [ AUTO_INCREMENT ]
--   [ TABLE ]

-- ANSWER:
--   CREATE TABLE Products
--   (
--     number int AUTO_INCREMENT
--   );


-- Now when inserting a new row, we do not need to specify the value of the id
-- column, as it will automatically be set.

-- For example:
INSERT INTO Customers (firstname, lastname, city, age) 
VALUES 
  ('demo', 'demo', 'Paris', 52), 
  ('test', 'test', 'London', 21);
SELECT 
  * 
FROM 
  Customers;

-- Note:
-- Run the code to see the result.


-- QUESTION:
-- How many rows will the following query insert into the 'demo' table?
--
--   [ 2 ]
--   [ 3 ]
--   [ 6 ]
--   [ 1 ]

-- ANSWER:
--   3


-- By default, the AUTO_INCREMENT column starts with the value 1.
-- This can be changed if needed, using the following:

ALTER TABLE Customers
AUTO_INCREMENT = 555

-- Now, when a new row is inserted, the id column will start from the given
-- value:

INSERT INTO Customers (firstname, lastname, city, age) 
VALUES 
  ('test', 'test', 'London', 21);
SELECT 
  * 
FROM 
  Customers;


-- QUESTION:
-- Drag & drop to set the starting value of the auto increment 'number' column
-- to 100.
--
--   _____ _____ Products
--   ______________ = ___
--
--   [ COLUMN ]  [ ALTER ]  [ 100 ]  [ AUTO_INCREMENT ]  [ number ]  [ id ]
--   [ SET ]  [ TABLE ]

-- ANSWER:
--   ALTER TABLE Products
--   AUTO_INCREMENT = 100


-- Lesson Takeaways 

-- Great progress!

-- Now you know that most of the tables have an identity column, often named id.
-- This column is a number, which is automatically incremented with each new
-- row.
-- The identity column can be created using the AUTO_INCREMENT keyword, defined
-- next to the column, when creating the table.

-- You will learn about keys in the next lesson!

-- PRACTICE (SOLUTION): Adding Data
-- There are new employees that need to be added to the Employees table.
-- Here is their data:

-- Firstname: Wang
-- Lastname: Lee
-- Salary: 1900

F-- irstname: Greta
-- Lastname: Wu
-- Salary: 1200

-- The Employees table has an identity column called id, which is set to
-- AUTO_INCREMENT.

-- Add the data to the table, then select the id, firstname, lastname and salary
-- columns sorted by the id column in descending order.

INSERT INTO Employees (firstname, lastname, salary) 
VALUES 
  ('Wang', 'Lee', 1900), 
  ('Greta', 'Wu', 1200);
SELECT 
  id, 
  firstname, 
  lastname, 
  salary 
FROM 
  Employees 
ORDER BY 
  id DESC
