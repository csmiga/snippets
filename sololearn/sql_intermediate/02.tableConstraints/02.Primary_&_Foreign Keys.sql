-- Primary & Foreign Keys

-- Keys 
-- Another important concept in databases are keys.
-- They are used to define relationships between tables.

-- Let's say we need to store multiple phone numbers for each of our Customers. 
-- Storing it in the same table would be problematic, as each customer can have
-- a variable number of phone numbers.

-- Note:
-- Let's see how we can store this data and how keys can help!


-- To store the phone numbers, we need to create another table. Let's call it
-- PhoneNumbers:

     | id | Customer_id | number | type |

-- Each row in the PhoneNumbers table represents a phone number of the given
-- type for a customer.

-- Note:
-- The customer_id column is used to link the row with the corresponding company
-- in the Customers table. We will learn how this is done soon, so stay tuned!


-- Primary Key 
-- Before looking at how the data will look in these tables, let's first create
-- the relationship between them using keys!
-- The primary key constraint is used to uniquely identify rows of a table.

-- In most cases, the primary key is the auto_increment column.
-- So, for our Customers and PhoneNumbers tables, it's the id column.
-- It is set when creating the table:

CREATE TABLE Customers
(
  id int NOT NULL AUTO_INCREMENT, 
  firstname varchar(255), 
  lastname varchar(255), 
  PRIMARY KEY (id)
);

-- QUESTION:
-- Drag & drop to set the 'number' columns as the primary key.
--
--   _____ _____ (_____)
--
--   [ KEY ]  [ id ]  [ IS ]  [ PRIMARY ]  [ AUTO ]  [ INT ]  [ number ]

-- ANSWER:
--   PRIMARY KEY number


-- Here are some rules for primary keys:
--   - A primary key must contain unique values.
--   - A primary key column cannot have NULL values.
--   - A table can have only one primary key.

-- Note:
-- This is why the AUTO_INCREMENT column is a good fit, as it satisfies these
-- conditions. 

-- QUESTION:
-- True or False: The primary key column can have NULL values.
--
--   [ False ]
--   [ True ]

-- ANSWER:
--   False


-- Foreign Key 
-- Another type of constraint is the Foreign Key.

-- A Foreign Key is a column in one table that refers to the Primary Key in
-- another table.

-- Note:
-- This constraint is used to prevent actions that would destroy links between
-- tables. 


-- QUESTION:
-- A foreign key refers to the linked table's:
--
--   [ auto_increment column ]
--   [ primary key ]
--   [ foreign key ]
--   [ name ]

-- ANSWER:
--   primary key


-- In our case, the customer_id column in the PhoneNumbers table is the foreign
-- key, which refers to the primary key id in the Customers table. 

CREATE TABLE PhoneNumbers
(
  id int NOT NULL AUTO_INCREMENT, 
  customer_id int NOT NULL, 
  number varchar(55), 
  type varchar(55), 
  PRIMARY KEY (id), 
  FOREIGN KEY (customer_id) REFERENCES Customers(id)
);

-- Note:
-- The Foreign Key constraint prevents invalid data from being inserted into the
-- foreign key column, because it has to be one of the values contained in the
-- linked table.

-- QUESTION:
-- Fill in the blanks to create a foreign key on the 'number' column of the
-- Houses table, referencing the 'id' column of the 'Prices' table.
--
--   FOREIGN KEY (______)
--   __________ ______ (__)
--
--   [ Houses ]  [ PRIMARY KEYS ]  [ NULL ]  [ INT ]  [ number ]
--   [ AUTO_INCREMENT ]  [ Prices ]  [ id ]  [ REFERENCES ]  

-- ANSWER:
--   FOREIGN KEY (number)
--   REFERENCES Prices (id)


-- Keys 
-- Here is how some example data in the Customers and PhoneNumbers table would
-- look:

-- Customers:
--   | id | firstname | lastname | city        | age |
--   | 1  | John      | Smith    | New York    | 24  |
--   | 2  | David     | Williams | Los Angeles | 42  |
--   | 3  | Chloe     | Anderson | Chicago     | 65  |

-- PhoneNumbers:
--   | id | customer_id | number         | type      |
--   | 1  | 1           | (555) 123456   | mobile    |
--   | 2  | 1           | (943) 554545   | hone      |
--   | 3  | 2           | (331) 111111   | mobile    |
--   | 4  | 2           | (88) 11 22 33  | work      |
--   | 5  | 2           | (999) 00 11 33 | emergency |

-- This is how relationships between tables are created. The foreign key column
-- is referencing the primary key column of another table, thus linking the data
-- in these tables.

-- This way, a customer can have any number of phone numbers associated with
-- them.

-- Note:
-- You can have tables referencing multiple other tables in a database.


-- QUESTION:
-- True or False: A table can have multiple foreign keys.
--
--   [ False ]
--   [ True ]

-- ANSWER:
--   True


-- Lesson Takeaways 
-- Awesome! Now you know how to create keys in tables, linking the data.

-- The primary key is used to uniquely identify each row of a table. It is
-- usually the identity column.
-- The foreign key is used to reference an identity column in another table.
-- This allows you to link the data between multiple tables and prevent actions
-- that would break the relationship.

-- Learn how to make data in a column unique in the next lesson!
