-- Module 2 Quiz

-- QUESTION:
-- Create a table with an auto incremented column called id, which is the
-- primary key.
--
--   CREATE _____ Users (
--   id int _____,
--   name varchar(255) NOT NULL,
--   _____ KEY (_____)
--   );
--
--   [ name ]  [ NULL ]  [ id ]  [ UNIQUE ]  [ AUTO_INCREMENT ]  [ TABLE ]
--   [ FOREIGN ]  [ PRIMARY ]  [ REFERENCE ]

-- ANSWER
--   CREATE TABLE Users
--   (
--     id int AUTO_INCREMENT,
--     name varchar(255) NOT NULL,
--     PRIMARY KEY (id)
--   );

-- QUESTION:
-- True or False: A table can have multiple primary keys.
--
--   True
--   False

-- ANSWER
--   False


-- QUESTION:
-- Drag & drop to create a table 'Users' with a primary key on the column id and
-- a foreign key column 'account_id', referring to the Accounts table's id
-- column.
--
--   CREATE TABLE Users
--   (
--     id int NOT NULL AUTO_INCREMENT,
--     name varchar(255) NOT NULL,
--     account_id int NOT NULL,
--     _______ KEY (__),
--     _______ KEY (__________)
--     REFERENCES ________ (__)
--   );
--
--   [ id ]  [ accout_id ]  [ Accounts ]  [ FOREIGN ]  [ PRIMARY ]  [ id ]

-- ANSWER
--   CREATE TABLE Users
--   (
--     id int NOT NULL AUTO_INCREMENT,
--     name varchar(255) NOT NULL,
--     account_id int NOT NULL,
--     PRIMARY KEY (id),
--     FOREIGN KEY (account_id)
--     REFERENCES Accounts (id)
--   );


-- QUESTION:
-- Can you have multiple NULL values in a primary key column?
--
--   [ No ]
--   [ Yes]

-- ANSWER
--   No


-- QUESTION:
-- A database has a table called 'Users' and a table called 'Transactions'. Each
-- user can have multiple transactions.
-- Which table should have a foreign key to create this relationship?
--
--   [ Transactions ]
--   [ Both ]
--   [ Customers ]

-- ANSWER
--   Transactions
