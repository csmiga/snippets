-- Multiple Tables

-- Often, data is stored in multiple linked tables.
-- As an example, consider our Customers and PhoneNumbers tables.

-- The Customers table includes information about customers, while the
-- PhoneNumbers table contains the phone numbers of the customers.

-- Note:
-- Real-life databases can store millions of records.


-- SQL enables you to work with multiple tables using a single query.
-- For example, let's say we want to select the phone numbers of the customers
-- in our Customers table that have a certain type and a certain age.

-- Note:
-- This means we need to have conditions on both tables and select the linked
-- data. 


-- QUESTION:
-- Let's practice some grouping!
-- Drag & drop to group the Houses table by the city column and calculate the
-- average price of the houses for each city.
--
--   SELECT city, ___(price)
--   ____ Houses
--   _____ By ____
--
--   [ Price ]  [ SUM ]  [ FROM ]  [ AVG ]  [ ORDER ]  [ LIMIT ]  [ GROUP ]
--   [ city ]

-- ANSWER:
--   SELECT city, AVG(price)
--   FROM Houses
--   GROUP By city


-- Here is an example of the result that we want to get with our query:

--  | firstname | lastname | city          | number         | type      |
--  | David     | Williams | Los Angeles   | (331) 111111   | mobile    |
--  | David     | Williams | Los Angeles   | (88) 11 22 33  | work      |
--  | David     | Williams | Los Angeles   | (999) 00 11 33 | emergency |

-- As you can see, the first columns are from the Customers table, while the
-- next ones are from the PhoneNumbers table.


-- QUESTION:
-- True or False: The foreign key column should be unique.
--
--   [ True ]
--   [ False ]

-- ANSWER:
--   False


-- We can select data from multiple tables by comma separating them in a SELECT
-- statement:

SELECT 
  firstname, 
  lastname, 
  city, 
  number, 
  type 
FROM 
  Customers, 
  PhoneNumbers 
WHERE 
  Customers.id = PhoneNumbers.customer_id

-- Note the WHERE condition: it tells SQL to combine only those rows that have
-- the corresponding customer_id.

-- Without it we would get all possible variants of the first table linked with
-- the second table.


-- QUESTION:
-- You are working with the tables Products and Orders. Each product has an id,
-- while the Orders table has a referencing product_id.
-- 
-- Drag & drop to select the name from the products table and the corresponding
-- price from the orders table.
--
--   SELECT name, price
--   FROM ________, Orders
--   WHERE ________.id = ______.product_id
--
--   [ Products ]  [ Orders ]  [ TABLE ]  [ Orders ]  [ id ]  [ Products ]

-- ANSWER:
--   SELECT name, price
--   FROM Products, Orders
--   WHERE Products.id = Orders.product_id


-- When working with multiple tables, it's common practice to define the columns
-- by their full name – the table name, followed by a dot and the column name.

-- For example: Customers.id is the id column of the Customers table, while
-- PhoneNumbers.id is the id column of the PhoneNumbers table.

-- Note:
-- This makes the code more readable and avoids mistakes, when both tables have
-- a column with the same name. 


-- QUESTION:
-- Fill in the blanks to select the age column from the Users table using the
-- full name of the column.
--
--   SELECT _____.___
--   FROM _____
--
--   [ TABLE ]  [ AS ]  [ AND ]  [ id ]  [ Users ]  [ age ]  [ Users ]

-- ANSWER:
--   SELECT
--     Users.age
--   FROM
--     Users


-- So, here is what our query would look like with the full column names:

SELECT 
  Customers.firstname, 
  Customers.lastname, 
  Customers.city, 
  PhoneNumbers.number, 
  PhoneNumbers.type 
FROM 
  Customers, 
  PhoneNumbers 
WHERE 
  Customers.id = PhoneNumbers.customer_id


-- QUESTION:
-- Which of the following is the full column name for the column 'name' of the
-- Employees table?
--
--   [ name.Employees ]
--   [ name AS Employees ]
--   [ Employees.name ]
--   [ Employees:name ]

-- ANSWER:
--   Employees.name


-- Lesson Takeaways 
-- Selecting data from multiple tables is easy!

-- Just separate their names in the SELECT statement with a comma and specify
-- the condition for the linked columns.

-- In the next lesson we will learn a better and cleaner way to combine data in
-- multiple tables.

-- PRACTICE (SOLUTION): Book Authors
-- You are working with a library database that stores data on books.
-- The Books table has the columns id, name, year, author_id.

-- The author_id column connects to the Authors table, which stores the id, name
-- columns for the book authors.

-- You need to select all the books with their authors, ordered by the author
-- name alphabetically, then by the year in ascending order.

-- The result set should contain only 3 columns: the book name, year and its
-- author (name the column author). 

-- Use the full column names, as both tables have a column called name. 

SELECT 
  books.name, 
  year, 
  Authors.name AS author 
FROM 
  books, 
  Authors 
WHERE 
  Authors.id = books.author_id 
ORDER BY 
  Authors.name, 
  year ASC;
  