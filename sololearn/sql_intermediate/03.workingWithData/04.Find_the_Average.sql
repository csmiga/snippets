-- Find the Average 
-- In this lesson we will learn how to solve a slightly more complex problem -
-- we need to find the average number of phone numbers the Customers in our
-- table have.

-- Here is the data of our Customers and PhoneNumbers tables:

SELECT * FROM Customers;
SELECT * FROM PhoneNumbers;

-- Each customer has 0, 1, or multiple phone numbers.

-- Note:
-- The data is for demonstration only. Real-life tables could store thousands or
-- millions of rows.


-- To calculate the average, we need to find the number of phone numbers that
-- each customer has, then use the AVG function over that result set.

-- First, let's join the tables:

SELECT 
  C.id, 
  C.firstname, 
  C.lastname, 
  PN.number, 
  PN.type 
FROM 
  Customers AS C 
  LEFT JOIN PhoneNumbers AS PN ON C.id = PN.customer_id 
ORDER BY 
  C.id

-- Note:
-- We used a LEFT JOIN as not all customers have a phone number.
-- This is an important point: By simply joining the tables we would get the
-- wrong result for the average.


-- QUESTION:
-- Drag & drop to select all the product names from the Products table with
-- their matching category records in the ProductCategories table. Products that
-- don't have any matching category records should also be in the result.
--
--   SELECT
--     P.name,
--     PC.category
--   ____
--     Products AS P
--   ____ ____ ProductCategories AS PC
--     __ P.id = PC.product_id
--
--   [ UNION ]  [ SELECT ]  [ RIGHT ]  [ LEFT ]  [ FROM ]  [ ON ]  [ IN ]
--   [ JOIN ]  [ WHERE ]

-- ANSWER:
--   SELECT
--     P.name,
--     PC.category
--   FROM
--     Products AS P 
--   LEFT JOIN ProductCategories AS PC
--     ON P.id = PC.product_id


-- Now we can group the data based on our customers and find the number of phone
-- numbers each of them has:

SELECT 
  C.id, 
  COUNT(PN.number) AS count 
FROM 
  Customers AS C 
  LEFT JOIN PhoneNumbers AS PN ON C.id = PN.customer_id 
GROUP BY 
  C.id

-- Our custom 'count' column now has the number of phone numbers for each
-- customer, as we grouped the query by the customer id.

-- Note:
-- As you can see from the results, the count also includes 0 values: that's
-- because we used a LEFT JOIN and some of the customers don't have any matching
-- phone numbers.


-- QUESTION:
-- If we used JOIN instead of LEFT JOIN, we would get:
--
--   [ the customers that don't have any numbers ]
--   [ the customers that have matching numbers ]
--   [ all the customers in our table ]

-- ANSWER:
--   the customers that have matching numbers


-- Now, we need to find the average of these values. 
-- For that, we need another SELECT query over the data of the join:

SELECT 
  AVG(count) 
FROM 
  (
    SELECT 
      C.id, 
      COUNT(PN.number) AS count 
    FROM 
      Customers AS C 
      LEFT JOIN PhoneNumbers AS PN ON C.id = PN.customer_id 
    GROUP BY 
      C.id
  ) AS Numbers

-- We aliased the query as 'Numbers' and selected the average value of the count
-- column from it.

-- Note:
-- By enclosing a SELECT query in parentheses, we are able to give it a name and
-- use it just like a table.
-- It is also important to give the custom columns name aliases, so you can
-- select them.


-- QUESTION:
-- To treat a SELECT statement as a table and give it a name, we need to enclose
-- it in:
--
--   a group by clause
--   an aggregate function
--   parentheses
--   quotes

-- ANSWER:
--   parentheses


-- Lesson Takeaways 
-- You learned how to solve a real-life problem!
-- The key takeaway from this lesson is that you are able to enclose a query
-- into parentheses and give it a name using the AS keyword. This enables us to
-- use the query as a table: select from it, use it in JOINS, run aggregate
-- functions, etc.

-- Congratulations! You have completed the last lesson of this course.
