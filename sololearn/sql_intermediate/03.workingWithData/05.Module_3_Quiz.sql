-- Module 3 Quiz

-- QUESTION:
-- The Orders table's foreign key prod_id refers to the Products table's id
-- column.
-- Drag & drop to create a query that selects the name and amount columns from
-- the Orders with the corresponding Product names.
--
--   SELECT
--     Orders.name,
--     _____.amount,
--     _____.name
--   FROM
--     Orders
--     _____ Products ON _____.prod_id = _____.id

-- ANSWER
--   SELECT 
--     Orders.name, 
--     Orders.amount, 
--     Products.name 
--   FROM 
--     Orders 
--   JOIN Products ON Orders.prod_id = Products.id


-- QUESTION:
-- Which of the following removes duplicate records when combining tables?
--
--   [ CASE ]
--   [ UNION ]
--   [ ORDER BY ]
--   [ UNION ALL ]

-- ANSWER
--   UNION


-- QUESTION:
-- The table Users contains 5 records, while the Accounts table is empty. The
-- Accounts table refers to the Users table using a foreign key.
-- How many rows will be in the result of Users RIGHT JOIN Account ON the linked
-- columns?
--
--   [ 1 ]
--   [ 5 ]
--   [ 0 ]

-- ANSWER
--   0


-- QUESTION:
-- Drag & drop to combine the records of the Products table with the Goods table
-- (both have the same columns).
--
--   ______ * FROM ________
--   _____
--   ______ * FROM Goods
--
--   [ Goods ]  [ SELECT ]  [ SELECT ]  [ UNION ]  [ WHEN ]  [ CASE ]  [ JOIN ]
--   [ Products ]

-- ANSWER
--   SELECT * FROM Products
--   UNION
--   SELECT * FROM Goods


-- QUESTION:
-- Table A contains 5 rows, while table B contains 3 rows.
-- How many rows will the following query result?
--
--   SELECT * FROM A, B;
--
--   [ 8 ]
--   [ 15 ]
--   [ 5 ]
--   [ 3 ]

-- ANSWER
--   15
