-- Union

-- Occasionally, you might need to combine data from multiple similar tables
-- into one comprehensive dataset.

-- For example, you might have multiple tables storing Customers data and you
-- want to combine them into one result set.

-- Note:
-- This can be done using the UNION statement.


-- The UNION operator is used to combine the result-sets of two or more SELECT
-- statements.

-- Consider having a Customers and Contacts tables, both having firstname,
-- lastname and age columns:

SELECT firstname, lastname, age FROM Customers
UNION
SELECT firstname, lastname, age FROM Contacts

-- All SELECT statements within the UNION must have the same number of columns.
-- The columns must also have the same data types. Also, the columns in each
-- SELECT statement must be in the same order.

-- Note:
-- UNION removes the duplicate records.


-- QUESTION:
-- UNION requires:
--
--   [ same number of rows ]
--   [ linked tables ]
--   [ same number of foreign keys ]
--   [ same number of columns ]

-- ANSWER:
--   same number of columns


-- UNION ALL 
-- UNION ALL is similar to UNION, but does not remove the duplicates:

SELECT firstname, lastname, age FROM Customers
UNION ALL
SELECT firstname, lastname, age FROM Contacts

-- Run the code to see the result.


-- QUESTION:
-- The table called 'A' contains 4 rows.
-- How many records will the following query return?
--
--   SELECT * FROM A
--   UNION
--   SELECT * FROM A
--
--   [ 8 ] 
--   [ none ]
--   [ 4 ]

-- ANSWER:
--   4


-- Remember, the SELECT statements need to have the same columns for the UNION
-- to work. In case one of the tables has extra columns that we need to select,
-- we can simply add them to the second select as NULL:

SELECT firstname, lastname, age, city FROM Customers
UNION
SELECT firstname, lastname, age, NULL FROM Contacts

-- Here, the Customers table has an extra city column.

-- Note:
-- We can also use other constant values for the extra columns. Just remember,
-- that the value has to have the same data type as the column of the first
-- table.


-- QUESTION:
-- Drag & drop to combine the rows of the Users and Clients tables.
--
--   SELECT 
--     *
--   _____
--     Users
--   ____
--   ______
--     *
--   FROM
--     Clients
--
--   [ UNION ]  [ WHERE ]  [ JOIN ]  [ NULL ]  [ FROM ]  [ SELECT ]

-- ANSWER:
--   SELECT
--     *
--   FROM
--     Users
--   UNION
--   SELECT
--     *
--   FROM
--     Clients


-- We can also set conditions for each select in the UNION.
-- For example:
SELECT 
  firstname, 
  lastname, 
  age 
FROM 
  Customers 
WHERE 
  age > 30 
UNION 
SELECT 
  firstname, 
  lastname, 
  age 
FROM 
  Contacts 
WHERE 
  age < 2


-- Note:
-- Each SELECT statement can have its specific conditions.


-- QUESTION:
-- Drag & drop to combine the rows of Users and Clients, but only for the ones
-- that have their balance greater than 1000.
--
--   ______
--     *
--   FROM
--     Users
--   _____
--     balance > 1000
--   UNION
--   SELECT
--     *
--   ____
--     Clients
--   WHERE
--     _______ > 1000
--
--   [ JOIN ]  [ balance ]  [ UNION ]  [ WHERE ]  [ LEFT ]  [ ON ]  [ FROM ]
--   [ SELECT ]

-- ANSWER:
--   SELECT
--     *
--   FROM
--     Users
--   WHERE
--     balance > 1000
--   UNION
--   SELECT
--     *
--   FROM
--     Clients
--   WHERE
--     balance > 1000


-- Lesson Takeaways 
-- Great progress!

-- To summarize this lesson:
--   - UNION allows you to combine records from multiple SELECT statements into
--     one dataset.
--   - For UNION to work, each SELECT statement needs to have the same number of
--     columns and matching data types.
--   - UNION removes duplicate records, while UNION ALL does not remove them.
--   - Each SELECT statement in a UNION can have its own conditions. 

-- Next you will learn how to solve a real-life SQL challenge!

-- PRACTICE (SOLUTION): New Arrivals
-- You are working with the library books database.
-- The Books table has the columns id, name, year.
-- The library has new books whose information is stored in another table called
-- "New", however they do not have a year column.

-- Write a query to select the books from both tables, Books and New, combining
-- their data. For the year column of the New books use the value 2022.
-- Also, select only the books that are released after the year 1900. 
-- The result set should contain the name and year columns only, ordered by the
-- name column alphabetically.

SELECT 
  name, 
  2022 AS year 
FROM 
  new 
UNION 
SELECT 
  name, 
  year 
FROM 
  books 
WHERE 
  year > 1900 
ORDER BY 
  name ASC
