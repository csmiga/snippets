-- Intermediate SQL

-- SQL databases are used everywhere, from e-commerce websites to social
-- networks and games.

-- In this course we will learn how to perform calculations, store and
-- manipulate data in multiple tables, create relations between tables and write
-- advanced SQL queries to analyze data. 

-- Note:
-- As a prerequisite for this course, we expect you will have already completed
-- the Introduction to SQL course.


-- SQL:
-- For our examples, let's consider data of customers with the following
-- structure:
--
--    --------------------------------------------------------------------------
--    | id | firstname | lastname | city         | age  |
--    | 1  | John      | Smith    | New York     | 24   |
--    | 2  | David     | Williams | Los Angeles  | 42   |
--    | 3  | Chloe     | Anderson | Chicago      | 65   |
--    | 4  | Emily     | Adams    | Houston      | NULL |
--    | 5  | James     | Roberts  | Philadelphia | 31   |
--    | 6  | Andrew    | Thomas   | New York     | 21   |
--    | 7  | Daniel    | Harris   | New York     | 67   |
--    | 8  | Charlotte | Walker   | Chicago      | NULL |
--    | 9  | Samual    | Clark    | San Diego    | NULL |
--    | 10 | Anthony   | Young    | Los Angeles  | 52   |
--
-- The data is stored in a table called Customers.


-- QUESTION:
-- In SQL, which of the following indicates the absence of value?
--
--   EMPTY
--   0
--   NULL
--   DEFAULT

-- ANSWER:
--   NULL


-- Let's create a query to select all the data in the table:

SELECT
  *
FROM
  Customers

-- Recall that the * symbol is used to select all of the columns in the table.

-- Note:
-- Run the code to see the result.


-- QUESTION:
-- Fill in the required symbol to select the name and salary columns from the
-- Employees table.
--
--   SELECT name_ salary
--   FROM Employees

-- ANSWER:
--   SELECT name, salary
--   FROM Employees


-- We can sort the customers by their age:

SELECT
  *
FROM
  Customers
WHERE
  age IS NOT NULL
ORDER BY
  age DESC
LIMIT
  2

-- This will select the 2 oldest customers.
-- We used the WHERE clause to filter out the rows that had no age value
-- defined.

-- Note:
-- In this course we will learn how to perform various data manipulations, as
-- well as use multiple tables to store more complex data and query them.

-- QUESTION:
-- Drag & drop to build a query that selects the 3rd to 5th rows from the
-- Employees table, ordered by the salary column in descending order.
--
--    ______ * ____ Employees
--    ________ salary ____
--    _____ 3 OFFSET 2
--
--    [ DESC ]  [ ORDER BY ]  [ ALL ]  [ FROM ]  [ SELECT ]  [ LIMIT ]
--    [ IN ]  [ SORT ]  [ ASC ]

-- ANSWER:
--    SELECT * FROM Employees
--    ORDER BY salary DESC
--    LIMIT 3 OFFSET 2


-- Lesson Takeaways
-- Great job! You are done with the introduction.

-- To recap:
--   - A SELECT statement is used to select data from a table.
--   - You can use the ORDER BY clause to order the results of the query by a
--     specified column or columns.
--   - The LIMIT keyword allows you to select only a subset of the data, by
--     specifying the start index and the number of rows with OFFSET.

-- We will learn how to use functions on text values in the next lesson!

-- PRACTICE (SOLUTION): Top Earners
-- Top Earners 
-- You are given an Employees table, with the following columns: firstname,
-- lastname, salary, department.
-- 
-- Select the data in all columns for the top 3 earners in the table. 
--
-- Use an ORDER BY clause to sort the data by the salary column in descending
-- order and then use the LIMIT keyword to get only the top 3 records.

SELECT
  *
FROM
  Employees
ORDER BY
  salary DESC
LIMIT
  3