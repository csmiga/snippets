-- Math
-- SQL also supports basic mathematical operations.

-- For example, let's say that we run a promotion and the customers get a
-- discount based on their age. The discount percentage is half of the age
-- value:

SELECT 
  firstname, 
  lastname, 
  age / 2 AS discount 
FROM 
  Customers

-- Note:
-- This will create a new column named discount in the result.


-- QUESTION:
-- Which operator is used for division?
--
-- [ \ ]  [ ^ ]  [ & ]  [ / ]  

-- ANSWER:
--   /


-- We can use mathematical operators on multiple columns to get a result.
-- Let's consider that our table includes weight and height columns for our
-- customers.
-- We can calculate the BMI based on these values:

SELECT 
  firstname, 
  lastname, 
  weight / (height * height) AS bmi 
FROM 
  Customers

-- The BMI is equal to the weight divided by the square of the height (the
-- values are in metric units).

-- Remember, these new columns exist only in the result table of the query, and
-- not in the original table.

-- Note:
-- In order to have the values permanently in the table, you need to create new
-- columns in the table and update the values.


-- QUESTION:
-- Which keyword is used to name a column in a SELECT statement?
--
--   [ NAME ]  [ ON ]  [ AS ]  [ = ]

-- ANSWER:
--   AS


-- Functions
-- SQL has many math functions.

-- We can query the min and max age numbers:
SELECT 
  MAX(age) 
FROM 
  Customers;

SELECT 
  MIN(age) 
FROM 
  Customers;

-- Note:
-- As you can see, we separated the queries using semicolons.


-- QUESTION:
-- Drag & drop to select all columns of the company with the maximum revenue
-- number.
--
--   SELECT
--     *
--   FROM
--     Companies
--     _____ revenue =
--   (
--     SELECT ___(_______)
--     ____ Companies
--   )
--
--   [ ALL ]  [ IN ]  [ MIN ]  [ SELECT ]  [ revenue ]  [ MAX ]  [ FROM ]
--   [ AS ]  [ WHERE ]

-- ANSWER:
--   SELECT
--     *
--   FROM
--     Companies
--     WHERE revenue =
--   (
--     SELECT MAX(revenue)
--     FROM Companies
--   )


-- FUNCTIONS
-- The AVG() function allows you to calculate the average value.
-- For example, let's calculate the average age of our customers:
SELECT 
  AVG(age) 
FROM 
  Customers


-- QUESTION:
-- Fill in the required symbols to calculate the average salary in the Employees
-- table.
--
--   SELECT
--     AVG _salary_
--   FROM
--     Employees

-- ANSWER:
--   SELECT
--     AVG (salary)
--   FROM
--     Employees


-- As you know, the COUNT function is used to return the count of rows in a
-- query.

-- We can combine it with a GROUP BY, to find out how many customers we have in
-- each city:
SELECT 
  city, 
  COUNT(*) AS ct 
FROM 
  Customers 
GROUP BY 
  city 
ORDER BY 
  ct DESC


-- QUESTION:
-- Drag & drop to select the average price from the Houses table and call it
-- 'mean' in the result.
--
--   SELECT
--     ___(price) __ mean
--   FROM ______
--
--   [ SUM ]  [ IN ]  [ TABLE ]  [ MEAN ]  [ AS ]  [ Houses ]  [ AVG ]  [ FOR ]

-- ANSWER:
--   SELECT
--     AVG(price) AS mean
--   FROM Houses


-- Lesson Takeaways
-- Great progress!
-- SQL supports mathematical operators, which can be used on columns and values.
-- SQL also provides a number of aggregation functions:
--   - MIN/MAX to get the lowest/highest value of a column, 
--   - AVG to get the average value, 
--   - COUNT to get the number of records.
-- Remember, the result of these functions and mathematical operators is not
-- stored in the original table; it is available only in the result set of the
-- query.
-- We will learn how to select values based on conditions in the next lesson!

-- PRACTICE (SOLUTION): Best Scores
-- You are working on a table that stores the game scores of multiple players.
-- Each player has a nickname associated with them. The table Scores has two
-- columns, the nickname and the score.
-- The table can store multiple rows for each player, which correspond to scores
-- they earned during multiple games.
-- You need to find the best score for each player and output the result sorted
-- by the best score in descending order.
-- The output should have the nickname column, followed by the best score column
-- called 'best'.
--
-- Hint: Group the table by the nickname column, then calculate the max score
--       for each player.
SELECT
  nickname,
  MAX(score) AS best
FROM
  Scores
GROUP BY 
  nickname
ORDER BY
  best DESC