-- Module 1 Quiz

-- QUESTION:
-- Drag & drop to calculate the average of the price column for houses which
-- have their city set to 'London'.
--
--   SELECT
--     ___(price)
--   ____
--     Houses
--   _____
--     city = 'London'
--
--   [ SUM ]  [ COUNT ]  [ IN ]  [ AS ]  [ CASE ]  [ WHERE ]  [ IS ]  [ FROM ]
--   [ AVG ]

-- ANSWER
--   SELECT
--     AVG(price)
--   FROM
--     Houses
--   WHERE
--     city = 'London'


-- QUESTION:
-- You have a Cars table with a column transmission, which is numeric: 1 for
-- Manual and 2 for Automatic. 
-- Drag & drop to create a query that selects the corresponding text value of
-- the transmission column.
--
--   SELECT name,
--     ____
--       WHEN transmission = 1 ____ '______'
--       ELSE '_________'
--     ___ AS 'transmission' 
--   FROM Cars
--
--   [ Manual ]  [ Automatic ]  [ THEN ]  [ END ]  [ CASE ]

-- ANSWER
--   SELECT name,
--     CASE
--       WHEN transmission = 1 THEN 'Manual'
--       ELSE 'Automatic'
--     END AS 'transmission'
--   FROM cars


-- QUESTION:
-- You want to create hashtags from a text in the table called Posts.
-- Write a query to replace all spaces with '_' underscores in the text column
-- of the Posts table and add a # symbol at the beginning. Call the new column
-- "tag".
--
--   SELECT
--     ______
--     (
--       '#',
--       _____ (text, ' ', '_')
--     ) AS ___
--   ____
--     Posts
--
--   [ SUB ]  [ ON ]  [ INSERT ]  [ UPPER ]  [ REPLACE ]  [ IN ]  [ tag ]
--   [ CONCAT ]  [ FROM ]

-- ANSWER
--   SELECT 
--     CONCAT
--     (
--       '#',
--       REPLACE (text, ' ', '_')
--     ) AS tag
--   FROM
--     Posts


-- QUESTION:
-- Select the top 5 highest priced records from the Houses table.
--
--   SELECT
--     *
--   FROM
--     Houses
--   _____ BY price ____
--   _____ 5
--
--   [ DESC ]  [ SORT ]  [ SEELCT ]  [ ORDER ]  [ TOP ]  [ LIMIT ]  [ AS ]
--   [ CASE ]

-- ANSWER
--   SELECT
--     *
--   FROM
--     Houses
--   ORDER BY price DESC
--   LIMIT 5
