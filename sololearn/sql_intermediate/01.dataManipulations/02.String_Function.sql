-- String Functions

-- SQL provides a number of useful functions to work with text data (or strings,
-- as they are called in programming).

-- For example, the CONCAT function allows you to combine text from multiple
-- columns into one. 
-- Let's select the full name of the customers as a new column called name:

SELECT 
  CONCAT(firstname, lastname) AS name 
FROM
  Customers

-- As you can see, the resulting name column has the first and lastnames of the
-- customers combined.

-- Note:
-- However, they don't have separators. Let's learn how to fix that in the next
-- lesson!


-- CONCAT:
-- The CONCAT function can take any number of arguments, combining them together
-- in the result.
-- So we can simply add a space between the columns to get the result we want:

SELECT 
  CONCAT(firstname, ' ', lastname) AS name 
FROM 
  Customers


-- QUESTION:
-- Fill in the blanks to create a query for the Players table that combines the
-- firstname with the age in a column named nickname, and orders the results by
-- the points column.
--
--   SELECT _____
--   (
--     firstname, age
--   )
--   __
--     nickname
--   ____
--     Players
--   _____ BY
--      points DESC
--
--   [ ORDER ]  [ WHERE ]  [ CONCAT ]  [ AS ]  [ ADD ]  [ FROM ] [ FUNC ]
--   [ IN ]

-- ANSWER:
--   SELECT CONCAT
--   (
--     firstname,
--     age
--   )
--   AS
--     nickname
--   FROM
--     Players
--   ORDER BY
--     points DESC


-- LOWER & UPPER 
-- The LOWER function converts the text in the provided column to lowercase.

-- For example:
SELECT 
  LOWER(firstname) 
FROM 
  Customers

-- Similarly, the UPPER function converts the text to uppercase:
SELECT 
  UPPER(firstname) 
FROM 
  Customers


-- SUBSTRING 
-- The SUBSTRING function allows you to extract part of the text in a column.
-- It takes the starting position and the number of characters we want to extract.

-- For example, let's take the first 3 characters of the firstname:
SELECT 
  SUBSTRING(firstname, 1, 3)
FROM 
  Customers

--Run the code to see the result.


-- QUESTION:
-- Fill in the blanks to select 3rd and 4th characters of the name column from
-- the Employees table.
--
--   SELECT SUBSTRIN
--   (
--     ____, _, _
--   )
--   FROM Employees

-- ANSWER:
--   SELECT SUBSTRIN
--   (
--     name, 3, 2
--   )
--   FROM Employees


-- REPLACE
-- The REPLACE function replaces all occurrences of the given string with
-- another one.

-- For example, let's replace New York with NY in the city column: 
SELECT 
  firstname, 
  lastname, 
  REPLACE(city, 'New York', 'NY') 
FROM 
  Customers

-- Note:
-- Again, remember that the change is done on the result set, and not on the actual
-- data of the table.


-- QUESTION
-- Drag & drop to replace all '!' characters with a dot '.' in the description
-- column of the Books table. Order the result by the year column.
--
--   SELECT
--     _____
--     (
--       description,
--       _____,
--       _____
--     )
--   FROM
--     _____
--   ORDER BY
--     _____
--
--   [ '!' ]  [ year ]  [ Books ]  [ SUBSTRING ]  [ REPLACE ]  [ CONCAT ] 
--   [ '.' ]  

-- ANSWER:
--   SELECT
--     REPLACE
--     (
--       description,
--       '!',
--       '.'
--     )
--   FROM
--     Books
--   ORDER BY
--     year


-- Functions
-- We can combine multiple functions into a single query.

-- For example, let's create a name column that has the first letter of the
-- firstname, followed by a dot and the lastname in all uppercase:
SELECT 
  CONCAT
  (
    SUBSTRING(firstname, 1, 1),
    '. ', 
    UPPER(lastname)
  ) AS name 
FROM 
  Customers

-- We used the SUBSTRING function to take the first letter of the firstname and
-- combine it with the uppercased lastname.


-- QUESTION:
-- What would be the result of the following statement if the name column
-- contains one row with the value "John"?
--
--   SELECT UPPER(SUBSTRING(name, 3, 1)) FROM Table
--
--   [ H ]  [ J ]  [ N ]  [ O ]

-- ANSWER:
--   H


-- Lesson Takeaways
-- Awesome! Now you know how to use functions on strings.
-- Here is a quick summary:
--   - CONCAT is used to combine multiple strings into one column.
--   - LOWER and UPPER convert the text to lowercase and uppercase
--   - SUBSTRING is used to extract a part of the text. 
--   - REPLACE is used to replace one value with another in the text. 
--   - Math and aggregation functions are coming next!

-- PRACTICE (SOLUTION): Email Generator
-- Email Generator 
-- You need to generate emails for the Employees.
-- The email format is firstname.lastname@company.com
-- So, for example, for John Smith the email would be: john.smith@company.com
-- Note, that the email should be in lowercase.
--
-- Write a query to create the email addresses for all employees in the table
-- and output the result in a new column called "email".
--
-- Note:
-- Sort the result by the email column in ascending order.

SELECT
  LOWER
  (
    CONCAT
    (
      firstname, '.', lastname, '@company.com'
    )
  ) AS email
FROM 
  Employees
ORDER BY email ASC
