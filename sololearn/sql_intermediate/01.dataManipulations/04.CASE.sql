-- CASE

-- Conditions 
-- When creating custom columns, SQL allows you to set the value based on
-- conditions.

-- Let's say we want to create a category based on the age value of our
-- customers.

-- Tap Continue to learn how to do that.


-- CASE
-- We want to set the category columns value to 'Senior', in case the age value
-- is greater than 65, 'Adult' in case it's in the range of 25 to 64, and
-- 'Youth', if it's under 25.

-- This is done using the CASE statement.

-- Here is one condition:

SELECT 
  firstname, 
  lastname, 
  CASE WHEN age >= 65 THEN 'Senior' END AS category 
FROM 
  Customers

-- As you can see, the CASE statement includes the condition in the WHEN clause
-- and sets the value using the THEN keyword.

-- The CASE statement has to close with the END keyword.


-- QUESTION:
-- Drag & drop to create a valid CASE statement.
--
--   ____ ____ rating >= 8 ____ 'Great' ___
--
--   [ WHEN ]  [ CASE ]  [ END ]  [ THEN ]

-- ANSWER:
--   CASE WHEN rating >= 8 THEN 'Great' END


-- We can add multiple conditions using multiple WHEN clauses:
-- Here is the second condition:

SELECT 
  firstname, 
  lastname, 
  CASE
    WHEN age >= 65 THEN 'Senior'
    WHEN age >= 25 
    AND age < 65 THEN 'Adult' END AS category 
FROM 
  Customers

-- Note:
-- The first condition that gets satisfied is used to set the value.


-- QUESTION:
-- How many WHEN statements can a CASE contain?
--
--   [ multiple ]
--   [ only one ]
--   [ none ]

-- ANSWER:
--   multiple


-- SELECT firstname, lastname,  
CASE
  WHEN age >= 65 THEN 'Senior'
  WHEN age >= 25 AND age < 65 THEN 'Adult'
  ELSE 'Youth'
END AS category
FROM Customers 

-- Do not forget the END keyword.

-- Note:
-- Run the code to see the result.


-- QUESTION:
-- Fill in the blanks to create a custom column called 'experience', which is
-- defined by the rating column.
--
--   SELECT
--     CASE
--       ____ rating < 3 THEN 'Bad'
--       WHEN rating >= 3 AND rating < 8 ____ 'OK'
--       ____ 'Great'
--     END __ experience
--   FROM
--     Hotels
--
--   [ IF ]  [ WHEN ]  [ THEN ]  [ CASE ]  [ AS ]  [ ELSE ]  [ END ]

-- ANSWER:
--   SELECT
--     CASE
--       WHEN rating < 3 THEN 'Bad'
--       WHEN rating >= 3 AND rating < 8 THEN 'OK'
--       ELSE 'Great'
--     END AS experience
--   FROM
--     Hotels


-- Lesson Takeaways 
-- You are almost done with the first module!
-- Here is a summary of the lesson:
--   - The CASE statement is used to set a value for a column based on
--     conditions.
--   - The conditions are set in WHEN clauses.
--   - The first WHEN clause that satisfies the condition is set as the value.
--   - The CASE statement should close with the END keyword.


-- PRACTICE (SOLUTION): Taxes
-- You are working on the Employees table, which stores the names and salaries
-- of employees.
-- You need to calculate the taxes for the salaries and output them as a new
-- column.
-- The tax percentage is based on the salary amount:
--   0 - 1500:    10%
--   1501 - 2000: 20%
--   001+:        30%
-- Output the firstname, lastname, salary and tax columns of the table, sorted
-- by the lastname column in ascending order.

-- Hint:
-- To calculate the percentage of a number, simply multiply it by the percentage
-- divided by 100. For example, to get 10%, multiply the number by 0.1.

SELECT 
  firstname, 
  lastname, 
  salary, 
  CASE
    WHEN salary >= 0
      AND salary <= 1500
      THEN salary * 0.1
    WHEN salary >= 1501
      AND salary <= 2000
      THEN salary * 0.2
    WHEN salary > 2001
      THEN salary * 0.3
      END AS tax 
FROM 
  employees 
ORDER BY 
  lastname
