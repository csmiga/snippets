// Creating Elements

// Use the following methods to create new nodes:
//   • element.cloneNode() clones an element and returns the resulting node.
//   • document.createElement(element) creates a new element node. 
//   • document.createTextNode(text) creates a new text node.

// For example:
var node = document.createTextNode("Some new text");

// This will create a new text node, but it will not appear in the document
// until you append it to an existing element with one of the following methods:
//   • element.appendChild(newNode) adds a new child node to an element as the
//     last child node.
//   • element.insertBefore(node1, node2) inserts node1 as a child before node2.

// Example:
// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <div id="demo">some content</div>
  </body>
</html>

// CSS:

// JS:
// Calling the function in window.onload to make sure the HTML is loaded
window.onload = function()
{
	// Creating a new paragraph
	var p = document.createElement("p");
	var node = document.createTextNode("Some new text");
	// Adding the text to the paragraph
	p.appendChild(node);

	var div = document.getElementById("demo");
	// Adding the paragraph to the div
	div.appendChild(p);
};

// Note:
// This creates a new paragraph and adds it to the existing div element on the
// page.


// QUESTION:
// Drag and drop from the options below to add a new <li> element to the
// unordered list with id="list".
//
//   var el = document._____________("li");
//   var txt = document.createTextNode("B");
//   el.appendChild(txt);
//   var ul = document.______________("list");
//   ul.___________(el);
//
//   [ appendChild ]  [ createElement ]  [ getElementById ]  

// ANSWER:
//   var el = document.createElement("li");
//   var txt = document.createTextNode("B");
//   el.appendChild(txt);
//   var ul = document.getElementById("list");
//   ul.appendChild(el);

// Want to know why?
// The code you provided uses the `createElement` method to create a new `li`
// element, adds text to it using the `createTextNode` method, appends the text
// to the `li` element using the `appendChild` method, gets the unordered list
// element with the id "list" using `getElementById`, and appends the new `li`
// element to it using the `appendChild` method. This correctly adds


// Removing Elements
// To remove an HTML element, you must select the parent of the element and use
// the removeChild(node) method.

// For example:
// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <div id="demo">
      <p id="p1">This is a paragraph.</p>
      <p id="p2">This is another paragraph.</p>
    </div>
  </body>
</html>

// CSS:

// JS:
// Calling the function in window.onload to make sure the HTML is loaded
window.onload = function()
{
	var parent = document.getElementById("demo");
	var child = document.getElementById("p1");
	parent.removeChild(child);
};

// This removes the paragraph with id = "p1" from the page.

// Note:
// An alternative way of achieving the same result would be the use of the
// parentNode property to get the parent of the element we want to remove:
//   var child = document.getElementById("p1");
//   child.parentNode.removeChild(child);


// QUESTION:
// Drag and drop from the options below to remove the node element from the page
// (par is node's parent).
//
//   var par = document.getElementById("par");
//   var node = document.getElementById("node");
//   ___.___________(____);
//
//   [ document ]  [ node ]  [ CreateElement ]  [ par ]  [ removeChild ]

// ANSWER:
//   var par = document.getElementById("par");
//   var node = document.getElementById("node");
//   par.removeChild(node);

// Want to know why?
// The answer is correct because it uses the `removeChild` method of the parent
// node (`par`) to remove the child node (`node`) from the DOM. `getElementById`
// is used to get references to both `par` and `node`.


// Replacing Elements
// To replace an HTML element, the element.replaceChild(newNode, oldNode) method
// is used.

// For example:
// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <div id="demo">
      <p id="p1">This is a paragraph.</p>
      <p id="p2">This is another paragraph.</p>
    </div>
  </body>
</html>

// CSS:

// JS:
// Calling the function in window.onload to make sure the HTML is loaded
window.onload = function()
{
    var p = document.createElement("p");
    var node = document.createTextNode("This is new");
    p.appendChild(node);

    var parent = document.getElementById("demo");
    var child = document.getElementById("p1");
    parent.replaceChild(p, child);
};

// Note;
// The code above creates a new paragraph element that replaces the existing p1
// paragraph.


// QUESTION:
// Which method is used to replace nodes?
//
//   replaceElements
//   replaceNodes
//   replace
//   replaceChild

// ANSWER:
// replaceChild

// Want to know why?
// Great job! `replaceChild()` is a method used to replace a child node with a
// new node in the DOM tree. It takes two arguments: the new node to be inserted
// and the node to be replaced. Keep up the good work!
