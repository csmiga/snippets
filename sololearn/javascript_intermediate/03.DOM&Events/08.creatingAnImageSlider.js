// Creating an Image Slider

// Image Slider
// Now we can create a sample image slider project. The images will be changed
// using "Next" and "Prev" buttons.
// Now, let’s create our HTML, which includes an image and the two navigation
// buttons:

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <div>
      <button> Prev </button>
      <img src="http://www.sololearn.com/uploads/slider/1.jpg" width="200px" height="100px" />
      <button> Next </button>
    </div>
  </body>
</html>

// CSS:

// JS:
// JavaScript code will go here

// Next, let's define our sample images in an array:
var images =
[
	"http://www.sololearn.com/uploads/slider/1.jpg",
	"http://www.sololearn.com/uploads/slider/2.jpg",
	"http://www.sololearn.com/uploads/slider/3.jpg"
];

// Note:
// We are going to use three sample images that we have uploaded to our server.
// You can use any number of images.


// QUESTION:
// Fill in the blanks to define an array.
//
//   var arr =
//   _
//       'A', 'B', 'C'
//   _;

// ANSWER:
//   var arr =
//   _
//       'A', 'B', 'C'
//   _;

// Want to know why?
// Good job! Your code correctly defines an array called "arr", which contains
// three string values. An array is an ordered list of related values or data
// types, which can be accessed and modified using its index positions. Your
// code creates an array using square brackets and separates its elements with
// commas.


// Now we need to handle the Next and Prev button clicks and call the
// corresponding functions to change the image.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <div>
      <button onclick="prev()"> Prev </button>
      <img id="slider" src="http://www.sololearn.com/uploads/slider/1.jpg" width="200px" height="100px" />
      <button onclick="next()"> Next </button>
    </div>
  </body>
</html>

// CSS:
button
{
    margin-top:30px;
    float:left;
    height:50px;
}

img
{
    float:left;
    margin-right:10px;
    margin-left:10px;
}

// JS:
var images =
[
	'http://www.sololearn.com/uploads/slider/1.jpg',
	'http://www.sololearn.com/uploads/slider/2.jpg',
	'http://www.sololearn.com/uploads/slider/3.jpg'
];
var num = 0;

function next()
{
	var slider = document.getElementById('slider');
	num++;
	if (num >= images.length)
    {
		num = 0;
	}
	slider.src = images[num];
}

function prev()
{
	var slider = document.getElementById('slider');
	num--;
	if (num < 0)
    {
		num = images.length - 1;
	}
	slider.src = images[num];
}

// The num variable holds the current image. The next and previous button clicks
// are handled by their corresponding functions, which change the source of the
// image to the next/previous image in the array.

// Note:
// We have created a functioning image slider!


// QUESTION:
// 
// What will be the content of the paragraph after the user clicks on it twice?
//
//   <p id = 'txt' onclick = 'test();'>20</p>
//   <script>
//   function test()
//   {
//       var x = document.getElementById('txt');
//       var n = x.innerHTML;
//       x.innerHTML = n/2;
//   }
//   </script>

//   [ 20 ]
//   [ 5  ]
//   [ 10 ]

// ANSWER:
//   5

// Want to know why?
// Your answer is correct because the "test()" function is triggered when the
// user clicks on the "<p>" element. It retrieves the "<p>" element by its ID,
// gets its current value which is 20, then divides it by 2 and sets the new
// value of the "<p>" element to 10. When the user clicks on it again, the
// "test()" function is triggered again and retrieves the `<p
