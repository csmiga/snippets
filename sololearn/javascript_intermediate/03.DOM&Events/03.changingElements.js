// Changing Elements

// Changing Attributes
// Once you have selected the element(s) you want to work with, you can change
// their attributes. 
// As we have seen in the previous lessons, we can change the text content of an
// element using the innerHTML property.
// Similarly, we can change the attributes of elements.
// For example, we can change the src attribute of an image:

//   <img id="myimg" src="orange.png" alt="" />
//   <script>
//       var el = document.getElementById("myimg");
//       el.src = "apple.png";
//   </script>

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
// Calling the function in window.onload to make sure the HTML is loaded
window.onload = function()
{
	var el = document.getElementsByTagName('a');
	el[0].href = 'http://www.sololearn.com';
};

// Note:
// Practically all attributes of an element can be changed using JavaScript.


// QUESTION:
// Fill in the blanks to select all images of the page and change their src
// attribute.
//
//   var arr = document.getElementsByTagName("___");
//   for(var x = 0; x < arr.______; x++)
//   {
//     arr[x].___ = "demo.jpg";
//   }

// ANSWER:
//   var arr = document.getElementsByTagName("img");
//   for(var x = 0; x < arr.length; x++)
//   {
//     arr[x].src = "demo.jpg";
//   }

// Want to know why?
// The solution provided is correct because it uses the `getElementsByTagName`
// method to select all the images on the page and then loops through an array
// of those images to set the `src` attribute to the desired value.


// Changing Style
// The style of HTML elements can also be changed using JavaScript.
// All style attributes can be accessed using the style object of the element. 

// For example:
// HTML:
<!DOCTYPE html>
<html>
	<head>
		<title>Page Title</title>
	</head>
	<body>
		<div id="demo" style="width:200px">some text</div>
	</body>
</html>

// CSS:

// JS:
// Calling the function in window.onload to make sure the HTML is loaded
window.onload = function()
{
	var x = document.getElementById("demo");
	x.style.color = '#6600FF';
	x.style.width = '100px';
};

// The code above changes the text color and width of the div element.

// Note:
// All CSS properties can be set and modified using JavaScript. Just remember,
// that you cannot use dashes (-) in the property names: these are replaced with
// camelCase versions, where the compound words begin with a capital letter.
// For example, the background-color property should be referred to as
// backgroundColor.

// QUESTION:
// Fill in the blanks to change the background color of all span elements of the
// page.
//
//   var s = document.getElementsByTagName("____");
//   ___(var x = 0; x < s.length; x++)
//   {
//       s[_].style.backgroundColor = "#33EA73";
//   }

// ANSWER:
//   var s = document.getElementsByTagName("span");
//   for(var x = 0; x < s.length; x++)
//   {
//       s[x].style.backgroundColor = "#33EA73";
//   }

// Want to know why?
// our answer uses the `getElementsByTagName` method to select all the `span`
// elements on the page, and then loops through them to set the
// `backgroundColor` property of each element to the specified color. This is
// the correct approach to change the background color of all span elements on
// the page using JavaScript.
