// Module 3 Quiz

// QUESTION:
// Fill in the blanks to change the content of all paragraph tags of the page to
// "SoloLearn".
//
//   var arr = ________.getElementsByTagName("_");
//   for(var x = 0; x < arr.length; x++) 
//   {
//       arr[_].innerHTML = "SoloLearn";
//   }

// ANSWER:
//   var arr = document.getElementsByTagName("p");
//   for(var x = 0; x < arr.length; x++) 
//   {
//       arr[x].innerHTML = "SoloLearn";
//   }

// Want to know why?
// The answer is correct because it selects all paragraph elements on the page
// using the `getElementsByTagName` method and then loops through the array
// using a `for` loop to change the `innerHTML` property of each element to
// "SoloLearn".


// QUESTION:
// What is the output of this code?
//
//   <div id = "test">
//     <p>some text</p>
//     </div>
//     <script>
//       var el = document.getElementById("test");
//       alert(el.hasChildNodes());
//     </script>
//
//   [ false ]
//   [ true ]
//   [ undefined ]

// ANSWER:
//   true

// Want to know why?
// Your answer is correct because the method `hasChildNodes()` returns a Boolean
// value ("true" or "false") indicating whether the given element has any child
// nodes (i.e., descendants) or not. In this case, the `div` element with ID
// "test" has one child node: a `p` element containing the text "some text".
// Therefore, the method returns `true`.


// QUESTION:
// Drag and drop from the options below to change the color of the paragraph
// with id = "p2" to red.
//
//   <script>
//     var d = document.______________.("__");
//     d._____._____ = "red";
//   </script>
//
//   [ style ]  [ color ]  [ getElementById ]  [ p2 ]  [ text-color ]

// ANSWER:
//   <script>
//     var d = document.getElementById.("p2");
//     d.style.color = "red";
//   </script>

// Want to know why?
// The answer is correct because it follows the correct syntax to access an
// element by its ID and then change its style property. The code assigns the
// element with ID "p2" to variable 'd' and then sets its color to red using the
// style.color property.


// QUESTION:
// Can you handle multiple events on the same HTML element?
//
//   [ No  ]
//   [ Yes ]

// ANSWER:
//   Yes

// Want to know why?
// Great job! Yes, you can handle multiple events on the same HTML element. This
// is a powerful feature of JavaScript and allows you to create dynamic and
// interactive web pages. Keep up the good work!


// QUESTION:
// Fill in the blanks to alert a message when the button is clicked.
//
//   <button _______ = "msg()">Click me</button>
//   <script>
//     ________ msg()
//     {
//       alert("Hi!");
//     }
//   </script>

// ANSWER:
//   <button onclick = "msg()">Click me</button>
//   <script>
//     function msg()
//     {
//       alert("Hi!");
//     }
//   </script>

// Want to know why?
// Your answer is correct because it includes an event listener for the button
// click that calls the `msg()` function when clicked. The `msg()` function then
// calls the `alert()` function to display the message "Hi!" in a pop-up window.
// This is a basic example of using JavaScript to manipulate the Document Object
// Model (DOM) and respond to user interaction.


// QUESTION:
// Display an alert when the mouse pointer is over the div tag:
//
//   <div ___________ = "alert('Hi!');">
//     put the mouse pointer over me
//   </div>

// ANSWER:
//   <div onmouseover = "alert('Hi!');">
//     put the mouse pointer over me
//   </div>
