// Creating Animations

// Animations
// Now that we know how to select and change DOM elements, we can create a
// simple animation.
// Let's create a simple HTML page with a box element that will be animated
// using JS.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <div id="container">
      <div id="box"></div>
    </div>
  </body>
</html>

// CSS:
#container
{
    width: 200px;
    height: 200px;
    background: green;
    position: relative;
}
#box
{
    width: 50px;
    height: 50px;
    background: red;
    position: absolute;
}

// Our box element is inside a container element. Note the position attribute
// used for the elements: the container is relative and the box is absolute.
// This will allow us to create the animation relative to the container.

// We will be animating the red box to make it move to the right side of the
// container.

// Note:
// You need to be familiar with CSS to better understand the code provided.


// QUESTION:
// To create an animation relative to a container, the position attribute for
// the container should be set to:
//
//   [ box      ]
//   [ relative ]
//   [ absolute ]

// ANSWER:
//   relative

// Want to know why?
// Your answer is correct because setting the `position` attribute of a
// container to `relative` will make all its child elements positioned relative
// to it, allowing you to create animations that are relative to the container.


// Animations

// To create an animation, we need to change the properties of an element at
// small intervals of time. We can achieve this by using the setInterval()
// method, which allows us to create a timer and call a function to change
// properties repeatedly at defined intervals (in milliseconds).

// For example:
var t = setInterval(move, 500);

// This code creates a timer that calls a move() function every 500
// milliseconds.
// Now we need to define the move() function, that changes the position of the
// box.

// starting position
var pos = 0; 
//our box element
var box = document.getElementById("box");

function move()
{
    pos += 1;
    box.style.left = pos+"px";  // px = pixels
}

// Note:
// The move() function increments the left property of the box element by one
// each time it is called.


// QUESTION:
// What is the interval for this timer?
//
//   [ 1 second    ]
//   [ 100 seconds ]
//   [ 10 seconds  ]

// ANSWER:
//   10 seconds

// Want to know why?
// Your answer is correct because the second argument of the `setInterval`
// function is the duration (in milliseconds) of the interval between each
// execution of the specified function. In this case, the interval is set to
// 10,000 milliseconds, or 10 seconds.


// The following code defines a timer that calls the move() function every 10
// milliseconds:

var t = setInterval(move, 10)

// However, this makes our box move to the right forever. To stop the animation
// when the box reaches the end of the container, we add a simple check to the
// move() function and use the clearInterval() method to stop the timer.

function move()
{
    if(pos >= 150)
    {
      [b]clearInterval(t);[/b]
    }

    else
    {
      pos += 1;
      box.style.left = pos+"px";
    }
  }

// When the left attribute of the box reaches the value of 150, the box 
// reaches the end of the container, based on a container width of 200 and a
// box width of 50.

// The final code:
// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <div id="container">
      <div id="box"></div>
    </div>
  </body>
</html>

// CSS:
#container
{
    width: 200px;
    height: 200px;
    background: green;
    position: relative;
}

#box
{
    width: 50px;
    height: 50px;
    background: red;
    position: absolute;
}

// JS:
// Calling the function in window.onload to make sure the HTML is loaded
window.onload = function()
{
	var pos = 0;
	
	// Our box element
	var box = document.getElementById('box');
	var t = setInterval(move, 10);

	function move()
	{
		if (pos >= 150)
		{
			clearInterval(t);
		} else
		{
			pos += 1;
			box.style.left = pos + 'px';
		}
	}
};

// Note:
// Congratulations, you have just created your first JavaScript animation!


// QUESTION:
// Which function is used to stop a setInterval timer?
//
//   [ clearTimer    ]
//   [ stopInterval  ]
//   [ stopTimer     ]
//   [ clearInterval ]

// ANSWER:
//   clearInterval

// Want to know why?
// The `clearInterval` function is used to stop a repeating action that was
// previously started with the `setInterval` function. It takes the ID returned
// by `setInterval` as an argument and clears that interval, effectively
// stopping the repeated action. Therefore, your answer is correct.