// Event Propagation

// There are two ways of event propagation in the HTML DOM: bubbling and
// capturing.

// Event propagation allows for the definition of the element order when an
// event occurs. If you have a <p> element inside a <div> element, and the user
// clicks on the <p> element, which element's "click" event should be handled
// first?

// In bubbling, the innermost element's event is handled first and then the
// outer element's event is handled. The <p> element's click event is handled
// first, followed by the <div> element's click event.

// In capturing, the outermost element's event is handled first and then the
// inner. The <div> element's click event is handled first, followed by the <p>
// element's click event.

// Note:
// Capturing goes down the DOM.
// Bubbling goes up the DOM.


// QUESTION:
// A paragraph is inside a div element. You want the paragraph’s click event to
// be handled first. You should use:
//
//   [ Handling  ]
//   [ Bubbling  ]
//   [ Capturing ]

// ANSWER:
//   Bubbling

// Want to know why?
// Your answer is correct because in the event propagation model, the click
// event on the paragraph element will be handled first before it bubbles up to
// the parent div element. This is known as event capturing, where the event is
// first captured by the deepest child element and then propagated up to its
// parent elements. Bubbling is the opposite, where the event starts at the
// deepest child element and then bubbles up to its parents.


// Capturing vs. Bubbling
// The addEventListener() method allows you to specify the propagation type with
// the "useCapture" parameter.

addEventListener(event, function, useCapture)

// The default value is false, which means the bubbling propagation is used;
// when the value is set to true, the event uses the capturing propagation.

// Capturing propagation
elem1.addEventListener("click", myFunction, true); 

// Bubbling propagation
elem2.addEventListener("click", myFunction, false);

// Note:
// This is particularly useful when you have the same event handled for multiple
// elements in the DOM hierarchy.

// QUESTION:
// Drag and drop from the options below to handle the click event and use
// capturing propagation.
//
//   x.addEventListener("_____", func, ____);
//
//   [ onclick ]  [ click ]  [ false ]  [ capture ]  [ true ]

// ANSWER:
//   x.addEventListener("click", func, true);

// Want to know why?
// Great job! The correct answer is to use the "addEventListener" method and
// pass in the "true" value as the third parameter, which enables capturing
// propagation. This means that the event will first be handled by the outermost
// parent element before reaching the target element. Your answer is correct.
