// Module 1 Quiz

// QUESTION:
// An object's properties are similar to variables; methods are similar to:
//
//   [ operators    ]
//   [ functions    ]
//   [ properties   ]
//   [ conditionals ]

// ANSWER:
//   functions

// Want to know why?
// Great job! Your answer is correct. Methods are functions that are associated
// with an object and can modify its properties or perform specific actions
// related to that object.

// QUESTION:
// What is the result of the following expression?
//
//   var myString = "abcdef";
//   document.write(myString.length);

// ANSWER:
//   6


// QUESTION:
// Complete the expression to create an object constructor, taking into account
// that height and weight are properties and calculate is a method for the given object:
//
//   function mathCalc (height, weight)
//   {
//       this.height = ______;
//       this.weight = ______;
//       this.sampleCalc = _________;
//   }

// ANSWER:
//   function mathCalc (height, weight)
//   {
//       this.height = height;
//       this.weight = weight;
//       this.sampleCalc = calculate;
//   }

// Want to know why?
// Your answer is correct because you have created an object constructor
// function with two properties, "height" and "weight". You have also added a
// method called "sampleCalc", but it is not clear from the question if this
// is the same as "calculate". If "calculate" is a separate method, you would
// need to define it within the function.
