// JavaScript Objects

// JavaScript variables are containers for data values. Objects are variables
// too, but they can contain many values.

// Think of an object as a list of values that are written as name:value pairs,
// with the names and the values separated by colons.

// Example:

var person =
{
    name: "John",
    age: 31,
    favColor: "green",
    height: 183
};

// These values are called properties.

//     -----------------------------
//     | Property | Property Value |
//     |----------|----------------|
//     | name     | John           |
//     | age      | 31             |
//     | favColor | green          |
//     | height   | 183            |
//     -----------------------------

// Note:
// JavaScript objects are containers for named values.


// QUESTION:
// In reference to an object, color, height, weight, and name are all examples
// of:
//
//   [ variables  ]
//   [ properties ]
//   [ methods    ]
//   [ nouns      ]

// ANSWER:
//   properties


// Object Properties
// You can access object properties in two ways.

objectName.propertyName
// or
objectName['propertyName']

// This example demonstrates how to access the age of our person object.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var person =
{
    name: "John", age: 31, 
    favColor: "green", height: 183
};
var x = person.age;
var y = person['age'];

document.write(x);
document.write(y);

// JavaScript's built-in length property is used to count the number of
// characters in a property or string.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var course =
{
	name: "JS",
	lessons: 41
};
document.write(course.name.length);

// Note:
// Objects are one of the core concepts in JavaScript.


// QUESTION:
// What built-in property is used to count the number of characters in an
// object's property?
//
//   [ write  ]
//   [ width  ]
//   [ length ]
//   [ size   ]

// ANSWER:
//     length


// Object Methods
// An object method is a property that contains a function definition.
// Use the following syntax to access an object method.

objectName.methodName()

// As you already know, document.write() outputs data. The write() function is
// actually a method of the document object.


// QUESTION:
// Access the color property of the hair object using dot syntax.
//
//   _____

// ANSWER:
//   hair.color
