// Object Initialization

// Use the object literal or initializer syntax to create single objects.

var John = {name: "John", age: 25};
var James = {name: "James", age: 21};

// Note:
// Objects consist of properties, which are used to describe an object. Values
// of object properties can either contain primitive data types or other
// objects.


// QUESTION:
// Fill in the blanks.
//
//   simba = 
//   _
//       category: "lion",
//       gender: "male"
//   _

// ANSWER:
//   simba = 
//   {
//       category: "lion",
//       gender: "male"
//   }

// Want to know why?
// Your answer is correct as it correctly defines an object `simba` with
// properties `category` and `gender` and assigns their corresponding values.
// This is a basic syntax for defining objects in JavaScript.


// Using Object Initializers
// Spaces and line breaks are not important. An object definition can span
// multiple lines.

var John = 
{
	name: "John",
	age: 25
};

var James =
{
	name: "James",
	age: 21
};

// No matter how the object is created, the syntax for accessing the properties
// and methods does not change.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var John = 
{
	name: "John",
	age: 25
};

var James = 
{
	name: "James",
	age: 21
};

document.write(John.age);

// Note:
// Don't forget about the second accessing syntax: John['age'].


// QUESTION:
// Complete the following expression to display the simba object's category
// property on the screen:
//
//   document.write(simba._________;

// ANSWER:
//   document.write(simba.category);

// Want to know why?
// Your answer is correct because it uses dot notation to access and display the
// value of the `category` property of the `simba` object using the
// `document.write()` method in JavaScript.
