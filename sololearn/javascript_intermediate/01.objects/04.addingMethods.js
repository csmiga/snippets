// Methods

// Methods are functions that are stored as object properties. 

// Use the following syntax to create an object method:

methodName = function() { code lines }

// Access an object method using the following syntax:

objectName.methodName()

// A method is a function, belonging to an object. It can be referenced using
// the this keyword.
// The this keyword is used as a reference to the current object, meaning that
// you can access the object's properties and methods using it.

// Defining methods is done inside the constructor function.

// For example:

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
function person(name, age) 
{
	this.name = name;
	this.age = age;
	this.changeName = function(name)
	{
		this.name = name;
	}
}

var p = new person("David", 21);
p.changeName("John");

document.write(p.name);

// In the example above, we have defined a method named changeName for our
// person, which is a function, that takes a parameter name and assigns it to
// the name property of the object.
// this.name refers to the name property of the object.

// Note:
// The changeName method changes the object's name property to its argument.

// PRACTICE (PROBLEM SET): Calculating The Discount
// A store manager needs a program to set discounts for products.
// The program should take the product ID, price and discount as input and
// output the discounted price. However, the changePrice method, which should
// calculate the discount, is incomplete. Fix it!

// Sample Input
// LD1493
// 1700
// 15

// Sample Output
// LD1493 price: 1700
// LD1493 new price: 1445

// Note:
// The first input is the product ID, the second is the price before
// discounting, and the third is discount percentage.
// So after discounting the new price will be 1700-(0.15*1700) = 1445.

function main()
{
    var prodID = readLine();
    var price = parseInt(readLine(), 10);
    var discount = parseInt(readLine(), 10);

    var prod1 = new Product(prodID, price);
    console.log(prod1.prodID + " price: " + prod1.price);

    prod1.changePrice(discount);
    console.log(prod1.prodID + " new price: " + prod1.price);
}

function Product(prodID, price)
{
    this.prodID = prodID;
    this.price = price;

    this.changePrice = function(discount)
    {
        // Your code goes here
        // PEMDAS
        //this.price = this.price - ((discount / 100) * this.price)
        this.price = this.price - discount / 100 * this.price;
    }
}


// QUESTION:
// The this keyword in the method means:
//
//   [ The name of the given method ]
//   [ The name of the web page     ]
//   [ The current object           ]

// ANSWER:
//   The current object

// Want to know why?
// The 'this' keyword refers to the object that the method is called on. It
// enables the method to access and manipulate the properties of the object.
// Therefore, your answer that 'this' refers to the current object is correct.


// You can also define the function outside of the constructor function and
// associate it with the object.

function person(name, age)
{
	this.name = name;
	this.age = age;
	this.yearOfBirth = bornYear;
}

function bornYear()
{
	return 2016 - this.age;
}

// As you can see, we have assigned the object's yearOfBirth property to the
// bornYear function.
// The this keyword is used to access the age property of the object, which is
// going to call the method.

// Note:
// Note that it's not necessary to write the function's parentheses when
// assigning it to an object.


// QUESTION:
// Please associate the testData constructor function below with a method called
// mymethod:
//
//   function testData (first, second)
//   {
//     this.first = first;
//     this.second = second;
//     this.checkData = ________
//   }

// ANSWER:
//   function testData (first, second)
//   {
//     this.first = first;
//     this.second = second;
//     this.checkData = mymethod
//   }

// Want to know why?
// Your answer is correct because you are creating a new method called
// `checkData` in the `testData` constructor function and assigning it the
// value of `mymethod`. This allows instances of `testData` to have access to
// the `mymethod` function.


// Call the method as usual.
// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
function person(name, age)
{
	this.name = name;
	this.age = age;
	this.yearOfBirth = bornYear;
}

function bornYear()
{
	return 2016 - this.age;
}

var p = new person("A", 22);

document.write(p.yearOfBirth());

// Note:
// Call the method by the property name you specified in the constructor
// function, rather than the function name.


// QUESTION:
// In order to use the object's properties within a function, use:
//
//   [ The "var" keyword             ]
//   [ The function's name           ]
//   [ Just the name of the property ]
//   [ The "this" keyword            ]

// ANSWER:
//   The "this" keyword 

// Want to know why?
// Great job! You are correct - the "this" keyword is used to refer to the
// current object and its properties inside a function. This allows you to
// access and manipulate the object's properties dynamically. Keep up the good
// work!
