// Intro to ES6

// ECMAScript (ES) is a scripting language specification created to standardize
// JavaScript.

// The Sixth Edition, initially known as ECMAScript 6 (ES6) and later renamed to
// ECMAScript 2015, adds significant new syntax for writing complex
// applications, including classes and modules, iterators and for/of loops,
// generators, arrow functions, binary data, typed arrays, collections (maps,
// sets and weak maps), promises, number and math enhancements, reflection, and
// proxies.

// In other words, ES6 is a superset of JavaScript (ES5). The reason that ES6
// became so popular is that it introduced new conventions and OOP concepts such
// as classes.

// Note:
// In this module, we cover the most important additions to ES6. 
// So, let's jump right in!


// QUESTION:
// JavaScript and ES6 are different technologies for different purposes.
//
//   [ True  ]
//   [ False ]

// ANSWER:
//   False

// Want to know why?
// Great job! Your answer is correct. JavaScript and ES6 are actually the same
// technology. ES6 refers to a specific version of JavaScript that introduced
// new syntax and features to the language. So, we can say that ES6 is a part of
// JavaScript, rather than a different technology.
