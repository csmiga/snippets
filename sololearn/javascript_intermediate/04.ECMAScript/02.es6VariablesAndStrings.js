// ES6 Variables and Strings

// var & let 
// In ES6 we have three ways of declaring variables:

var a = 10;
const b = 'hello';
let c = true;

// The type of declaration used depends on the necessary scope. Scope is the
// fundamental concept in all programming languages that defines the visibility
// of a variable.

// var & let
// Unlike the "var" keyword, which defines a variable globally, or locally to an
// entire function regardless of block scope, "let" allows you to declare
// variables that are limited in scope to the block, statement, or expression in
// which they are used.

// For example:
// HTML:

// CSS:

// JS:
if (true)
{
	let name = 'Jack';
}
alert(name);

// In this case, the name variable is accessible only in the scope of the "if"
// statement because it was declared as "let".

// To demonstrate the difference in scope between "var" and "let", consider this
// example:

// HTML:

// CSS:

// JS:
function varTest()
{
	var x = 1;
	if (true)
	{
		var x = 2;  // Same variable
		console.log(x);  // 2
	}
	console.log(x);  // 2
}

function letTest()
{
	let x = 1;
	if (true)
    {
		let x = 2;  // Different variable
		console.log(x);  // 2
	}
	console.log(x);  // 1
}

varTest();
letTest();

// One of the best uses for "let" is in loops:

// HTML:

// CSS:

// JS:
for (let i = 0; i < 3; i++)
{
	document.write(i);
}

// Here, the "i" variable is accessible only within the scope of the for loop,
// where it is needed.

// Note:
// "let" is not subject to Variable Hoisting, which means that "let"
// declarations do not move to the top of the current execution context.


// QUESTION:
// What is the output of this code?
//
//   function letItBe()
//   {
//     let v = 2;
//     if (true)
//     {
//         let v = 4;  
//         console.log(v); 
//     }
//     console.log(v); 
//   }
//   letItBe();
//
//   [ 4 2 ]
//   [ 4 4 ]
//   [ 2 2 ]
//   [ 2 4 ]

// ANSWER:
//   4 2

// Want to know why?
// Your answer is correct because the `let` keyword creates block-scoped
// variables. Inside the if statement block, there is a new variable `v` with
// the value of 4, but it does not affect the outer variable `v` defined in the
// function scope, which keeps its value of 2. When the function is called, it
// logs 4 and then logs 2.


// const
// "const" variables have the same scope as variables declared using "let". The
// difference is that "const" variables are immutable - they are not allowed to
// be reassigned.

// For example, the following generates an exception: 
// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <button onclick = "show();">Click Me</button>
  </body>
</html>

// CSS:

// JS:
const a = 'Hello';
a = 'Bye';

// Note:
// "const" is not subject to Variable Hoisting too, which means that "const"
// declarations do not move to the top of the current execution context.
// Also note that ES6 code will run only in browsers that support it. Older
// devices and browsers that do not support ES6 will return a syntax error.


// QUESTION:
// Fill in the blanks to make a constant named "total" and the variable "i" that
// is only accessible inside the loop.
//
//   _____ total = 100;
//   let sum = 0;
//   for (___ i = 0; i < total; i++)
//   {
//       sum += i;
//   }

// ANSWER:
//   const total = 100;
//   let sum = 0;
//   for (let i = 0; i < total; i++)
//   {
//       sum += i;
//   }

// Want to know why?
// Your answer is correct because it declares a constant named "total" with a
// value of "100", initializes a variable named "sum" with a value of "0", and
// uses a "for" loop to iterate over the values of "i" starting from "0" and
// less than the value of "total". The "let" keyword ensures that the variable
// "i" is only accessible within the loop.


// Template Literals in ES6
// Template literals are a way to output variables in the string. 
// Prior to ES6 we had to break the string, for example:

// HTML:

// CSS:

// JS:
let name = 'David';
let msg = 'Welcome ' + name + '!';
console.log(msg);

// ES6 introduces a new way of outputting variable values in strings. The same
// code above can be rewritten as:

let name = 'David';
let msg = `Welcome ${name}!`;
console.log(msg);

// Notice, that template literals are enclosed by the backtick ( ` ) character
// instead of double or single quotes.
// The ${expression} is a placeholder, and can include any expression, which
// will get evaluated and inserted into the template literal.

// For example:
// HTML:

// CSS:

// JS:
let a = 8;
let b = 34;
let msg = `The sum is ${a + b}`;
console.log(msg);

// Note:
// To escape a backtick in a template literal, put a backslash "\" before the
// backtick.


// PRACTICE (PROBLEM SET): London is the capital of GB
// Complete the function to return a string in the required format for your
// country card program.

// ES6 Variables and Strings
// You need to make country cards for a school project.

// The given program takes the country and its capital name as input.
// Complete the function to return a string in the format you are given in the
// sample output:

// Sample Input
// Portugal
// Lisbon

// Sample Output
// Name: Portugal, Capital: Lisbon

// Note:
// Use template literals to output variables in strings.

function main()
{
    var country = readLine();
    var capital = readLine();
    
    console.log(countryCard(country, capital));
}

function countryCard(country, capital)
{
    // Complete the function
    // Use backtick (` `) for template literal
    country = `Name: ${country}`;
    capital = `Capital: ${capital}`;
    return(country + ", " + capital);
}


// QUESTION:
// Fill in the blanks to output "We are learning ES6!".
//
//   let n = 6;
//   let s = 'ES';
//   let msg = `We are learning _{s + n}!`;
//   console.log(___);

// ANSWER:
//   let n = 6;
//   let s = 'ES';
//   let msg = `We are learning ${s + n}!`;
//   console.log(msg);

// Want to know why?
// Your answer is correct because you used template literals with string
// interpolation to concatenate the values of `s` and `n` and create the string
// "ES6". Then, you used that string within the template literal to create the
// final message "We are learning ES6!".
