// ES6 Objects

// JavaScript variables can be Object data types that contain many values called
// properties. 
// An object can also have properties that are function definitions called
// methods for performing actions on the object. 
// ES6 introduces shorthand notations and computed property names that make
// declaring and using objects easier to understand.
// The new method definition shorthand does not require the colon (:) or
// function keyword, as in the grow function of the tree object declaration:

// HTML:

// CSS:

// JS:
let tree =
{
	height: 10,
	color: 'green',
	grow()
	{
		this.height += 2;
	}
};

tree.grow();
console.log(tree.height);  // 12

// You can also use a property value shorthand when initializing properties with
// a variable by the same name.
// For example, properties height and health are being initialized with
// variables named height and health:

let height = 5;
let health = 100;

let athlete =
{
	height,  // height: height,
	health  // health: health
};

console.log(athlete.height);  // 5

// When creating an object by using duplicate property names, the last property
// will overwrite the prior ones of the same name.

var a = {x: 1, x: 2, x: 3, x: 4};
console.log(a.x);  // 4

// Note:
// Duplicate property names generated a SyntaxError in ES5 when using strict
// mode. However, ES6 removed this limitation.


// QUESTION:
// Fill in the blanks to make this code run and print 60.
//
//   let car =
//   {
//       speed: 40,
//       accelerate()
//       {
//           ____.speed += 10;
//       }
//   };
//   ___.accelerate();
//   car.accelerate();
//   console.log(car._____);

// ANSWER:
//   let car =
//   {
//       speed: 40,
//       accelerate()
//       {
//           this.speed += 10;
//       }
//   };
//   car.accelerate();
//   car.accelerate();
//   console.log(car.speed);

// Want to know why?
// Your answer correctly defines an object "car" with a "speed" property and an
// "accelerate()" method that increments the "speed" property by 10. Then the
// "accelerate()" method is called twice on the "car" object and the resulting
// "speed" property value is logged to the console. This outputs "60" which is
// the expected value.


// Computed Property Names 
// With ES6, you can now use computed property names. Using the square bracket
// notation [], we can use an expression for a property name, including
// concatenating strings. This can be useful in cases where we want to create
// certain objects based on user data (e.g. id, email, and so on).

// Here are three examples:
// Example 1:
let prop = 'name';
let id = '1234';
let mobile = '08923';

let user =
{
	[prop]: 'Jack',
	[`user_${id}`]: `${mobile}`
};

console.log(user.name);  // Jack
console.log(user.user_1234);  // 08923

// Example 2:
var i = 0;
var a =
{
	['foo' + ++i]: i,
	['foo' + ++i]: i,
	['foo' + ++i]: i
};

console.log(a.foo1); // 1
console.log(a.foo2); // 2
console.log(a.foo3); // 3


// Example 3:
var param = 'size';
var config =
{
	[param]: 12,
	['mobile' + param.charAt(0).toUpperCase() + param.slice(1)]: 4
};

console.log(config.mobileSize);  // 4

// Note:
// It is very useful when you need to create custom objects based on some
// variables.


// QUESTION:
// Fill in the blanks to create an object with its properties.
//
//   let prop = 'foo';
//   let o =
//   {
//       _prop]: 'lol'_ ['b' + 'ar']_ '123'
//   };

// ANSWER:
//   let prop = 'foo';
//   let o =
//   {
//       [prop]: 'lol', ['b' + 'ar']: '123'
//   };

// Want to know why?
// The answer is correct because it creates an object 'o' with two properties:
// 'foo' with value 'lol', and 'bar' with value '123'. It uses computed property
// names to dynamically create the property names.


// Object.assign() in ES6
// ES6 adds a new Object method assign() that allows us to combine multiple
// sources into one target to create a single new object. 
// Object.assign() is also useful for creating a duplicate of an existing
// object.
// Let's look at the following example to see how to combine objects:

// HTML:

// CSS:

// JS:
let person =
{
	name: 'Jack',
	age: 18,
	sex: 'male'
};
let student =
{
	name: 'Bob',
	age: 20,
	xp: '2'
};

let newStudent = Object.assign({}, person, student);

console.log(newStudent.name); // Bob
console.log(newStudent.age);  // 20
console.log(newStudent.sex);  // male
console.log(newStudent.xp);   // 2

// Here we used Object.assign() where the first parameter is the target object
// you want to apply new properties to.
// Every parameter after the first will be used as sources for the target. There
// are no limitations on the number of source parameters. However, order is
// important because properties in the second parameter will be overridden by
// properties of the same name in third parameter, and so on.

// In the example above, we used a new object {} as the target and used two
// objects as sources.

// Note:
// Try changing the order of second and third parameters to see what happens to
// the result.

// Now, let's see how we can use assign() to create a duplicate object without
// creating a reference (mutating) to the base object.

// In the following example, assignment was used to try to generate a new
// object. However, using = creates a reference to the base object. Because of
// this reference, changes intended for a new object mutate the original object:

// HTML:

// CSS:

// JS:
let person =
{
	name: 'Jack',
	age: 18
};

let newPerson = person;
newPerson.name = 'Bob';

console.log(person.name);    // Bob
console.log(newPerson.name); // Bob

// To avoid this (mutations), use Object.assign() to create a new object.
// For example:

// HTML:

// CSS:

// JS:
let person =
{
	name: 'Jack',
	age: 18
};

let newPerson = Object.assign({}, person);
newPerson.name = 'Bob';

console.log(person.name);    // Jack
console.log(newPerson.name); // Bob

// Finally, you can assign a value to an object property in the Object.assign()
// statement.

// For example:
let person =
{
	name: 'Jack',
	age: 18
};

let newPerson = Object.assign({}, person,
{
	name: 'Bob'
});

console.log(newPerson.name); // Bob

// Note:
// Run the code and see how it works!

// PRACTICE (PROBLEM SET): Workout harder!
// Complete the code to combine basic and advanced level exercises into one new
// object named total.

// ES6 Objects 
// You are making a workout app. After completing the basic exercises in the
// app, the user is able to access advanced content.
// The given program declares two classes - basic and advanced with
// corresponding properties. Complete the code to combine basic and advanced
// level exercises into one new object named total, so that the given code for
// final output works correctly.

// Note:
// Use Object.assign() to perform the requested operation.

let basic =
{
	ex1: 'PushUps: 20 times',
	ex2: 'Jumps: 20 times'
};

let advanced =
{
	ex3: 'Squats: 30 times',
	ex4: 'Run: 2km'
};

// Your code goes here
let total = Object.assign({}, basic, advanced);

for (let ex in total)
{
	console.log(total[ex])
};


// QUESTION:
// What is the output of this code?
//
//   const obj1 =
//   {
//       a: 0,
//       b: 2,
//       c: 4
//   };
//   const obj2 = Object.assign({c: 5, d: 6}, obj1);
//   console.log(obj2.c, obj2.d);
//
//   [ 0 5 ]
//   [ 5 6 ]
//   [ 4 6 ]

// ANSWER:
//   4 6

// Want to know why?
// Your answer is correct because "Object.assign()" copies the values from
// "obj1" to "obj2", but since "obj2" already has a property "c" with a value of
// "5", the value of "c" in "obj1" is overwritten by "5". Therefore, "obj2.c" is
// "5" and "obj2.d" is "6".
