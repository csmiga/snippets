// Module 4 Quiz

// QUESTION:
// Which of the following is not one of the new ES6 features?
//
//   [ Template Literals    ]
//   [ Modules              ]
//   [ Hoisting             ]
//   [ Object destructuring ]

// ANSWER:
//   Hoisting

// Want to know why?
// Your answer that "Hoisting" is not one of the new ES6 features is correct.
// Hoisting is not a new feature introduced in ES6, it has been a part of
// JavaScript since its inception. It refers to the behavior of moving variable
// and function declarations to the top of their respective scopes during the
// compilation phase. ES6 introduced several new features such as let and const
// keywords, arrow functions, and classes.


// QUESTION:
// Fill in the blanks to declare a constant num and an arrow function calc.
//
//   _____ num = 5;
//   const calc = (x, y, z = num) __
//   {
//       return x + y + z;
//   }

// ANSWER:
//   const num = 5;
//   const calc = (x, y, z = num) =>
//   {
//       return x + y + z;
//   }

// Want to know why?
// Your answer is correct because you have declared a constant `num` with a
// value of 5 and an arrow function `calc` that takes in two parameters `x` and
// `y`, and a third parameter `z` that is assigned the value of `num` by
// default. The function returns the sum of `x`, `y`, and `z`.


// QUESTION:
// Fill in blanks to make the variable arr3 look like the following:
// [1, 2, 3, 4, 5, 6, 7, 8].
//
//   const ____ = [1, 2, 3];
//   const arr2 = [5, 6, 7, 8];
//   let arr3 = [___arr1, _, ...arr2];

// ANSWER:
//   const arr1 = [1, 2, 3];
//   const arr2 = [5, 6, 7, 8];
//   let arr3 = [...arr1, 4, ...arr2];

// Want to know why?
// Your answer is correct because you used the spread operator (...) to combine
// the elements of arr1, arr2, and the number 4 into a new array assigned to
// arr3. The resulting array matches the expected output.


// QUESTION:
// What is the output of the following code?
// 
//   const arr1 = [1, 2, 3, 4, 5];
//   const arr2 = [...arr1, 6];
//   const func = (...rest) =>
//   {
//       console.log(rest.length);
//   }
//   func(...arr1);
//   func(...arr2);
//
//   [ 5 6  ]
//   [ rest ]
//   [ 1    ]
//   [ 6 5  ]

// ANSWER:
//   5 6

// Want to know why?
// Your answer is correct because the first call to the "func" function passes
// the spread elements of "arr1", which has a length of 5. The second call
// passes the spread elements of "arr2", which has a length of 6 since it
// includes an additional element. Therefore, the output is "5 6".


// QUESTION:
// What is the output of this code?
//
//   const square = num => num * num;
//   console.log(square(6) + 6);

// ANSWER:
//   42


// QUESTION:
// Fill in the blanks to copy the user object to the newUser object by
// destructuring the name and age properties. Pass the value 9999 for the id
// property.
//
//   const user =
//   {
//       name: 'David',
//       age: 28,
//       id: 1234
//   };
//   let newUser = Object.assign({} {name, ___, } = ____, {__: 9999});
//   console.log(newUser);
//   

// ANSWER:
//   const user =
//   {
//       name: 'David',
//       age: 28,
//       id: 1234
//   };
//   let newUser = Object.assign({} {name, age, } = user, {id: 9999});
//   console.log(newUser);

// Want to know why?
// The answer is correct because it correctly uses object destructuring to
// extract the "name" and "age" properties from the "user" object and assign
// them to "newUser". It also correctly sets the "id" property of "newUser" to
// "9999" using "Object.assign()".


// QUESTION:
// Fill in the blanks to get the following output:
// zero = 0
// one = 1
//
//   let myMap = new Map();
//   myMap.set('zero', _);
//   myMap.___ ('one', 1);
//   ___(let [key, value] of myMap)
//   {
//       console.log(`${___} = ${value}`);

// ANSWER:
//   let myMap = new Map();
//   myMap.set('zero', 0);
//   myMap.set ('one', 1);
//   for(let [key, value] of myMap)
//   {
//       console.log(`${key} = ${value}`);

// Want to know why?
// Your answer is correct because it creates a new Map object, sets the
// key-value pairs for 'zero' and 0, and 'one' and 1. Then, it iterates over the
// Map using a for loop and uses string interpolation to print the key-value
// pairs in the desired format.
