// ES6 Rest Parameters

// Prior to ES6, if we wanted to pass a variable number of arguments to a
// function, we could use the arguments object, an array-like object, to access
// the parameters passed to the function.

// For example, let's write a function that checks if an array contains all the
// arguments passed: 

// HTML:

// CSS:

// JS:
function containsAll(arr)
{
	for (let k = 1; k < arguments.length; k++)
	{
		let num = arguments[k];
		if (arr.indexOf(num) === -1)
		{
			return false;
		}
	}
	return true;
}

let x = [2, 4, 6, 7];
console.log(containsAll(x, 2, 4, 7));
console.log(containsAll(x, 6, 4, 9));

// We can pass any number of arguments to the function and access it using the
// arguments object.
// While this does the job, ES6 provides a more readable syntax to achieve
// variable number of parameters by using a rest parameter:

// HTML:

// CSS:

// JS:
function containsAll(arr, ...nums)
{
	for (let num of nums) 
	
		if (arr.indexOf(num) === -1)
		{
			return false;
		}
	}
	return true;
}

let x = [2, 4, 6, 7];
console.log(containsAll(x, 2, 4, 7));
console.log(containsAll(x, 6, 4, 9));

// The ...nums parameter is called a rest parameter. It takes all the "extra"
// arguments passed to the function. The three dots (...) are called the Spread
// operator.

// Note:
// Only the last parameter of a function may be marked as a rest parameter. If
// there are no extra arguments, the rest parameter will simply be an empty
// array; the rest parameter will never be undefined.

// PRACTICE (PROBLEM SET): Summary calculator
// Create a program to calculate the sum of any number of values.

// Rest & Spread
// You are making a program to calculate the sum of any number of values.
// Complete the given function so that it takes as parameters as many numbers as
// needed and returns the sum.

// Complete the function
// Complete the function
function Add(...numbers)
{
    let sum = 0
    numbers.forEach(number => {sum += number})
    return sum;
}

console.log(Add(1,2,3));
console.log(Add(4,14,5,9,14));
console.log(Add(2,36));


// QUESTION:
// What is the output of the following code?
//
//   function magic(...nums)
//   {
//       let sum = 0;
//       nums.filter(n => n % 2 == 0).map(el => sum+= el);
//       return sum;
//   }
//   console.log(magic(1, 2, 3, 4, 5, 6));

// ANSWER:
//   12


// The Spread Operator 
// This operator is similar to the Rest Parameter, but it has another purpose
// when used in objects or arrays or function calls (arguments).

// Spread in function calls 
// It is common to pass the elements of an array as arguments to a function.
// Before ES6, we used the following method:

// HTML:

// CSS:

// JS:
function myFunction(w, x, y, z)
{
	console.log(w + x + y + z);
}
var args = [1, 2, 3];
myFunction.apply(null, args.concat(4));

// ES6 provides an easy way to do the example above with spread operators:

// HTML:

// CSS:

// JS:
const myFunction = (w, x, y, z) =>
{
    console.log(w + x + y + z);
};
let args = [1, 2, 3];
myFunction(...args, 4);

// Example:

// HTML:

// CSS:

// JS:
var dateFields = [1970, 0, 1];  // 1 Jan 1970
var date = new Date(...dateFields);
console.log(date);

// Spread in array literals 
// Before ES6, we used the following syntax to add an item at middle of an
// array:

// HTML:

// CSS:

// JS:
var arr = ["One", "Two", "Five"];

arr.splice(2, 0, "Three");
arr.splice(3, 0, "Four");
console.log(arr);

// You can use methods such as push, splice, and concat, for example, to achieve
// this in different positions of the array. However, in ES6 the spread operator
// lets us do this more easily:

// HTML:

// CSS:

// JS:
let newArr = ['Three', 'Four']; 
let arr = ['One', 'Two', ...newArr, 'Five'];
console.log(arr);

// Spread in object literals 
// In objects it copies the own enumerable properties from the provided object
// onto a new object.

// HTML:

// CSS:

// JS:
const obj1 = { foo: 'bar', x: 42 };
const obj2 = { foo: 'baz', y: 5 };

const clonedObj = { ...obj1 }; // { foo: "bar", x: 42 }
const mergedObj = { ...obj1, ...obj2 }; // { foo: "baz", x: 42, y: 5 }

// However, if you try to merge them you will not get the result you expected:

// HTML:

// CSS:

// JS:
const obj1 = { foo: 'bar', x: 42 };
const obj2 = { foo: 'baz', y: 5 };
const merge = (...objects) => ({ ...objects });

let mergedObj = merge (obj1, obj2);
// { 0: { foo: 'bar', x: 42 }, 1: { foo: 'baz', y: 5 } }

let mergedObj2 = merge ({}, obj1, obj2);
// { 0: {}, 1: { foo: 'bar', x: 42 }, 2: { foo: 'baz', y: 5 } }

// Note:
// Shallow cloning or merging objects is possible with another operator called
// Object.assign().


// QUESTION:
// What is the output of the following code?
//
//   let nums = [3, 4, 5];
//   let all = [1, 2, ...nums, 6];
//   console.log(all[3]);

// ANSWER:
//   4
