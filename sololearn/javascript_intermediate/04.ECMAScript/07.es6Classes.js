// Classes in ES6

// In this lesson we'll explain how to create a class that can be used to create
// multiple objects of the same structure. 
// A class uses the keyword class and contains a constructor method for
// initializing.

// For example:

class Rectangle
{
    constructor(height, width)
    {
      this.height = height;
      this.width = width;
    }
}

// A declared class can then be used to create multiple objects using the
// keyword new.
// For example:

// HTML:

// CSS:

// JS:
class Rectangle
{
    constructor(height, width)
    {
        this.height = height;
        this.width = width;
    }
}
const square = new Rectangle(5, 5);
const poster = new Rectangle(2, 3); 

console.log(square.height);

// Note:
// Class Declarations are not hoisted while Function Declarations are. If you
// try to access your class before declaring it, ReferenceError will be
// returned.

// You can also define a class with a class expression, where the class can be
// named or unnamed.
// A named class looks like:

// HTML:

// CSS:

// JS:
var Square = class Rectangle
{
	constructor(height, width)
	{
		this.height = height;
		this.width = width;
	}
};
const square = new Square(5, 5);
const poster = new Square(2, 3);

console.log(square.height);

// In the unnamed class expression, a variable is simply assigned the class
// definition:

var Square = class
{
	constructor(height, width)
	{
		this.height = height;
		this.width = width;
	}
};
const square = new Square(5, 5);
const poster = new Square(2, 3);

console.log(square.height);

// Note:
// The constructor is a special method which is used for creating and
// initializing an object created with a class.
// There can be only one constructor in each class.


// QUESTION:
// Fill in the blanks to declare a class Point with a constructor initializing
// its "x" and "y" members.
//
//   _____ Point
//   {
//       _____ (a, b)
//       {
//           this.x = a;
//           this.y = b;
//       }
//       getX() { return this.x; }
//       getY() { return this.y; }
//   _

// ANSWER:
//   class Point
//   {
//       constructor (a, b)
//       {
//           this.x = a;
//           this.y = b;
//       }
//       getX() { return this.x; }
//       getY() { return this.y; }
//   }

// Want to know why?
// Your answer is correct because it declares a class named Point with a
// constructor method that takes two parameters, a and b, which are used to
// initialize the "x" and "y" properties of the object. It also has two getter 
// methods that return the values of "x" and "y".


// Class Methods in ES6 
// ES6 introduced a shorthand that does not require the keyword function for a
// function assigned to a method's name. One type of class method is the 
// prototype method, which is available to objects of the class.

// For Example:
// HTML:

// CSS:

// JS:
class Rectangle
{
	constructor(height, width)
	{
		this.height = height;
		this.width = width;
	}
}

// A declared class can then be used to create multiple objects using the
// keyword new.

// For example:
// HTML:

// CSS:

// JS:
class Rectangle
{
	constructor(height, width)
	{
		this.height = height;
		this.width = width;
	}
}
const square = new Rectangle(5, 5);
const poster = new Rectangle(2, 3);

console.log(square.height);

// Note:
// Class Declarations are not hoisted while Function Declarations are. If you
// try to access your class before declaring it, ReferenceError will be
// returned.

// You can also define a class with a class expression, where the class can be
// named or unnamed.
// A named class looks like:
// HTML:

// CSS:

// JS:
var Square = class Rectangle 
{
	constructor(height, width)
	{
		this.height = height;
		this.width = width;
	}
};
const square = new Square(5, 5);
const poster = new Square(2, 3);

console.log(square.height);

// In the unnamed class expression, a variable is simply assigned the class
// definition:

// HTML:

// CSS:

// JS:
var Square = class
{
	constructor(height, width)
	{
		this.height = height;
		this.width = width;
	}
};
const square = new Square(5, 5);
const poster = new Square(2, 3);

console.log(square.height);

// Note:
// The constructor is a special method which is used for creating and
// initializing an object created with a class.
// There can be only one constructor in each class.


// QUESTION:
// Fill in the blanks to declare a class Point with a constructor initializing
// its "x" and "y" members.
//
//   ____ Point
//   {
//       ___________ (a, b)
//       {
//           this.x = a;
//           this.y = b;
//       }
//       getX() { return this.x; }
//       getY() { return this.y; }
//   _

// ANSWER:
//   class Point
//   {
//       constructor (a, b)
//       {
//           this.x = a;
//           this.y = b;
//       }
//       getX() { return this.x; }
//       getY() { return this.y; }
//   }

// Want to know why?
// Your answer is correct because it declares a class named Point with a
// constructor that initializes the "x" and "y" members using two parameters "a"
// and "b". It also defines two methods getX and getY that return the value of
// "x" and "y" respectively.


// Class Methods in ES6
// ES6 introduced a shorthand that does not require the keyword function for a
// function assigned to a method's name. One type of class method is the
// prototype method, which is available to objects of the class.

// For Example:
// HTML:

// CSS:

// JS:
class Rectangle
{
	constructor(height, width)
	{
		this.height = height;
		this.width = width;
	}

	get area()
	{
		return this.calcArea();
	}

	calcArea()
	{
		return this.height * this.width;
	}
}
const square = new Rectangle(5, 5);

console.log(square.area);  // 25

// Note:
// In the code above, area is a getter, calcArea is a method.
// Another type of method is the static method, which cannot be called through a
// class instance. Static methods are often used to create utility functions for
// an application.

// For Example:
// HTML:

// CSS:

// JS:
class Point
{
	constructor(x, y)
	{
		this.x = x;
		this.y = y;
	}

	static distance(a, b)
	{
		const dx = a.x - b.x;
		const dy = a.y - b.y;

		return Math.hypot(dx, dy);
	}
}
const p1 = new Point(7, 2);
const p2 = new Point(3, 8);

console.log(Point.distance(p1, p2));

// Note:
// As you can see, the static distance method is called directly using the class
// name, without an object.

// PRACTICE (PROBLEM SET): Average exam score
// Add a static method average() to class Exam, so that the code in main work
// correctly.

// Class Methods in ES6 
// You are making a program so that students are able to calculate their average
// of 3 exam scores.
// The given program takes the scores of 3 exams as input and declares the Exams
// class.
// Add a static method average() to class Exams, which will take the scores as
// parameters, and calculate and return the average score rounded to the nearest
// integer so that the code in main works correctly.

// Sample Input
// 74
// 80
// 68

// Sample Output
// 74

// Note:
// Recall Math.round() function to round the number with the floating point to
// the nearest integer.

function main()
{
	var exam1 = parseInt(readLine(), 10);
	var exam2 = parseInt(readLine(), 10);
	var exam3 = parseInt(readLine(), 10);

	console.log(Exams.average(exam1, exam2, exam3));
}

class Exams
{
	// Your code goes here
    static average (a, b, c)
    {
        const avg = (a + b + c)/3;
        return Math.round(avg);
    };
}


// QUESTION:
// Fill in the blanks to output "Rex barks."
//   class Dog
//   {
//       constructor(name)
//       {
//           ____.name = name;
//       }
//
//       bark()
//       {
//           console.log(this.____ + ' barks.');
//       }
//   }
//   let d = new Dog('Rex');
//   d.____();

// ANSWER:
//   class Dog
//   {
//       constructor(name)
//       {
//           thi.name = name;
//       }
//
//       bark()
//       {
//           console.log(this.name + ' barks.');
//       }
//   }
//   let d = new Dog('Rex');
//   d.bark();

// Want to know why?
// Your answer is correct because you have created a `Dog` class with a
// constructor that sets the dog's name and a `bark` method that logs the name
// followed by "barks." You then created an instance of the `Dog` class with the
// name "Rex" and called the `bark` method on it, resulting in the output
// "Rex barks."


// Inheritance in ES6 
// The extends keyword is used in class declarations or class expressions to
// create a child of a class. The child inherits the properties and methods of
// the parent.

// For example:
// HTML:

// CSS:

// JS:
class Animal
{
	constructor(name)
	{
		this.name = name;
	}
	speak()
	{
		console.log(this.name + ' makes a noise.');
	}
}

class Dog extends Animal
{
	speak()
	{
		console.log(this.name + ' barks.');
	}
}
let dog = new Dog('Rex');
dog.speak();  // Rex barks.

// In the code above, the Dog class is a child of the Animal class, inheriting
// its properties and methods.

// Note:
// If there is a constructor present in the subclass, it needs to first call
// super() before using this. Also, the super keyword is used to call parent's
// methods.

// For example, we can modify the program above to the following:
class Animal
{
	constructor(name)
	{
		this.name = name;
	}
	speak()
	{
		console.log(this.name + ' makes a noise.');
	}
}

class Dog extends Animal
{
	speak()
	{
		super.speak();  // Super
		console.log(this.name + ' barks.');
	}
}

let dog = new Dog('Rex');
dog.speak();
// Rex makes a noise.
// Rex barks.

// Note:
// In the code above, the parent's speak() method is called using the super
// keyword.


// QUESTION:
// Fill in the blanks to declare a class Student which inherits from the Human
// class.
//
//   _____ Human
//   {
//       constructor(name)
//       {
//           this.name = name;
//       }
//   }
//
//   class Student _______ Human
//   {
//       constructor(name, age)
//       {
//           _____(name);
//           this.age = age;
//       }
//   }

// ANSWER:
//   class Human
//   {
//       constructor(name)
//       {
//           this.name = name;
//       }
//   }
//
//   class Student extends Human
//   {
//       constructor(name, age)
//       {
//           super(name);
//           this.age = age;
//       }
//   }

// Want to know why?
// Your answer is correct because it defines a new class "Student" using the
// "extends" keyword to inherit from the "Human" class. The "constructor" method
// of the "Student" class calls the parent class constructor via the "super"
// keyword to set the "name" property, and also sets a new "age" property
// specific to the "Student" class.
