// ES6 Map 

// A Map object can be used to hold key/value pairs. A key or value in a map can
// be anything (objects and primitive values). 

// The syntax new Map([iterable]) creates a Map object where iterable is an
// array or any other iterable object whose elements are arrays (with a
// key/value pair each).

// An Object is similar to Map but there are important differences that make
// using a Map preferable in certain cases:

// 1) The keys can be any type including functions, objects, and any primitive.
// 2) You can get the size of a Map.
// 3) You can directly iterate over Map.
// 4) Performance of the Map is better in scenarios involving frequent addition
//    and removal of key/value pairs.

// The size property returns the number of key/value pairs in a map.

// For example:
// HTML:

// CSS:

// JS:
let map = new Map([['k1', 'v1'], ['k2', 'v2']]);

console.log(map.size);  // 2

// Methods
// set(key, value) Adds a specified key/value pair to the map. If the specified key already exists, value corresponding to it is replaced with the specified value.
// get(key) Gets the value corresponding to a specified key in the map. If the specified key doesn't exist, undefined is returned.
// has(key) Returns true if a specified key exists in the map and false otherwise.
// delete(key) Deletes the key/value pair with a specified key from the map and returns true. Returns false if the element does not exist.
// clear() Removes all key/value pairs from map.
// keys() Returns an Iterator of keys in the map for each element.
// values() Returns an Iterator of values in the map for each element.
// entries() Returns an Iterator of array[key, value] in the map for each element.

// For example:
// HTML:

// CSS:

// JS:
let map = new Map();
map.set('k1', 'v1').set('k2', 'v2');

console.log(map.get('k1')); // v1
console.log(map.has('k2')); // true

for (let kv of map.entries())
	console.log(kv[0] + " : " + kv[1]);

// The above example demonstrates some of the ES6 Map methods.

// Note:
// Map supports different data types i.e. 1 and "1" are two different
// keys/values.

// PRACTICE (PROBLEM SET): Employee map
// Complete the program to output the list of employees in format shown in
// sample output.
//
// ES6 Map 
// Five employees at a company are stored in Map in the program you are given.
// Their names are set as keys and their positions as values. The company is
// hiring one more employee. The program should take the name and the position
// as inputs and store them in the existing map.
// Complete the program to perform that operation and output to the console the
// list of employees in the format shown in the sample output.

// Sample Input
// Bob
// Developer

// Sample Output
// Richard : Developer
// Maria : SEO Specialist
// Tom : Product Manager
// David : Accountant
// Sophia : HR Manager
// Bob : Developer

// Note:
// The entries() method returns an Iterator of array[key, value] in the map for
// each element.
// Don't forget to put spaces before and after the colon (:) in the output.

function main()
{
    var name = readLine();
    var position = readLine();
    let employees = new Map([
    ["Richard", "Developer"],
    ["Maria", "SEO Specialist"],
    ["Tom", "Product Manager"],
    ["David", "Accountant"],
    ["Sophia", "HR Manager"]
    ]);
    
    // Add the new pair to the map
    
    // Your code heres goes
    employees.set(name,position)
    for(var i of employees.entries())
    {
        console.log(i [0] + " : " + i [1])
    }
}


// QUESTION:
// What is the output of this code?
//
//   const map = new Map();
//   map.set('one', 1);
//   map.set('2', 'two');
//   if (map.has('two'))
//   {
//       console.log('two');
//   }
//   else
//   {
//       console.log(map.get('one'));
//   }
//
//   [ 1              ]
//   [ Invalid Syntax ]
//   [ True           ]
//   [ undefined      ]

// ANSWER:
//   1

// Want to know why?
// Your answer is correct because the "if" statement checks if the map has a key
// named "two", which it doesn't. Therefore, the "else" block is executed and
// "map.get('one')" returns the value associated with the "one" key, which is 1.
// So, the output will be 1.


// ES6 Set 
// A Set object can be used to hold unique values (no repetitions are allowed).
// A value in a set can be anything (objects and primitive values). 

// The syntax new Set([iterable]) creates a Set object where iterable is an
// array or any other iterable object of values.

// The size property returns the number of distinct values in a set.

// For example:
// HTML:

// CSS:

// JS:
let set = new Set([1, 2, 4, 2, 59, 9, 4, 9, 1]);
console.log(set.size);  // 5

// Methods
// add(value) Adds a new element with the given value to the Set.
// delete(value) Deletes a specified value from the set.
// has(value) Returns true if a specified value exists in the set and false
//            otherwise.
// clear() Clears the set.
// values() Returns an Iterator of values in the set.

// For example:
// HTML:

// CSS:

// JS:
let set = new Set();
set.add(5).add(9).add(59).add(9);

console.log(set.has(9));

for (let v of set.values())
    console.log(v);

// The above example demonstrates some of the ES6 Set methods.

// Note:
// Set supports different data types i.e. 1 and "1" are two different values.
// NaN and undefined can also be stored in Set.

// QUESTION:
// Fill in the blanks to create and output a set with the values 1, 2, 3.
//
//   const set = ___ Set();
//   set.add(1).add(2).___(3);
//   or(let v of set.values())
//   console.log(_____);

// ANSWER:
//   const set = new Set();
//   set.add(1).add(2).add(3);
//   or(let v of set.values())
//   console.log(v);

// Want to know why?
// Your answer is correct because it creates a new empty Set object using the
// "new Set()" constructor and adds the values 1, 2, and 3 to it using the
// "add()" method. Then it iterates through the set values using a "for..of"
// loop and prints each value to the console using "console.log()".
