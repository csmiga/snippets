// The Math Object

// The Math object allows you to perform mathematical tasks and includes several
// properties.

//    ----------------------------------------------------
//   | Property | Description                             |
//   |----------|-----------------------------------------|
//   | E        | Euler's Constant                        |
//   | LN2      | Natural Log of the value 2              |
//   | LN10     | Natural Log of the value 10             |
//   | LOG2E    | The base 2 log of Euler's constant (E)  |
//   | LOG10E   | The base 10 log of Euler's constant (E) |
//   | PI       | Retuns the constant Pi                  |
//    ----------------------------------------------------

// For example:

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
document.write(Math.PI);


// QUESTION:
// In the Math Object, which of the following constants does NOT exist?
//
//   [ Math.E   ]
//   [ Math.PI  ]
//   [ Math.ABC ]

// ANSWER:
//   Math.ABC

// Want to know why?
// Your answer is correct because there is no constant called "Math.ABC" in the
// Math Object. The available constants in the Math Object are E, LN10, LN2,
// LOG10E, LOG2E, PI, SQRT1_2, and SQRT2. Therefore, Math.ABC is not a valid
// constant in the Math Object.


// Math Object Methods
// The Math object contains a number of methods that are used for calculations:

//    ------------------------------------------------------------------------
//   | Method           | Description                                         |
//   |------------------|-----------------------------------------------------|
//   | abs(x)           | Returns the absolute value of x                     |
//   | acos(x)          | Returns the arccosine of x, in radians              |
//   | asin(x)          | Returns the arcsine of x, in radians                |
//   | atan(x)          | Returns the arctangent of x as a numeric value      |
//   |                  | between -Pi/2 and Pi/2 radians                      |
//   | atan2(y,x)       | Returns the arctagent of the quotant of its         |
//   |                  | arguments                                           |
//   | ceil(x)          | Returns x, rounded upwards to the nearest integer   |
//   | cos(x)           | Returns to cosine of x (x is in radians)            |
//   | exp(x)           | Returns the value of E^2                            |
//   | floor(x)         | Returns x, rounded downwards to the nearest integer |
//   | log(x)           | Returns the natural logarithm (base E of x)         |
//   | max(x,y,z,...,n) | Returns the number with the highest value           |
//   | min(x,y,z,...,n) | Returns the number with the lowest value            |
//   | pow(x,y)         | Returns the value of x to the power of y            |
//   | random()         | Returns a random number between 0 and 1             |
//   | round(x)         | Rounds x to the nearest integer                     |
//   | sin(x)           | Returns the sine of x (x is in radians)             |
//   | sqrt(x)          | Returns the square root of x                        |
//   | tan(x)           | Returns the tangent of an angle                     |
//    ------------------------------------------------------------------------

// For example, the following will calculate the square root of a number.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var number = Math.sqrt(4); 
document.write(number);

// Note:
// To get a random number between 1-10, use Math.random(), which gives you a
// number between 0-1. Then multiply the number by 10, and then take Math.ceil()
// from it: Math.ceil(Math.random() * 10).

// Practice: Which Century? (The Math Object)
// Create a function that returns the century depending on the year given as a
// parameter.

// Sample Input
// 1993

// Sample Output
// 20

// Hint
// You need to divide 1993 by 100: 1993/100 = 19.93, then round it to the
// nearest integer, which is 20 in this case.

// Use Math.ceil(x), which returns x rounded upwards to the nearest integer.

// Note:
// Use Math.ceil(x), which returns x rounded upwards to the nearest integer.

function main()
{
    var year = parseInt(readLine(), 10)
    
    // The output
    console.log(calcCent(year));
    
}

// Complete the function
function calcCent()
{
    var century = year / 100;
    return Math.ceil(century);
}


// QUESTION:
// In the Math Object, which of the following methods is used to calculate the
// square root?
//
//   [ round ]
//   [ sqrt ]
//   [ root ]
//   [ ceil ]

// ANSWER:
//   sqrt

// Want to know why?
// Your answer is correct. The `Math.sqrt()` method is used to calculate the
// square root of a number in JavaScript. This method takes a single argument
// and returns the square root of that number.


// Let's create a program that will ask the user to input a number and alert its
// square root.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var n = prompt("Enter a number", "");
var answer = Math.sqrt(n);
alert("The square root of " + n + " is " + answer);

// Result:
//    ----------------
//   | Enter a number |
//   |----------------|
//   |       64       |
//    ----------------

// Enter a number, such as 64.

//    --------------------------------------------------------
//   |                                                        |
//   | The square root of 64 is 8                             |
//   | [ ] Prevent this page from creating additional dialogs |
//   |                                                        |
//   |                                                 [ OK ] |
//   |                                                        |
//    --------------------------------------------------------

// Note:
// Math is a handy object. You can save a lot of time using Math, instead of
// writing your own functions every time.


// QUESTION:
// Math.sqrt(81);

// ANSWER:
//   9
