// Associative Arrays

// While many programming languages support arrays with named indexes
// (text instead of numbers), called associative arrays, JavaScript does not.
// However, you still can use the named array syntax, which will produce an
// object.

// For example:

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var person = [];  // Empty array
person["name"] = "John";
person["age"] = 46;
document.write(person["age"]);

// Now, the person is treated as an object, instead of being an array.
// The named indexes name and age become properties of the person object.

// Note:
// As the person array is treated as an object, the standard array methods and
// properties will produce incorrect results. For example, person.length will
// return 0.


// QUESTION:
// In associative arrays, index numbers are replaced with
//
//   [ variable names ]
//   [ constants      ]
//   [ functions      ]
//   [ strings        ]

// ANSWER:
//   strings

// Want to know why?
// The statement is correct because in Javascript, associative arrays are
// actually objects, and the keys used to access their values are strings.
// Therefore, the index numbers in associative arrays are replaced with strings.


// Remember that JavaScript does not support arrays with named indexes.
// In JavaScript, arrays always use numbered indexes.
// It is better to use an object when you want the index to be a string (text).
// Use an array when you want the index to be a number.

// If you use a named index, JavaScript will redefine the array to a standard
// object.


// QUESTION:
// In order to use associative arrays, the "associated" name is put in:
//
//   [ curly braces { } ]
//   [ brackets [ ]     ]
//   [ parentheses ( )  ]

// ANSWER:
//   brackets [ ]

// Want to know why?
// The answer "brackets [ ]" is correct because in JavaScript, associative
// arrays are implemented using objects, and properties of an object can be
// accessed using bracket notation with the property name as a string.
// Therefore, to access a value associated with a specific name in an
// associative array, you would use brackets with the name as the key.

