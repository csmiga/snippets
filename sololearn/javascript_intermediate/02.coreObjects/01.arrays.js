// JavaScript Arrays

// Arrays store multiple values in a single variable. 
// To store three course names, you need three variables.

var course1 = "HTML";
var course2 = "CSS";
var course3 = "JS";

// But what if you had 500 courses? The solution is an array.

var courses = new Array("HTML", "CSS", "JS");

// Note:
// This syntax declares an array named courses, which stores three values, or
// elements.


// QUESTION:
// What two keywords do we need to create an array?
//
//   [ ☐ object   ]
//   [ ☐ function ]
//   [ ☐ Array    ]
//   [ ☐ new      ]

// ANSWER:
//   ☑ Array
//   ☑ new

// Want to know why?
// Your answer of "Array" and "new" is correct because "Array" is the keyword
// used to declare an array and "new" is used to create a new instance of an
// array object. Together, "Array" and "new" are used to create an empty array
// or an array with initial values.


// Accessing an Array

// You refer to an array element by referring to the index number written in
// square brackets.
// This statement accesses the value of the first element in courses and changes
// the value of the second element.

var courses = new Array("HTML", "CSS", "JS");
var course = courses[0];  // HTML
courses[1] = "C++";       // Changes the second element

// Note:
// [0] is the first element in an array. [1] is the second. Array indexes start
// with 0.


// QUESTION:
// What is the output of this code?
//
//   var arr = new Array(3, 6, 8);
//   document.write(arr[1]);

// ANSWER:
//   6


// Attempting to access an index outside of the array, returns the value
// undefined.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var courses = new Array("HTML", "CSS", "JS");
document.write(courses[10]);

// Note:
// Our courses array has just 3 elements, so the 10th index, which is the 11th
// element, does not exist (is undefined).

// Practice: Fluffy Pancakes (Arrays)
// The array you are given represents the menu of breakfast options available at
// the hotel.
// The Chef decided to replace one of the options with "Fluffy Pancakes".
// Write a program to take the index as input, replace the element with that
// index with "Fluffy Pancakes", and output the new menu to the console as an
// array.

// Sample Input
// 2

// Sample Output
// [
//     'Cinnamon Doughnuts',
//     'Waffles',
//     'Fluffy Pancakes',
//     'Chorizo Burrito',
//     'French Toast'
// ]

// The element with index 2 has been replaced in the output array.

// Note:
// Remember that the first element of an array has 0 index.

function main()
{
	var breakfasts =
    [
        'Cinnamon Doughnuts',
        'Waffles', 'Granola',
        'Chorizo Burrito',
        'French Toast'
    ];
	var index = parseInt(readLine(), 10)

	// Replace the corresponding element by "Fluffy Pancakes"
    breakfasts[index] = 'Fluffy Pancakes';

	// Output the menu to the console
    console.log(breakfasts);
}


// QUESTION:
// What is the result of trying to reference an array member which does not
// exist?
//
//   [ false     ]
//   [ 0         ]
//   [ undefined ]
//   [ null      ]

// ANSWER:
//   undefined

// Want to know why?
// Great job! Your answer is correct. When trying to reference an array member
// that does not exist, JavaScript returns "undefined" because there is no value
// to return.
