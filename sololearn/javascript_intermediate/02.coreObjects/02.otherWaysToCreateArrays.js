// Creating Arrays

// You can also declare an array, tell it the number of elements it will store,
// and add the elements later.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var courses = new Array(3);
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";

document.write(courses[2]);

// Note:
// An array is a special type of object.
// An array uses numbers to access its elements, and an object uses names to
// access its members.


// QUESTION:
// Please insert the missing characters to output the third member of the array:
//
//   document.write(example___);

// ANSWER:
//   document.write(example[2]);

// Want to know why?
// Great job! Your answer is correct because to access a specific element in an
// array, you can use square brackets with the index number of the element you
// want to access. In this case, the third element has an index of 2 (remember
// that arrays in JavaScript are zero-indexed). The `document.write()` function
// is then used to output the value of this element.


// Creating Arrays

// JavaScript arrays are dynamic, so you can declare an array and not pass any
// arguments with the Array() constructor. You can then add the elements
// dynamically.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var courses = new Array();
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";
courses[3] = "C++";

document.write(courses[2]);

// Note:
// You can add as many elements as you need to.


// Array Literal

// For greater simplicity, readability, and execution speed, you can also
// declare arrays using the array literal syntax.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var courses = ["HTML", "CSS", "JS"];

document.write(courses[2]);

// This results in the same array as the one created with the new Array()
// syntax.

// Note:
// You can access and modify the elements of the array using the index number,
// as you did before.
// The array literal syntax is the recommended way to declare arrays.


// QUESTION:
// By entering var example = new Array(); we create an empty array that can be
// filled...
//
//   [ nevermore     ]
//   [ just after it ]
//   [ anytime later ]

// ANSWER:
//   anytime later

// Want to know why?
// Great job! Your answer is correct because creating a new array with
// `var example = new Array();` means that an empty array is created but it can
// be filled with any type of data at any time later in the code.
