// Array Properties & Methods

// The length Property
// JavaScript arrays have useful built-in properties and methods.
// An array's length property returns the number of its elements.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var courses = ["HTML", "CSS", "JS"];
document.write(courses.length);

// Note:
// The length property is always one more than the highest array index.
// If the array is empty, the length property returns 0.


// QUESTION:
// Array has the length property, because it is:
//
//   [ a variable ]
//   [ an object  ]
//   [ a function ]

// ANSWER:
//   an object

// Want to know why?
// Your answer is correct because in JavaScript, arrays are considered objects.
// Like every object, an array has properties and methods that can be accessed
// using dot notation. One of the properties of an array is its length, which
// returns the number of elements in the array.


// Combining Arrays

// JavaScript's concat() method allows you to join arrays and create an entirely
// new array.

// Example:

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var c1 = ["HTML", "CSS"];
var c2 = ["JS", "C++"];
var courses = c1.concat(c2);

//document.write(courses);
document.write(courses[2]);

// The courses array that results contains 4 elements (HTML, CSS, JS, C++).

// Note:
// The concat operation does not affect the c1 and c2 arrays - it returns the
// resulting concatenation as a new array.


// Practice: Level Up! (Array Properties & Methods)
// The player receives points after passing each level of a game.
// The program given takes the number of passed levels as input, followed by the
// points gained for each level, and creates the corresponding array of points.
// Complete the program to calculate and output to the console the sum of all
// gained points.

// Sample Input
// 3
// 1
// 4
// 8

// Sample Output
// 13

// Explanation
// The first input represents the number of passed levels, -- in this case,
// 3 (the size of an array to be created). The next 3 inputs are the points
// awarded to the player for passing each level. The player gained 1+4+8 points
// for 3 passed levels, which is then output.

// Note:
// Notice that the first inputted number can be used as the length of an array.

function main()
{
    // Take the number of passed levels
    var levels = parseInt(readLine(),10);
    var points = new Array();
    
    var count = 0;
    while(count < levels)
    {
        var elem = parseInt(readLine(),10);
        points[count] = elem;
        count++;
    }
    
    var sum = 0;
    // Calculate the sum of points
    for (let count = 0; count < points.length; count++)
    {
        sum += points[count];
    }
    
    // Output
    console.log(sum);
}


// QUESTION:
// The concat method takes two arrays and:
//
//   [ outputs them to the screen     ]
//   [ combines them in one new array ]
//   [ compares their members         ]

// ANSWER:
//   combines them in one new array

// Want to know why?
// Your answer is correct because the `concat` method in JavaScript combines two
// arrays into a new array, without modifying the original arrays. The resulting
// array contains all the elements from the original arrays, in the order they
// were specified as arguments.
