// Module 2 Quiz

// QUESTION:
// Given the array below, please complete the expression to be alerted with
// "apple".
//
//   var fruits = new Array("pear", "orange", "apple", "grapefruit");
//   alert(fruits___);

// ANSWER:
//   var fruits = new Array("pear", "orange", "apple", "grapefruit");
//   alert(fruits[2]);

// Want to know why?
// Your answer is correct because you declared an array `fruits` with four
// elements and `alert(fruits[2]);` displays the third element `"apple"`.


// QUESTION:
// What is the result of the following expression?
//
//   alert(Math.sqrt(36));
//
//   [ 12 ]
//   [ 0 ]
//   [ 36 ]
//   [ 6 ]

// ANSWER:
//   6

// Want to know why?
// The correct answer is 6. The `Math.sqrt()` method returns the square root of
// a number. In this case, the number is 36. The square root of 36 is 6. The
// `alert()` function displays the result in an alert box.


// QUESTION:
// Please fill in the blanks to output the current minutes.
//
//   var date = new Date();
//   alert(____.___Minutes());

// ANSWER:
//   var date = new Date();
//   alert(date.getMinutes());

// Want to know why?
// The code you provided correctly creates a Date object representing the
// current date and time. When you call the `getMinutes()` method on this
// object, it returns the current minute of the hour as a number from 0 to 59.
// Therefore, your code correctly outputs the current minutes.


// QUESTION:
// What is the output of this code?
//
//   var arr = new Array("a", "b", "c"); 
//   alert(arr[1]);

// ANSWER:
//   b


// QUESTION:
// Drag and drop from the options below to get alerted with the value of the PI
// constant.
//
//   ____(____.__);
// 
//   [ Math ]  [ PI ]  [ print ]  [ const ]  [ alert ]  

// ANSWER:
//   Alert(Math.PI);

// Want to know why?
// Your answer is correct because `Math.PI` is a property of the built-in `Math`
// object in JavaScript that represents the value of the mathematical constant
// pi (approximately 3.141592653589793). When passed as an argument to the
// `alert()` function, it displays the value of `Math.PI` in an alert dialog
// box.
