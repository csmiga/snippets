// The Date Object

// setInterval
// The setInterval() method calls a function or evaluates an expression at
// specified intervals (in milliseconds).
// It will continue calling the function until clearInterval() is called or the
// window is closed.

// For example:

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
function myAlert()
{
    alert("Hi");
}
setInterval(myAlert, 3000);

// This will call the myAlert function every 3 seconds (1000 ms = 1 second).

// Note:
// Write the name of the function without parentheses when passing it into the
// setInterval method.


// QUESTION:
// Fill in the blanks to call the function calc() every 2 seconds:
//
// setInterval(____, ____);

// ANSWER:
// setInterval(calc, 2000);

// Want to know why?
// Your answer is correct because the `setInterval()` method is used to
// repeatedly call a function or execute a code snippet at a specified time
// interval. In this case, `calc` is the function to be called and `2000` is the
// time interval in milliseconds, so `calc()` will be executed every 2 seconds
// (2000 milliseconds).


// The Date Object
// The Date object enables us to work with dates.
// A date consists of a year, a month, a day, an hour, a minute, a second, and
// milliseconds.

// Using new Date(), create a new date object with the current date and time.

var d = new Date();
// d stores the current date and time

// The other ways to initialize dates allow for the creation of new date objects
// from the specified date and time.

new Date(milliseconds)
new Date(dateString)
new Date(year, month, day, hours, minutes, seconds, milliseconds)

// Note:
// JavaScript dates are calculated in milliseconds from 01 January 1970 at
// 00:00:00 Universal Time (UTC). One day contains 86,400,000 milliseconds.

// For example:

// Fri Jan 02 1970 00:00:00
var d1 = new Date(86400000);

// Fri Jan 02 2015 10:42:00
var d2 = new Date("January 2, 2015 10:42:00");

// Sat Jun 11 1988 11:42:00
var d3 = new Date(88,5,11,11,42,0,0);

// Note:
// JavaScript counts months from 0 to 11. January is 0, and December is 11.
// Date objects are static, rather than dynamic. The computer time is ticking,
// but date objects don't change, once created.


// QUESTION:
// What information results from creating a Date Object?
//
//   [ An empty string                       ]
//   [ The current date and time             ]
//   [ The date when the web page was hosted ]

// ANSWER:
//   The current date and time

// Want to know why?
// Good job! Your answer is correct. Creating a Date object in JavaScript will
// result in the current date and time information. This object provides various
// methods to manipulate and retrieve information about the date and time.

// Date Methods
// When a Date object is created, a number of methods make it possible to
// perform operations on it. 

//    -----------------------------------------------
//   | Method            | Description               |
//   |-------------------|---------------------------|
//   | getFullYear       | gets the year             |
//   | getMonth()        | gets the month            |
//   | getDate()         | gets the day of the month |
//   | getDay()          | gets the day of the week  |
//   | getHours()        | gets the hours            |
//   | getMinutes()      | gets the minutes          |
//   | getSeconds()      | gets the seconds          |
//   | getMilliseconds() | gets the milliseconds     |
//    -----------------------------------------------

// For example:

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
var d = new Date();
var hours = d.getHours();

console.log(hours);

// Let's create a program that prints the current time to the browser once every
// second.

// HTML:
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body></body>
</html>

// CSS:

// JS:
function printTime()
{
    var d = new Date();
    var hours = d.getHours();
    var mins = d.getMinutes();
    var secs = d.getSeconds();
    document.body.innerHTML = hours+":"+mins+":"+secs;
}
setInterval(printTime, 1000);

// We declared a function printTime(), which gets the current time from the date
// object, and prints it to the screen.
// We then called the function once every second, using the setInterval method.

// Note:
// The innerHTML property sets or returns the HTML content of an element.

// In our case, we are changing the HTML content of our document's body. This
// overwrites the content every second, instead of printing it repeatedly to the
// screen.

// Practice: Monday to Sunday (The Date Object)
// The program you are given takes year, month and day as input.
// Create a function that takes them as arguments and returns the corresponding
// day of the week.

// Sample Input
// 
// 1993
// 7
// 12

// Sample Output
// Thursday

// Hint:
// The given code creates a Date object from the parameters. Use the getDay()
// method of the date object to get the index, then use it in the given names
// array to return the name of the day.

function main()
{
   var year = parseInt(readLine(), 10);
   var month = parseInt(readLine(), 10);
   var day = parseInt(readLine(), 10);
    
   console.log(getWeekDay(year, month, day));
}

function getWeekDay(year, month, day)
{
   var names = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
   var d = new Date(year, month, day);

   // Complete the function
   return names[d.getDay()];
}


// QUESTION:
// Fill in the blanks to initialize a date object representing the current date
// and time:
//
//   var date = ___ Date();

// ANSWER:
//   var date = new Date();

// Want to know why?
// Your answer is correct because the `Date()` constructor with no arguments
// returns a new `Date` object representing the current date and time based on
// the user's system clock. Storing this object in a variable allows you to work
// with the current date and time in your JavaScript code.