// Loops in ECMAScript 6:
// In JavaScript we commonly use the for loop to iterate over values in a list:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6465/1
// HTML:

// CSS:

// JS:
let arr = [1, 2, 3];
for (let k = 0; k < arr.length; k++) {
    console.log(arr[k]);
}

// The for...in loop is intended for iterating over the enumerable keys of an
// object.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6465/1
// HTML:

// CSS:

// JS:
let obj = { a: 1, b: 2, c: 3 };
for (let v in obj) {
    console.log(v);
}

// Note: The for...in loop should NOT be used to iterate over arrays because,
// depending on the JavaScript engine, it could iterate in an arbitrary order.
// Also, the iterating variable is a string, not a number, so if you try to do
// any math with the variable, you'll be performing string concatenation instead
// of addition.

// ES6 introduces the new for...of loop, which creates a loop iterating over
// iterable objects.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6465/1
// HTML:

// CSS:

// JS:
let list = ["x", "y", "z"];
for (let val of list) {
    console.log(val);
}

// During each iteration the val variable is assigned the corresponding element
// in the list.

// The for...of loop works for other iterable objects as well, including strings

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6465/1
// HTML:

// CSS:

// JS:
for (let ch of "Hello") {
    console.log(ch);
}

// Note: The for...of loop also works on the newly introduced collections (Map,
// Set, WeakMap, and WeakSet). We will learn about them in the upcoming lessons.
// Note that ES6 code will run only in browsers that support it.Older devices
// and browsers that do not support ES6 will return a syntax error.


// Question:
//   Loops in ECMAScript 6
//   Fill in the blanks to iterate through all the characters using the
//   for...of loop.
//     ___ (let ch __ "SoloLearn") {
//         console.log(ch);
//     }

// Answer:
//   for (let ch of "SoloLearn") {
//       console.log(ch);
//   }


// Functions in ECMAScript 6:
// Prior to ES6, a JavaScript function was defined like this:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6513/1
// HTML:

// CSS:

// JS:
function add(x, y) {
    var sum = x + y;
    console.log(sum);
}
add(35, 7);


// ES6 introduces a new syntax for writing functions. The same function from
// above can be written as:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6513/1
// HTML:

// CSS:

// JS:
const add = (x, y) => {
    let sum = x + y;
    console.log(sum);
}
add(35, 7);

// This new syntax is quite handy when you just need a simple function with one
// argument.
// You can skip typing function and return, as well as some parentheses and
// braces.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6513/1
// HTML:

// CSS:

// JS:
const greet = x => "Welcome " + x;
alert(greet("David"));

// The code above defines a function named greet that has one argument and
// returns a message.

// If there are no parameters, an empty pair of parentheses should be used, as
// in

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6513/1
// HTML:

// CSS:

// JS:
const x = () => alert("Hi");
x();

// The syntax is very useful for inline functions.For example, let's say we have
// an array, and for each element of the array we need to execute a function. We
// use the forEach method of the array to call a function for each element:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6513/1
// HTML:

// CSS:

// JS:
var arr = [2, 3, 7, 8];

arr.forEach(function (el) {
    console.log(el * 2);
});

// However, in ES6, the code above can be rewritten as following:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6513/1
// HTML:

// CSS:

// JS:
const arr = [2, 3, 7, 8];

arr.forEach(v => {
    console.log(v * 2);
});

// Note: The code is shorter and looks pretty nice, doesn't it? :)


// Question:
//   Functions in ECMAScript 6
//   Fill in the blanks to declare an arrow function that takes an array and
//   prints the odd elements.
//     const printOdds = (arr) __ {
//         ___.forEach(__ => {
//             if (el % 2 != 0) console.log(el);
//         });
//     }

// Answer:
//   const printOdds = (arr) => {
//       arr.forEach(el => {
//           if (el % 2 != 0) console.log(el);
//       });
//   }


// Default Parameters in ES6:
// In ES6, we can put the default values right in the signature of the
// functions.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2970/6518/1
// HTML:

// CSS:

// JS:
/*
function test(a, b = 3, c = 42) {
  return a + b + c;
}
console.log(test(5));
*/

// Full ES6 equivalent
const test = (a, b = 3, c = 42) => a + b + c;
console.log(test(5));

// And here's an example of an arrow function with default parameters:

//   const test = (a, b = 3, c = 42) => {
//       return a + b + c;
//   }
//   console.log(test(5)); //50

// Note: Default value expressions are evaluated at function call time from left
// to right. This also means that default expressions can use the values of
// previously-filled parameters.

// Question:
//   Default Parameters in ES6:
//   What is the output of this code?
//     function magic(a, b = 40) {
//         return a + b;
//     }
//     console.log(magic(2));

// Answer:
//   42
