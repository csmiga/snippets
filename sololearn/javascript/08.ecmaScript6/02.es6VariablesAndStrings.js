// var & let:
// In ES6 we have three ways of declaring variables:

//   var a = 10;
//   const b = 'hello';
//   let c = true;

// The type of declaration used depends on the necessary scope. Scope is the
// fundamental concept in all programming languages that defines the visibility
// of a variable.

// var & let
// Unlike the var keyword, which defines a variable globally, or locally to an
// entire function regardless of block scope, let allows you to declare
// variables that are limited in scope to the block, statement, or expression in
// which they are used.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2969/6464/1
// HTML:

// CSS:

// JS:
if (true) {
    let name = 'Jack';
}
alert(name);

// In this case, the name variable is accessible only in the scope of the if
// statement because it was declared as let.

// To demonstrate the difference in scope between var and let, consider this
// example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2969/6464/1
// HTML:

// CSS:

// JS:
function varTest() {
    var x = 1;
    if (true) {
        var x = 2;  // same variable
        console.log(x);  // 2
    }
    console.log(x);  // 2
}

function letTest() {
    let x = 1;
    if (true) {
        let x = 2;  // different variable
        console.log(x);  // 2
    }
    console.log(x);  // 1
}

varTest();
letTest();

// One of the best uses for let is in loops: 

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2969/6464/1
// HTML:

// CSS:

// JS:
for (let i = 0; i < 3; i++) {
    document.write(i);
}

// Here, the i variable is accessible only within the scope of the for loop,
// where it is needed.

// Note: let is not subject to Variable Hoisting, which means that let
// declarations do not move to the top of the current execution context.


// Question:
//   var & let
//   What is the output of this code?
//     function letItBe() {
//         let v = 2;
//         if (true) {
//             let v = 4;
//             console.log(v);
//         }
//         console.log(v);
//     }
//     letItBe();

// Answer:
//   4 2


// const:
// const variables have the same scope as variables declared using let. The
// difference is that const variables are immutable - they are not allowed to be
// reassigned.

// For example, the following generates an exception:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2969/6486/1
// HTML:

// CSS:

// JS:
const a = 'Hello';
a = 'Bye';

// Note: const is not subject to Variable Hoisting too, which means that const
// declarations do not move to the top of the current execution context.
// Also note that ES6 code will run only in browsers that support it. Older
// devices and browsers that do not support ES6 will return a syntax error.


// Question:
//   const
//   Fill in the blanks to make a constant named total and the variable i that
//   is only accessible inside the loop.
//     _____ total = 100;
//     let sum = 0;
//     for(___ i = 0; i < total; i++) {
//         sum += i;
//     }

// Answer:
//   const total = 100;
//   let sum = 0;
//   for(let i = 0; i < total; i++) {
//       sum += i;
//   }


// Template Literals in ES6
// Template literals are a way to output variables in the string.
// Prior to ES6 we had to break the string, for example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2969/6547/1
// HTML:

// CSS:

// JS:
let name = 'David';
let msg = 'Welcome ' + name + '!';
console.log(msg);

// ES6 introduces a new way of outputting variable values in strings. The same
// code above can be rewritten as:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2969/6547/1
// HTML:

// CSS:

// JS:
let name = 'David';
let msg = `Welcome ${name}!`;
console.log(msg);

// Notice, that template literals are enclosed by the backtick(` `) character
// instead of double or single quotes.
// The ${ expression } is a placeholder, and can include any expression, which
// will get evaluated and inserted into the template literal.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2969/6547/1
// HTML:

// CSS:

// JS:
let a = 8;
let b = 34;
let msg = `The sum is ${a + b}`;
console.log(msg);

// Note: To escape a backtick in a template literal, put a backslash \ before
// the backtick.


// Question:
//   Template Literals in ES6
//   Fill in the blanks to output "We are learning ES6!".
//     let n = 6;
//     let s = 'ES';
//     let msg = `We are learning _{s + n}!`;
//     console.log(___);

// Answer:
//   let n = 6;
//   let s = 'ES';
//   let msg = `We are learning ${s + n}!`;
//   console.log(msg);
