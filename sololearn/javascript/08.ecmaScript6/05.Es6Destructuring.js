// Array Destructuring in ES6:
// The destructuring assignment syntax is a JavaScript expression that makes it
// possible to unpack values from arrays, or properties from objects, into
// distinct variables.
// ES6 has added a shorthand syntax for destructuring an array.

// The following example demonstrates how to unpack the elements of an array
// into distinct variables:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2977/6472/1
// HTML:

// CSS:

// JS:
let arr = ['1', '2', '3'];

let [one, two, three] = arr;

console.log(one); // 1
console.log(two); // 2
console.log(three); // 3

// We can use also destructure an array returned by a function.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2977/6472/1
// HTML:

// CSS:

// JS:
let a = () => {
    return [1, 3, 2];
};

let [one, , two] = a();

console.log(one); // 1
console.log(two); // 2

// Notice that we left the second argument's place empty.

// The destructuring syntax also simplifies assignment and swapping values:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2977/6472/1
// HTML:

// CSS:

// JS:
let a, b, c = 4, d = 8;

[a, b = 6] = [2];
console.log(a); // 2
console.log(b); // 6

[c, d] = [d, c];
console.log(c); // 8
console.log(d); // 4

// Note: Run the code and see how it works!


// Question:
//   Array Destructuring in ES6
//   What is the output of the following code?
//     let names = ['John', 'Fred', 'Ann'];
//     let [Ann, Fred, John] = names;
//     console.log(John);

// Answer:
//   Ann


// Object Destructuring in ES6:
// Similar to Array destructuring, Object destructuring unpacks properties into
// distinct variables.
// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2977/6473/1
// HTML:

// CSS:

// JS:
let obj = {h:100, s: true};
let {h, s} = obj;

console.log(h);
console.log(s);

// We can assign without declaration, but there are some syntax requirements for
// that:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2977/6473/1
// HTML:

// CSS:

// JS:
let a, b;
({a, b} = {a: 'Hello ', b: 'Jack'});

console.log(a + b);

// The () with a semicolon (;) at the end are mandatory when destructuring
// without a declaration. However, you can also do it as follows where the ()
// are not required:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2977/6473/1
// HTML:

// CSS:

// JS:
let {a, b} = {a: 'Hello ', b: 'Jack'};

console.log(a + b);

// You can also assign the object to new variable names.
// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2977/6473/1
// HTML:

// CSS:

// JS:
var o = {h: 42, s: true};
var {h: foo, s: bar} = o;

//console.log(h); // Error
console.log(foo); // 42

// Finally you can assign default values to variables, in case the value
// unpacked from the object is undefined.
// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2977/6473/1
// HTML:

// CSS:

// JS:
var obj = {id: 42, name: "Jack"};

let {id = 10, age = 20} = obj;

console.log(id); // 42
console.log(age); // 20

// Note: Run the code and see how it works!


// Question:
//   Object Destructuring in ES6
//   What is the output of the following code?
//     const obj = {one: 1, two: 2};
//     let {one:first, two:second} = obj;
//     console.log(one);

// Answer:
//   Error
