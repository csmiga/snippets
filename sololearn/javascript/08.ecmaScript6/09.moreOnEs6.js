// ES6 Promises:
// A Promise is a better way for asynchronous programming when compared to the
// common way of using a setTimeout() type of method.

// Consider this example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6481/1
// HTML:

// CSS:

// JS:
setTimeout(function () {
    console.log("Work 1");
    setTimeout(function () {
        console.log("Work 2");
    }, 1000);
}, 1000);
console.log("End");

// It prints "End", "Work 1" and "Work 2" in that order(the work is done
// asynchronously).But if there are more events like this, the code becomes very
// complex.

// ES6 comes to the rescue in such situations. A promise can be created as
// follows:

//   new Promise(function (resolve, reject) {
         // Work
//       if (success)
//           resolve(result);
//       else
//           reject(Error("failure"));
//       });

// Here, resolve is the method for success and reject is the method for
// failure.

// If a method returns a promise, its calls should use the then method which
// takes two methods as input; one for success and the other for the failure.

// For Example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6481/1
// HTML:

// CSS:

// JS:
function asyncFunc(work) {
    return new Promise(function (resolve, reject) {
        if (work === "")
            reject(Error("Nothing"));
        setTimeout(function () {
            resolve(work);
        }, 1000);
    });
}

asyncFunc("Work 1") // Task 1
    .then(function (result) {
        console.log(result);
        return asyncFunc("Work 2"); // Task 2
    }, function (error) {
        console.log(error);
    })
    .then(function (result) {
        console.log(result);
    }, function (error) {
        console.log(error);
    });
console.log("End");

// It also prints "End", "Work 1" and "Work 2" (the work is done
// asynchronously). But, this is clearly more readable than the previous example
// and in more complex situations it is easier to work with.

// Note: Run the code and see how it works!


// Question:
//   ES6 Promises
//   Fill in the blanks to define a function that returns a Promise object.
//     function foo() {
//         return new Promise((_______,______) => {
//              let result = getSomeResult();
//              if (result)
//                  resolve('Success');
//              else
//                  reject('Something went wrong');
//         });
//     }

// Answer:
//   function foo() {
//       return new Promise((resolve,reject) => {
//            let result = getSomeResult();
//            if (result)
//                resolve('Success');
//            else
//                reject('Something went wrong');
//       });
//   }


// Iterators & Generators:
// Symbol.iterator is the default iterator for an object.The for...of loops are
// based on this type of iterator.

// In the example below, we will see how we should implement it and how
// generator functions are used.
// Example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6544/1
// HTML:

// CSS:

// JS:
let myIterableObj = {
    [Symbol.iterator]: function* () {
        yield 1; yield 2; yield 3;
    }
};
console.log([...myIterableObj]); // [ 1, 2, 3 ]


// First, we create an object, and use the Symbol.iterator and generator
// function to fill it with some values.

// In the second line of the code, we use a * with the function keyword. It's
// called a generator function (or gen function).

// For example, here is a simple case of how gen functions can be useful:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6544/1
// HTML:

// CSS:

// JS:
function* idMaker() {
    let index = 0;
    while (index < 5)
        yield index++;
}
var gen = idMaker();

console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
// Try to add one more console.log, just like the above see what happens.

// We can exit and re - enter generator functions later.Their variable
// bindings(context) will be saved across re - entrances.They are a very
// powerful tool for asynchronous programming, especially when combined with
// Promises. They can also be useful for creating loops with special requirements.

// We can nest generator functions inside each other to create more complex
// structures and pass them arguments while we are calling them.
// The example below will show a useful case of how we can use generator
// functions and Symbol.iterators together.
// Example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6544/1
// HTML:

// CSS:

// JS:
const arr = ['0', '1', '4', 'a', '9', 'c', '16'];
const my_obj = {
    [Symbol.iterator]: function* () {
        for (let index of arr) {
            yield `${index}`;
        }
    }
};

const all = [...my_obj] /* Here you can replace the '[...my_obj]' with 'arr'. */
    .map(i => parseInt(i, 10))
    .map(Math.sqrt)
    .filter((i) => i < 5) /* try changing the value of 5 to 4 see what happens.*/
    .reduce((i, d) => i + d); /* comment this line while you are changing the value of the line above */

console.log(all);

// We create an object of 7 elements by using Symbol.iterator and generator
// functions.In the second part, we assign our object to a constant all. At the
// end, we print its value.

// Note: Run the code and see how it works!


// Question:
//   Iterators & Generators
//   You can exit and re - enter generator functions, and their variable
//   bindings will be saved across re - entrances.

// Answer:
//   True


// Modules:
// It is a good practice to divide your related code into modules.Before ES6
// there were some libraries which made this possible
// (e.g., RequireJS, CommonJS). ES6 is now supporting this feature natively.

// Considerations when using modules:
// The first consideration is maintainability.A module is independent of other
// modules, making improvements and expansion possible without any dependency on
// code in other modules.

// The second consideration is namespacing.In an earlier lesson, we talked about
// variables and scope.As you know, vars are globally declared, so it's common
// to have namespace pollution where unrelated variables are accessible all over
// our code. Modules solve this problem by creating a private space for
// variables.

// Another important consideration is reusability.When we write code that can be
// used in other projects, modules make it possible to easily reuse the code
// without having to rewrite it in a new project.

// Let's see how we should use modules in JS files.
// For Example:

     // lib/math.js
//   export let sum = (x, y) => { return x + y; }
//   export let pi = 3.14;

     // app.js
//   import * as math from "lib/math"
//   console.log(`2p = + ${math.sum(math.pi, math.pi)}`)

// Here we are exporting the sum function and the pi variable so we can use them
// in different files.

// Note: ES6 supports modules officially, however, some browsers are not
// supporting modules natively yet. So, we should use bundlers (builders) such
// as Webpack or Browserify to run our code.


// Question:
//   Modules
//   Fill in the blanks to import the following from "util/calc.js":
//     export const hit = (x, y, z) => {
//         return x * y + z / 2;
//     }
//     export const degree = 50;

//     ______ * __ calc from "util/calc";
//     calc.hit(1, 2, calc.degree);

// Answer:
//   import * as calc from "util/calc";
//   calc.hit(1, 2, calc.degree);


// Built-in Methods:
// ES6 also introduced new built -in methods to make several tasks easier. Here
// we will cover the most common ones.

// Array Element Finding
// The legacy way to find the first element of an array by its value and a rule
// was the following:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6546/1
// HTML:

// CSS:

// JS:
[4, 5, 1, 8, 2, 0].filter(function (x) {
    return x > 3;
})[0];

// The new syntax is cleaner and more robust:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6546/1
// HTML:

// CSS:

// JS:
[4, 5, 1, 8, 2, 0].find(x => x > 3);

// You can also get the index of the item above by using the findIndex() method:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6546/1
// HTML:

// CSS:

// JS:
[4, 5, 1, 8, 2, 0].findIndex(x => x > 3);

// Repeating Strings
// Before ES6 the following syntax was the correct way to repeat a string
// multiple times:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6546/1
// HTML:

// CSS:

// JS:
console.log(Array(3 + 1).join("foo"));

// With the new syntax, it becomes:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6546/1
// HTML:

// CSS:

// JS:
console.log("foo".repeat(3));

// Searching Strings
// Before ES6 we only used the indexOf() method to find the position of the text
// in the string.For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6546/1
// HTML:

// CSS:

// JS:
console.log("SoloLearn".indexOf("Solo") === 0); // true
console.log("SoloLearn".indexOf("Solo") === (4 - "Solo".length)); // true
console.log("SoloLearn".indexOf("loLe") !== -1); // true
console.log("SoloLearn".indexOf("olo", 1) !== -1); // true
console.log("SoloLearn".indexOf("olo", 2) !== -1); // false

// ES6 has replaced this with a version that has cleaner and more simplified
// syntax:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2981/6546/1
// HTML:

// CSS:

// JS:
console.log("SoloLearn".startsWith("Solo", 0)); // true
console.log("SoloLearn".endsWith("Solo", 4)); // true
console.log("SoloLearn".includes("loLe")); // true
console.log("SoloLearn".includes("olo", 1)); // true
console.log("SoloLearn".includes("olo", 2)); // false

// Note: It is always a good practice to refactor your code with the new syntax
// to learn new things and make your code more understandable.


// Question:
//   Built-in Methods
//   What is the output of this code ?
//     const arr = ['3', '5', '8'];
//     console.log(
//         arr.find(x => x == 8).repeat(2)
//     );

// Answer:
//   88
