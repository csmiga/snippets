// Question:
//   Which of the following is not one of the new ES6 features?

// Answer:
//   Hoisting


// Question:
//   Fill in the blanks to declare a constant num and an arrow function calc.
//     _____ num = 5;
//     const calc = (x, y, z = num) __ {
//         return x + y + z;
//     }

// Answer:
//   const num = 5;
//   const calc = (x, y, z = num) => {
//       return x + y + z;
//   }


// Question:
//   Fill in blanks to make the variable arr3 look like the following:
//   [1, 2, 3, 4, 5, 6, 7, 8].
//     const ____ = [1, 2, 3];
//     const arr2 = [5, 6, 7, 8];
//     let arr3 = [___arr1, _, ...arr2];

// Answer:
//   const arr1 = [1, 2, 3];
//   const arr2 = [5, 6, 7, 8];
//   let arr3 = [...arr1, 4, ...arr2];


// Question:
//   What is the output of the following code?
//   const arr1 = [1, 2, 3, 4, 5];
//   const arr2 = [...arr1, 6];
//   const func = (...rest) => {
//       console.log(rest.length);
//   }
//   func(...arr1);
//   func(...arr2);

// Answer:
//   5 6


// Question:
//   What is the output of this code?
//     const square = num => num * num;
//     console.log(square(6) + 6);

// Answer:
//   42


// Question:
//   Fill in the blanks to copy the user object to the newUser object by
//   destructuring the name and age properties. Pass the value 9999 for the id
//   property.
//     const user = {
//         name: 'David',
//         age: 28,
//         id: 1234
//     };
//     let newUser = Object.assign({},
//         {name, ___} = ____,
//         {__:9999});
//     console.log(newUser);

// Answer:
//   const user = {
//       name: 'David',
//       age: 28,
//       id: 1234
//   };
//   let newUser = Object.assign({},
//       {name, age} = user,
//       {id:9999});
//   console.log(newUser);


// Question:
//   Fill in the blanks to get the following output:
//   zero = 0
//   one = 1
//     let myMap = new Map();
//     myMap.set('zero', _);
//     myMap.___('one', 1);
//     ___ (let [key, value] of myMap) {
//         console.log(`${___} = ${value}`);
//     }

// Answer:
//   let myMap = new Map();
//   myMap.set('zero', 0);
//   myMap.set('one', 1);
//   for (let [key, value] of myMap) {
//       console.log(`${key} = ${value}`);
//   }
