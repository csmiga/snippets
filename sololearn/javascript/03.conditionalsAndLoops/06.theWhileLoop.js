// The While Loop:
// Time to move on to the second of our three loop statements, while.
// The while loop repeats through a block of code, but only as long as a specified condition is true.

// Here's the syntax:

//   while (condition) {
//       code block
//   }

// Note: Heads up! The condition can be any conditional statement that returns
// true or false.


// Question:
//   The result of the condition statement is always:

// Answer:
//   A Boolean value (true or false)


// The While Loop:
// Ok, we've got the theory, let's look at a real example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1141/1417/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var i = 0;
while (i <= 10) {
    document.write(i + "<br />");
    i++;
}

// The loop in this code will continue to run as long as i is less than, or
// equal to, 10. And each time the loop runs, it will increase by 1.

// This will output the values from 0 to 10.
//   0
//   1
//   2
//   3
//   4
//   5
//   6
//   7
//   8
//   9
//   10

// Note: Heads up!: Be careful when writing conditions. If a condition is always
// true, the loop will run forever!


// Question:
//   Fill in the blanks to print x's values from 1 to 5.
//     var x = 1;
//     _____ (x <= _) {
//         document.write(x + "<br />");
//         x = _____ + 1;
//     }

// Answer:
     var x = 1;
     while (x <= 5) {
         document.write(x + "<br />");
         x = x + 1;
     }


// The While Loop:
// Endless loops are not good. And one way of this happening is if we forget to
// increase the variable used in the condition.

// Note: Heads up! Make sure that the condition in a while loop eventually
// becomes false.


// Question:
//   How many times will the while loop run, if we remove the counting variable
//   increment statement?

// Answer:
//   Infinite
