// Switch:
// But what if you need to test for multiple conditions? In those cases,
// writing if else statements for each condition might not be the best solution.

// Instead, we can use the switch statement to perform different actions based
// on different conditions.

// Here's what that looks like:

//   switch (expression) {
//       case n1:
//           statements
//           break;
//       case n2:
//           statements
//           break;
//       default:
//           statements
//   }

// The switch expression is evaluated once. The value of the expression is
// compared with the values of each case, and if there’s a match, that block of
// code is executed.

// Note: Heads up! You can achieve the same result with multiple if...else
// statements, but the switch statement is more effective in such situations.


// Question:
//   The switch statement can be used to replace…

// Answer:
//   multiple if else statements


// The switch Statement:
// Let's look at another example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1139/1405/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var day = 2;
switch (day) {
    case 1:
        document.write("Monday");
        break;
    case 2:
        document.write("Tuesday");
        break;
    case 3:
        document.write("Wednesday");
        break;
    default:
        document.write("Another day");
}

// Simple, right?

// Note: Heads up! You can have as many case statements as you need.


// Question:
//   How many "case" statements are usually used in the "switch" statement?

// Answer:
//   One for each possible answer


// The break Keyword:
// So we have learned that the switch statement tests a code block, but we won't
// always want it to test the whole block. The break keyword essentially
// switches the switch statement off.

// Breaking out of the switch block stops the execution of more code and case
// testing inside the block.

// Note: Heads up! Usually, a break should be put in each case statement.


// Question:
//   What’s the output of this code?
//     var x = 3;
//     switch (x) {
//         case 1:
//             document.write(x);
//             break;
//         case 2:
//             document.write(x + 2);
//             break;
//         default:
//             document.write(x + 5);
//     }

// Answer:
//   8


// The default Keyword:
// Often there will be no match, but we still need the program to output
// something...for this we use the default keyword, which specifies the code to
// run if there’s no case match.

// Like this:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1139/1407/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var color = "yellow";
switch (color) {
    case "blue":
        document.write("This is blue.");
        break;
    case "red":
        document.write("This is red.");
        break;
    case "green":
        document.write("This is green.");
        break;
    case "orange":
        document.write("This is orange.");
        break;
    default:
        document.write("Color not found.");
}

// Note: Heads up! The default block can be omitted, if there is no need to
// handle the case when no match is found.


// Question:
//   The "default" statement is used…

// Answer:
//   When no match is found
