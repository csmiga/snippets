// The else Statement:
// Right, so we’ve seen that the action gets skipped when a code block using the
// if statement evaluates to false, but what if we want something else to
// happen.
// Well, we use the "else" statement, of course!

// We can use the else statement to specify a block of code that will execute if
// the condition is false.Like this:

     if (expression) {
         // executed if condition is true
     }
     else {
         // executed if condition is false
     }

// Note: Heads up! You can skip the curly braces if the code under the condition
// contains only one command.


// Question:
//   The "else" statement is created to do what?

// Answer:
//   Tell JavaScript to execute something if the condition is false.


// The else Statement:
// Here’s another example of the if and else statements working together:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1137/1398/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var myNum1 = 7;
var myNum2 = 10;
if (myNum1 > myNum2) {
    alert("This is my first condition");
}
else {
    alert("This is my second condition");
}

// Let's translate that example. It says:
// - If myNum1 is greater than myNum2, alert "This is my first condition";
// - Else, alert "This is my second condition".

// So the browser will print out the second condition, as 7 is not greater than
//  10.

// Result:
// Alert box similar to example below pops up in browser.
// 
//   |-----------------------------|
//   |                             |
//   | This is my second condition |
//   |                             |
//   |                        [OK] |
//   |                             |
//   |-----------------------------|

// Note: Heads up! There's another way to do this check using the ? operator:
// a > b ? alert(a) : alert(b).


// Question:
//   Fill in the blanks to create a valid if...else statement:
//     var age = 25;
//     __ (age >= 18) {
//         alert("Allowed.");
//     }
//     ____ {
//         alert("Not allowed.");
//    }

// Answer:
     var age = 25;
     if (age >= 18) {
         alert("Allowed.");
     }
     else {
         alert("Not allowed.");
     }
