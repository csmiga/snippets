// The Do...While Loop:
// Almost done with loops! You're doing great!

// The last loop we’re looking at in this module is the do...while loop, it's a
// variant of the while loop but with one important difference.

// This loop will execute the code block once, before checking if the condition
// is true, and then it will repeat the loop as long as the condition is true.

// Here’s the Syntax:

//   do {
//       code block
//   }
//   while (condition);

// Note: Heads up! Note the semicolon used at the end of the do...while loop.
// This is important.

// Here’s a real example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1142/1421/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var i = 20;
do {
    document.write(i + "<br />");
    i++;
}
while (i <= 25); 

// This example prints out numbers from 20 to 25.
//   20
//   21
//   22
//   23
//   24
//   25

// Note: Heads up! The loop will always be executed at least once, even if the
// condition is false, because the code block is executed before the condition
// is tested.


// Question:
//   Apply the "do" and "while" keywords in their corresponding positions.
//     var count=1;
//     __ {
//         document.write("hello <br />");
//         count++;
//     }
//     _____ (count<=10);

// Answer:
     var count=1;
     do {
         document.write("hello <br />");
         count++;
     }
     while (count<=10);
