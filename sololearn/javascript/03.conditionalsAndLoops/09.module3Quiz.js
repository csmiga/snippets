// Question:
//   What’s the output of this code?
//     var x = 0;
//     while (x < 6) {
//         x++;
//     }
//     document.write(x);

// Answer:
//   6


// Question:
//   Fill in the right keywords to test the conditions:
//     ______ (day_of_week) {
//         case 1:
//         case 2:
//         case 3:
//         case 4:
//         case 5:
//             document.write("Working Days");
//             _____;
//         case 6:
//             document.write("Saturday");
//             _____;
//         default:
//             document.write("Today is Sunday");
//             break;
//     }

// Answer:
     switch (day_of_week) {
         case 1:
         case 2:
         case 3:
         case 4:
         case 5:
             document.write("Working Days");
             break;
         case 6:
             document.write("Saturday");
             break;
         default:
             document.write("Today is Sunday");
             break;
     }


// Question:
//   Please fill in the right keywords to compose a loop:
//     __ {
//         document.write(i);
//         i++;
//     }
//     _____ (i < 10);

// Answer:
     do {
         document.write(i);
         i++;
     }
    while (i < 10);
