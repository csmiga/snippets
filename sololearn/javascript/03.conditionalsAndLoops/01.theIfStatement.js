// The if Statement:
// Well done! You’re making great progress. On to module 3!

// Often when we write code, we want to perform different actions based on
// different conditions.

// And this is where conditional statements come in.

// There are a bunch of different conditionals, to cover, but we’re starting
// with one of the most useful: "if"

// We use if to specify a block of code that we want to be executed if a
// specified condition is true.

//   if (condition) {
//       statements
//   }

// The statements will only be executed if the specified condition is true.
// Let’s take a look at an example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1136/1395/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var myNum1 = 7;
var myNum2 = 10;
if (myNum1 < myNum2) {
    alert("JavaScript is easy to learn.");
}

// Result:
// Alert box similar to example below pops up in browser.
// 
//   |-----------------------------|
//   |                             |
//   | JavaScript is easy to learn |
//   |                             |
//   |                        [OK] |
//   |                             |
//   |-----------------------------|

// Note: Heads up! You can see from the example above, we’ve used the
// JavaScript alert() to generate a popup alert box that contains the
// information inside the parentheses.


// Question:
//   Add the characters that complete the statement:
//     if _var1 > var2_
//     _
//         document.write("OK");
//     _

// Answer:
     if (var1 > var2) 
     {
         document.write("OK");
     }


// The if Statement:
// Here’s a little more detail on the if statement. This is an example of a
// false conditional statement:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1136/1395/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var myNum1 = 7;
var myNum2 = 10;
if (myNum1 > myNum2) {
    alert("JavaScript is easy to learn.");
}

// Note: Because the condition evaluates to false, the alert statement gets
// skipped and the program continues with the line after the if statement's
// closing curly brace.

// Note: Heads up! if is in lowercase letters.Uppercase letters(If or IF) won’t
// work.


// Question:
//   What happens if the tested condition is false?

// Answer:
//   The code does nothing and moves to the next section.
