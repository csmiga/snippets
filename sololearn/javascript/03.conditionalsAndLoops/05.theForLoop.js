// Loops:
// Loops can execute a block of code a number of times. They’re handy when you
// want to run the same code repeatedly, adding a different value each time.

// JavaScript has three types of loops: "for", "while", and "do while".

// We’ll start here with the classic "for" loop.

// Here's the syntax:

//   for (statement 1; statement 2; statement 3) {
//       code block to be executed
//   }

// And here’s what happens when it runs:

// Statement 1 is executed before the loop(the code block) starts.
// Statement 2 defines the condition for running the loop(the code block).
// Statement 3 is executed each time after the loop(the code block) has been
// executed.

// Note: Heads up! As you can see, the classic for loop has three components,
// or statements.


// Question:
//   The classic "for" loop consists of how many components?

// Answer:
//   3


// The For Loop:
// Now we've got the theory, let's look at a specific example.

// This example creates a for loop that prints numbers 1 through 5:.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1140/1412/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
for (i = 1; i <= 5; i++) {
    document.write(i + "<br />");
}

// So what's actually happening?

// In this example,
// Statement 1 sets a variable before the loop starts(var i = 1).
// Statement 2 defines the condition for the loop to
//   run(it must be less than or equal to 5).
// Statement 3 increases a value(i++) each time the code block in the loop has
// been executed.

// Result:
//   1
//   2
//   3
//   4
//   5

// Statement 1 is optional, and can be left out, if your values are set before
// the loop starts.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1140/1412/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var i = 1;
for (; i <= 5; i++) {
    document.write(i + "<br />");
}

// You can also initiate more than one value in statement 1, using commas to
// separate them.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1140/1412/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
for (i = 1, text = ""; i <= 5; i++) {
    text = i;
    document.write(i + "<br />");
}

// Note: Heads up! ES6 introduces other for loop types; you can learn about
// them in the ES6 course later.


// Question:
//   Fill in the blanks to compose a valid for loop:
//     var i = 1;
//     ___ (k = 1; k < 10_ k++) _
//         i += k;
//     }

// Answer:
     var i = 1;
     for (k = 1; k < 10; k++) {
         i += k;
     }


// The For Loop:
// If statement 2 returns true, the loop will start over again, if it returns
// false, the loop will end.
 
// Statement 2 is also optional, but only if you put a break inside the loop.
// Otherwise, the loop will never end!

// Statement 3 is used to change the initial variable.It can do anything,
// including negative increment(i--), positive increment(i = i + 15).

// Statement 3 is also optional, but only if you increment your values inside
// the loop.Like this:

// Try It Yourself:
// 
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var i = 0;
for (; i < 10;) {
    document.write(i);
    i++;
}

// Note: Heads up! You can have multiple nested for loops.


// Question:
//   Fill in the blanks to print EVEN values from 0 to 20 using a for loop:
//     var x = 0;
//     for (; x <= _____; x += _) {
//         document.write(x); 
//     }

// Answer:
     var x = 0;
     for (; x <= 20; x += 2) {
         document.write(x); 
     }
