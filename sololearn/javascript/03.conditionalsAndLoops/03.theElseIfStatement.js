// else if:
// We've seen else, we've seen if, time to meet else if.

// The else if statement is useful because it lets us specify a new condition if
// the first condition is false.

// Like this:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1138/1400/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var course = 1;
if (course == 1) {
    document.write("<h1>HTML Tutorial</h1>");
} else if (course == 2) {
    document.write("<h1>CSS Tutorial</h1>");
} else {
    document.write("<h1>JavaScript Tutorial</h1>");
}

// This is what's happening in the code above:
// - if course is equal to 1, output "HTML Tutorial";
// - else, if course is equal to 2, output "CSS Tutorial";
// - if none of the above condition is true, then output "JavaScript Tutorial";

// course is equal to 1, so we get the following result:

// Note: Heads Up! The final else statement "ends" the else if statement and
// should be always written after the if and else if statements.


// Question:
// What keyword is used to end the "else if" statement?

// Answer:
//   else


// else if:
// The final else block will be executed when none of the conditions is true.
// Let's change the value of the course variable in our previous example.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1138/1400/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var course = 3;
if (course == 1) {
    document.write("<h1>HTML Tutorial</h1>");
} else if (course == 2) {
    document.write("<h1>CSS Tutorial</h1>");
} else {
    document.write("<h1>JavaScript Tutorial</h1>");
}

// The result:

//   JavaScript Tutorial

// Note: You can write as many else if statements as you need.


// Question:
//   Fill in the blanks to create a valid if...else...if statement:
//     var status = 1;
//     var msg;
//     __ (status == 1) {
//         msg = "Online";
//     }
//     _______ (status == 2) {
//         msg = "Away";
//     }
//     else {
//         msg = "Offline";
//     }

// Answer:
     var status = 1;
     var msg;
     if (status == 1) {
         msg = "Online";
     }
     else if (status == 2) {
         msg = "Away";
     }
     else {
         msg = "Offline";
     }
