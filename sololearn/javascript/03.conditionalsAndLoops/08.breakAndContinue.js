// Break:
// We've met the break statement earlier in this module, we use it to "jump out"
// of a loop and continue executing the code after the loop.

// Like this:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1143/1422/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
for (i = 0; i <= 10; i++) {
    if (i == 5) {
        break;
    }
    document.write(i + "<br />");
}

// In this example, once it reaches 5, it will break out of the loop.

// Result:
//   0
//   1
//   2
//   3
//   4

// Note: Heads up! You can use the return keyword to return some value
// immediately from the loop inside of a function.This will also break the loop. 


// Question:
//   The "break" statement:

// Answer:
//   Ends the execution of the loop


// Continue:
// We're nearly done with module 3! One last thing to cover.
// Unlike the break statement, the continue statement breaks only one iteration
// in the loop, and continues with the next iteration.

// Like this:

// Try It Yourself:
// 
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
for (i = 0; i <= 10; i++) {
    if (i == 5) {
        continue;
    }
    document.write(i + "<br />");
}

// Result:
//   0
//   1
//   2
//   3
//   4
//   6
//   7
//   8
//   9
//   10

// Note: Heads up! The value 5 is not printed, because continue skips that
// iteration of the loop.


// Question:
//   What’s the output of this code?
//     var sum = 0;
//     for (i = 4; i < 8; i++) {
//         if (i == 6) {
//             continue;
//         }
//         sum += i;
//     }
//     document.write(sum);

// Answer:
//   16
