// Creating Arrays:
// You can also declare an array, tell it the number of elements it will store,
// and add the elements later.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1240/1776/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var courses = new Array(3);
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";

document.write(courses[2]);

// Note: An array is a special type of object.
// An array uses numbers to access its elements, and an object uses names to
// access its members.


// Question:
//   Creating Arrays
//   Please insert the missing characters to output the third member of the
//   array:
//     document.write(example___);

// Answer:
//   document.write(example[2]);


// Creating Arrays:
// JavaScript arrays are dynamic, so you can declare an array and not pass any
// arguments with the Array() constructor. You can then add the elements
// dynamically.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1240/1777/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var courses = new Array();
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";
courses[3] = "C++";

document.write(courses[2]);

// Note: You can add as many elements as you need to.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1240/1777/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var courses = ["HTML", "CSS", "JS"]; 

document.write(courses[2]);

// This results in the same array as the one created with the new Array()
// syntax.

// Note: You can access and modify the elements of the array using the index
// number, as you did before. The array literal syntax is the recommended way to
// declare arrays.


// Question:
//   Creating Arrays
//   By entering var example = new Array(); we create an empty array which can
//   be filled...

// Answer:
//   anytime later
