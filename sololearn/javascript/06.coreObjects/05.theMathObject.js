// The Math Object:
// The Math object allows you to perform mathematical tasks, and includes
// several properties.

//   Property    Description
//   ========    ===========
//   E           Euler's constant
//   LN2         Natural log of the value 2
//   LN10        Natural log of the value 10
//   LOG2E       The base 2 log of Euler's constant (E)
//   LOG10E      The base 10 log of Euler's constant (E)
//   PI          

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1243/1783/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
document.write(Math.PI);

// Note: Math has no constructor. There's no need to create a Math object first.


// Question:
//   The Math Object
//   In the Math Object, which of the following constants does NOT exist?

// Answer:
//   Math.ABC


// Math Object Methods:
// The Math object contains a number of methods that are used for calculations:

//   Method              Description
//   ======              ===========
//   abs(x)              Returns the absolute value of x
//   acos(x)             Returns the arccosine of x, in radians
//   asin(x)             Returns the arcsine of x, in radians
//   atan(x)             Returns the arctagent of x as a numeric value between
//                       -PI/2 and PI/2 radians
//   atan2(y,x)          Returns the arctangent of the quotient of its arguments
//   ceil(x)             Returns x, rounded upward to the nearest integer
//   cos(x)              Returns the cosine of x (x is in radians)
//   exp(x)              Returns the value of E^x
//   floor(x)            Returns x, rounded downwards to the nearest integer
//   log(x)              Returns the natural logarithm (base E) of x
//   max(x,y,z,...,n)    Returns the number with the highest value
//   min(x,y,z,...,n)    Returns the number with the lowest value

// For example, the following will calculate the square root of a number.

// // Try It Yourself:
// https://www.sololearn.com/learning/1024/1243/1784/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var number = Math.sqrt(4); 
document.write(number);

// Note: To get a random number between 1-10, use Math.random(), which gives you
// a number between 0-1. Then multiply the number by 10, and then take
// Math.ceil() from it: Math.ceil(Math.random() * 10).


// Question:
//   Math Object Methods
//   In the Math Object, which of the following methods is used to calculate the
//   square root?

// Answer:
//   sqrt


// The Math Object:
// Let's create a program that will ask the user to input a number and alert its
// square root.

// // Try It Yourself:
// https://www.sololearn.com/learning/1024/1243/1785/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var n = prompt("Enter a number", "");
var answer = Math.sqrt(n);
alert("The square root of " + n + " is " + answer);

// Result:
//
//   |----------------------------|
//   |                            |
//   |       Enter a number       |
//   |                            |
//   |              [OK] [Cancel] |
//   |                            |
//   |----------------------------|

// Enter a number, such as 64.
//
//   |---------------------------------------------------------|
//   |                                                         |
//   |                The square root of 64 is 8               |
//   |                                                         |
//   | [ ] Prevent this pagbe from creating additional dialogs |
//   |                                                         |
//   |                                                   [OK]  |
//   |                                                         |
//   |---------------------------------------------------------|

// Note: Math is a handy object. You can save a lot of time using Math, instead
// of writing your own functions every time.


// Question:
//   The Math Object:
//   What is the result of the following expression:
//   Math.sqrt(81);

// Answer:
//   9
