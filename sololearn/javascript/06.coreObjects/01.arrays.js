// JavaScript Arrays:
// Arrays store multiple values in a single variable.
// To store three course names, you need three variables.

     var course1 = "HTML";
     var course2 = "CSS";
     var course3 = "JS";

// But what if you had 500 courses? The solution is an array

     var courses = new Array("HTML", "CSS", "JS");

// Note: This syntax declares an array named courses, which stores three values,
// or elements.


// Question:
//   JavaScript Arrays
//   What two keywords do we need to create an array?

// Answer:
//   new, Array


// Accessing an Array:
// You refer to an array element by referring to the index number written in
// square brackets.
// This statement accesses the value of the first element in courses and changes
// the value of the second element.

     var courses = new Array("HTML", "CSS", "JS");
     var course = courses[0]; // HTML
     courses[1] = "C++"; //Changes the second element

// Note: [0] is the first element in an array. [1] is the second. Array indexes
// start with 0.


// Question: 
//   Accessing an Array
//   What is the output of this code?
//     var arr = new Array(3, 6, 8);
//     document.write(arr[1]);

// Answer:
//   6


// Accessing an Array:
// Attempting to access an index outside of the array, returns the value
// "undefined"

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1239/1774/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var courses = new Array("HTML", "CSS", "JS"); 
document.write(courses[10]);

// Note: Our courses array has just 3 elements, so the 10th index, which is the
// 11th element, does not exist (is undefined).


// Question:
//   Accessing an Array
//   What is the result of trying to reference an array member which does not
//   exist?

// Answer:
//   undefined
