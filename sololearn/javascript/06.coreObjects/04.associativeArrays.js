// Associative Arrays:
// While many programming languages support arrays with named indexes (text
// instead of numbers) called associative arrays, JavaScript does not.
// However, you still can use the named array syntax, which will produce an
// object.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1242/1780/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var person = []; //empty array
person["name"] = "John";
person["age"] = 46;
document.write(person["age"]);

// Now, person is treated as an object, instead of being an array.
// The named indexes "name" and "age" become properties of the person object.

// Note: As the person array is treated as an object, the standard array methods
// and properties will produce incorrect results. For example, person.length
// will return 0.


// Question:
//   Associative Arrays
//   In associative arrays, index numbers are replaced with:

// Answer:
//   strings


// Associative Arrays:
// Remember that JavaScript does not support arrays with named indexes.
// In JavaScript, arrays always use numbered indexes.
// It is better to use an object when you want the index to be a string (text).
// Use an array when you want the index to be a number.

// Note: If you use a named index, JavaScript will redefine the array to a
// standard object.


// Question:
//   Associative Arrays
//   In order to use associative arrays, the "associated" name is put in:

// Answer:
//   brackets [ ]
