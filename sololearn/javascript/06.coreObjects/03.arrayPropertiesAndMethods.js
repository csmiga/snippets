// The length Property:
// JavaScript arrays have useful built-in properties and methods.
// An array's length property returns the number of it's elements.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1241/1778/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var courses = ["HTML", "CSS", "JS"];
document.write(courses.length);

// Note: The length property is always one more than the highest array index.
// If the array is empty, the length property returns 0.


// Question:
//   The length Property
//   Array has the "length" property, because it is:

// Answer:
//   an object


// Combining Arrays:
// JavaScript's concat() method allows you to join arrays and create an entirely
// new array.

// Example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1241/1778/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var c1 = ["HTML", "CSS"];
var c2 = ["JS", "C++"];
var courses = c1.concat(c2);

document.write(courses[2]);

// The courses array that results contains 4 elements (HTML, CSS, JS, C++).

// Note: The concat operation does not affect the c1 and c2 arrays - it returns
// the resulting concatenation as a new array.


// Question:
//   Combining Arrays
//   The "concat" method takes two arrays and:

// Answer:
//   combines them in one new array
