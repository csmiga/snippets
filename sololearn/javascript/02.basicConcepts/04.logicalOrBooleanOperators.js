// Logical Operators:
// Logical Operators, also known as Boolean Operators, (or the Vulcan
// Operators….ok, that one isn't true) evaluate an expression and return true or
// false.

// Check out the table below to see more details on the logical operators(AND,
// OR, NOT).

//   Logical Operators
//   &&    Return true, if both operands are true
//   ||    Returns true, if one of the operands is true
//   !     Returns true, if the operand is false, and false, if the operand is
//         true

// Note: Heads up! You can check all types of data; comparison operators always
// return true or false. 


// Question:
//   Logical AND (&&) returns true if:

// Answer:
//   If both operands are true


// Logical Operators:
// Let's take a look at an example. Here we’ve connected two Boolean expressions
// with the AND operator.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1133/1389/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
document.write((4 > 2) && (10 < 15));

// For this expression to be true, both conditions need to be true.
// - The first condition determines whether 4 is greater than 2, which is true.
// - The second condition determines whether 10 is less than 15, which is also
//   true.

// Ta da! The whole expression is true...very logical!

// Conditional(Ternary) Operator

// Conditional, or Ternary, operators assign a value to a variable, based on
// some condition.

// This is what the syntax would look like: 

//   variable = (condition) ? value1: value2 

// And here’s an example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1133/1389/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var age = 42;
var isAdult = (age < 18) ? "Too young" : "Old enough";
document.write(isAdult);

// If the variable age is a value below 18, the value of the variable isAdult
// will be "Too young". Otherwise the value of isAdult will be "Old enough".

// Note: Heads up! With logical operators you can connect as many expressions as
// you want or need to.


// Question:
//   Logical NOT returns true, if: 

// Answer:
//   The operand is false
