// Arithmetic Operators:
// The name might be a bit of a giveaway but, Arithmetic operators
// perform arithmetic functions on numbers(both literals and variables).

//   Operator    Description       Example
//   ========    ===========       =======
//   +           Addition          25 + 5 = 30
//   -           Subtraction       25 - 5 = 20
//   *           Multiplication    10 * 20 = 200
//   /           Division          20 / 2 = 10
//   %           Modulus           56 % 3 = 2
//   ++          Increment         var a = 10; a++; Now a = 11
//   --          Decrement         var a = 10; a--; Now a = 9

// Below you can see the addition operator(+) in action determining the sum of
// two numbers.

// Below you can see the addition operator (+) in action determining the sum of
// two numbers.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1130/1375/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var x = 10 + 5;
document.write(x);

// You can add as many numbers or variables together as you want or need to.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1130/1375/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var x = 10;
var y = x + 5 + 22 + 45 + 6548;
document.write(y);

// Note: Heads up! You can get the result of a string expression using the
// eval() function, which takes a string expression argument like
// eval("10 * 20 + 8") and returns the result.If the argument is empty, it
// returns undefined.


// Question:
//   What will the following statements display?
//     var test = 5 + 7;
//     document.write(test);

// Answer:
//   12


// Multiplication:
// Want to hear a joke?
// What tool is best suited for math?... Multi - pliers!
// JavaScript is pretty good at math too though!

// We use the * operator to multiply one number by the other.
// Like this:

// Try It Yourself:
// 
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var x = 10 * 5;
document.write(x);

// Note: Heads up! 10 * '5' or '10' * '5' will give the same result.But trying
// to multiply a number with string values that aren’t numbers, like
// 'sololearn' * 5 will return NaN(Not a Number).


// Question:
//   What character do we use for multiplication?

// Answer:
//   *


// Division:
// What’s a swimmer's favorite kind of math?... Dive-ision!
// Sorry! Back to business!

// We use the / operator to perform division operations.
// Like this:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1130/1377/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var x = 100 / 5;
document.write(x);

// Note: Heads up! Beware of situations where there could be a division by 0,
// things get messed up when we do impossible math! 


// Question:
//   What character do we use for division?

// Answer:
//   /


// The Modulus
// Time to talk remainders.You hated them in school, but they’re pretty easy
// here, promise.

// The Modulus(%) operator returns the division remainder(what’s left over).
// Like this:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1130/1378/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var myVariable = 26 % 6;
document.write(myVariable);

// Because you’re left with a remainder of 2 when you divide 26 by 6.

// Note: Heads up! In JavaScript, we can use the modulus operator on integers
// AND on floating point numbers.


// Question:
//   What’s the result of using a modulus operator for 38 % 5?

// Answer:
//   3


// Increment & Decrement:
// "Wait I've heard of an increment, but what the heck is a decrement?" We hear
// some of you say. Well, throw an increment into reverse and presto, you got
// yourself a decrement. Let’s dig a little deeper...

// Increment++
// The increment operator increases the numeric value of its operand by 1. When
// placed before the operand, it’ll return the incremented value. When placed
// after it, it’ll return the original value and then increments the operand.

// Decrement--
// The decrement operator decreases the numeric value of its operand by 1.
// When placed before the operand, it’ll return the decremented value. When
// placed after the operand, it’ll return the original value and then decrements
// the operand.

// Some examples:

//   Operator    Description       Example                Result
//   ========    ===========       =======                ======
//   var++       Post Increment    var a = 0, b = 10;     a = 10 and b = 11
//                                 var a = b++;
//   ++var       Pre Increment     var a = 0, b = 10;     a = 11 and b = 11
//                                 var a = ++b;
//   var--       Post Increment    var a = 0,  b = 10;    a = 10 and b = 9
//                                 var a = b--;
//   --var       Pre Increment     var a = 0, b = 10;     a = 9 and b = 9
//                                 var a = --b;

// Note: Heads up! Just like the math you learned in school, you can change the
// order of the arithmetic operations by using parentheses.
// Like this: var x = (100 + 50) * 3;


// Question:
//   What are increment and decrement used for?

// Answer:
//   Adding or subtracting 1 from a number
