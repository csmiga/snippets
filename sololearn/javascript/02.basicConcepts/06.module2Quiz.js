// Question:
//   Which of these names are acceptable for JavaScript variables?

// Answer:
//   firstNumber
//   _module


// Question:
//   Fill in the data types of the data shown below in the comments field:
//     12 // number
//     "some text" // ______
//     true // _______

// Answer:
//   12 // number
//   "some text" // string
//   true // boolean


// Question:
//   What's the result of the expression var1 && var2,
//   if var1 = true and var2 = false?

// Answer:
//   false
