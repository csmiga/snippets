// Assignment Operators:
// Next in a series of very logically named operators is...
// Assignment operators!

// And you guessed it, we use these guys to assign values to JavaScript
// variables.

//   Operator    Example    Is equivalent to
//   ========    =======    ================
//   =           x = y      x = y
//   +=          x += y     x = x + y
//   -=          x -= y     x = x - y
//   *=          x *= y     x = x * y
//   /=          x /= y     x = x / y
//   %=          x %= y     x = x % y

// Note: Heads up! You can use multiple assignment operators in one line, such
// as x -= y += 9.


// Question:
//   Calculate and enter the resulting value of this expression:
//   var result = 20;
//   result *= 5;

// Answer:
//   100
