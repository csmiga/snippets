// String Operators:
// Time to introduce the most useful operator for strings... drum roll please.

// ...Concatenation.

// We can use concatenation(represented by the + sign) to build strings made up
// of multiple smaller strings, or by joining strings with other types. Check it
// out:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1134/1390/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var mystring1 = "I am learning ";
var mystring2 = "JavaScript with SoloLearn.";
document.write(mystring1 + mystring2);

// This example declares and initializes two string variables, and then
// concatenates them. Simple... but super useful!

// Result:
// I am learning JavaScript with SoloLearn. 

// Note: Heads up! Numbers in quotes are treated as strings: So "42" is not the
// number 42, it’s a string that includes the two separate characters, 4 and 2. 


// Question:
//   What’s the output of the following code?
//     var x = "50";
//     var y = "100";
//     document.write(x + y);

// Answer:
//   50100
