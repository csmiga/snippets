// Comparison Operators:
// We can use comparison operators in logical statements to find out if
// variables or values are different.

// You get either true or false.
// For example, the equal to(==) operator checks whether the operands' values
// are equal.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1132/1384/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
var num = 10;
document.write(num == 8);

// Note: Heads up! You can compare all types of data with comparison operators,
// they’ll always return true or false.


// Question:
//   What do comparison operators return?

// Answer:
//   false
//   true


// Comparison Operators:
// Check out this table to see a breakdown of comparison operators.

//   Operator    Description                           Example
//   ========    ===========                           =======
//   ==          Equal to                              5 == 10 false
//   ===         Identical (equal and of same type)    5 === 10 false
//   !=          Not equal to                          5 != 10 true
//   !==         Not Identical                         10 !== 10 fasle
//   >           Greater than                          10 > 5 true
//   >=          Greater than or equal to              10 >= 5 true
//   <           Less than                             10 < 5 false
//   <=          Less than or equal to                 10 <= 5 false

// Note: Heads up! One important thing to remember when we use operators, is
// that they only work when they’re comparing the same data type; numbers with
// numbers, strings with strings, you get the idea. 


// Question:
//   Enter the corresponding operators according to the comments at right.
//     val1 ___ val2 // are equal
//     val1 !___ val2 // not equal
//     val1 ___ val2 // less than
//     val1 ___ val2 // are strict equal (identical)

// Answer:
//   val1 == val2 // are equal
//   val1 != val2 // not equal
//   val1 < val2 // less than
//   val1 === val2 // are strict equal (identical)
