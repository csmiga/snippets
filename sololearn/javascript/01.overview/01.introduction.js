// Welcome to JavaScript
// Do we even need to introduce JavaScript? It’s one of the most popular
// programming languages on the planet!

// Ever visited a website that made you think… "Hey, this website is really cool
// and interactive"? Well, JavaScript was probably making it happen.

// So it’s just useful for websites right? Wrong! Process data, mobile and
// desktop apps, games, the world’s your oyster with JavaScript.

// Note: Whether it’s client - side or server - side, create the program of your
// dreams by unlocking the fundamentals of JavaScript.


// Question:
//   Which is the correct statement?

// Answer:
//   JavaScript can be used to create interactive web elements
