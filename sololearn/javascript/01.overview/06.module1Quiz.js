// Question:
//   Fill in the blanks to output "JS is cool!" to the console:
//     _______.___("JS is cool!");

// Answer:
     console.log("JS is cool!");


// Question:
//   Declare a variable called x, assign the value 42 to it and output it to the
//   console.
//     x = __;
//     _______.log(_);

//     42, 24, x, document, console, out, y

// Answer:
     x = 42;
     console.log(x);


// Question:
//   What is the output of this code?
//     // x = 8;
//     x = 2;
//     // x = 3;
//     console.log(x);

// Answer:
//   2


// Question:
//   Rearrange to form valid JavaScript code that declares a variable and
//   outputs it to the console.
//     = </script>
//     = console.log(name);
//     = name = "James";
//     = <script>

// Answer:
     <script>
         name = "James";
         console.log(name);
     </script>
