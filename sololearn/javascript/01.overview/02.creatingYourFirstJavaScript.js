// Ready.Set.JavaScript!:
// Let’s start with the basics and add some JavaScript to a webpage.

// On the web, JavaScript code lives inside the HTML document, and needs to be
// enclosed by < script > and </script >: 

//   <script>
//       ...
//   </script>

// Notice: Heads up!
// You can put the script tag anywhere in the HTML document.


// Question:
//   What tag do you need to use to enclose the JavaScript code?

// Answer:
//   script


// Output:
// Let's use JavaScript to print "Hello World" to the browser. This is what that
// would look like.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1124/1344/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
document.write("Hello World!");

// Notice some extra stuff there? Nothing gets past you!

// Time to introduce the document.write() function.This is what we need to use
// to write text into our HTML document.

// Feeling fancy? Of course you are! You can also use standard HTML markup
// language to customize the appearance text in the output:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1124/1344/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
document.write("<h1>Hello World!</h1>");

// Note: Heads up!
// document.write() should be used only for testing.We’ll cover some other
// output mechanisms real soon.


// Question:
//   Output "Hello!" in the browser.
//     <script>
//         ________._____("Hello!");
//     </script>

//   out, print, write, document

// Answer:
     <script>
         document.write("Hello!");
     </script>


// Output to console
// Right, we’re now experts in writing HTML output with document.write().
// Time for a different type of output.Let’s learn about output to the browser
//  console.

// For this we’ll be needing the trusty console.log() function.

// Wait, not so fast! What’s this console we’re talking about ?

// The console is part of the web browser and allows you to log messages, run
// JavaScript code, and see errors and warnings.

// It looks like this:

// Try It Yourself:
// https://code.sololearn.com/nodejs
console.log("Hello from console!")

// Note: Heads Up!
// Devs mostly use the console to test their JavaScript code. 


// Question:
//   Complete the code to output "Hi!" to the console.
//     _______.___("Hi!")

// Answer:
     console.log("Hi!")
