// JavaScript Comments:
// Great stuff! You’ll soon be a variable master.

// Ok, let’s talk about comments in JavaScript. So we know about statements,
// these are the instructions within our program that get "executed" when the
// program runs. But! Not all JavaScript statements are "executed". Any code
// after a double slash //, or between /* and */, is treated as a comment, and
// will be ignored, and not executed.

// To write a Single line comment we use double slashes. Like this:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1128/1365/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
// This is a single line comment
alert("This is an alert box!");

// Result:
// Alert box similar to example below pops up in browser.
//
//   |-----------------------|
//   |                       |
//   | This is an alert box! |
//   |                       |
//   |                  [OK] |
//   |                       |
//   |-----------------------|

// But why write code that is never going to be executed. Isn’t that a waste of
// time?
// Not at all! Comments are a good idea, especially ones relating to large
// functions, as they help make our code more readable for others.So be kind,
// and comment!

// Note: Heads up! alert() is used to create a message box.


// Question:
//   What does a single line comment look like?

// Answer:
     // this is a comment


// Multiple-Line Comments:
// What if we have more to say?
// If we want to create a multi-line comment, we write it between /*and */
// Like this:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1126/1357/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

    </body>
</html>

// CSS:

// JS:
/* This code 
   creates an 
   alert box */
alert("This is an alert box!");

// Note: Heads up! We use comments to describe and explain what the code is
// doing.


// Question:
//   Create a multi-line comment in JavaScript.
//     __ this is a
//     multiline
//     comment __

// Answer:
     /* this is a
     multiline
     comment */
