// Multiple Parameters:
// You can define multiple parameters for a function by comma - separating them.

//   function myFunc(x, y) {
//       // some code
//   }

// Note: The example above defines the function myFunc to take two parameters.


// Question:
//   What character is used to separate parameters from each other?

// Answer:
//   ,


// Multiple Parameters:
// The parameters are used within the function's definition.

     function sayHello(name, age) {
         document.write(name + " is " + age + " years old.");
     }

// Note: Function parameters are the names listed in the function definition.


// Question:
//   What is the output of this code?
//     function test(x, y) {
//         if (x > y) {
//             document.write(x);
//         }
//         else {
//             document.write(y);
//         }
//     }
//     test(5, 8);

// Answer:
//   8


// Multiple Parameters:
// When calling the function, provide the arguments in the same order in which
// you defined them.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1147/1438/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function sayHello(name, age) {
    document.write(name + " is " + age + " years old.");
}
sayHello("John", 20)

// Result:
// John is 20 years old.

// Note: If you pass more arguments than are defined, they will be assigned to
// an array called arguments. They can be used like this:
// arguments[0], arguments[1], etc.


// Question:
//   Fill in the blanks to create a function alerting the sum of the two
//   parameters.
//     ________ myFunction(x, y)
//     {
//         alert(x _ _);
//     }

// Answer:
       function myFunction(x, y) {
           alert(x + y);
       }


// Multiple Parameters:
// After defining the function, you can call it as many times as needed.
// JavaScript functions do not check the number of arguments received.

// Note: If a function is called with missing arguments (fewer than declared),
// the missing values are set to undefined, which indicates that a variable has
// not been assigned a value.


// Question:
//   How many times can the declared function be used?

// Answer:
//   Any
