// The Alert Box:
// JavaScript offers three types of popup boxes, the Alert, Prompt, and Confirm
// boxes.

// Alert Box:

// An alert box is used when you want to ensure that information gets through to
// the user.
// When an alert box pops up, the user must click OK to proceed.
// The alert function takes a single parameter, which is the text displayed in
// the popup box.

// Example:
// Try It Yourself:
// https://www.sololearn.com/learning/1024/1149/1443/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
alert("Do you really want to leave this page?"); 

// Result:
// Alert box similar to example below pops up in browser.
//
//   |----------------------------------------|
//   |                                        |
//   | Do you really want to leave this page? |
//   |                                        |
//   |                                   [OK] |
//   |                                        |
//   |----------------------------------------|

// To display line breaks within a popup box, use a backslash followed by the
// character n.

// Example:
// Try It Yourself:
// https://www.sololearn.com/learning/1024/1149/1443/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
alert("Hello\nHow are you?");

// Result:
// Alert box similar to example below pops up in browser.
//
//   |--------------|
//   |              |
//   | Hello        |
//   | How are you? |
//   |              |
//   |         [OK] |
//   |              |
//   |--------------|

// Note: Be careful when using alert boxes, as the user can continue using the
// page only after clicking OK.


// Question:
// How many parameters can be accepted by the "alert" function?

// Answer:
// 1


// Prompt Box:
// A prompt box is often used to have the user input a value before entering a
// page.
// When a prompt box pops up, the user will have to click either OK or Cancel to
// proceed after entering the input value.
// If the user clicks OK, the box returns the input value. If the user clicks
// Cancel, the box returns null.

// The prompt() method takes two parameters.
// - The first is the label, which you want to display in the text box.
// - The second is a default string to display in the text box(optional).

// Example:
// Try It Yourself:
// https://www.sololearn.com/learning/1024/1149/1445/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var user = prompt("Please enter your name");
alert(user);

// The prompt appears as:
//
//   |------------------------|
//   |                        |
//   | Please enter your name |
//   | [                    ] |
//   |                        |
//   |          [Cancel] [OK] |
//   |                        |
//   |------------------------|

// When a prompt box pops up, the user will have to click either "OK" or
// "Cancel" to proceed after entering an input value.Do not overuse this method,
// because it prevents the user from accessing other parts of the page until the
// box is closed.


// Question:
//   Fill in the blanks to obtain the name of the user and alert it to the screen:
//     var name = ______ ("Enter your name:");
//     alert(____);

// Answer:
     var name = prompt("Enter your name:");
     alert(name);


// Confirm Box:
// A confirm box is often used to have the user verify or accept something.
// When a confirm box pops up, the user must click either OK or Cancel to
// proceed.
// If the user clicks OK, the box returns true.If the user clicks Cancel, the
// box returns false.

// Example:
// Try It Yourself:
// https://www.sololearn.com/learning/1024/1149/1444/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var result = confirm("Do you really want to leave this page?");
if (result == true) {
    alert("Thanks for visiting");
}
else {
    alert("Thanks for staying with us");
}

// Result:
//
//   |----------------------------------------|
//   |                                        |
//   | Do you really want to leave this page? |
//   |                                        |
//   |                          [Cancel] [OK] |
//   |                                        |
//   |----------------------------------------|

// The result when the user clicks OK:
//
//   |--------------------------------------------------------|
//   |                                                        |
//   |                  Thanks for visiting                   |
//   |                                                        |
//   | [ ] Prevent this page from creating additional dialogs |
//   |                                                        |
//   |                                                   [OK] |
//   |                                                        |
//   |--------------------------------------------------------|

// The result when the user clicks Cancel:
//
//   |--------------------------------------------------------|
//   |                                                        |
//   |               Thanks for staying with us               |
//   |                                                        |
//   | [ ] Prevent this page from creating additional dialogs |
//   |                                                        |
//   |                                                   [OK] |
//   |                                                        |
//   |--------------------------------------------------------|

// Note: Do not overuse this method, because it also prevents the user from
// accessing other parts of the page until the box is closed.


// Question:
//   In the "confirm" dialog box, "OK" returns true, and "Cancel" returns ...

// Answer:
//   false
