// Question:
//   The following code will result in what value?
//     function test(number) {
//         while (number < 5) {
//             number++;
//         }
//         return number;
//     }
//     alert(test(2));

// Answer:
//   5


// Question:
//   What is the output of the following expression?
//     function multNmbrs(a, b) {
//         var c = a * b;
//     }
//     multNmbrs(2, 6);

// Answer:
//   Nothing


// Question:
//   Please fill in the corresponding names for the built-in dialog boxes:
//     ______ is for getting input from the user;
//     _____ is for displaying a message in a box;

// Answer:
//   prompt is for getting input from the user;
//   alert is for displaying a message in a box;


// Question:
//   Fill in the blanks to calculate the maximum of the parameters:
//     function max(a, b) {
//         __ (a >= b)
//             return _;
//         ____
//             return b;
//     }

// Answer:
     function max(a, b) {
         if (a >= b)
             return a;
         else
             return b;
     }


// Question:
//   What is the correct syntax for referring to an external script called
//   "script.js"?

// Answer:
//   <script src="script.js">


// Question:
//   What alert will display on the screen?
//     function test(a, b) {
//         if (a > b) {
//             return a * b;
//         } else {
//             return b / a;
//         }
//     }
//     alert(test(5, 15));

// Answer:
//   3
