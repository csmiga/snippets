// Function Return:
// A function can have an optional return statement. It is used to return a
// value from the function.

// This statement is useful when making calculations that require a result. 

// Note: When JavaScript reaches a return statement, the function stops
// executing.


// Question:
//   When is the "return" statement most frequently needed?

// Answer:
//   When you need to make a calculation and receive the result


// Function Return:
// Use the return statement to return a value.
// For example, let's calculate the product of two numbers, and return the
// result.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1148/1441/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function myFunction(a, b) {
    return a * b;
}
var x = myFunction(5, 6);
document.write(x);

// Result:
//   30

// Note: If you do not return anything from a function, it will return
// undefined.


// Question:
//   Where is the "return" statement placed?

// Answer:
//   At the end of the function description


// Function Return:

// Another example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1148/1442/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function addNumbers(a, b) {
    var c = a + b;
    return c;
}
document.write(addNumbers(40, 2));

// Result:
//   42

// Note: The document.write command outputs the value returned by the function,
// which is the sum of the two parameters.


// Question:
//   Please enter the corresponding keyword to have the result of the function
//   below displayed on the screen:
//     function substrNumbrs(first, second) {
//         var result = first - second;
//         ______ result;
//     }
//     document.write(substrNumbrs(10, 5));

// Answer:
       function substrNumbrs(first, second) {
           var result = first - second;
           return result;
       }
       document.write(substrNumbrs(10, 5));
