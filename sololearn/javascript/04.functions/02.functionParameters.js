// Function Parameters:
// Functions can take parameters.
// Function parameters are the names listed in the function's definition.

// Syntax:

//   functionName(param1, param2, param3) {
//       // some code
//   }

// Note: As with variables, parameters should be given names, which are
// separated by commas within the parentheses.


// Question:
//   What do you need to do to create a parameter?

// Answer:
//   Write a variable name in the parentheses


// Using Parameters:
// After defining the parameters, you can use them inside the function.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1146/1434/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function sayHello(name) {
    alert("Hi, " + name);
}
sayHello("David");

// Result:
// Alert box similar to example below pops up in browser.
//
//   |-----------|
//   |           |
//   | Hi, David |
//   |           |
//   |      [OK] |
//   |           |
//   |-----------|

// This function takes in one parameter, which is called name. When calling the
// function, provide the parameter's value (argument) inside the parentheses.

// Note: Function arguments are the real values passed to (and received by) the
// function.


// Question:
//   When and how is the parameter used?

// Answer:
//   By calling the function and placing the value in the parentheses


// Function Parameters:
// You can define a single function, and pass different parameter
// values(arguments) to it.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1146/1435/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function sayHello(name) {
    alert("Hi, " + name);
}
sayHello("David");
sayHello("Sarah");
sayHello("John");

// Result:
// 1st alert box similar to example below pops up in browser.
//
//   |-----------|
//   |           |
//   | Hi, David |
//   |           |
//   |      [OK] |
//   |           |
//   |-----------|

// 2nd alert box similar to example below pops up in browser.
//
//   |--------------------------------------------------------|
//   |                                                        |
//   |                       Hi, Sarah                        |
//   |                                                        |
//   | [ ] Prevent this page from creating additional dialogs |
//   |                                                        |
//   |                                                   [OK] |
//   |                                                        |
//   |--------------------------------------------------------|

// 3rd alert box similar to example below pops up in browser.
//
//   |--------------------------------------------------------|
//   |                                                        |
//   |                        Hi, John                        |
//   |                                                        |
//   | [ ] Prevent this page from creating additional dialogs |
//   |                                                        |
//   |                                                   [OK] |
//   |                                                        |
//   |--------------------------------------------------------|

// Note: This will execute the function's code each time for the provided
// argument.


// Question:
//   Drag and drop from the options below to declare a function and call it, by
//   passing "Test" as the argument:
//     ________ myAlert(txt) {
//         alert("Hello " + txt);
//     }
//     _______ _____ ;

//     var, (), myAlert, function, Test, ("Test")

// Answer:
       function myAlert(txt) {
           alert("Hello " + txt);
       }
       myAlert("Test");
