// JavaScript Functions:
// A JavaScript function is a block of code designed to perform a particular
// task.

// The main advantages of using functions:
// Code reuse: Define the code once, and use it many times.
// Use the same code many times with different arguments, to produce different
// results.

// Note: A JavaScript function is executed when "something" invokes, or calls,
// it.


// Question:
//   What is a function?

// Answer:
//   A certain block of code that can be reused over and over again


// Defining a Function:
// To define a JavaScript function, use the function keyword, followed by a
// name, followed by a set of parentheses().

// The code to be executed by the function is placed inside curly brackets { }.

//   function name() {
//       //code to be executed
//   }

// Note: Function names can contain letters, digits, underscores, and dollar
// signs (same rules as variables).


// Question:
//   Add the corresponding keyword and symbols to create a function named
//   "test".
//     ________ test()
//     _
//         /* some code */
//     _

// Answer:
     function test()
     {
         /* some code */
     }


// Calling a Function:
// To execute the function, you need to call it.
// To call a function, start with the name of the function, then follow it with
// the arguments in parentheses.

// Example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1145/1430/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function myFunction() {
    alert("Calling a Function!");
}
myFunction();

// Result:
// Alert box similar to example below pops up in browser.
//
//   |--------------------|
//   |                    |
//   | Calling a Function |
//   |                    |
//   |               [OK] |
//   |                    |
//   |--------------------|

// Note: Always remember to end the statement with a semicolon after calling the
// function.


// Question:
//   Fill in the blanks to define and call the "hello" function.
//     ________ hello() {
//         alert("Hi there");
//     }
//     _____();

// Answer:
     function hello() {
         alert("Hi there");
     }
     hello();


// Calling Functions:
// Once the function is defined, JavaScript allows you to call it as many
// times as you want to.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1145/1431/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function myFunction() {
    alert("Alert box!");
}
myFunction();
// some other code
myFunction();

// Result:
// 1st alert box similar to example below pops up in browser.
//
//   |------------|
//   |            |
//   | Alert box! |
//   |            |
//   |       [OK] |
//   |            |
//   |------------|

// 2nd alert box similar to example below pops up in browser.
//
//   |--------------------------------------------------------|
//   |                                                        |
//   |                       Alert box!                       |
//   |                                                        |
//   | [ ] Prevent this page from creating additional dialogs |
//   |                                                        |
//   |                                                   [OK] |
//   |                                                        |
//   |--------------------------------------------------------|

// Note: You can also call a function using this syntax: myFunction.call(). The
// difference is that when calling in this way, you're passing the 'this'
// keyword to a function. You'll learn about it later.


// Question:
//   How many times can the function be executed inside a web page?

// Answer:
//   As many as needed
