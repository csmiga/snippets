// The Object Constructor:
// In the previous lesson, we created an object using the object
// literal(or initializer) syntax.

     var person = {
     name: "John", age: 42, favColor: "green"
     };

// This allows you to create only a single object.
// Sometimes, we need to set an "object type" that can be used to create a
// number of objects of a single type.
// The standard way to create an "object type" is to use an object constructor
// function.

     function person(name, age, color) {
         this.name = name;
         this.age = age;
         this.favColor = color;
     }

// The above function (person) is an object constructor, which takes parameters
// and assigns them to the object properties.

// Note: The this keyword refers to the current object. Note that this is not a
// variable. It is a keyword, and its value cannot be changed.


// Question:
//   The Object Constructor
//   Fill in the blanks to create a constructor function:
//     function movie(title, director) {
//         this.title = _____;
//         this.director = ________;
//     }

// Answer:
     function movie(title, director) {
         this.title = title;
         this.director = director;
     }


// Creating Objects:
// Once you have an object constructor, you can use the new keyword to create
// new objects of the same type.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1152/1459/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function person(name, age, color) {
    this.name = name;
    this.age = age;
    this.favColor = color;
}

var p1 = new person("John", 42, "green");
var p2 = new person("Amy", 21, "red");

document.write(p1.age);
document.write(p2.name);

// Note: p1 and p2 are now objects of the person type. Their properties are
// assigned to the corresponding values.


// Question:
//   Creating Objects
//   What keyword is used for creating an instance of an object?

// Answer:
//    new


// Creating Objects:
// Consider the following example.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1152/1460/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function person(name, age) {
    this.name = name;
    this.age = age;
}
var John = new person("John", 25);
var James = new person("James", 21);

document.write(John.age);

// Access the object's properties by using the dot syntax, as you did before.

//   Object's . Property's Name
//   ========   ===============
//       John . name
//       John . age
//      James . name
//      James . age

// Note: Understanding the creation of objects is essential.


// Question:
//   Creating Objects:
//   Which two components are necessary in order to use information contained
//   within an object?

// Answer:
//   object's name
//   property's name
