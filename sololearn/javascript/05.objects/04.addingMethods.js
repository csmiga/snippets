// Methods:
// Methods are functions that are stored as object properties.
// Use the following syntax to create an object method:

//   methodName = function() { code lines }

// Access an object method using the following syntax:

//   objectName.methodName()

// A method is a function, belonging to an object. It can be referenced using
// the this keyword.
// The this keyword is used as a reference to the current object, meaning that
// you can access the objects properties and methods using it.

// Defining methods is done inside the constructor function.

// For Example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1154/1466/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function person(name, age) {
    this.name = name;
    this.age = age;
    this.changeName = function (name) {
        this.name = name;
    }
}

var p = new person("David", 21);
p.changeName("John");

document.write(p.name);

// In the example above, we have defined a method named changeName for our
// person, which is a function, that takes a parameter name and assigns it to
// the name property of the object.
// this.name refers to the name property of the object.

// Note: The changeName method changes the object's name property to its
// argument.


// Question:
//   Methods
//   The "this" keyword in the method means:

// Answer:
//   The current object


// Methods:
// You can also define the function outside of the constructor function and
// associate it with the object.

//   function person(name, age) {
//       this.name = name;
//       this.age = age;
//       this.yearOfBirth = bornYear;
//   }
//   function bornYear() {
//       return 2016 - this.age;
//   }

// As you can see, we have assigned the object's yearOfBirth property to the
// bornYear function.
// The this keyword is used to access the age property of the object, which is
// going to call the method.

// Note: As you can see, we have assigned the object's yearOfBirth property to
// the bornYear function. The this keyword is used to access the age property of
// the object, which is going to call the method.


// Question:
//   Methods:
//   Please associate the "testData" constructor function below with a method
//   called "mymethod":
//     function testData (first, second) {
//         this.first = first;
//         this.second = second;
//         this.checkData = ________;
//   }

// Answer:
//   function testData (first, second) {
//       this.first = first;
//       this.second = second;
//       this.checkData = mymethod;
//   }


// Methods:
//   Call the method as usual.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1154/1468/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
function person(name, age) {
    this.name = name;
    this.age = age;
    this.yearOfBirth = bornYear;
}
function bornYear() {
    return 2016 - this.age;
}

var p = new person("A", 22);

document.write(p.yearOfBirth());

// Note: Call the method by the property name you specified in the constructor
// function, rather than the function name.


// Question:
//   Methods:
//   In order to use the object's properties within a function, use:

// Answer:
//   The "this" keyword
