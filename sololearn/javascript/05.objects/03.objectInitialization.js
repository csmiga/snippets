// Object Initialization:
// Use the object literal or initializer syntax to create single objects.

//   var John = {name: "John", age: 25};
//   var James = { name: "James", age: 21 };

// Note: Objects consist of properties, which are used to describe an object.
// Values of object properties can either contain primitive data types or other
//objects.


// Question:
//   Object Initialization
//   Fill in the blanks:
//     simba = _ category: "lion",
//     gender: "male" _

// Answer:
//   simba = {category: "lion",
//   gender: "male"}


// Using Object Initializers:
// Spaces and line breaks are not important.An object definition can span
// multiple lines.

//   var John = {
//       name: "John",
//       age: 25
//   };
//   var James = {
//       name: "James",
//       age: 21
//   };

// No matter how the object is created, the syntax for accessing the properties
// and methods does not change.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/1152/1460/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
        </head>
        <body>

        </body>
    </html>

// CSS:

// JS:
var John = {
    name: "John",
    age: 25
};
var James = {
    name: "James",
    age: 21
};
document.write(John.age);

// Note: Don't forget about the second accessing syntax: John['age'].


// Question:
//   Using Object Initializers
//   Complete the following expression to display the "simba" object's
//   "category" property on the screen:
//     document.write(simba._________;

// Answer:
//   document.write(simba.category);
