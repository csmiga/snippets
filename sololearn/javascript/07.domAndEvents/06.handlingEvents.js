// Events:
// You can write JavaScript code that executes when an event occurs, such as
// when a user clicks an HTML element, moves the mouse, or submits a form.

// When an event occurs on a target element, a handler function is executed.

// Common HTML events include:

//   Event          Description
//   =====          ===========
//   onclick        occurs when the user clicks on an element
//   onload         occurs when an object has loaded
//   onunload       occurs once a page has unloaded (for <body>)
//   onchange       occurs when the content of a form element,the selection, or
//                  the checked state have changed.
//                  (for <input>, <keygen>, <select>, and <textarea>)
//   onmouseover    occurs when the pointer is moved onto an element, or onto
//                  one of its children
//   onmouseout     occurs when a user moves the mouse pointer out of an
//                  element, or out of one of its children
//   onmousedown    occurs when the user presses a mouse button over an element
//   onmouseup      occurs when a user releases a mouse button over an element
//   onblur         occurs when an element loses focus
//   onfocus        occurs when an element gets focus

// Note: Corresponding events can be added to HTML elements as attributes.
// For example: <p onclick="someFunc()">some text</p>


// Question:
//   Events
//   The type of function that executes when an event occurs is called:

// Answer:
//   event handler


// Handling Events:
// Let's display an alert popup when the user clicks a specified button:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2758/5847/1
// HTML:
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <button onclick="show();">Click Me</button>
    </body>
</html>

// CSS:

// JS:
function show() {
    alert("Hi there");
}


// Event handlers can be assigned to elements.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2758/5847/1
// HTML:
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <button id="demo">Click Me</button>
    </body>
</html>

// CSS:

// JS:
// calling the function in window.onload to make sure the HTML is loaded
window.onload = function() {
    var x = document.getElementById('demo');
    x.onclick = function () {
        document.body.innerHTML = Date();
    }
};

// Note: You can attach events to almost all HTML elements.


// Question:
//   Handling Events
//   Fill in the blanks to call func() when the button is clicked.
//     <button _______ = "____()">
//         Click Here
//     </button>

// Answer:
//   <button onclick = "func()">
//       Click Here
//   </button>


// Events:
// The onload and onunload events are triggered when the user enters or leaves
// the page. These can be useful when performing actions after the page is
// loaded.

// HTML
//   <body onload="doSomething()">

// Similarly, the window.onload event can be used to run code after the whole
// page is loaded.

// JS
//   window.onload ​= function() {
//       //some code
//   }

// The onchange event is mostly used on textboxes. The event handler gets called
// when the text inside the textbox changes and focus is lost from the element.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2758/5848/1
// HTML:
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <input type="text" id="name" onchange="change()">
    </body>
</html>

</body>
// CSS:

// JS:
function change() {
    var x = document.getElementById('name');
    x.value = x.value.toUpperCase();
}

// Note: It’s important to understand events, because they are an essential part
// of dynamic web pages.


// Question:
//   Events
//   Drag and drop from the options below to call the clear() function after body is loaded.
//     <body ______ = "_______">

//   onclick, onload, onblur, clear()

// Answer:
//   <body onload = "clear()">


// Event Listeners:
// The addEventListener() method attaches an event handler to an element without
// overwriting existing event handlers. You can add many event handlers to one
// element.
// You can also add many event handlers of the same type to one element, i.e.,
// two "click" events.

//   element.addEventListener(event, function, useCapture);

// The first parameter is the event's type (like "click" or "mousedown").
// The second parameter is the function we want to call when the event occurs.
// The third parameter is a Boolean value specifying whether to use event
// bubbling or event capturing. This parameter is optional, and will be
// described in the next lesson.

// Note: Note that you don't use the "on" prefix for this event; use "click"
// instead of "onclick".

// Example:

//   element.addEventListener("click", myFunction);
//   element.addEventListener("mouseover", myFunction);

//   function myFunction() {
//       alert("Hello World!");
//   }

// Let's create an event handler that removes itself after being executed:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2758/5849/1
// HTML:
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <button id="demo">Start</button>
    </body>
</html>

// CSS:

// JS:
// calling the function in window.onload to make sure the HTML is loaded
window.onload = function() {
    var btn = document.getElementById("demo");
    btn.addEventListener("click", myFunction);

    function myFunction() {
        alert(Math.random());
        btn.removeEventListener("click", myFunction);
    }
};

// After clicking the button, an alert with a random number displays and the
// event listener is removed.

// Note: Internet Explorer version 8 and lower do not support the
// addEventListener() and removeEventListener() methods. However, you can use
// the document.attachEvent() method to attach event handlers in Internet
// Explorer.


// Question: 
//   Event Listeners
//   Can multiple event handlers be added to a single element?

// Answer:
//   Yes
