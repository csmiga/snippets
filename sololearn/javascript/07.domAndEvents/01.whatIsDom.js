// The DOM:
// When you open any webpage in a browser, the HTML of the page is loaded and
// rendered visually on the screen.
// To accomplish that, the browser builds the Document Object Model of that
// page, which is an object oriented model of its logical structure.
// The DOM of an HTML document can be represented as a nested set of boxes:

//         Document
//            |
//          <html>
//            |
//   <head> ------ <body>
//     |             |
//     |          -------
//     |          |     |
//   <title>    <h1>   <a>

// Note: JavaScript can be used to manipulate the DOM of a page dynamically to
// add, delete and modify elements.


// Question:
//   The DOM
//   What is DOM?

// Answer:
//   Document Object Model


// DOM Tree:
// The DOM represents a document as a tree structure.
// HTML elements become interrelated nodes in the tree.
// All those nodes in the tree have some kind of relations among each other.
// Nodes can have child nodes.Nodes on the same tree level are called siblings.
// For example, consider the following structure:

//         Document
//            |
//          <html>
//            |
//   <head> ------ <body>
//     |             |
//     |          -------
//     |          |     |
//   <title>    <h1>   <a>

// For the example above:
// <html> has two children (<head>, <body>);
// <head> has one child (<title>) and one parent (<html>);
// <title> has one parent (<head>) and no children;
// <body> has two children (<h1> and <a>) and one parent (<html>);

// It is important to understand the relationships between elements in an HTML
// document in order to be able to manipulate them with JavaScript.


// Question:
//   DOM Tree
//   In the following HTML, which element is the parent of h1?
//     <body>
//         <p><h1>Hi</h1></p>
//     </body>

// Answer:
//   p


// The document Object:
// There is a predefined document object in JavaScript, which can be used to
// access all elements on the DOM.
// In other words, the document object is the owner(or root) of all objects in
// your webpage.
// So, if you want to access objects in an HTML page, you always start with
// accessing the document object.

// For example:

     document.body.innerHTML = "Some text";

// As body is an element of the DOM, we can access it using the document object
// and change the content of the innerHTML property.

// Note: The innerHTML property can be used on almost all HTML elements to
// change its content.


// Question:
//   The document Object:
//   Select all that apply:

// Answer:
//   * The documents object is the root of the DOM
//   * innerHTML is a property
