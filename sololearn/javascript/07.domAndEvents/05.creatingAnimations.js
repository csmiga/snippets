// Animations:
// Now that we know how to select and change DOM elements, we can create a
// simple animation.
// Let's create a simple HTML page with a box element that will be animated
// using JS.

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2756/5843/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div id="container">
            <div id="box"> </div>
        </div>
    </body>
</html>

// CSS:
#container {
    width: 200px;
    height: 200px;
    background: green;
    position: relative;
}
#box {
    width: 50px;
    height: 50px;
    background: red;
    position: absolute;
}

// JS:
// JavaScript code will go here

// Our box element is inside a container element. Note the position attribute
// used for the elements: the container is relative and the box is absolute.
// This will allow us to create the animation relative to the container.

// We will be animating the red box to make it move to the right side of the
// container.

// Note: You need to be familiar with CSS to better understand the code
// provided.


// Question:
//   Animations
//   To create an animation relative to a container, the position attribute for
//   the container should be set to:

// Answer:
//   relative


// Animations:
// To create an animation, we need to change the properties of an element at
// small intervals of time.We can achieve this by using the setInterval()
// method, which allows us to create a timer and call a function to change
// properties repeatedly at defined intervals(in milliseconds).

// For example:

//   var t = setInterval(move, 500);

// This code creates a timer that calls a move() function every 500
// milliseconds.
// Now we need to define the move() function, that changes the position of the
// box.

    // starting position
    var pos = 0;
    //our box element
    var box = document.getElementById("box");

    function move() {
        pos += 1;
        box.style.left = pos + "px"; //px = pixels
    }

// Note: The move() function increments the left property of the box element by
// one each time it is called.


// Question:
//   Animations
//   What is the interval for this timer ?
//     var t = setInterval(func, 10000);

// Answer:
//   10 seconds


// Animations:
// The following code defines a timer that calls the move() function every 10
// milliseconds:

//   var t = setInterval(move, 10);

// However, this makes our box move to the right forever. To stop the animation
// when the box reaches the end of the container, we add a simple check to the
// move() function and use the clearInterval() method to stop the timer.

    function move() {
        if (pos >= 150) {
            clearInterval(t);
        }
        else {
            pos += 1;
            box.style.left = pos + "px";
        }
    }

// When the left attribute of the box reaches the value of 150, the box reaches
// the end of the container, based on a container width of 200 and a box width
// of 50.

// The final code:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2756/5845/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div id="container">
            <div id="box"> </div>
        </div>
    </body>
</html>

// CSS:
#container {
    width: 200px;
    height: 200px;
    background: green;
    position: relative;
}
#box {
    width: 50px;
    height: 50px;
    background: red;
    position: absolute;
}

// JS:
//calling the function in window.onload to make sure the HTML is loaded
window.onload = function () {
    var pos = 0;
    //our box element
    var box = document.getElementById('box');
    var t = setInterval(move, 10);

    function move() {
        if (pos >= 150) {
            clearInterval(t);
        }
        else {
            pos += 1;
            box.style.left = pos + 'px';
        }
    }
};

// Note: Congratulations, you have just created your first JavaScript animation!


// Question:
//   Animations
//   Which function is used to stop a setInterval timer?

// Answer:
//   clearInterval
