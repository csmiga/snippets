// Creating Elements:
// Use the following methods to create new nodes:
//   element.cloneNode()                 - clones an element and returns the
//                                         resulting node.
//   document.createElement(element)     - creates a new element node.
//   document.createTextNode(text)       - creates a new text node.

// For example:

//   var node = document.createTextNode("Some new text");

// This will create a new text node, but it will not appear in the document
// until you append it to an existing element with one of the following methods:

//   element.appendChild(newNode)        - adds a new child node to an element
//                                         as the last child node.
//   element.insertBefore(node1, node2)  - inserts node1 as a child before
//                                         node2.

// Example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2754/5838/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div id="demo">some content</div>
    </body>
</html>

// CSS:

// JS:
// calling the function in window.onload to make sure the HTML is loaded
window.onload = function () {
    //creating a new paragraph
    var p = document.createElement("p");
    var node = document.createTextNode("Some new text");
    //adding the text to the paragraph
    p.appendChild(node);

    var div = document.getElementById("demo");
    //adding the paragraph to the div
    div.appendChild(p);
};

// Note: This creates a new paragraph and adds it to the existing div element on
// the page.


// Question:
//   Creating Elements
//   Drag and drop from the options below to add a new < li > element to the
//   unordered list with id = "list".
//     var el = document._____________("li");
//     var txt = document.createTextNode("B");
//     el.appendChild(txt);
//     var ul = document.__________("list");
//     ul.___________(el);

//     appendChild, getElemntById, createElement

// Answer:
var el = document.createElement("li");
var txt = document.createTextNode("B");
el.appendChild(txt);
var ul = document.getElement("list");
ul.appendChild(el);


// Removing Elements:
// To remove an HTML element, you must select the parent of the element and use
// the removeChild(node) method.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2755/5841/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div id="demo">
            <p id="p1">This is a paragraph.</p>
            <p id="p2">This is another paragraph.</p>
        </div>
    </body>
</html>

// CSS:

// JS:
//calling the function in window.onload to make sure the HTML is loaded
window.onload = function () {
    var parent = document.getElementById("demo");
    var child = document.getElementById("p1");
    parent.removeChild(child);
};

// This removes the paragraph with id="p1" from the page.

// Note: An alternative way of achieving the same result would be the use of the
// parentNode property to get the parent of the element we want to remove:
// var child = document.getElementById("p1");
// child.parentNode.removeChild(child);


// Question:
//   Removing Elements
//   Drag and drop from the options below to remove the node element from the
//   page(par is node's parent).
//     var par = document.getElementById("par");
//     var node = document.getElementById("node");
//     ___.___________(____);

//   createElement, node, document, par, removeChild

// Answer:
var par = document.getElementById("par");
var node = document.getElementById("node");
par.removeChild(node);


// Replacing Elements:
// To replace an HTML element, the element.replaceChild(newNode, oldNode) method
// is used.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2755/5842/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div id="demo">
            <p id="p1">This is a paragraph.</p>
            <p id="p2">This is another paragraph.</p>
        </div>
    </body>
</html>

// CSS:

// JS:
// calling the function in window.onload to make sure the HTML is loaded
window.onload = function () {
    var p = document.createElement("p");
    var node = document.createTextNode("This is new");
    p.appendChild(node);

    var parent = document.getElementById("demo");
    var child = document.getElementById("p1");
    parent.replaceChild(p, child);
};

// Note: The code above creates a new paragraph element that replaces the
// existing p1 paragraph.


// Question:
//   Replacing Elements
//   Which method is used to replace nodes?

// Answer:
//   replaceChild
