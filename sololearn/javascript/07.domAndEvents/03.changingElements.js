// Changing Attributes:
// Once you have selected the element(s) you want to work with, you can change
// their attributes.
// As we have seen in the previous lessons, we can change the text content of an
// element using the innerHTML property.
// Similarly, we can change the attributes of elements.
// For example, we can change the src attribute of an image:

//   <img id="myimg" src="orange.png" alt="" />
//   <script>
//   var el = document.getElementById("myimg");
//   el.src = "apple.png";
//   </script>

// We can change the href attribute of a link:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2754/5838/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <a href="http://www.example.com">Some link</a>
    </body>
</html>

// CSS:

// JS:
// calling the function in window.onload to make sure the HTML is loaded
window.onload = function () {
    var el = document.getElementsByTagName('a');
    el[0].href = 'http://www.sololearn.com';
};

// Note: Practically all attributes of an element can be changed using
// JavaScript.


// Question:
//   Changing Attributes
//   Fill in the blanks to select all images of the page and change their src
//   attribute.
//     var arr = document.
//         getElementsByTagName("___");
//     for (var x = 0; x < arr.______; x++) {
//         arr[x].___ = "demo.jpg";
//     }

// Answer:
var arr = document.getElementsByTagName("img");
for (var x = 0; x < arr.length; x++) {
    arr[x].src = "demo.jpg";
}


// Changing Style:
// The style of HTML elements can also be changed using JavaScript.
// All style attributes can be accessed using the style object of the element.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2754/5839/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div id="demo" style="width:200px">some text</div>
    </body>
</html>

// CSS:

// JS:
// calling the function in window.onload to make sure the HTML is loaded
window.onload = function () {
    var x = document.getElementById("demo");
    x.style.color = '#6600FF';
    x.style.width = '100px';
};

// The code above changes the text color and width of the div element.

// Note: All CSS properties can be set and modified using JavaScript. Just
// remember, that you cannot use dashes (-) in the property names: these are
// replaced with camelCase versions, where the compound words begin with a
// capital letter.
// For example: the background - color property should be referred to as
// backgroundColor.


// Question:
//   Changing Style
//   Fill in the blanks to change the background color of all span elements of
//   the page.
//     var s = document.getElementsByTagName("_____");
//     _____(var x=0; x<s.length;x++) {
//         s[_____].style.backgroundColor = "#33EA73";
//     }

// Answer:
var s = document.getElementsByTagName("span");
for(var x=0; x<s.length;x++) {
    s[x].style.backgroundColor = "#33EA73";
}
