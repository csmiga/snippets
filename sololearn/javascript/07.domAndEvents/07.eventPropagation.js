// Event Propagation:
// There are two ways of event propagation in the HTML DOM: bubbling and
// capturing.

// Event propagation allows for the definition of the element order when an
// event occurs. If you have a <p> element inside a < div > element, and the
// user clicks on the < p > element, which element's "click" event should be
// handled first?

// In bubbling, the innermost element's event is handled first and then the
// outer element's event is handled. The <p> element's click event is handled
// first, followed by the <div> element's click event.

// In capturing, the outermost element's event is handled first and then the
// inner. The <div> element's click event is handled first, followed by the
// <p> element's click event.

// Note: Capturing goes down the DOM.
// Bubbling goes up the DOM.


// Question:
//   Event Propagation
//   A paragraph is inside a div element. You want the paragraph’s click event
//   to be handled first. You should use:

// Answer:
//   Bubbling


// Capturing vs. Bubbling:
// The addEventListener() method allows you to specify the propagation type with
// the "useCapture" parameter.

     addEventListener(event, function, useCapture)

// The default value is false, which means the bubbling propagation is used;
// when the value is set to true, the event uses the capturing propagation.

     // Capturing propagation
     elem1.addEventListener("click", myFunction, true);

     // Bubbling propagation
     elem2.addEventListener("click", myFunction, false);

// Note: This is particularly useful when you have the same event handled for
// multiple elements in the DOM hierarchy.


// Question:
//   Capturing vs. Bubbling
//   Drag and drop from the options below to handle the click event and use
//   capturing propagation.
//     x.addEventListener("_____", func,
//     ____);

//     onclick, click, true, false, capture

// Answer:
//   x.addEventListener("click", func,
//   true);
