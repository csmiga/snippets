// Question:
//   Fill in the blanks to change the content of all paragraph tags of the page
//   to "SoloLearn".
//     var arr = ________.getElementsByTagName("_")'
//     for(var x=0; x<arr.length; x++) {
//         arr[_].innerHTML="SoloLearn";
//     }

// Answer:
//   var arr = document.getElementsByTagName("p")'
//   for(var x=0; x<arr.length; x++) {
//       arr[x].innerHTML="SoloLearn";
//   }


// Question:
//   What is the output of this code?
//     <div id="test">
//         <p>some text</p>
//     </div>
//     <script>
//         var el=document.getElementById("test");
//         alert(el.hasChildNodes());
//     </script>

// Answer:
//   true


// Question:
//   Drag and drop from the options below to change the color of the paragraph
//   with id="p2" to red.
//     <script>
//         var d = document.______________("__");
//         d._____._____ = "red";
//     </script>

//     color, style, text-color, getElementById, p2

// Answer:
//   <script>
//       var d = document.getElementById("p2");
//       d.style.color = "red";
//   </script>


// Question:
//   Can you handle multiple events on the same HTML element?

// Answer:
//   Yes


// Question:
//   Fill in the blanks to alert a message when the button is clicked.
//     <button _______ ="msg()">Click me </button>
//     <script>
//         ________ msg() {
//             alert("Hi!");
//         }
//     </script>

// Answer:
//   <button onclick ="msg()">Click me </button>
//   <script>
//       function msg() {
//           alert("Hi!");
//       }
//   </script>


// Question:
//   Display an alert when the mouse pointer is over the div tag:
//     <div _______ ="alert('Hi!');">
//         put the mouse pointer over me
//     </div>

// Answer:
//   <div onmouseover ="alert('Hi!');">
//       put the mouse pointer over me
//   </div>
