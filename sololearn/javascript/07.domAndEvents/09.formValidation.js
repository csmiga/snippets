// Form Validation:
// HTML5 adds some attributes that allow form validation.For example, the
// required attribute can be added to an input field to make it mandatory to
// fill in.
// More complex form validation can be done using JavaScript.
// The form element has an onsubmit event that can be handled to perform
// validation.
// For example, let's create a form with two inputs and one button. The text in
// both fields should be the same and not blank to pass the validation.
 
//   <form onsubmit="return validate()" method="post">
//       Number: <input type="text" name="num1" id="num1" />
//       <br />
//       Repeat: <input type="text" name="num2" id="num2" />
//       <br />
//       <input type="submit" value="Submit" />
//   </form >

// Now we need to define the validate() function:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2761/5854/1
// HTML:
<form onsubmit="return validate()" method="post">
    Number: <input type="text" name="num1" id="num1" /><br />
    Repeat: <input type="text" name="num2" id="num2" /><br />
    <input type="submit" value="Submit" />
</form>

// CSS:


// JS:
function validate() {
    var n1 = document.getElementById('num1');
    var n2 = document.getElementById('num2');
    if (n1.value != '' && n2.value != '') {
        if (n1.value == n2.value) {
            return true;
        }
    }
    alert("The values should be equal and not blank");
    return false;
}

// We return true only when the values are not blank and are equal.

// Note: The form will not get submitted if its onsubmit event returns false.


// Question:
//   Form Validation
//   The form will submit to its action if onsubmit returns:

// Answer:
//   true
