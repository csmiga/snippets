// Image Slider:
// Now we can create a sample image slider project.The images will be changed
// using "Next" and "Prev" buttons.

// Now, let’s create our HTML, which includes an image and the two navigation
// buttons:

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2760/5852/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div>
            <button> Prev </button>
            <img src="http://www.sololearn.com/uploads/slider/1.jpg" width="200px" height="100px" />
            <button> Next </button>
        </div>
    </body>
</html>

// CSS:
button {
    margin-top: 30px;
    float: left;
    height: 50px;
}
img {
    float: left;
    margin-right: 10px;
    margin-left: 10px;
}

// JS:
//JavaScript code will go here

// Next, let's define our sample images in an array: 

     var images = [
         "http://www.sololearn.com/uploads/slider/1.jpg",
         "http://www.sololearn.com/uploads/slider/2.jpg",
         "http://www.sololearn.com/uploads/slider/3.jpg"
     ];

// Note: We are going to use three sample images that we have uploaded to our
// server. You can use any number of images.


// Question:
//   Image Slider
//   Fill in the blanks to define an array.
//     ___ arr = _ 'A', 'B', 'C' _;

// Answer:
//   var arr = [ 'A', 'B', 'C' ];


// Image Slider:
// Now we need to handle the Next and Prev button clicks and call the 
// corresponding functions to change the image.

// HTML:
     <div>
         <button onclick="prev()"> Prev </button>
         <img id="slider" src="http://www.sololearn.com/uploads/slider/1.jpg"
             width="200px" height="100px" />
         <button onclick="next()"> Next </button>
     </div>

// Try It Yourself:
// https://www.sololearn.com/learning/1024/2760/5853/1
// HTML:
<!DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div>
            <button onclick="prev()"> Prev </button>
            <img id="slider" src="http://www.sololearn.com/uploads/slider/1.jpg" width="200px" height="100px" />
            <button onclick="next()"> Next </button>
        </div>
    </body>
</html>

// CSS:
button {
    margin - top: 30px;
    float: left;
    height: 50px;
}
img {
    float: left;
    margin - right: 10px;
    margin - left: 10px;
}

// JS:
var images = [
    'http://www.sololearn.com/uploads/slider/1.jpg',
    'http://www.sololearn.com/uploads/slider/2.jpg',
    'http://www.sololearn.com/uploads/slider/3.jpg'
];
var num = 0;
function next() {
    var slider = document.getElementById('slider');
    num++;
    if (num >= images.length) {
        num = 0;
    }
    slider.src = images[num];
}
function prev() {
    var slider = document.getElementById('slider');
    num--;
    if (num < 0) {
        num = images.length - 1;
    }
    slider.src = images[num];
}

// The num variable holds the current image. The next and previous button clicks
// are handled by their corresponding functions, which change the source of the
// image to the next/previous image in the array.

// Note: We have created a functioning image slider!


// Question:
// Image Slider
// What will be the content of the paragraph after the user clicks on it twice?
//   <p id='txt' onclick='test();'>20</p>
//   <script>
//       function test() {
//           var x=document.getElementById('txt');
//           var n = x.innerHTML;
//           x.innerHTML = n/2;
//       }
//   </script>

// Answer:
//   5


// 