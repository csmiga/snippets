// Dimensions:
// The width() and height() methods can be used to get and set the width and
// height of HTML elements.

// Let's set both the width and height of a div to 100px, as well as set a
// background color for it:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2806/5989/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div></div>
    </body>
</html>

// CSS:


// JS:
$(function () {
    $("div").css("background-color", "red");
    $("div").width(100);
    $("div").height(100);
});

// Note: Run the code and see how it works!


// Question:
//   Dimensions
//   Fill in the blanks to set the height of the paragraph with the id = "demo"
//   to 68px.
//     $("_____").______(__);

// Answer:
//   $("#demo").height(68);


// Dimensions:
// width() and height()           - methods get and set the dimensions without
//                                  the padding, borders and margins.
// innerWidth() and innerHeight() - methods also include the padding.
// outerWidth() and outerHeight() - methods include the padding and borders.

// Check out this image to understand how they work:
//   Follow the link.
//   https://api.sololearn.com/DownloadFile?id=3120

// The following example demonstrates how the methods work:

// HTML:
//   <div></div>

// CSS:
//   div {
//       width: 300px;
//       height: 100px;
//       padding: 10px;
//       margin: 20px;
//       border: 3px solid blue;
//       background - color: red;
//       color: white;
//   }

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2806/5990/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div></div>
    </body>
</html>

// CSS:
div {
    width: 300px;
    height: 100px;
    padding: 10px;
    margin: 20px;
    border: 3px solid blue;
    background - color: red;
    color: white;
}

// JS:
$(function () {
    var txt = "";
    txt += "width: " + $("div").width() + " ";
    txt += "height: " + $("div").height() + "<br/>";
    txt += "innerWidth: " + $("div").innerWidth() + " ";
    txt += "innerHeight: " + $("div").innerHeight() + "<br/>";
    txt += "outerWidth: " + $("div").outerWidth() + " ";
    txt += "outerHeight: " + $("div").outerHeight();

    $("div").html(txt);
});

// Note: Run the code to see the values returned by the dimension methods.


// Question:
//   Dimensions
//   What is the output of this code?
//     <div style="width:200px"></div>
//     <script>
//         $(function() {
//             $("div").css("padding", "5px");
//             alert($("div").innerWidth());
//         });
//     </script>

// Answer:
//   210
