// Manipulating CSS:
// jQuery has several methods for CSS manipulation.
// The addClass() method adds one or more classes to the selected elements. For
// example:

// HTML:
//   <div>Some text</div>

// CSS:
//   .header {
//       color: blue;
//       font - size: x - large;
//   }

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2796/5962/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div>Some text</div>
    </body>
</html>

// CSS:
.header {
    color: blue;
    font - size: x - large;
}

// JS:
$(function () {
    $("div").addClass("header");
});

// The above code assigns the div element the class "header".

// Note: To specify multiple classes within the addClass() method, just separate
// them using spaces. For example, $("div").addClass("class1 class2 class3").


// Question:
//   Manipulating CSS
//   Fill in the blanks to add the class names "text" and "menu" to the <p>
//   element.
//     _("p").addClass("____ menu");

// Answer:
//   $("p").addClass("text menu");


// Manipulating CSS:
// The removeClass() method removes one or more class names from the selected
// elements.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2796/6015/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div class="header red">Some text</div>
    </body>
</html>

// CSS:
.header {
    color: blue;
    font - size: x - large;
}
.red {
    color: red;
    font - weight: bold;
}

// JS:
$(function () {
    $("div").removeClass("red");
});

// The code above removes the class "red" from the div element.

// Note: Again, multiple class names can be specified by separating them using
// spaces.


// Question:
//   Manipulating CSS
//   Which class name will the <p class="a b"></p> element have after the
//   following code?
//     $("p").addClass("c");
//     $("p").removeClass("a c");

// Answer:
//    b


// toggleClass():
// The toggleClass() method toggles between adding / removing classes from the
// selected elements, meaning that if the specified class exists for the
// element, it is removed, and if it does not exist, it is added.
// To demonstrate this in action, we will handle a button click event to toggle
// a class.We will learn more about events and their syntax in the coming
// modules.

// HTML:
//   <p>Some text</p>
//   <button>Toggle Class</button>

// CSS:
//   .red { 
//       color: red;
//       font - weight: bold;
//   }

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2796/5967/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>Some text</p>
        <button>Toggle Class</button>
    </body>
</html>

// CSS:
.red {
    color: red;
    font - weight: bold;
}

// JS:
$(function () {
    $("button").click(function () {
        $("p").toggleClass("red");
    });
});

// The code above toggles the class name "red" upon clicking the button.


// Question:
//   toggleClass()
//   Will the paragraph have a border after this code?
//     <style> 
//       .test{ border-style: solid; }
//     </style> 
//     <p>Some text</p> 
//     <script> 
//       $(function() {
//         $("p").addClass("test"); 
//         $("p").toggleClass("test");
//       }); 
//     </script>

// Answer:
//   No
