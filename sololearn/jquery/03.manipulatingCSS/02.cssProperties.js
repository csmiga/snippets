//  CSS Properties
//  Similar to the html() method, the css() method can be used to get and set
//  CSS property values.For example:

// HTML:
//   <p>Some text</p>

// CSS:
//   p {
//     background - color: red;
//     color: white;
//   }

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2805/5986/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>Some text</p>
    </body>
</html>

// CSS:
p {
    background - color: red;
    color: white;
}

// JS:
$(function () {
    alert($("p").css("background-color"));
    $("p").css("background-color", "blue");
});

// Note: The code above alerts the background-color property of the paragraph
// and then sets it to blue.


// Question:
//   CSS Properties
//   Fill in the blanks to set the font size and color of the paragraph:
//     $("p").___("_________", "16pt");
//     $("p").css("_____", "blue");

// Answer:
//   $("p").css("font-size", "16pt");
//   $("p").css("color", "blue");


// Multiple Properties:
// To set multiple CSS properties, the css() method uses JSON syntax, which is:

// JS
//   css({ "property": "value", "property": "value", ...});

// As you can see, the syntax consists of "property": "value" pairs, which are
// comma separated and enclosed in curly brackets { }.

// For Example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2805/5988/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>Some text</p>
    </body>
</html>

// CSS:


// JS:
$(function () {
    $("p").css({ "background-color": "red", "font-size": "200%" });
});

// This will set the color and font-size properties of the paragraph.

// Note: You can specify any number of properties using this JSON syntax.


// Question:
// Multiple Properties:
// Fill in the blanks to set the color and width property of the div.
//   $("div").___(_"color":"red"_"_____":"50px"});

// Answer:
//   $("div").css({"color":"red","width":"50px"});
