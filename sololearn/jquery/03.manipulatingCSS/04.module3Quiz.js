// Question:
//   Fill in the blanks to set the color and padding properties of the
//   paragraph:
//     $("p").___({"_____":"blue","_______":"5px"});

// Answer:
//   $("p").css({"color":"blue","padding":"5px"});


// Question:
//   What is the value of outerHeight() for the paragraph after the following
//   code?
//     $("p").height(84);
//     $("p").css("margin", 8);
//     $("p").css("padding", 2);

// Answer:
//   88


// Question:
//   When specifying multiple class names for the addClass method, we need to
//   separate them using:

// Answer:
//   spaces


// Question:
//   Fill in the blanks to create a new <p> element, set its height to 50px and
//   add it before the element with id="test".
//     var e = _("<p></p>").text("Some text");
//     e.______(50);
//     $("_____").before(_);

// Answer:
//   var e = $("<p></p>").text("Some text");
//   e.height(50);
//   $("#test").before(e);
