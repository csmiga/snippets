// Hide/Show:
// jQuery has some easy - to - implement effects to create animations.

// hide() and show() - methods are used to hide and show the selected elements.
// toggle()          - method is used to toggle between hiding and showing
//                     elements.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2815/6004/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>Click to toggle show/hide</p>
        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </div>
    </body>
</html>

// CSS:
p {
    background - color: grey;
    text - align: center;
    color: white;
    padding: 5px;
    cursor: pointer;
}
div {
    background - color: grey;
    color: white;
}

// JS:
$(function () {
    $("p").click(function () {
        $("div").toggle();
    });
});

// The hide/show/toggle methods can take an optional argument, speed, which
// specifies the animation speed in milliseconds.
// For example, let's pass 1000 millisecond as the speed argument to the
// toggle() method:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2815/6004/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>Click to toggle show/hide</p>
        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </div>
    </body>
</html>

// CSS:
p {
    background - color: grey;
    text - align: center;
    color: white;
    padding: 5px;
    cursor: pointer;
}
div {
    background - color: grey;
    color: white;
}

// JS:
$(function () {
    $("p").click(function () {
        $("div").toggle(1000);
    });
});

// Note: The hide/show/toggle methods can also take a second optional parameter
// callback, which is a function to be executed after the animation completes.


// Question:
//   Hide/Show
//   The speed parameter for the toggle() method is in:

// Answer:
//   milliseconds


// Fade In/Out:
// Similar to the hide / show methods, jQuery provides the fadeInfadeOut
// methods, which fade an element in and out of visibility.
// Just like the toggle() method switches between hiding and showing, the
// fadeToggle() method fades in and out.
// Let's see fadeToggle() in action:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2815/6005/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>Click to toggle fading</p>
        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </div>
    </body>
</html>

// CSS:
p {
    background - color: grey;
    text - align: center;
    color: white;
    padding: 5px;
    cursor: pointer;
}
div {
    background - color: grey;
    color: white;
}

// JS:
$(function () {
    $("p").click(function () {
        $("div").fadeToggle(1000);
    });
});

// Just like toggle(), fadeToggle() takes two optional parameters: speed and
// callback.

// Note: Another method used for fading is fadeTo(), which allows fading to a
// given opacity (value between 0 and 1).
// For example: $("div").fadeTo(1500, 0.7);


// Question:
//   Fade In/Out
//   Fill in the blanks to fade the paragraph to 60 % opacity in 2 seconds.
//     $("p").fadeTo(____,___);

// Answer:
//   $("p").fadeTo(2000,0.6);


// Slide Up/Down
// The slideUp() and slideDown() methods are used to create a sliding effect on
// elements.
// Again, similar to the previous toggle methods, the slideToggle() method
// switches between the sliding effects and can take two optional parameters:
// speed and callback.
// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2815/6027/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>Click to toggle sliding</p>
        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </div>
    </body>
</html>

// CSS:
p {
    background - color: grey;
    text - align: center;
    color: white;
    padding: 5px;
    cursor: pointer;
}
div {
    background - color: grey;
    color: white;
}

// JS:
$(function () {
    $("p").click(function () {
        $("div").slideToggle(500);
    });
});

// Note: Run the code and see how it works!


// Question:
//   Slide Up/Down
//   Fill in the blanks to slide down the div element in 3 seconds upon
//   clicking on the paragraph.
//     $("p")._____(function() {
//         _____("div").slideDown(_____);
//     });

// Answer:
//   $("p")click(function() {
//       $("div").slideDown(3000);
//   });
