// animate():
// The animate() method lets you animate to a set value, or to a value relative
// to the current value.
// You need to define the CSS properties to be animated as its parameter in JSON
// format("key": "value" pairs).
// The second parameter defines the speed of the animation.
// For example, the following code animates the width property of the div in 1
// second to the value 250px:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2816/6007/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div>Click me</div>
    </body>
</html>

// CSS:
div {
    display: inline - block;
    padding: 25px;
    background - color: grey;
    color: white;
    text - align: center;
    cursor: pointer;
}

// JS:
$(function () {
    $("div").click(function () {
        $("div").animate({ width: '250px' }, 1000);
    });
});

// Note the JSON format for providing the CSS parameters. The JSON syntax was
// also used in the previous modules when manipulating CSS properties.

// Note: You can animate any CSS property using the above mentioned syntax, but
// there is one important thing to remember: all property names must be
// camel-cased when used with the animate() method (camelCase is the practice
// of writing compound words or phrases such that each word or abbreviation
// begins with a capital letter with the first word in lowercase).
// You will need to write paddingLeft instead of padding - left, marginRight
// instead of margin - right, and so on.


// Question:
//   animate()
//   Which of the following can be used with the animate() method?

// Answer:
//   paddingLeft


// Multiple properties can be animated at the same time by separating them with
// commas.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2816/6126/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div>Click me</div>
    </body>
</html>

// CSS:
div {
    display: inline - block;
    padding: 25px;
    background - color: grey;
    color: white;
    text - align: center;
    cursor: pointer;
}

// JS:
$(function () {
    $("div").click(function () {
        $("div").animate({
            width: '250px',
            height: '250px'
        }, 1000);
    });
});

// It is also possible to define relative values (the value is then relative to
// the element's current value). This is done by putting += or -= in front of
// the value:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2816/6126/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div>Click me</div>
    </body>
</html>

// CSS:
div {
    display: inline - block;
    padding: 25px;
    background - color: grey;
    color: white;
    text - align: center;
    cursor: pointer;
}

// JS:
$(function () {
    $("div").click(function () {
        $("div").animate({
            width: '+=250px',
            height: '+=250px'
        }, 1000);
    });
});

// Note: To stop an animation before it is finished, jQuery provides the stop()
// method.


// Question:
//   animate()
//   Fill in the blanks to animate the opacity and height properties of the div
//   element in 5 seconds.
//     $("div").______({
//         opacity: 0.5_ height: '+=100px'
//     }, ____);

// Answer:
//   $("div").animate({
//       opacity: 0.5, height: '+=100px'
//   }, 5000);


// Animation Queue
// By default, jQuery comes with queue functionality for animations.
// This means that if you write multiple animate() calls one after another,
// jQuery creates an "internal" queue for these method calls.Then it runs the
// animate calls one - by - one.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2816/6008/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div></div>
    </body>
</html>

// CSS:
div {
    background: orange;
    height: 80px; width: 80px;
    position: absolute;
    border - radius: 50 %;
    opacity: 0.5;
}

// JS:
$(function () {
    var div = $("div");
    div.animate({ opacity: 1 });
    div.animate({ height: '+=100px', width: '+=100px', top: '+=100px' }, 500);
    div.animate({ height: '-=100px', width: '-=100px', left: '+=100px' }, 500);
    div.animate({ height: '+=100px', width: '+=100px', top: '-=100px' }, 500);
    div.animate({ height: '-=100px', width: '-=100px', left: '-=100px' }, 500);
    div.animate({ opacity: 0.5 });
});

// Each animate() method call will run one after another.
// Remember, to manipulate the position of elements, you need to set the CSS
// position property of the element to relative, fixed, or absolute.

// Note: The animate() method, just like the hide/show/fade/slide methods, can
// take an optional callback function as its parameter, which is executed after
// the current effect is finished.


// Question:
//   Animation Queue
//   By default, jQuery creates a queue for each animate() call.

// Answer:
//   True
