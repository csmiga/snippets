// Drop-Down Menu:
// Let's create a simple drop-down menu that will open upon clicking on the menu
// item.

// HTML:
//   <div class="menu">
//     <div id="item">Drop-Down</div>
//     <div id="submenu">
//       <a href="#">Link 1</a>
//       <a href="#">Link 2</a>
//       <a href="#">Link 3</a>
//     </div>
//   </div>

// Try It Yourself:
// 
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div class="menu">
            <div id="item">Dropdown</div>
            <div id="submenu">
                <a href="#">Link 1</a>
                <a href="#">Link 2</a>
                <a href="#">Link 3</a>
            </div>
        </div>
    </body>
</html>

// CSS:
#item {
    background - color: #4CAF50;
    color: white;
    padding: 16px;
    font - size: 16px;
    border: none;
    cursor: pointer;
}
#item: hover, #item: focus {
    background - color: #3e8e41;
}
.menu {
    position: relative;
    display: inline - block;
}
#submenu {
    display: none;
    position: absolute;
    background - color: #3e8e41;
    min - width: 160px;
    box - shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
}
#submenu a {
    color: white;
    padding: 12px 16px;
    text - decoration: none;
    display: block;
}
#submenu a: hover {
    background - color: #4CAF50
}

// JS:
$(function () {
    $("#item").click(function () {
        $("#submenu").slideToggle(500);
    });
});

// The code above handles the click event of the id="item" element and
// opens/closes the submenu in 500 milliseconds.

// Note: Run the code to see it in action. You can also check out the CSS used
// for styling the items.


// Question:
//   Drop-Down Menu
//   Fill in the blanks to hide the element in 1.5 seconds.
//     $("p").____(____);

// Answer:
//   $("p").hide(1500);
