// Question:
//   Fill in the blanks to hide all paragraphs upon clicking the div element.
//     $("div")._____(function () {
//         $("_____")._____();
//     });

// Answer:
//   $("div").click(function () {
//       $("p").hide();
//   });


// Question:
//   For how many seconds will the following animation group run?
//   $("p").animate({ height: "10px" }, 1000);
//   $("p").animate({ width: "10px" }, 1000);
//   $("p").animate({ opacity: 0.5 }, 3000);

// Answer:
//   5


// Question:
//   Fill in the blanks to create a valid animation.
//     $("p")._______(_
//     height:"10px"_
//     width: "+=100px",
//     opacity: 0.5
//     _, 1000);

// Answer:
//   $("p").animate({
//   height:"10px",
//   width: "+=100px",
//   opacity: 0.5
//   }, 1000);
