// Attributes:
// We can manipulate attributes assigned to HTML elements easily through jQuery.
// href, src, id, class, style are all examples of HTML attributes.

// The attr() method is used for getting the value of an attribute.For Example:

// HTML:
//   <a href="www.sololearn.com">
//       Click here
//   </a>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2794/5957/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <a href="https://www.sololearn.com/">Click here</a>
    </body>
</html>

// CSS:

// JS:
$(function () {
    var val = $("a").attr("href");
    alert(val);
});

// Note: In the code above we selected and alerted the value of the href
// attribute of the <a> element.


// Question:
//   Attributes
//   Which of the following are HTML attributes?

// Answer:
//   class
//   src


// Attributes:
// The attr() method also allows us to set a value for an attribute by
// specifying it as the second parameter.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2794/5958/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <a href="https://www.sololearn.com/">Click here</a>
    </body>
</html>

// CSS:

// JS:
$(function () {
    $("a").attr("href", "http://www.jquery.com");
});

// Note: This will change the href attribute of the <a> element to the provided
// value.


// Question:
//   Attributes
//   Fill in the blanks to change the image to the file "1.jpg".
//     _("img").____("src", "1.jpg");

// Answer:
//   $("img").attr("src", "1.jpg");
