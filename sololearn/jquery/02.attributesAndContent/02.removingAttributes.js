// Removing Attributes:
// You can also remove attributes from HTML elements.
// The removeAttr() method is used for removing any attribute of an element. In
// the example below we remove the border and class attributes of the table:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2797/5965/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <table border="5" class="tbl" >
            <tr>
                <td>one</td>
                <td>two</td>
            </tr>
            <tr>
                <td>three</td>
                <td>four</td>
            </tr>
        </table>
    </body>
</html>

// CSS:
.tbl {
    background - color: aqua;
    font - weight: bold;
}

// JS:
$(function () {
    $("table").removeAttr("border");
    $("table").removeAttr("class");
});

// Note: Run the code and see how it works!


// Question:
//   Removing Attributes
//   Drag and drop from the options below to remove the border attribute of the
//   element with id = "text".
//     $("_____").__________("______");

//     #text, attr, id, border, removeAttr

// Answer:
//   $("#text").removeAttr("border");
