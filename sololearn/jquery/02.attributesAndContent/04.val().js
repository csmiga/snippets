// val():
// We have seen in the previous lesson how we can manipulate the content of HTML
// elements using the text() and html() methods.
// Another useful method is the val() method, which allows us to get and set the
// values of form fields, such as textboxes, dropdowns, and similar inputs.
// For Example:

// HTML:
//   <input type="text" id="name" value="Your Name"></input>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2822/5968/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <input type="text" id="name" value="Your Name">
    </body>
</html>

</body></html>
// CSS:

// JS:
$(function() {
        alert($("#name").val());
});

// Similarly, you can set the value for the field by providing it as a parameter
// to the val() method.

// Note: Getting and setting form field values is very useful when you need to
// handle form events and validation. We will cover events later in the course.


// Question:
//   val()
//   Rearrange the code to get the value of the form field and set it as the
//   value of the paragraph with id="demo".

//   * $("#demo").text(t);
//   * $(function() {
//   * });
//   * var t = $("#user").val();

// Answer:
//    $(function() {
//        var t = $("#user").val();
//        $("#demo").text(t);
//    });


// Summary
// The following jQuery methods are available to get and set content and
// attributes of selected HTML elements:
//   text()       - sets or returns the text content of selected elements.
//   html()       - sets or returns the content of selected elements(including
//                  HTML markup).
//   val()        - sets or returns the value of form fields.
//   attr()       - sets or returns the value of attributes.
//   removeAttr() - removes the specified attribute.

// Note: Write code and test every method at least one time.


// Question:
//   Summary
//   Which method gets the value of an input field?

// Answer:
//   val()
