// Adding Content:
// As we have seen in the previous lessons, the html() and text() methods can be
// used to get and set the content of a selected element.However, when these
// methods are used to set content, the existing content is lost.
// jQuery has methods that are used to add new content to a selected element
// without deleting the existing content:
//   append()  - inserts content at the end of the selected elements.
//   prepend() - inserts content at the beginning of the selected elements.
//   after()   - inserts content after the selected elements.
//   before()  - inserts content before the selected elements.

// Note: Let's see them in action!Let's see them in action!


// Question:
//   Adding Content
//   Which method is used to insert content at the beginning of the selected
//   element?

// Answer:
//   prepend()


// Adding Content:
// The append() method inserts content AT THE END of the selected HTML element.
// For example:

// HTML:
//   <p id="demo">Hi</p>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2822/5968/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p id="demo">Hi </p>
    </body>
</html>

// CSS:

// JS:
$(function () {
    $("#demo").append("David");
});

// Note: Similarly, the prepend() method inserts content AT THE BEGINNING of the
// selected element.
// You can also use HTML markup for the content.


// Question:
//   Adding Content
//   What is the result of the following code if the HTML contains <p>1</p >?
//     $("p").append("2");
//     $("p").prepend("1");

// Answer:
//   112


// Adding Content:
// The jQuery after() and before() methods insert content AFTER and BEFORE the
// selected HTML element.For example:

// HTML:
//   <p id="demo">Hi</p>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2824/6046/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p id="demo">Hi</p>
    </body>
</html>

// CSS:

// JS:
$(function () {
    $("#demo").before("<i>Some Title</i>");
    $("#demo").after("<b>Welcome</b>");
});

// Note: Tap Try It Yourself to play around with the code!


// Question:
//   Adding Content:
//   Can you use HTML markup for the after() method?

// Answer:
//   Yes


// Adding New Elements:
// The append(), prepend(), before() and after() methods can also be used to
// add newly created elements.The easiest way of creating a new HTML element
// with jQuery is the following:

//   var txt = $("<p></p>").text("Hi");

// The code above creates a new <p> element, which contains the text Hi and
// assigns it to a variable called txt.
// Now, we can use that variable as a parameter of the above mentioned methods
// to add it to our HTML, for example:

// HTML:
//   <p id="demo">Hello</p>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2824/6047/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p id="demo">Hello</p>
    </body>
</html>

// CSS:

// JS:
$(function () {
    var txt = $("<p></p>").text("Hi");
    $("#demo").after(txt);
});

// This will insert the newly created <p> element after the #demo paragraph.
// You can also specify multiple elements as arguments for the before(),
// after(), append(), prepend() methods by separating them using commas:
// $("#demo").append(var1, var2, var3).

// The above mentioned syntax for creating elements can be used to create any
// new HTML element, for example $("<div></div>") creates a new div.


// Question:
//   Adding New Elements
//   Fill in the blanks to create a new <span> element and append it to the
//   element with id = "txt".
//     var a = $("<____></span>");
//     _("#txt").append(_);

// Answer:
//   var a = $("<span></span>");
//   $("#txt").append(a);
