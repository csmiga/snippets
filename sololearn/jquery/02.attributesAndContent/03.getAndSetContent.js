// Get Content:
// There are several methods for manipulating the content of HTML elements via
// jQuery.
// The html() method is used to get the content of the selected element,
// including the HTML markup. For example:

// HTML:
//   <p>
//       JQuery is <b> fun </b >
//   </p>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2798/6020/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>JQuery is <b>fun</b></p>
    </body>
</html>

// CSS:

// JS:
$(function () {
    var val = $("p").html();
    alert(val);
});

// Notice, that the HTML markup (the <b> tags) is also returned,
// If you need only the text content, without the HTML markup, you can use the
// text() method:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2798/6020/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>JQuery is <b>fun</b></p>
    </body>
</html>

// CSS:

// JS:
$(function () {
    var val = $("p").text();
    alert(val);
});

// Note: The html() and text() methods can be used for all HTML elements that
// can contain content.


// Quesion:
//   Get Content
//   What is the output of this code ?
//     <div id="test">
//         <p>p</p>
//     </div>
//     <script>
//         alert($("#test").text())
//     </script>

// Answer:
//   p


// Set Content:
// The same html() and text() methods can be used to change the content of HTML
// elements.
// The content to be set is provided as a parameter to the method, for example:

// HTML:
//   <div id="test">
//       <p>some text</p>
//   </div>

// JS:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2798/6022/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div id="test">
            <p>some text</p>
        </div>
    </body>
</html>

// CSS:

// JS:
$(function () {
    $("#test").text("hello!");
});

// The code above changes the content of the element with id="test" to "hello!".

// Note: If the content you are setting contains HTML markup, you should use the
// html() method instead of text().


// Question:
//   Set Content
//   Fill in the blanks to change the content of the paragraphs with id = "demo"
//   to "<b>Hi</b>" maintaining the HTML markup.
//     $(function () {
//         $("_____").____("<b>Hi</b>");
//     });

// Answer:
//   $(function () {
//       $("#demo").html("<b>Hi</b>");
//   });
