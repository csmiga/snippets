// Question:
//   Fill in the blanks to set the alt attribute of the image with id="img", and
//   alert the value of its src attribute.
//     $("#img").____("alt", "Demo");
//     alert($("____").attr("___"));

// Answer:
//   $("#img").attr("alt", "Demo");
//   alert($("#img").attr("src"));


// Question:
//   The text() method returns the text content of the element, including the
//   HTML markup.

// Answer:
//   false


// Question:
//   Fill in to get the value of the form field with id="name" and set it as the
//   text of the paragraph with id="txt".
//     var v = $("#name").___;
//     $("____").text(_);

// Answer:
//   var v = $("#name").val();
//   $("#txt").text(v);

// Question:
//   What is the output of this code if the HTML code is <p>a<span>b</span></p>?
//     $(function () {
//         $("p span").text("a");
//     });

// Answer:
//   aa


// Question:
//   Fill in the blanks to create a new div element with the text "Hi" and
//   insert it before the element with id="demo".
//     _____ a = _____ ("<div></div>").text("Hi");
//     $("#demo")._____(_____);

// Answer:
//   var a = $("<div></div>").text("Hi");
//   $("#demo").before(a);
