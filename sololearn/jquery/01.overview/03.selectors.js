// Selectors:
// Let's have a look at all jQuery selectors.
// As you have seen in the previous lesson, the jQuery selectors start with the
// dollar sign and parentheses: $().
// The most basic selector is the element selector, which selects all the
// elements based on the element name.

//   $("div")  // selects all <div> elements

// Next are the id and class selectors, which select the elements by their id
// and class name:

//   $("#test") // select the element with the id="test"
//   $(".menu") //selects all elements with class="menu"

// Run the code and see how it works!


// Question:
//   Selectors
//   Which of the following selects all elements with class= "demo"?

// Answer:
//   $(".demo")


// Selectors:
// You can also use the following syntax for selectors:

//   $("div.menu")  // all <div> elements with class="menu"
//   $("p:first")   // the first <p> element
//   $("h1, p")     // all <h1> and all <p> elements
//   $("div p")     // all <p> elements that are descendants of a <div> element
//   $("*")         // all elements of the DOM

// Note: Selectors make accessing HTML DOM elements easy compared to pure
// JavaScript.


// Question:
//   Selectors
//   Select all <a> links which are inside paragraph tags.
//   _("___")

// Answer:
//   $("p a")


// Useful Selectors

//   Selector             Sample                    Description
//   ========             ======                    ===========
//   *                    $("*")                    all elements
//   #id                  $("#div")                 element with id ="div"
//   .class               $(".div")                 all elements with
//                                                  class="div"
//   element              $("p")                    all <p> elements
//   el1,el2,el3          $("h1,h2,h3")             all <h1>, <h2> and <h3>
//                                                  elements
//   :first               $("h1:first")             first <h1> element
//   :last                $("h1:last")              last <h1> element
//   :first-child         :$("b:first-child")       all <b> that are the first
//                                                  child of their parent
//   :last-child          $("b:last-child")         all <b> elements that are
//                                                  the last child of their
//                                                  parent
//   :nth-child(n)        $("div:nth-child(2)")     all <div> elements that are
//                                                  2nd child of their parent
//   parent > child       $("div > p")              all <p> elements that are a
//                                                  direct child of a <div>
//                                                  element
//   parent descendant    $("span p")               all <p> elements that are
//                                                  descendants of a <span>
//                                                  element
//   :eq(index)           $("ul li:eq(2)")          the third element in a list
//                                                  (index starts at 0)
//   :contains(text)      $(":contains('solo')")    all elements which contain
//                                                  the text 'solo'
//   [attribute]          $("[src]")                all elements with a src attribute
//   :input               $(":input")               all input elements
//   :text                $(":text")                all input elements with
//                                                  type="text"

// Note: Don't try to remember all this, just know that they exist. You can
// reread this when you need to know more.


// Question:
//   Useful Selectors
//   Select all elements that are direct children of div elements.
//     $("div > _")

// Answer:
//   $("div > *")
