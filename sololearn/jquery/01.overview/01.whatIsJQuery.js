// What is jQuery:
// jQuery is a fast, small, and feature - rich JavaScript library.
// It makes things like HTML document traversal and manipulation, event
// handling, and animation much simpler.

// All the power of jQuery is accessed via JavaScript, so having a strong grasp
// of JavaScript is essential for understanding, structuring, and debugging your
// code.

// If you want to study JavaScript, check out our free JavaScript Tutorial.


// Question:
//   What is jQuery

// Answer:
//   JS Library


// What is jQuery:
// First, let's take a look at an example HTML manipulation with JavaScript.
// To get the element with the id = "start" and change its html to "Go" we will
// need to do the following:

//   var el = document.getElementById("start");
//   el.innerHTML = "Go";

// To do the same manipulation with jQuery, we need just a single line of code:

//   $("#start").html("Go");

// You will learn about the new syntax in the coming lessons, but as you can
// see, the code is much shorter and easier to understand.

// Note: Another great advantage of jQuery is that you do not need to worry
// about browser support, your code will run exactly the same in all major
// browsers, including Internet Explorer 6!


// Question:
//   What is jQuery:
//   Fill in the blanks to change the elements HTML to "Test" using pure
//   JavaScript.
//     ___ e = document.getElementById('test');
//     _.innerHTML _ "Test";

// Answer:
//   var e = document.getElementById('test');
//   e.innerHTML = "Test";
