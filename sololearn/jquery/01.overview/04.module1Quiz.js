// Question:
//   Type in the symbol used to access jQuery.

// Answer:
//   $


// Question:
//   Fill in the blanks to run the jQuery manipulations as soon as the document
//   is loaded.
//     _(________(){
//         // jQuery code goes here
//     });

// Answer:
//   $(function(){
//       // jQuery code goes here
//   });


// Question:
//   Fill in the blanks to select the paragraph from the following HTML:
//   <p id="test"></p>
//     _("_____")

// Answer:
//   $("#test")


// Question:
//   Which of the following selects all h1 and h2 elements?

// Answer:
//   $("h1, h2")


// Question:
//   Fill in the blanks to select all <p> elements, that are children of the
//   element with id="demo".
//     _("_____ p")


// Answer:
//   $("#demo p")
