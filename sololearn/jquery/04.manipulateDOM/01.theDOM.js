// The DOM:
// When you open any webpage in a browser, the HTML of the page is loaded and
// rendered visually on the screen.
// To accomplish this, the browser builds the Document Object Model(DOM) of that
// page, which is an object oriented model of its logical structure.
// The DOM of an HTML document can be represented as a nested set of boxes:

//             <Document>
//                 |
//       -------<html>------
//      |                   |
//    <head>              <body>
//      |                   |
//   <title>           ----------
//                    |          |
//                   <h1>       <a>

// The DOM represents a document as a tree structure where HTML elements are
// interrelated nodes in the tree.
// Nodes can have child nodes.Nodes on the same tree level are called siblings.
// jQuery traversing is the term used to describe the process of moving through
// the DOM and finding(selecting) HTML elements based on their relation to other
// elements.

// Note: jQuery makes it easy to traverse the DOM and work with HTML elements.


// Question:
// The DOM
// What kind of structure does the DOM have?

// Answer:
//   tree


// DOM Traversal:
// For example, consider the HTML represented by the following structure:

//          <html>
//            |
//          <body>
//            |
//      --------------
//     |              |
//   <h1>            <a>

// The <html> element is the parent of <body> and an ancestor of everything
// below it.
//   <body>         - element is the parent of the <h1> and <a> elements.
//   <h1> and <a>   - elements are child elements of the <body> element and
//                    descendants of <html>.
//   <h1> and <a>   - elements are siblings (they share the same parent).

// Summary
// An ancestor is a parent, grandparent, great - grandparent, and so on.
// A descendant is a child, grandchild, great - grandchild, and so on.
// Siblings share the same parent.

// Note: Understanding the relationship between the DOM elements is important to
// be able to traverse the DOM correctly.


// Question:
//   DOM Traversal
//   Elements that share the same parent are called:

// Answer:
//   siblings
