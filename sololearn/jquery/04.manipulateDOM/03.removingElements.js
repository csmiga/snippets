// Remove Elements:
// We remove selected elements from the DOM using the remove() method. For
// example:

// HTML:
//   <p style="color:red">Red</p>
//   <p style="color:green">Green</p>
//   <p style="color:blue">Blue</p>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2823/6054/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
            <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
        </head>
        <body>
            <p style="color:red">Red</p>
            <p style="color:green">Green</p>
            <p style="color:blue">Blue</p>
        </body>
    </html>

// CSS:

// JS:
$(function () {
    $("p").eq(1).remove();
});

// This removes Green, the second paragraph element.
// You can also use the remove() method on multiple selected elements, for
// example $("p").remove() removes all paragraphs.

// Note: The jQuery remove() method removes the selected element(s), as well as
// its child elements.


// Question:
//   Remove Elements
//   Fill in the blanks to remove all siblings of the element with id = "txt".
//     $("_txt").________().______();

// Answer:
//   $("#txt").siblings().remove();


// Removing Content"
// The empty() method is used to remove the child elements of the selected
// element(s).For example:

// HTML:
//   <div>
//     <p style="color:red">Red</p>
//     <p style="color:green">Green</p>
//     <p style="color:blue">Blue</p>
//   </div>

// CSS:
//   div {
//     background - color: aqua;
//     width: 300px;
//     height: 200px;
//   }

// HTML:
//   <p style="color:red">Red</p>
//   <p style="color:green">Green</p>
//   <p style="color:blue">Blue</p>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2823/6055/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div>
            <p style="color:red">Red</p>
            <p style="color:green">Green</p>
            <p style="color:blue">Blue</p>
        </div>
    </body>
</html>

// CSS:
div {
    background - color: aqua;
    width: 300px;
    height: 200px;
}

// JS:
$(function () {
    $("div").empty();
});

// Note: This removes all the three child elements of the div, leaving it empty.


// Question:
//   Removing Content
//   Fill in the blanks to empty the second child element of the element with
//   id = "nav".
//     var e = $("____").________();
//     e.eq(_)._____();

// Answer:
//   var e = $("#nav").children();
//   e.eq(1).empty();
