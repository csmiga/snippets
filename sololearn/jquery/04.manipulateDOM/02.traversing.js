// DOM Traversal:
// jQuery has many useful methods for DOM traversal.
// The parent() method returns the direct parent element of the selected
//  element. For example:

// HTML:
//   <div> div element
//       <p>paragraph</p> 
//   </div >

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2800/5979/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
            <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
        </head>
        <body>
            <div> div element
    <p>paragraph</p>
            </div>
        </body>
    </html>

// CSS:
div {
    padding: 10px;
}
p {
    border: 1px solid black;
    padding: 5px;
}

// JS:
$(function () {
    var e = $("p").parent();
    e.css("border", "2px solid red");
});

// The code above selects the parent element of the paragraph and sets a red
// border for it.

// Note: Run the code to see it in action.


// Question:
//   DOM Traversal
//   Which element is the parent of the < p > element in the following HTML?
//     <div><ul>
//         <li><p></p></li>
//     </ul></div>

// Answer:
//   <li>


// DOM Traversal:
// The parent() method can only traverse a single level up the DOM tree.
// To get all ancestors of the selected element you can use the parents()
// method. For example:

// HTML:
//   <body>  body
//     <div style="width:300px;"> div
//       <ul> ul
//         <li> li
//           <p>paragraph</p>
//         </li>
//       </ul>
//     </div>
//   </body>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2800/5978/1
// HTML:
< !DOCTYPE html >
    <html>
        <head>
            <title>Page Title</title>
            <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
        </head>
        <body>body
    <div style="width:300px;">div
        <ul>ul
            <li>li
                <p>paragraph</p>
                    </li>
                </ul>
            </div>
        </body>
    </html>

// CSS:
body, div, ul, li {
    padding: 5px;
    margin: 15px;
}
p {
    border: 1px solid black;
    padding: 5px;
}

// JS:
$(function () {
    var e = $("p").parents();
    e.css("border", "2px solid red");
});

// The code above sets a red border for all parents of the paragraph.

// Some of the most used traversal methods are presented below:

//   Method              What It Returns
//   ======              ===============
//   parent()            direct parent element of the selected element
//   parents()           all ancestor elements of the selected element
//   children()          all direct children of the selected element
//   siblings()          all sibling elements
//   next()/nextAll()    next/all next sibling element/s
//   prev()/prevAll()    previous/all previous sibling element of the selected
//                       element
//   eq()                element with a specific index number of the selected
//                       elements

// Note: Run the code and see how it works!


// Question:
//   DOM Traversal
//   Fill in the blanks to select all siblings of the div element and call the
//   hide() method on them.
//     ___ items = $("div").________();
//     _____.hide();

// Answer:
//   var items = $("div").siblings();
//   items.hide();


// DOM Traversal:
// The eq() method can be used to select a specific element from multiple
// selected elements.
// For example, if the page contains multiple div elements and we want to select
// the third one:

//   $("div").eq(2);

// Note: The index numbers start at 0, so the first element will have the index
// number 0.


// Question:
//   DOM Traversal
//   What is the output of this code?
//     <p>a</p><p>b</p><p>c</p>
//     <script>
//         alert($("p").eq(1).text());
//     </script>

// Answer:
//   b
