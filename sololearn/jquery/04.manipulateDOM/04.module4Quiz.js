// Question:
//   How many siblings does the <p> element with the id="txt" have in the
//   following HTML?
//   <div>
//     <p></p>
//     <p id="txt"></p>
//     <p></p>
//   </div>

// Answer:
//   2


// Question:
//   Fill in the blanks to select the first child of the div element.
//     _("div").children().eq(_);

// Answer:
//   $("div").children().eq(0);


// Question:
//   Drag and drop from the options below to remove all children of the
//   paragraph tag.
//     $("p")._____._____;

//   parents(), siblings(), remove(), children()

// Answer:
//   $("p").chidlren.remove;


// Question:
//   What is the output of this code?
//     <div><p>1</p></div>
//     <div>2</div>
//     <script>
//       alert($("p").parent().siblings().eq(0).text());
//     </script>

// Answer:
//   2

