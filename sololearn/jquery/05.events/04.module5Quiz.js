// Question:
//   Fill in the blanks to add an event handler to the paragraph.
//     $("p").__("click",________() {
//         //some code
//     });

// Answer:
//   $("p").on("click",function() {
//       //some code
//   });


// Question:
//   What will be the output after the div is clicked two times?
//     <div>1</div>
//     <script>
//         $("div").click(function() {
//             $("div").text($("div").text()+1);
//         });
//     </script>

// Answer:
//   111


// Question:
//   Fill in the blanks to remove the "click" event handler from all <a>
//   elements.
//     $("_____")._____("_____");

// Answer:
//   $("a").off("click");


// Question:
//   How many "a" characters will be output after the div is clicked three
//   times?
//     <div>a</div>
//     <script>
//         $("div").click(function() {
//             $("div").append("a");
//             $("div").off("click");
//         });
//     </script>

// Answer:
//   

