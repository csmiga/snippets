// To-Do List:
// Let's create a To-Do list project using the concepts we have learned.
// The To - Do list will be able to add new items to a list, as well as remove
// existing items.

// First, we create the HTML:

//   <h1>My To-Do List</h1>
//   <input type="text" placeholder="New item" />
//   <button id="add">Add</button>
//   <ol id="mylist"></ol>

// Note: Clicking the button should add the value of the input field to our <ol>
// list.


// Question:
//   To-Do List
//   Fill in the blanks:
//     <h1>Some heading<_____>
//     <ol>
//         <li>One</__>
//         <li>Two</li>
//     </_____>

// Answer:
//   <h1>Some heading</h1>
//   <ol>
//       <li>One</li>
//       <li>Two</li>
//   </ol>


// To-Do List:
// Now, having the HTML ready, we can start writing our jQuery code.
// First, we handle the click event for the button:

// JS:
//   $(function() {
//       $("#add").on("click", function () {
//           //event handler
//       });
//   });

// Inside the event handler we select the value of the input field and create a
// new <li> element, adding it to the list:

// JS:
//   var val = $("input").val();
//   if (val !== '') {
//       var elem = $("<li></li>").text(val);
//       $(elem).append("<button class='rem'>X</button>");
//       $("#mylist").append(elem);
//       $("input").val(""); //clear the input
//   }

// The code above takes the value of the input field, assigns it to the val
// variable. The if statement checks that the value is not blank and then
// creates a new <li> element. A button for removing it is added, after which
// the newly created element is added to the <ol id="mylist"> list.

// Here's the complete code in action:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2819/6120/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>My To-Do List</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <h1>My To-Do List</h1>
        <input type="text" placeholder="New item" />
        <button id="add">Add</button>
        <ol id="mylist"></ol>
    </body>
</html>

// CSS:
h1 {
    color: #1abc9c;
}
.rem {
    margin - left: 5px;
    background - color: white;
    color: red;
    border: none;
    cursor: pointer;
}

// JS:
$(function () {
    $("#add").on("click", function () {
        var val = $("input").val();
        if (val !== '') {
            var elem = $("<li></li>").text(val);
            $(elem).append("<button class='rem'>X</button>");
            $("#mylist").append(elem);
            $("input").val("");
        }
    });
});

// Note: The remove button is not working yet. We will handle it in the next
// section!


// Question:
//   To-Do List
//   Fill in the blanks to create a new div element and add it to the element
//   with id = "test".
//     var e = $("<div><_____>");
//     _____("#test").append(_____);

// Answer:
//   var e = $("<div></div>");
//   $("#test").append(e);


// To-Do List
// All that is left to do is handle the click event on the class="rem" button
// and remove the corresponding < li > element from the list.

// JS:
//   $(".rem").on("click", function() {
//     $(this).parent().remove();
//   });

// Remember, this is the current object. The code above removes the parent of
// the current object, which in our case is the parent of the remove button, the
// <li> element.

// The complete code:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2819/6121/1
// HTML:
< 
// Question:
//   !DOCTYPE html >
<html>
    <head>
        <title>My To-Do List</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <h1>My To-Do List</h1>
        <input type="text" placeholder="New item" />
        <button id="add">Add</button>
        <ol id="mylist"></ol>
    </body>
</html>

// CSS:
h1 {
    color: #1abc9c;
}
.rem {
    margin - left: 5px;
    background - color: white;
    color: red;
    border: none;
    cursor: pointer;
}

// JS:
$(function () {
    $("#add").on("click", function () {
        var val = $("input").val();
        if (val !== '') {
            var elem = $("<li></li>").text(val);
            $(elem).append("<button class='rem'>X</button>");
            $("#mylist").append(elem);
            $("input").val("");
            $(".rem").on("click", function () {
                $(this).parent().remove();
            });
        }
    });
});

// Note: The To-Do List was just a short demonstration of how to handle events
// and build a simple project.


// Question:
//   To-Do List
//   Which keyword represents the current object?

// Answer:
//   this
