// Handling Events:
// JQuery provides an efficient way to handle events.Events occur when the user
// performs an action, such as clicking an element, moving the mouse, or
// submitting a form.
// When an event occurs on a target element, a handler function is executed.
// For example, let's say we want to handle the click event on an element with
// id="demo" and display the current date when the button is clicked. Using pure
// JavaScript, the code looks like:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2808/5993/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <div id="demo">Click Me</div>
    </body>
</html>

// CSS:

// JS:
window.onload = function () {
    var x = document.getElementById("demo");
    x.onclick = function () {
        document.body.innerHTML = Date();
    }
};

// The same event could be handled using jQuery with the following code:

// Try It Yourself:
// hhttps://www.sololearn.com/learning/1082/2808/5993/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div id="demo">Click Me</div>
    </body>
</html>

// CSS:

// JS:
$(function () {
    $("#demo").click(function () {
        $("body").html(Date());
    });
});

// As you can see, the jQuery code is shorter and easier to read and write.
// Notice, that the event name is provided without the "on" prefix(i.e., onclick
// in JavaScript is click in jQuery).

// Note: The function that is executed when an event is fired is called the
// event handler.


// Question:
//   Handling Events
//   The event handler is a:

// Answer:
//   function


// Common Events:
// The following are the most commonly used events:

// Mouse Events:
//   click occurs when an element is clicked.
//   dblclick occurs when an element is double - clicked.
//   mouseenter occurs when the mouse pointer is over(enters) the selected element.
//   mouseleave occurs when the mouse pointer leaves the selected element.
//   mouseover occurs when the mouse pointer is over the selected element.

// Keyboard Events:
//   keydown occurs when a keyboard key is pressed down.
//   keyup occurs when a keyboard key is released.

// Form Events:
//   submit occurs when a form is submitted.
//   change occurs when the value of an element has been changed.
//   focus occurs when an element gets focus.
//   blur occurs when an element loses focus.

// Document Events:
//   ready occurs when the DOM has been loaded.
//   resize occurs when the browser window changes size.
//   scroll occurs when the user scrolls in the specified element.

// As an example, let's change the content of a div when the user types in an
// input field. To do that, we need to handle the keydown event, which occurs
// when a key on the keyboard is pressed:

// HTML:
//   <input type="text" id="name" />
//   <div id="msg"></div>

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2808/6026/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <input type="text" id="name" />
        <div id="msg"></div>
    </body>
</html>

// CSS:
#msg {
    color: blue;
    font - size: 16pt;
    font - weight: bold;
}

// JS:
$(function () {
    $("#name").keydown(function () {
        $("#msg").html($("#name").val());
    });
});

// The code above handles the keydown event for the element with id="name" and
// assigns the content of the div with id="msg" the value of the input field.

// Note: The event names are self-explanatory, so just experiment to see them in
// action.


// Question:
//   Common Events
//   Fill in the blanks to handle the click event on the paragraph tag.
//     $("p")._____(_____() {alert("Clicked!");
//     _____);

// Answer:
//   $("p").click(function() {alert("Clicked!");
//   });


// Handling Events:
// Another way to handle events in jQuery is by using the on() method.
// The on() method is used to attach an event to the selected element.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2808/5994/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <p>Click Me</p>
    </body>
</html>

// CSS:


// JS:
$(function () {
    $("p").on("click", function () {
        alert("clicked");
    });
});

// As you can see, the event name is passed as the first argument to the on()
// method. The second argument is the handler function.

// Note: The on() method is useful for binding the same handler function to
// multiple events. You can provide multiple event names separated by spaces as
// the first argument. For example, you could use the same event handler for the
// click and dblclick events.


// Question:
//   Handling Events
//   Fill in the blanks to handle the submit event for the form element using
//   the on() method:
//     _____("form")._____("_____",function() {
//         // some code
//     });

// Answer:
//   $("form").on("submit",function() {
//       // some code
//   });


// off():
// You can remove event handlers using the off() method.

// For example:

// Try It Yourself:
// https://www.sololearn.com/learning/1082/2808/5997/1
// HTML:
< !DOCTYPE html >
<html>
    <head>
        <title>Page Title</title>
        <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    </head>
    <body>
        <div>Click Me</div>
    </body>
</html>

// CSS:

// JS:
$(function () {
    $("div").on("click", function () {
        alert('Hi there!');
    });
    $("div").off("click");
});

// Note: The argument of the off() method is the event name you want to remove
// the handler for.


// Question:
//   off()
//   Fill in the blanks to remove the event handler for the focus event on the
//   element with id = "test".
//     $("_____")._____("_____");

// Answer:
//   $("#test").off("focus);
