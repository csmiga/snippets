#!/usr/bin/env bash

curl -X GET http://ec2-18-207-7-218.compute-1.amazonaws.com:8080/v1/catalog/nodes?raw | python -m json.tool > nodes.json
sg_fqdn=$(grep 'SG\-' nodes.json | awk -F '["]' '{print $4}')
echo ${sg_fqdn}
