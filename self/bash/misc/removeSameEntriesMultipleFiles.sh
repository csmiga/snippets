#!/usr/bin/env bash

array=( $( ls *.xml ) )
fileCount="${#array[@]}"
for (( i=0; i<$fileCount; i++ ));
do
    #echo "${array[$i]}"
    sed -i '/UPDATED ON\|UPDATED BY\|CSmiga@Synamedia.com\|COMMENTS\|of tests/d' ${array[$i]}
done
