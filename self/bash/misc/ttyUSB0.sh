#!/usr/bin/env bash

# Terminate serial communication
# <ctrl> + <a> , <k>

# Scrolling
# <ctrl> + <a>, <esc>

sudo screen /dev/ttyUSB0 115200,cs8,ixon

