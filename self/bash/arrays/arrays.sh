
# Create array
    $> directories=( $(ls /var/log) )

# Number of items in array
    $> echo ${#directories[@]}
       2

# List value of items in array
    $> echo ${directories[@]}
       20190325-1010
       20190325-1015

# List index values in array
    $> echo ${!directories[@]}
       0 1
