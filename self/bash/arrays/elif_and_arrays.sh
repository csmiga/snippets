set -x
 
cd $COMMANDER_WORKSPACE
 
if [ ! -d tsung_log ];
then
        mkdir tsung_log
fi
 
directories=( $(ls tsung_log) )
 
if [ ${#directories[@]} = 0 ];
then
        mkdir tsung_log/YYYYMMDD-HHMM1
        echo '{"Status": "FAILED"}' > tsung_log/YYYYMMDD-HHMM1/result.json
        echo "Created one missing directory and result.json file"
        mkdir tsung_log/YYYYMMDD-HHMM2
        echo '{"Status": "FAILED"}' > tsung_log/YYYYMMDD-HHMM2/result.json
        echo "Created second missing directory and result.json file"
elif [ ${#directories[@]} = 1 ];
then
       mkdir tsung_log/YYYYMMDD-HHMM1
       echo '{"Status": "FAILED"}' > tsung_log/YYYYMMDD-HHMM1/result.json
       echo "Created one missing directory and result.json file"
fi
 
for index in ${!directories[@]}
do
if [ ! -f tsung_log/${directories[index]}/result.json ];
then
        echo '{"Status": "FAILED"}' > tsung_log/${directories[index]}/result.json
        echo "Created missing result.json file for existing directory(s)"
fi
done

