#!/usr/bin/env bash

FILE=./etc_hosts
grep tsungWorker ${FILE} | awk '{print $2}' > etc_hosts.name

# IFS - Internal Field Separator
#IFS=$'\n' read -d '' -r -a ARRAY < etc_hosts.name
#readarray ARRAY < etc_hosts.name

IFS=$'\n' read -d '' -r -a ARRAY < etc_hosts.name
for INDEX in ${!ARRAY[@]}
do
    ssh -l nfptest -o StrictHostKeyChecking=no ${ARRAY[INDEX]} 'exit'
done

# -- OR --

grep tsungWorker ${FILE} | awk '{print $2}' > ./etc_hosts.name
for NAME in $(cat ./etc_hosts.name)
do
        ssh -l nfptest -o StrictHostKeyChecking=no ${NAME} 'exit'
done

cp /root/.ssh/known_hosts /home/nfptest/.ssh/known_hosts
chown nfptest:nfptest /home/nfptest/.ssh/known_hosts
chmod 600 /home/nfptest/.ssh/known_hosts

# -- OR --

grep tsungWorker etc_hosts > .tmp
for HOST in $(cat .tmp)
do
    ssh -l nfptest -o StrictHostKeyChecking=no ${HOST} 'exit'
    echo 'Key gathered from '${HOST}
done

rm -f .tmp

