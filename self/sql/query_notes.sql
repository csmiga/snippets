-- QUERY NOTES

-- Contents:
--   A Cleaner Way to Utilize 'WHERE'
--   How to Find Specific COLUMN in SQL Server Database?


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- A CLEANER WAY TO UTILIZE 'WHERE'
-- Note: Utilize "in" for cleaner queries in place of "or" and longer strings.

-- Option 1:
SELECT TOP (100) <column1>,<column5> FROM [<database>].[<db_object>].[<table>]
WHERE <column1> in (<value1>,<value2>)

-- Option 2:
SELECT * FROM <table> WHERE <column> in (<value1>,value2)


-- HOW TO FIND SPECIFIC COLUMN IN SQL SERVER DATABASE?
-- You can query the database's information_schema.columns table which holds the
-- schema structure of all columns defined in your database.
SELECT * FROM information_schema.columns WHERE column_name = '<column_name_value>'


-- 
